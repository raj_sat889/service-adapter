
package net.ecubicle.webservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.ecubicle.webservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _String_QNAME = new QName("http://www.ecubicle.net/webservices/", "string");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.ecubicle.webservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindCountryAsString }
     * 
     */
    public FindCountryAsString createFindCountryAsString() {
        return new FindCountryAsString();
    }

    /**
     * Create an instance of {@link FindCountryAsXmlResponse }
     * 
     */
    public FindCountryAsXmlResponse createFindCountryAsXmlResponse() {
        return new FindCountryAsXmlResponse();
    }

    /**
     * Create an instance of {@link FindCountryAsXml }
     * 
     */
    public FindCountryAsXml createFindCountryAsXml() {
        return new FindCountryAsXml();
    }

    /**
     * Create an instance of {@link FindCountryAsStringResponse }
     * 
     */
    public FindCountryAsStringResponse createFindCountryAsStringResponse() {
        return new FindCountryAsStringResponse();
    }

    /**
     * Create an instance of {@link FindCountryAsXmlResponse.FindCountryAsXmlResult }
     * 
     */
    public FindCountryAsXmlResponse.FindCountryAsXmlResult createFindCountryAsXmlResponseFindCountryAsXmlResult() {
        return new FindCountryAsXmlResponse.FindCountryAsXmlResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ecubicle.net/webservices/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

}
