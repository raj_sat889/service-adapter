
package net.ecubicle.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="V4IPAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "v4IPAddress"
})
@XmlRootElement(name = "FindCountryAsString")
public class FindCountryAsString {

    @XmlElement(name = "V4IPAddress")
    protected String v4IPAddress;

    /**
     * Gets the value of the v4IPAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getV4IPAddress() {
        return v4IPAddress;
    }

    /**
     * Sets the value of the v4IPAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setV4IPAddress(String value) {
        this.v4IPAddress = value;
    }

}
