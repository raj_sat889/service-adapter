
package net.ecubicle.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FindCountryAsStringResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "findCountryAsStringResult"
})
@XmlRootElement(name = "FindCountryAsStringResponse")
public class FindCountryAsStringResponse {

    @XmlElement(name = "FindCountryAsStringResult")
    protected String findCountryAsStringResult;

    /**
     * Gets the value of the findCountryAsStringResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindCountryAsStringResult() {
        return findCountryAsStringResult;
    }

    /**
     * Sets the value of the findCountryAsStringResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindCountryAsStringResult(String value) {
        this.findCountryAsStringResult = value;
    }

}
