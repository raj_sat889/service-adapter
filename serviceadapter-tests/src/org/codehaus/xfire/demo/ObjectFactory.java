
package org.codehaus.xfire.demo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.jawise.serviceadapter.test.svc.soap.bookshop.BookExceptionDetail;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.codehaus.xfire.demo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BookFault_QNAME = new QName("http://demo.xfire.codehaus.org", "BookFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.codehaus.xfire.demo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BookExceptionDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://demo.xfire.codehaus.org", name = "BookFault")
    public JAXBElement<BookExceptionDetail> createBookFault(BookExceptionDetail value) {
        return new JAXBElement<BookExceptionDetail>(_BookFault_QNAME, BookExceptionDetail.class, null, value);
    }

}
