package com.jawise.serviceadapter.test;


import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

import org.apache.xml.security.utils.Base64;

public class Base64Test {

	/**
	 * @param args
	 * @throws DecoderException 
	 */
	public static void main(String[] args) throws Exception {
		
		String s = "MIIDojCCAoqgAwIBAgIQE4Y1TR0/BvLB+WUF1ZAcYjANBgkqhkiG9w0BAQUFADBrMQswCQYDVQQGEwJVUzENMAsGA1UEChMEVklTQTEvMC0GA1UECxMmVmlzYSBJbnRlcm5hdGlvbmFsIFNlcnZpY2UgQXNzb2NpYXRpb24xHDAaBgNVBAMTE1Zpc2EgZUNvbW1lcmNlIFJvb3QwHhcNMDIwNjI2MDIxODM2WhcNMjIwNjI0MDAxNjEyWjBrMQswCQYDVQQGEwJVUzENMAsGA1UEChMEVklTQTEvMC0GA1UECxMmVmlzYSBJbnRlcm5hdGlvbmFsIFNlcnZpY2UgQXNzb2NpYXRpb24xHDAaBgNVBAMTE1Zpc2EgZUNvbW1lcmNlIFJvb3QwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvV95WHm6h2mCxlCfLF9sHP4CFT8icttD0b0/Pmdjh28JIXDqsOTPHH2qLJj0rNfVIsZHBAk4ElpF7sDPwsRROEW+1QK8bRaVK7362rPKgH1g/EkZgPI2h4H3PVz4zHvtH8aoVlwdVZqW1LS7YgFmypw23RuwhY/81q6UCzyr0TP579ZRdhE2o8mCP2w4lPJ9zcc+U30rq299yOIzzlr3xF7zSujtFWsan9sYXiwGd/BmoKoMWuDpI/k4+oKsGGelT84ATB+0tvz8KPFUgOSwsAGl0lUq8ILKpeeUYiZGo3BxN77t+Nwtd/jmliFKMAGzsGHxBvfaLdXe6YJ2E5/4tAgMBAAGjQjBAMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMB0GA1UdDgQWBBQVOIMPPyw/cDMezUb+B4wg4NfDtzANBgkqhkiG9w0BAQUFAAOCAQEAX/FBfXxcCLkr4NWSR/pnXKUTwwMhmytMiUbPWU3J/qVAtmPN3XEolWcRzCSs00Rsca4BIGsDoo8Ytyk6feUWYFN4PMCvFYP3j1IzJL1kk5fui/fbGKhtcbP3LBfQdCVp9/5rPJS+TUtBjE7ic9DjkCJzQ83z7+pzzkWKsKZJ/0x9nXGIxHYdkFsd7v3M9+79YKWxehZx0RbQfBI8bGmX265fOZpwLwU8GUYEmSA20GBuYQa7FkKMcPcw++DbZqMAAb3mLNqRX6BGi01qnD093QVG/na/oAo85ADmJ7f/hC3euiInlhBx6yLt398znM/jra6O1I7mT1GvFpLgXPYHDw==";
		
		byte[] decode = Base64.decode(s.getBytes());
		System.out.println(new String(decode));
		
		 java.nio.ByteBuffer buf = java.nio.ByteBuffer.wrap(decode);
		 Charset charset = Charset.forName("ISO-8859-1");
		 CharsetDecoder decoder = charset.newDecoder();
		 CharBuffer decode2 = decoder.decode(buf);
		 System.out.println(decode2.toString());
		
		
		
		s = "MIICNTCCAZ6gAwIBAAIEQAUZZjANBgkqhkiG9w0BAQUFADBfMQswCQYDVQQGEwJVSzEPMA0GA1UECBMGTG9uZG9uMQswCQYDVQQHEwJFVTEOMAwGA1UEChMFUHJvdHgxEjAQBgNVBAsTCVByb3R4IE1QSTEOMAwGA1UEAxMFUHJvdHgwHhcNMDQwMTE0MTAyNjQ2WhcNMzEwNjAxMTAyNjQ2WjBfMQswCQYDVQQGEwJVSzEPMA0GA1UECBMGTG9uZG9uMQswCQYDVQQHEwJFVTEOMAwGA1UEChMFUHJvdHgxEjAQBgNVBAsTCVByb3R4IE1QSTEOMAwGA1UEAxMFUHJvdHgwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAKA0Gcn2PG0ZkhTE4C26T6lcS0TdL4OFPUcj6Eaa5DNMgUH1ZXcjXo+yYLV5J/iqVVkcgsSQros6i11rNnG3fh+PnOWoxsmgQg5aTc4/T6j+rZeCcnbZHkAKvFOAZQ2l31+uVWogxL5PtaIcX2vhg3PCVaIoAjD3O6+x+i8VSxIlAgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAjn3tCSO3u2PjPwSrgpkSyOcDGwqdiywiVNp+RvkytuA3rUq4VnZeVi14Qu0x/z/E+UhE9Bj6jTUeRh2k99v8yfBqcCWuYl8ZAFGETTfXAeZavsOgJsOsrjxlN++fIy6n7OzgE2ANtrh6SGbWDxniJRDvpFFqhkTi6krIz6fMv6E=";
		
		decode = Base64.decode(s.getBytes());
		System.out.println(decode);

		 buf = java.nio.ByteBuffer.wrap(decode);
		 charset = Charset.forName("UTF-8");
		 decoder = charset.newDecoder();
		 decode2 = decoder.decode(buf);
		 System.out.println(decode2.toString());		
	}

}
