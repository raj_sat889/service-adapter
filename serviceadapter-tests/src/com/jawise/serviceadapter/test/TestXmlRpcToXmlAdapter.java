package com.jawise.serviceadapter.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class TestXmlRpcToXmlAdapter extends TestCase {
	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=sathyan&service=JobFinderService2&messageconversion=quickjobconverter";

	private static final Logger logger = Logger
			.getLogger(TestXmlRpcToXmlAdapter.class);

	protected TestFixture serviceAdapterTestFixture;

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	private XmlRpcClient getXmlRpcClient(String url)
			throws MalformedURLException {
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		config.setServerURL(new URL(url));
		XmlRpcClient client = new XmlRpcClient();
		client.setConfig(config);
		return client;
	}

	// adapter interface tests
	
	@SuppressWarnings("unchecked")
	public void testSubmitPurchaceOrderInvalidParmeters()
			throws MalformedURLException {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","java"};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e
					.getMessage()
					.indexOf(
							"Parameter count mismatch. The operation requires 5 parameters.") >= 0);

		}

	}	
	
	@SuppressWarnings("unchecked")
	public void testInvalidParmeterType1() throws MalformedURLException {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","java",""};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
			"Parameter locations[] should be an array type") >= 0);
		}

	}
	
	public void testInvalidParmeterType2() throws MalformedURLException {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] locations = new String[]{"Australia"};
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper",new Integer(10021),locations};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
			"Parameter skills not correct type") >= 0);
		}

	}	
	
	
	@SuppressWarnings("unchecked")
	public void testNoValidationAdapteeParameters() throws MalformedURLException {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] locations = new String[]{"Australia"};
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","java c++ VB Oracel SQL",locations};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			assertTrue(result.length == 3);			
			Map m = (Map) result[0];
			assertTrue( m.get("tittle").equals("Java Software Developer"));
		} catch (XmlRpcException e) {
			e.printStackTrace();
			fail();
		}
	}	
	
	
	@SuppressWarnings("unchecked")
	public void testNoneOptionalValue() throws MalformedURLException {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] locations = new String[]{"Australia"};
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","",locations};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e
					.getMessage()
					.indexOf(
							"message conversion exception, messages part skills not optional") >= 0);
		}
	}		
	
	// test adaptee transformation and script
	public void testExceptionInScript() throws MalformedURLException {
		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] locations = new String[]{"Australia"};
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","throw error",locations};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
			"error occurred evaluating the script") >= 0);
		}		
	}
	
	
	@SuppressWarnings("unchecked")
	public void testSuccessfulRequestWithZeroLengthArray() throws MalformedURLException {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] locations = new String[0];
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","java",locations};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			assertTrue(result.length == 3);			
			Map m = (Map) result[0];
			assertTrue( m.get("tittle").equals("Java Software Developer"));
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			fail();
		}
	}

	//test behaviour
	
	public void testValidation1() throws MalformedURLException {
		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] locations = new String[]{"Africa"};
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","java",locations};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			assertTrue(result.length == 0);			
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			fail();
		}		
	}
	
	public void testValidation2() throws MalformedURLException {
		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] locations = new String[]{"China"};
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","java",locations};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf("The response message is empty or its not provided")>=0);
		}		
	}	
	
	@SuppressWarnings("unchecked")
	public void testSuccessfulRequest() throws MalformedURLException {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			Object[] locations = new String[]{"Australia"};
			Object[] params = new Object[] { new String("raj"),
					new String(""), "java devloper","java",locations};			
			Object[] result = (Object[]) client.execute(
					"JobFinderService.findJobs", params);
			assertTrue(result.length == 3);			
			Map m = (Map) result[0];
			assertTrue( m.get("tittle").equals("Java Software Developer"));
		} catch (XmlRpcException e) {
			e.printStackTrace();
			fail();
		}
	}	

}
