
package com.jawise.serviceadapter.test.svc.soap.computerparts;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

public class ZComputerPartsSupplierClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap endpoints = new HashMap();
    private Service service0;

    public ZComputerPartsSupplierClient() {
        create0();
        Endpoint ZComputerPartsSupplierPortTypeLocalEndpointEP = service0 .addEndpoint(new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierPortTypeLocalEndpoint"), new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierPortTypeLocalBinding"), "xfire.local://ZComputerPartsSupplier");
        endpoints.put(new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierPortTypeLocalEndpoint"), ZComputerPartsSupplierPortTypeLocalEndpointEP);
        Endpoint ZComputerPartsSupplierHttpPortEP = service0 .addEndpoint(new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierHttpPort"), new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierHttpBinding"), "http://localhost:8080/serviceadapter-testservices/soa/ZComputerPartsSupplier");
        endpoints.put(new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierHttpPort"), ZComputerPartsSupplierHttpPortEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((com.jawise.serviceadapter.test.svc.soap.computerparts.ZComputerPartsSupplierPortType.class), props);
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierHttpBinding"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierPortTypeLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public ZComputerPartsSupplierPortType getZComputerPartsSupplierPortTypeLocalEndpoint() {
        return ((ZComputerPartsSupplierPortType)(this).getEndpoint(new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierPortTypeLocalEndpoint")));
    }

    public ZComputerPartsSupplierPortType getZComputerPartsSupplierPortTypeLocalEndpoint(String url) {
        ZComputerPartsSupplierPortType var = getZComputerPartsSupplierPortTypeLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public ZComputerPartsSupplierPortType getZComputerPartsSupplierHttpPort() {
        return ((ZComputerPartsSupplierPortType)(this).getEndpoint(new QName("http://soap.svc.test.serviceadapter.jawise.com", "ZComputerPartsSupplierHttpPort")));
    }

    public ZComputerPartsSupplierPortType getZComputerPartsSupplierHttpPort(String url) {
        ZComputerPartsSupplierPortType var = getZComputerPartsSupplierHttpPort();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
