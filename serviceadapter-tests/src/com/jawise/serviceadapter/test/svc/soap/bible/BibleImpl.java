
package com.jawise.serviceadapter.test.svc.soap.bible;

import javax.jws.WebService;

@WebService(serviceName = "Bible", targetNamespace = "http://www.stgregorioschurchdc.org/Bible", endpointInterface = "com.jawise.serviceadapter.test.svc.soap.bible.BibleVersesSoapPort")
public class BibleImpl
    implements BibleVersesSoapPort
{


    public String read_bible(String from) {
        throw new UnsupportedOperationException();
    }

}
