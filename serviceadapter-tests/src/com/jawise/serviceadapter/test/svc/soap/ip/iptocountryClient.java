
package com.jawise.serviceadapter.test.svc.soap.ip;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

public class iptocountryClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap endpoints = new HashMap();
    private Service service2;

    public iptocountryClient() {
        create2();
        Endpoint iptocountrySoapLocalEndpointEP = service2 .addEndpoint(new QName("http://www.ecubicle.net/webservices/", "iptocountrySoapLocalEndpoint"), new QName("http://www.ecubicle.net/webservices/", "iptocountrySoapLocalBinding"), "xfire.local://iptocountry");
        endpoints.put(new QName("http://www.ecubicle.net/webservices/", "iptocountrySoapLocalEndpoint"), iptocountrySoapLocalEndpointEP);
        Endpoint iptocountrySoapEP = service2 .addEndpoint(new QName("http://www.ecubicle.net/webservices/", "iptocountrySoap"), new QName("http://www.ecubicle.net/webservices/", "iptocountrySoap"), "http://www.ecubicle.net/iptocountry.asmx");
        endpoints.put(new QName("http://www.ecubicle.net/webservices/", "iptocountrySoap"), iptocountrySoapEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create2() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service2 = asf.create((com.jawise.serviceadapter.test.svc.soap.ip.iptocountrySoap.class), props);
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service2, new QName("http://www.ecubicle.net/webservices/", "iptocountrySoap"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service2, new QName("http://www.ecubicle.net/webservices/", "iptocountrySoapLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public iptocountrySoap getiptocountrySoapLocalEndpoint() {
        return ((iptocountrySoap)(this).getEndpoint(new QName("http://www.ecubicle.net/webservices/", "iptocountrySoapLocalEndpoint")));
    }

    public iptocountrySoap getiptocountrySoapLocalEndpoint(String url) {
        iptocountrySoap var = getiptocountrySoapLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public iptocountrySoap getiptocountrySoap() {
        return ((iptocountrySoap)(this).getEndpoint(new QName("http://www.ecubicle.net/webservices/", "iptocountrySoap")));
    }

    public iptocountrySoap getiptocountrySoap(String url) {
        iptocountrySoap var = getiptocountrySoap();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
