
package com.jawise.serviceadapter.test.svc.soap.ip;

import javax.jws.WebService;
import net.ecubicle.webservices.FindCountryAsStringResponse;
import net.ecubicle.webservices.FindCountryAsXmlResponse;

@WebService(serviceName = "iptocountry", targetNamespace = "http://www.ecubicle.net/webservices/", endpointInterface = "com.jawise.serviceadapter.test.svc.soap.ip.iptocountrySoap")
public class iptocountryImpl
    implements iptocountrySoap
{


    public FindCountryAsStringResponse findCountryAsString(net.ecubicle.webservices.FindCountryAsString FindCountryAsString) {
        throw new UnsupportedOperationException();
    }

    public FindCountryAsXmlResponse findCountryAsXml(net.ecubicle.webservices.FindCountryAsXml FindCountryAsXml) {
        throw new UnsupportedOperationException();
    }

}
