/**
 * CarRentalServiceALocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jawise.serviceadapter.test.svc.soap;

public class CarRentalServiceALocator extends org.apache.axis.client.Service implements com.jawise.serviceadapter.test.svc.soap.CarRentalServiceA {

    public CarRentalServiceALocator() {
    }


    public CarRentalServiceALocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CarRentalServiceALocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CarRentalServiceAHttpPort
    private java.lang.String CarRentalServiceAHttpPort_address = "http://localhost:8080/serviceadapter-testservices/soa/CarRentalServiceA";

    public java.lang.String getCarRentalServiceAHttpPortAddress() {
        return CarRentalServiceAHttpPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CarRentalServiceAHttpPortWSDDServiceName = "CarRentalServiceAHttpPort";

    public java.lang.String getCarRentalServiceAHttpPortWSDDServiceName() {
        return CarRentalServiceAHttpPortWSDDServiceName;
    }

    public void setCarRentalServiceAHttpPortWSDDServiceName(java.lang.String name) {
        CarRentalServiceAHttpPortWSDDServiceName = name;
    }

    public com.jawise.serviceadapter.test.svc.soap.CarRentalServiceAPortType getCarRentalServiceAHttpPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CarRentalServiceAHttpPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCarRentalServiceAHttpPort(endpoint);
    }

    public com.jawise.serviceadapter.test.svc.soap.CarRentalServiceAPortType getCarRentalServiceAHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.jawise.serviceadapter.test.svc.soap.CarRentalServiceAHttpBindingStub _stub = new com.jawise.serviceadapter.test.svc.soap.CarRentalServiceAHttpBindingStub(portAddress, this);
            _stub.setPortName(getCarRentalServiceAHttpPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCarRentalServiceAHttpPortEndpointAddress(java.lang.String address) {
        CarRentalServiceAHttpPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.jawise.serviceadapter.test.svc.soap.CarRentalServiceAPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.jawise.serviceadapter.test.svc.soap.CarRentalServiceAHttpBindingStub _stub = new com.jawise.serviceadapter.test.svc.soap.CarRentalServiceAHttpBindingStub(new java.net.URL(CarRentalServiceAHttpPort_address), this);
                _stub.setPortName(getCarRentalServiceAHttpPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CarRentalServiceAHttpPort".equals(inputPortName)) {
            return getCarRentalServiceAHttpPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://soap.svc.test.serviceadapter.jawise.com", "CarRentalServiceA");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://soap.svc.test.serviceadapter.jawise.com", "CarRentalServiceAHttpPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CarRentalServiceAHttpPort".equals(portName)) {
            setCarRentalServiceAHttpPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
