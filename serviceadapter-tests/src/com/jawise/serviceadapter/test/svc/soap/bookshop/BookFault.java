
package com.jawise.serviceadapter.test.svc.soap.bookshop;

import javax.xml.namespace.QName;
import org.codehaus.xfire.fault.FaultInfoException;

public class BookFault
    extends FaultInfoException
{

    private BookExceptionDetail faultInfo;

    public BookFault(String message, BookExceptionDetail faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    public BookFault(String message, Throwable t, BookExceptionDetail faultInfo) {
        super(message, t);
        this.faultInfo = faultInfo;
    }

    public BookExceptionDetail getFaultInfo() {
        return faultInfo;
    }

    public static QName getFaultName() {
        return new QName("http://demo.xfire.codehaus.org", "BookFault");
    }

}
