
package com.jawise.serviceadapter.test.svc.soap.bible;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

public class BibleClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap endpoints = new HashMap();
    private Service service0;

    public BibleClient() {
        create0();
        Endpoint BibleVersesSoapPortLocalEndpointEP = service0 .addEndpoint(new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapPortLocalEndpoint"), new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapPortLocalBinding"), "xfire.local://Bible");
        endpoints.put(new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapPortLocalEndpoint"), BibleVersesSoapPortLocalEndpointEP);
        Endpoint BibleVersesSoapPortEP = service0 .addEndpoint(new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapPort"), new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapBinding"), "http://www.stgregorioschurchdc.org/cgi/websvcbible.cgi");
        endpoints.put(new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapPort"), BibleVersesSoapPortEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((com.jawise.serviceadapter.test.svc.soap.bible.BibleVersesSoapPort.class), props);
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapBinding"), "http://schemas.xmlsoap.org/soap/http");
        }
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapPortLocalBinding"), "urn:xfire:transport:local");
        }
    }

    public BibleVersesSoapPort getBibleVersesSoapPortLocalEndpoint() {
        return ((BibleVersesSoapPort)(this).getEndpoint(new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapPortLocalEndpoint")));
    }

    public BibleVersesSoapPort getBibleVersesSoapPortLocalEndpoint(String url) {
        BibleVersesSoapPort var = getBibleVersesSoapPortLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public BibleVersesSoapPort getBibleVersesSoapPort() {
        return ((BibleVersesSoapPort)(this).getEndpoint(new QName("http://www.stgregorioschurchdc.org/Bible", "BibleVersesSoapPort")));
    }

    public BibleVersesSoapPort getBibleVersesSoapPort(String url) {
        BibleVersesSoapPort var = getBibleVersesSoapPort();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
