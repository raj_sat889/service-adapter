
package com.jawise.serviceadapter.test.svc.soap.bookshop;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "BookShopServicePortType", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface BookShopServicePortType {


    @WebMethod(operationName = "getBookPrice", action = "")
    @WebResult(name = "out", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
    public String getBookPrice(
        @WebParam(name = "isbn", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
        String isbn);

    @WebMethod(operationName = "getBooksMap", action = "")
    @WebResult(name = "out", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
    public AnyType2AnyTypeMap getBooksMap();

    @WebMethod(operationName = "findBook", action = "")
    @WebResult(name = "out", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
    public Book findBook(
        @WebParam(name = "isbn", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
        String isbn)
        throws BookFault
    ;

    @WebMethod(operationName = "getBooks", action = "")
    @WebResult(name = "out", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
    public ArrayOfBook getBooks();

}
