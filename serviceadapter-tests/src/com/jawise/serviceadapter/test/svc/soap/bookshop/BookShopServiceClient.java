
package com.jawise.serviceadapter.test.svc.soap.bookshop;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

public class BookShopServiceClient {

    private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
    private HashMap endpoints = new HashMap();
    private Service service0;

    public BookShopServiceClient() {
        create0();
        Endpoint BookShopServiceHttpPortEP = service0 .addEndpoint(new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServiceHttpPort"), new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServiceHttpBinding"), "http://localhost:8080/serviceadapter-testservices/soa/BookShopService");
        endpoints.put(new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServiceHttpPort"), BookShopServiceHttpPortEP);
        Endpoint BookShopServicePortTypeLocalEndpointEP = service0 .addEndpoint(new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServicePortTypeLocalEndpoint"), new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServicePortTypeLocalBinding"), "xfire.local://BookShopService");
        endpoints.put(new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServicePortTypeLocalEndpoint"), BookShopServicePortTypeLocalEndpointEP);
    }

    public Object getEndpoint(Endpoint endpoint) {
        try {
            return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
        } catch (MalformedURLException e) {
            throw new XFireRuntimeException("Invalid URL", e);
        }
    }

    public Object getEndpoint(QName name) {
        Endpoint endpoint = ((Endpoint) endpoints.get((name)));
        if ((endpoint) == null) {
            throw new IllegalStateException("No such endpoint!");
        }
        return getEndpoint((endpoint));
    }

    public Collection getEndpoints() {
        return endpoints.values();
    }

    private void create0() {
        TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
        HashMap props = new HashMap();
        props.put("annotations.allow.interface", true);
        AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm, new AegisBindingProvider(new JaxbTypeRegistry()));
        asf.setBindingCreationEnabled(false);
        service0 = asf.create((com.jawise.serviceadapter.test.svc.soap.bookshop.BookShopServicePortType.class), props);
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServicePortTypeLocalBinding"), "urn:xfire:transport:local");
        }
        {
            AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServiceHttpBinding"), "http://schemas.xmlsoap.org/soap/http");
        }
    }

    public BookShopServicePortType getBookShopServiceHttpPort() {
        return ((BookShopServicePortType)(this).getEndpoint(new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServiceHttpPort")));
    }

    public BookShopServicePortType getBookShopServiceHttpPort(String url) {
        BookShopServicePortType var = getBookShopServiceHttpPort();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

    public BookShopServicePortType getBookShopServicePortTypeLocalEndpoint() {
        return ((BookShopServicePortType)(this).getEndpoint(new QName("http://soap.svc.test.serviceadapter.jawise.com", "BookShopServicePortTypeLocalEndpoint")));
    }

    public BookShopServicePortType getBookShopServicePortTypeLocalEndpoint(String url) {
        BookShopServicePortType var = getBookShopServicePortTypeLocalEndpoint();
        org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
        return var;
    }

}
