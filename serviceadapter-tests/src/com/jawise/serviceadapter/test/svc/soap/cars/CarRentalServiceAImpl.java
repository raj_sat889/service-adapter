
package com.jawise.serviceadapter.test.svc.soap.cars;

import javax.jws.WebService;
import com.jawise.serviceadapter.test.svc.soap.ArrayOfCar;

@WebService(serviceName = "CarRentalServiceA", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com", endpointInterface = "com.jawise.serviceadapter.test.svc.soap.cars.CarRentalServiceAPortType")
public class CarRentalServiceAImpl
    implements CarRentalServiceAPortType
{


    public ArrayOfCar listRentalCars(String location, String collectiondate, String returndate) {
        throw new UnsupportedOperationException();
    }

}
