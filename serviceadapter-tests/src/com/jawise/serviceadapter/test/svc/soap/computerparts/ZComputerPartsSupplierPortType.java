
package com.jawise.serviceadapter.test.svc.soap.computerparts;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "ZComputerPartsSupplierPortType", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface ZComputerPartsSupplierPortType {


    @WebMethod(operationName = "purchaseOrder", action = "")
    @WebResult(name = "out", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
    public String purchaseOrder(
        @WebParam(name = "username", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
        String username,
        @WebParam(name = "password", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
        String password,
        @WebParam(name = "order", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
        Order order);

}
