
package com.jawise.serviceadapter.test.svc.soap.computerparts;

import javax.jws.WebService;

@WebService(serviceName = "ZComputerPartsSupplier", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com", endpointInterface = "com.jawise.serviceadapter.test.svc.soap.computerparts.ZComputerPartsSupplierPortType")
public class ZComputerPartsSupplierImpl
    implements ZComputerPartsSupplierPortType
{


    public String purchaseOrder(String username, String password, Order order) {
        throw new UnsupportedOperationException();
    }

}
