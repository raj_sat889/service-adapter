
package com.jawise.serviceadapter.test.svc.soap.cars;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import com.jawise.serviceadapter.test.svc.soap.ArrayOfCar;

@WebService(name = "CarRentalServiceAPortType", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface CarRentalServiceAPortType {


    @WebMethod(operationName = "listRentalCars", action = "")
    @WebResult(name = "out", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
    public ArrayOfCar listRentalCars(
        @WebParam(name = "location", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
        String location,
        @WebParam(name = "collectiondate", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
        String collectiondate,
        @WebParam(name = "returndate", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com")
        String returndate);

}
