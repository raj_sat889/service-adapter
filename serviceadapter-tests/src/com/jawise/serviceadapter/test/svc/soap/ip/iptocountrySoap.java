
package com.jawise.serviceadapter.test.svc.soap.ip;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import net.ecubicle.webservices.FindCountryAsStringResponse;
import net.ecubicle.webservices.FindCountryAsXmlResponse;

@WebService(name = "iptocountrySoap", targetNamespace = "http://www.ecubicle.net/webservices/")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface iptocountrySoap {


    @WebMethod(operationName = "FindCountryAsString", action = "http://www.ecubicle.net/webservices/FindCountryAsString")
    @WebResult(name = "FindCountryAsStringResponse", targetNamespace = "http://www.ecubicle.net/webservices/")
    public FindCountryAsStringResponse findCountryAsString(
        @WebParam(name = "FindCountryAsString", targetNamespace = "http://www.ecubicle.net/webservices/")
        net.ecubicle.webservices.FindCountryAsString FindCountryAsString);

    @WebMethod(operationName = "FindCountryAsXml", action = "http://www.ecubicle.net/webservices/FindCountryAsXml")
    @WebResult(name = "FindCountryAsXmlResponse", targetNamespace = "http://www.ecubicle.net/webservices/")
    public FindCountryAsXmlResponse findCountryAsXml(
        @WebParam(name = "FindCountryAsXml", targetNamespace = "http://www.ecubicle.net/webservices/")
        net.ecubicle.webservices.FindCountryAsXml FindCountryAsXml);

}
