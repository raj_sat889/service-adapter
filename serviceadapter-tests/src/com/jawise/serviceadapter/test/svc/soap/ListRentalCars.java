
package com.jawise.serviceadapter.test.svc.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="collectiondate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="returndate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "location",
    "collectiondate",
    "returndate"
})
@XmlRootElement(name = "listRentalCars")
public class ListRentalCars {

    @XmlElement(required = true, nillable = true)
    protected String location;
    @XmlElement(required = true, nillable = true)
    protected String collectiondate;
    @XmlElement(required = true, nillable = true)
    protected String returndate;

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Gets the value of the collectiondate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectiondate() {
        return collectiondate;
    }

    /**
     * Sets the value of the collectiondate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectiondate(String value) {
        this.collectiondate = value;
    }

    /**
     * Gets the value of the returndate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturndate() {
        return returndate;
    }

    /**
     * Sets the value of the returndate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturndate(String value) {
        this.returndate = value;
    }

}
