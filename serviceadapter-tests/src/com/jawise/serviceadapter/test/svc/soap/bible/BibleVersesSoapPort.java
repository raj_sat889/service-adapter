
package com.jawise.serviceadapter.test.svc.soap.bible;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "BibleVersesSoapPort", targetNamespace = "http://www.stgregorioschurchdc.org/Bible")
@SOAPBinding(style = SOAPBinding.Style.RPC, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface BibleVersesSoapPort {


    @WebMethod(operationName = "read_bible", action = "http://www.stgregorioschurchdc.org/Bible#read_bible")
    @WebResult(name = "verses", targetNamespace = "http://www.stgregorioschurchdc.org/Bible")
    public String read_bible(
        @WebParam(name = "from", targetNamespace = "http://www.stgregorioschurchdc.org/Bible")
        String from);

}
