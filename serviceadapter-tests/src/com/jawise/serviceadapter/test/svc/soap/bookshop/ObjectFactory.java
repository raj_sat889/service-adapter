
package com.jawise.serviceadapter.test.svc.soap.bookshop;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jawise.serviceadapter.test.svc.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BookAuthor_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "author");
    private final static QName _BookTitle_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "title");
    private final static QName _BookIsbn_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "isbn");
    private final static QName _BookExceptionDetailCode_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "code");
    private final static QName _BookExceptionDetailDetailMessage_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "detailMessage");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jawise.serviceadapter.test.svc.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBooksMap }
     * 
     */
    public GetBooksMap createGetBooksMap() {
        return new GetBooksMap();
    }

    /**
     * Create an instance of {@link GetBooks }
     * 
     */
    public GetBooks createGetBooks() {
        return new GetBooks();
    }

    /**
     * Create an instance of {@link AnyType2AnyTypeMap.Entry }
     * 
     */
    public AnyType2AnyTypeMap.Entry createAnyType2AnyTypeMapEntry() {
        return new AnyType2AnyTypeMap.Entry();
    }

    /**
     * Create an instance of {@link ArrayOfBook }
     * 
     */
    public ArrayOfBook createArrayOfBook() {
        return new ArrayOfBook();
    }

    /**
     * Create an instance of {@link GetBookPrice }
     * 
     */
    public GetBookPrice createGetBookPrice() {
        return new GetBookPrice();
    }

    /**
     * Create an instance of {@link GetBooksResponse }
     * 
     */
    public GetBooksResponse createGetBooksResponse() {
        return new GetBooksResponse();
    }

    /**
     * Create an instance of {@link AnyType2AnyTypeMap }
     * 
     */
    public AnyType2AnyTypeMap createAnyType2AnyTypeMap() {
        return new AnyType2AnyTypeMap();
    }

    /**
     * Create an instance of {@link Book }
     * 
     */
    public Book createBook() {
        return new Book();
    }

    /**
     * Create an instance of {@link FindBook }
     * 
     */
    public FindBook createFindBook() {
        return new FindBook();
    }

    /**
     * Create an instance of {@link GetBooksMapResponse }
     * 
     */
    public GetBooksMapResponse createGetBooksMapResponse() {
        return new GetBooksMapResponse();
    }

    /**
     * Create an instance of {@link FindBookResponse }
     * 
     */
    public FindBookResponse createFindBookResponse() {
        return new FindBookResponse();
    }

    /**
     * Create an instance of {@link GetBookPriceResponse }
     * 
     */
    public GetBookPriceResponse createGetBookPriceResponse() {
        return new GetBookPriceResponse();
    }

    /**
     * Create an instance of {@link BookExceptionDetail }
     * 
     */
    public BookExceptionDetail createBookExceptionDetail() {
        return new BookExceptionDetail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "author", scope = Book.class)
    public JAXBElement<String> createBookAuthor(String value) {
        return new JAXBElement<String>(_BookAuthor_QNAME, String.class, Book.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "title", scope = Book.class)
    public JAXBElement<String> createBookTitle(String value) {
        return new JAXBElement<String>(_BookTitle_QNAME, String.class, Book.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "isbn", scope = Book.class)
    public JAXBElement<String> createBookIsbn(String value) {
        return new JAXBElement<String>(_BookIsbn_QNAME, String.class, Book.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "code", scope = BookExceptionDetail.class)
    public JAXBElement<String> createBookExceptionDetailCode(String value) {
        return new JAXBElement<String>(_BookExceptionDetailCode_QNAME, String.class, BookExceptionDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "detailMessage", scope = BookExceptionDetail.class)
    public JAXBElement<String> createBookExceptionDetailDetailMessage(String value) {
        return new JAXBElement<String>(_BookExceptionDetailDetailMessage_QNAME, String.class, BookExceptionDetail.class, value);
    }

}
