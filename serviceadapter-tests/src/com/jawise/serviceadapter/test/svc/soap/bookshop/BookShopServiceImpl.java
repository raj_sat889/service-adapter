
package com.jawise.serviceadapter.test.svc.soap.bookshop;

import javax.jws.WebService;

@WebService(serviceName = "BookShopService", targetNamespace = "http://soap.svc.test.serviceadapter.jawise.com", endpointInterface = "com.jawise.serviceadapter.test.svc.soap.bookshop.BookShopServicePortType")
public class BookShopServiceImpl
    implements BookShopServicePortType
{


    public String getBookPrice(String isbn) {
        throw new UnsupportedOperationException();
    }

    public AnyType2AnyTypeMap getBooksMap() {
        throw new UnsupportedOperationException();
    }

    public Book findBook(String isbn)
        throws BookFault
    {
        throw new UnsupportedOperationException();
    }

    public ArrayOfBook getBooks() {
        throw new UnsupportedOperationException();
    }

}
