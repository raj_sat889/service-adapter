package com.jawise.serviceadapter.test;

import java.util.HashMap;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

public class TestXmlToXmlRpcAdapter extends TestCase {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(TestXmlToXmlRpcAdapter.class);

	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=QuickJobSearchXml&messageconversion=JobSearchConverter";

	protected TestFixture serviceAdapterTestFixture;

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	// *****************************************************************************************
	/* adapter interface teststs */
	// *****************************************************************************************
	public void testMethodSignature() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("", "Australia");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part JobSearch/SearchPhrase not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testOptionalParameter() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("Java", "");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("Java Software Developer") >= 0);
			assertTrue(res.getText().indexOf("Java/J2EE Senior Developer") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testSuccessfulCallWithoutSourceParamterValidation() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("Java",
					"Ausralia, United Kingtom, Sri Lanka, Inida , Jappan, United States Of Americ");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("Java Software Developer") >= 0);
			assertTrue(res.getText().indexOf("Java/J2EE Senior Developer") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testInvalidRequestMessageFormat() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("Java", "Ausralia");
			String xml = postReq.getParameter("xml");
			xml = xml.replaceAll("<Area>", "<Area><Area>");
			postReq.setParameter("xml", xml);
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"The element type \"Area\" must be terminated by the matching end-tag") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	// *****************************************************************************************
	/* test transformation and scripts */
	// *****************************************************************************************

	public void testCallWithArithemeticExceptionThowingScript() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("error", "Australia");

			WebResponse res = null;
			try {

				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("Arithemetic Exception") >= 0);

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithNullExceptionThowingScript() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("throw error",
					"Australia");
			WebResponse res = null;
			try {

				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("TargetError") >= 0);

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithEmptryParameter() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("", "Ausralia");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part JobSearch/SearchPhrase not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	// *****************************************************************************************
	// adaptee service behaviour tests
	// *****************************************************************************************

	public void testSuccessfulRequest() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("VB", "Australia");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("Java Software Developer") >= 0);
			assertTrue(res.getText().indexOf("Java/J2EE Senior Developer") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testAdapteeValidationError1() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest("VB", "China");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			// decoding xml encoded values
			assertTrue(res.getText().indexOf("Jobs in China not avilable") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	// *****************************************************************************************
	// service adapter invocation
	// *****************************************************************************************

	public void testServiceAdapterInvocationWrongService() {
		try {

			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"QuickJobSearchXml", "ERQuickJobSearchXml"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"service requested is not found in the registry.") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceAdapterInvocationWrongConverter() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"JobSearchConverter", "SomeJobSearchConverter"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion requested is not found in the registry.") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceAdapterInvocationWithWrongUser() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"raj", "sdfsdf"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("userid is invalid") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	private PostMethodWebRequest buildRequest(String url) throws Exception{
		PostMethodWebRequest req = new PostMethodWebRequest(url);
		String xmlmsg = buildXML("Java", "australia");
		req.setParameter("xml", xmlmsg);
		return req;
	}

	private PostMethodWebRequest buildRequest(String searchPhrase, String area)
			throws Exception {
		PostMethodWebRequest req = new PostMethodWebRequest(
				TestXmlToXmlRpcAdapter.SERVICE_URL);
		String xmlmsg = buildXML(searchPhrase, area);
		req.setParameter("xml", xmlmsg);
		return req;
	}

	private String buildXML(String searchPhrase, String area) throws Exception {
		XmlComposer composer = new XmlComposer();
		HashMap<XmlPath, String> xmldata = new HashMap<XmlPath, String>();
		if (!searchPhrase.isEmpty())
			xmldata.put(new XmlPath("JobSearch/SearchPhrase"), searchPhrase);
		if (!area.isEmpty())
			xmldata.put(new XmlPath("JobSearch/Area"), area);

		String xmlmsg = composer.compose(xmldata);
		return xmlmsg;
	}
}
