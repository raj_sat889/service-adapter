package com.jawise.serviceadapter.test;

import java.io.ByteArrayInputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

public class TestXmlToNvpAdapter extends TestCase {

	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=sathyan&service=FraudScreeningService&messageconversion=rtfcTOcheepFraudConversion";

	private static final Logger logger = Logger
			.getLogger(TestXmlToNvpAdapter.class);

	protected TestFixture serviceAdapterTestFixture;

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}	
	// adapter interface tests

	public void testInvalidRequest() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();

			String xml = postReq.getParameter("xml");
			xml = xml.replaceFirst("</transaction_reference>",
					"<transaction_reference>");
			postReq.setParameter("xml", xml);

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"The following error occurred processing the request. error : The element type \"transaction_reference\" must be terminated by the matching end-tag \"</transaction_reference>\".") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testOptionalPrameter() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"4929000000006", "4122", "", ""));

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testNoneOptionalPrameter() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML("", "4122",
					"raj@gami.com", ""));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);

			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part RealTime/card_number not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testSuccessfulCallWithoutSourceParamterValidation() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"49290000000064929000000006212", "4122", "raj@gami.com", ""));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	// adapter transformation and script tests

/*	public void testCallWithInvalidScript() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"4929000000006", "4122", "raj@gami.com", "Northampton"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);

			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("Parse error") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}*/

	/*
	 * convered by opional paramter public void testConversionOfEmptryValues(){ }
	 */
	public void testCallWithArithemeticExceptionThowingScript() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"4929000000006", "error", "raj@gami.com", "Northampton"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);

			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("Arithemetic Exception") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testCallWithNullExceptionThowingScript() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"4929000000006", "throw error", "raj@gami.com",
					"Northampton"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);

			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("TargetError") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}
/*
	public void testCallWithRecurciverError() {
		assertTrue(res.getText().indexOf(
				"error occurred evaluating the script") >= 0);
	}
*/
	// adaptee interface tests

	public void testInvlaidResponseFromAdaptee(){
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"49290100000006", "4122", "", ""));

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("message communication error occurred. error : Server returned HTTP response code: 500") >=0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}		
	}
	
	// adaptee service behaviour

	public void testSuccessfulRequest() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testAdapteeValidationError(){
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"4929888888886", "4122", "", ""));

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("error occurred evaluating the script")>=0);
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}		
	}	
	
	public void testCallTimeout() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"49290200000006", "4122", "", ""));

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("message communication error occurred. error : Read timed out") >=0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}		
	}
	
	public void testEcondedResponse() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(buildXML(
					"49290300000006", "4122", "", ""));

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			//TODO better error halding
			assertTrue(res.getText().indexOf("error occurred evaluating the script")>=0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}			
		
	}

	// *****************************************************************************************
	// service adapter invocation
	// *****************************************************************************************

	public void testServiceAdapterInvocationWrongService() {
		try {

			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequestFor(SERVICE_URL.replaceAll(
					"FraudScreeningService", "CCCFraudScreeningService"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"service requested is not found in the registry.") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceAdapterInvocationWrongConverter() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequestFor(SERVICE_URL.replaceAll(
					"rtfcTOcheepFraudConversion", "rdsdtfcTOminFraudConversion"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion requested is not found in the registry.") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceAdapterInvocationWithWrongUser() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequestFor(SERVICE_URL.replaceAll(
					"sathyan", "sdfsdf"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("userid is invalid") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}
	
	private void assertNotSuccessResponse(Document doc)
			throws TransformerException {
		Node scoreNode = XPathAPI.selectSingleNode(doc,
				"RealTimeResponse/Score");
		assertNotNull(scoreNode);
		String score = scoreNode.getFirstChild().getNodeValue();
		assertTrue("69".equals(score));

		Node recommendationNode = XPathAPI.selectSingleNode(doc,
				"RealTimeResponse/recommendation");
		assertNotNull(recommendationNode);
		String recommendation = recommendationNode.getFirstChild()
				.getNodeValue();
		assertTrue("0".equals(recommendation));

	}

	private Document parseResponse(String text) throws Exception {

		DocumentBuilderFactory i = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = i.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(text.getBytes()));
		return doc;
	}

	private PostMethodWebRequest buildRequestFor(String url) throws Exception {
		PostMethodWebRequest req = new PostMethodWebRequest(url);
		String xmlmsg = buildXML("4929000000006", "4122", "raj@gami.com","");
		req.setParameter("xml", xmlmsg);
		return req;
	}
	
	private PostMethodWebRequest buildRequest() throws Exception {
		PostMethodWebRequest req = new PostMethodWebRequest(
				TestXmlToNvpAdapter.SERVICE_URL);
		String xmlmsg = buildXML("4929000000006", "4122", "raj@gami.com","");
		req.setParameter("xml", xmlmsg);
		return req;
	}

	private PostMethodWebRequest buildRequest(String xml) throws Exception {
		PostMethodWebRequest req = new PostMethodWebRequest(
				TestXmlToNvpAdapter.SERVICE_URL);
		req.setParameter("xml", xml);
		return req;
	}

	private String buildXML(String card_number,
			String billing_postcode_zipcode, String customer_email_address,
			String delivery_city) throws Exception {
		XmlComposer composer = new XmlComposer();
		HashMap<XmlPath, String> xmldata = new HashMap<XmlPath, String>();
		xmldata.put(new XmlPath("RealTime/transaction_reference"), new GUID()
				.toString());
		xmldata.put(new XmlPath("RealTime/transaction_date_time"),
				"010808 10:12:00");
		xmldata.put(new XmlPath("RealTime/aggregator_identifier"), "JAWISE");
		xmldata.put(new XmlPath("RealTime/merchant_identifier"), "1231313");
		xmldata.put(new XmlPath("RealTime/merchant_order_ref"), "ORDER121");
		xmldata.put(new XmlPath("RealTime/customer_ref"), "2829827");
		xmldata.put(new XmlPath("RealTime/sales_channel"), "E");
		xmldata.put(new XmlPath("RealTime/cardholder_title"), "Mr");
		xmldata.put(new XmlPath("RealTime/cardholder_first_name"), "Bill");
		xmldata.put(new XmlPath("RealTime/cardholder_surname"), "Gates");
		xmldata.put(new XmlPath("RealTime/card_bin"), "492900");
		xmldata.put(new XmlPath("RealTime/card_last_4_digits"), "0006");

		if (!card_number.isEmpty())
			xmldata.put(new XmlPath("RealTime/card_number"), card_number);

		xmldata.put(new XmlPath("RealTime/card_expiry_date"), "0109");
		xmldata.put(new XmlPath("RealTime/amount"), "0999");
		xmldata.put(new XmlPath("RealTime/currency"), "GBP");
		xmldata.put(new XmlPath("RealTime/transaction_type"), "Auth");
		xmldata.put(new XmlPath("RealTime/authorisation_code"), "ABC000");
		xmldata.put(new XmlPath("RealTime/bank_response_code"), "ABC000");
		xmldata.put(new XmlPath("RealTime/cv2_response"), "2");
		xmldata.put(new XmlPath("RealTime/avs_address_response"), "2");
		xmldata.put(new XmlPath("RealTime/avs_postcode_response"), "2");

		xmldata.put(new XmlPath("RealTime/threed_secure_eci_indicator"), "6");
		xmldata.put(new XmlPath("RealTime/threed_secure_cavv_avv"),
				"AAABARR5kwAAAAAAAAAAAAAAAAA=");
		xmldata.put(new XmlPath("RealTime/home_telephone_number"), "2332323");
		xmldata.put(new XmlPath("RealTime/delivery_telephone_number"), "");
		xmldata.put(new XmlPath("RealTime/mobile_telephone_number"),
				"040200223");
		if (!customer_email_address.isEmpty())
			xmldata.put(new XmlPath("RealTime/customer_email_address"),
					customer_email_address);

		xmldata.put(new XmlPath("RealTime/ip_address"), "123.232.232.232");
		xmldata.put(new XmlPath("RealTime/billing_street_address_1"),
				"45 Kuran Place");
		if (!billing_postcode_zipcode.isEmpty())
			xmldata.put(new XmlPath("RealTime/billing_postcode_zipcode"),
					billing_postcode_zipcode);

		xmldata.put(new XmlPath("RealTime/billing_country"), "US");

		xmldata.put(new XmlPath("RealTime/delivery_street_address_1"),
				"45 Kuran Place");
		xmldata.put(new XmlPath("RealTime/billing_city"), "Brumbale");
		xmldata.put(new XmlPath("RealTime/delivery_postcode_zipcode"),
				"QLD 4321");
		xmldata.put(new XmlPath("RealTime/delivery_county"), "UK");

		xmldata.put(new XmlPath("RealTime/real_time_callback_format"), "HTTP");
		xmldata.put(new XmlPath("RealTime/real_time_callback"), "");
		xmldata.put(new XmlPath("RealTime/real_time_callback_options"), "1");
		xmldata.put(new XmlPath("RealTime/real_time_sha1"), "dsfgdgfdgdfrcvc");
		xmldata.put(new XmlPath("RealTime/merchant_attributes"), "");
		xmldata.put(new XmlPath("RealTime/merchant_attributes"), "");
		xmldata.put(new XmlPath("RealTime/http_header_fields"), "");
		xmldata.put(new XmlPath("RealTime/field_delimiter"), ">");
		if (!delivery_city.isEmpty()) {
			xmldata.put(new XmlPath("RealTime/delivery_city"), delivery_city);
		}
		
		xmldata.put(new XmlPath("RealTime/http_header_fields"), "");

		//String xmlmsg = composer.compose(xmldata);
		
		
		HashMap<XmlPath, String> recNodes = new HashMap<XmlPath, String>();
		recNodes.put(new XmlPath(""), "<products><product><name>DVD-CatsDogs</name><price>10.99</price></product><product><name>DVD-SuperMan</name><price>10.99</price></product></products>");
		String xmlmsg = composer.compose(xmldata,recNodes);
		
		return xmlmsg;
	}
}
