package com.jawise.serviceadapter.test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class XmlComposer {
	private static final Logger logger = Logger.getLogger(XmlComposer.class);

	public String compose(HashMap<XmlPath, String> xmldata,
			HashMap<XmlPath, String> recursiveNodes) throws Exception {
		Document doc = composeDoc(xmldata);

		DocumentBuilderFactory i = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = i.newDocumentBuilder();

		Set<XmlPath> keySet = recursiveNodes.keySet();
		for (XmlPath path : keySet) {
			String val = recursiveNodes.get(path);
			Document recursiveDoc = db.parse(new ByteArrayInputStream(val
					.getBytes()));
			appendNode(doc, path, recursiveDoc);

		}

		return serializeDoc(doc);
	}

	public String compose(HashMap<XmlPath, String> xmldata) throws Exception {

		Document doc = composeDoc(xmldata);
		return serializeDoc(doc);
	}

	private String serializeDoc(Document doc) throws IOException {
		StringWriter sw = new StringWriter();
		OutputFormat outputFormat = new OutputFormat();
		outputFormat.setIndent(4);
		outputFormat.setIndenting(true);
		XMLSerializer serial = new XMLSerializer(sw, outputFormat);
		serial.serialize(doc);
		return sw.toString();
	}

	private Document composeDoc(HashMap<XmlPath, String> xmldata)
			throws ParserConfigurationException, Exception {
		DocumentBuilderFactory i = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = i.newDocumentBuilder();
		Document doc = db.newDocument();

		Set<XmlPath> keySet = xmldata.keySet();
		for (XmlPath path : keySet) {
			String val = xmldata.get(path);
			logger.debug("trying to append value to " + path.getPath());
			appendValue(doc, path, val);
		}
		return doc;
	}

	private void appendNode(Document doc, XmlPath path, Document recursiveDoc)
			throws TransformerException {

		if (path.getPath().isEmpty()) {
			// add it root
			logger.debug("adding recursiveDoc to root");
			Node importNode = doc.importNode(recursiveDoc.getDocumentElement(),
					true);
			if (doc.getDocumentElement() != null) {
				doc.getDocumentElement().appendChild(importNode);
			} else {
				doc.appendChild(importNode);
			}
		} else {
			// add to selected node
			logger.debug("adding recursiveDoc to parent of path : "
					+ path.getPath());
			Node sidblingtNode = XPathAPI.selectSingleNode(doc, path.getPath());
			Node parentNode = sidblingtNode.getParentNode();
			if (parentNode == null) {
				parentNode = sidblingtNode;
			}
			Node importNode = doc.importNode(recursiveDoc.getDocumentElement(),
					true);
			parentNode.appendChild(importNode);

		}
	}

	private void appendValue(Document root, XmlPath xmlpath, String value)
			throws Exception {

		StringTokenizer pathTokens = new StringTokenizer(xmlpath.getPath(), "/");

		ArrayList<String> pathlist = new ArrayList<String>();
		while (pathTokens.hasMoreTokens()) {
			pathlist.add(pathTokens.nextToken());
		}

		Node parentNode = root;
		Element newElement = null;
		String finalPart = pathlist.get(pathlist.size() - 1);
		String currentPath = "";

		for (String pathPart : pathlist) {

			String elementName = null;
			String attributeName = null;

			// select the correct elementname or attribute name
			int pos = -1;
			if ((pos = pathPart.indexOf("[")) >= 0) {
				elementName = pathPart.substring(0, pos);
				currentPath += (currentPath.isEmpty() ? "" : "/") + pathPart;
			} else if ((pos = pathPart.indexOf("@")) >= 0) {
				elementName = pathPart.substring(0, pos);
				attributeName = pathPart.substring(pos + 1, pathPart.length());
				if (attributeName == null || attributeName.isEmpty()) {
					logger.error("invalid path  - " + xmlpath);
				}
				currentPath += (currentPath.isEmpty() ? "" : "/") + elementName;
			} else {
				elementName = pathPart;
				currentPath += (currentPath.isEmpty() ? "" : "/") + pathPart;
			}
			// Saxon XPath API
			// http://saxon.sourceforge.net/saxon7.5/api-guide.html
			Node currentNode = XPathAPI.selectSingleNode(root, currentPath);
			if (currentNode == null) {
				// does not exist so add
				newElement = root.createElement(elementName);
				parentNode.appendChild(newElement);
				parentNode = newElement;
			} else {
				parentNode = currentNode;
				newElement = (Element) currentNode;
			}

			if (attributeName != null) {
				newElement.setAttribute(attributeName, value);
			} else if (pathPart.equals(finalPart)) {
				Text textNode = root.createTextNode(value);
				newElement.appendChild(textNode);
			}
		}
	}
}
