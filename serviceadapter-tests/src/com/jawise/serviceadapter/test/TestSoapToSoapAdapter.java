package com.jawise.serviceadapter.test;


import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.test.svc.soap.bookshop.Book;
import com.jawise.serviceadapter.test.svc.soap.bookshop.BookFault;
import com.jawise.serviceadapter.test.svc.soap.bookshop.BookShopServiceClient;
import com.jawise.serviceadapter.test.svc.soap.bookshop.BookShopServicePortType;

public class TestSoapToSoapAdapter extends TestCase {

	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=BookShopService&messageconversion=bookfinderConversion";
	private static final String SERVICE_URL2 = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=BookShopService2&messageconversion=findBookToGetBookConverter";

	private static final Logger logger = Logger
			.getLogger(TestSoapToSoapAdapter.class);

	private TestFixture serviceAdapterTestFixture;

	protected void setUp() throws Exception {
		org.apache.log4j.BasicConfigurator.configure();		
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	public void testSoapCall() throws BookFault {

		BookShopServiceClient c = new BookShopServiceClient();
		BookShopServicePortType bookShopServicePortTypeLocalEndpoint = c
				.getBookShopServiceHttpPort("http://localhost:8080/serviceadapter-testservices/soa/BookShopService");

		Book findBook = bookShopServicePortTypeLocalEndpoint
				.findBook("1234-5678-6645-1");
		String value = findBook.getIsbn().getValue();
		System.out.println(value);
		assertTrue("1234-5678-6645-1".equals(value));

	}

	public void testServiceAdapterCall() throws BookFault {

		BookShopServiceClient c = new BookShopServiceClient();
		BookShopServicePortType bookShopServicePortTypeLocalEndpoint = c
				.getBookShopServiceHttpPort(SERVICE_URL);

		Book findBook = bookShopServicePortTypeLocalEndpoint
				.findBook("1234-5678-6645-1");
		String value = findBook.getIsbn().getValue();
		System.out.println(value);
		assertTrue("1234-5678-6645-1".equals(value));

	}

	public void testServiceAdapterCall2() throws BookFault {

		try {
			BookShopServiceClient c = new BookShopServiceClient();
			BookShopServicePortType bookShopServicePortTypeLocalEndpoint = c
					.getBookShopServiceHttpPort(SERVICE_URL2);

			Book findBook = bookShopServicePortTypeLocalEndpoint
					.findBook("1234-5678-6645-1");
			String value = findBook.getIsbn().getValue();
			fail();
			System.out.println(value);
			assertTrue("1234-5678-6645-1".equals(value));
		} catch (Exception e) {			
			e.printStackTrace();					
		}

	}	
	
	public void testRemoteServiceFault() throws BookFault {

		try {
			BookShopServiceClient c = new BookShopServiceClient();
			BookShopServicePortType bookShopServicePortTypeLocalEndpoint = c
					.getBookShopServiceHttpPort(SERVICE_URL);

			Book findBook = bookShopServicePortTypeLocalEndpoint
					.findBook("FAULT1");
			String value = findBook.getIsbn().getValue();			
			fail();
		} catch (Exception e) {			
			e.printStackTrace();
			assertTrue(e.getMessage().indexOf("invalid isbn vlaue") >=0);
		}

	}		
}
