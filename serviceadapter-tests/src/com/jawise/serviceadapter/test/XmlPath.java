package com.jawise.serviceadapter.test;

public class XmlPath {
	String path;

	public XmlPath(String path) {
		super();
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {

		if (path == null || path.isEmpty())
			return path;
		int pos = -1;
		if ((pos = path.lastIndexOf("@")) >= 0) {
			return removeArrayPrefix(path.substring(pos + 1));
		} else if ((pos = path.lastIndexOf("/")) >= 0) {
			return removeArrayPrefix(path.substring(pos + 1));
		} else {
			return removeArrayPrefix(path);
		}
	}

	public String removeArrayPrefix(String name) {
		if (name.indexOf("[]") >= 0) {
			name = name.replaceAll("\\x5b\\x5d", "");
		}
		return name;
	}
}
