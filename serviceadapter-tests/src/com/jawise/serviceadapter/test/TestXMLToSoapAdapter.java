package com.jawise.serviceadapter.test;

import java.io.ByteArrayInputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

public class TestXMLToSoapAdapter extends TestCase {

	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=sathyan&service=XMLBooksDataService&messageconversion=findConverter";

	private static final Logger logger = Logger
			.getLogger(TestXmlToNvpAdapter.class);

	protected TestFixture serviceAdapterTestFixture;

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	public void testSuccessfulCall() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();

			PostMethodWebRequest postReq = new PostMethodWebRequest(SERVICE_URL);
			String xmlmsg = buildXML("1234-5678-6645-1", "", "");
			postReq.setParameter("xml", xmlmsg);

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);

			assertNotSuccessResponse(parseResponse(res.getText()),
					"1234-5678-6645-1");
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	private void assertNotSuccessResponse(Document doc, String isbn)
			throws TransformerException {
		Node scoreNode = XPathAPI.selectSingleNode(doc, "bookresponse/isbn");
		assertNotNull(scoreNode);
		String isbnreturned = scoreNode.getFirstChild().getNodeValue();
		assertTrue(isbn.equals(isbnreturned));

	}

	private Document parseResponse(String text) throws Exception {

		DocumentBuilderFactory i = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = i.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(text.getBytes()));
		return doc;
	}

	private String buildXML(String isbn, String title, String author)
			throws Exception {

		XmlComposer composer = new XmlComposer();
		HashMap<XmlPath, String> xmldata = new HashMap<XmlPath, String>();
		xmldata.put(new XmlPath("bookrequest/isbn"), isbn);
		xmldata.put(new XmlPath("bookrequest/title"), "");
		xmldata.put(new XmlPath("bookrequest/author"), "");

		String xmlmsg = composer.compose(xmldata);

		return xmlmsg;
	}
}
