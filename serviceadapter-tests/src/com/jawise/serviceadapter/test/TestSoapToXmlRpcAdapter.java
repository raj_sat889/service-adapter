package com.jawise.serviceadapter.test;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.test.svc.soap.computerparts.ArrayOfPart;
import com.jawise.serviceadapter.test.svc.soap.computerparts.ObjectFactory;
import com.jawise.serviceadapter.test.svc.soap.computerparts.Order;
import com.jawise.serviceadapter.test.svc.soap.computerparts.Part;
import com.jawise.serviceadapter.test.svc.soap.computerparts.ZComputerPartsSupplierClient;
import com.jawise.serviceadapter.test.svc.soap.computerparts.ZComputerPartsSupplierPortType;

public class TestSoapToXmlRpcAdapter extends TestCase {
	protected TestFixture serviceAdapterTestFixture;
	private static final Logger logger = Logger.getLogger(TestSoapToXmlRpcAdapter.class);
	
	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=ZComputerPartsSupplier&messageconversion=ztoxPurchaseOrderConverter";

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	
	public void testSuccesfullSoapCallToZComputerPartsSupplier() {
		
		try {
			ZComputerPartsSupplierClient s = new ZComputerPartsSupplierClient();
			
			ZComputerPartsSupplierPortType computerPartsSupplierHttpPort = s.getZComputerPartsSupplierHttpPort();
			
			ObjectFactory f = new ObjectFactory();
			Order order = f.createOrder();
			
			
			order.setEmail(f.createOrderEmail("raj@gmail.com"));
			order.setTelephone(f.createOrderTelephone( "023232232"));
			order.setId(f.createOrderId("1"));
			order.setRequestdate(f.createOrderRequestdate("01/09/09"));
			order.setTotal(f.createOrderTotal("400.00"));
			
			Part[] parts = new Part[2];
			parts[0] = f.createPart(); 
			parts[0].setId(f.createPartId("P232")); 
			parts[0].setDetails(f.createPartDetails("WD hardrive"));
			parts[0].setQuantity(f.createPartQuantity("2"));
			parts[0].setAmout(f.createPartAmout("250"));
			parts[1] = f.createPart();
			parts[1].setId(f.createPartId("P132")); 
			parts[1].setDetails(f.createPartDetails("hardrive coller"));
			parts[1].setQuantity(f.createPartQuantity("2"));
			parts[1].setAmout(f.createPartAmout("250"));
			
			ArrayOfPart arrayOfPart = f.createArrayOfPart();
			arrayOfPart.getPart().add(parts[0]);
			arrayOfPart.getPart().add(parts[1]);
			order.setParts(f.createOrderParts(arrayOfPart)); 
			
			String r = computerPartsSupplierHttpPort.purchaseOrder("xcomputerparts", "sathyan", order);
						
			assertTrue(r != null);
			assertTrue("ID1231".equals(r));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
	}
	
	
	
public void testSuccesfullAdapterCall() {
		
		try {
			ZComputerPartsSupplierClient s = new ZComputerPartsSupplierClient();
			
			ZComputerPartsSupplierPortType computerPartsSupplierHttpPort = s.getZComputerPartsSupplierHttpPort(SERVICE_URL);
			
			ObjectFactory f = new ObjectFactory();
			Order order = f.createOrder();
			
			
			order.setEmail(f.createOrderEmail("raj@gmail.com"));
			order.setTelephone(f.createOrderTelephone( "023232232"));
			order.setId(f.createOrderId("1"));
			order.setRequestdate(f.createOrderRequestdate("01/09/09"));
			order.setTotal(f.createOrderTotal("400.00"));
			
			Part[] parts = new Part[2];
			parts[0] = f.createPart(); 
			parts[0].setId(f.createPartId("P232")); 
			parts[0].setDetails(f.createPartDetails("WD hardrive"));
			parts[0].setQuantity(f.createPartQuantity("2"));
			parts[0].setAmout(f.createPartAmout("250"));
			parts[1] = f.createPart();
			parts[1].setId(f.createPartId("P132")); 
			parts[1].setDetails(f.createPartDetails("hardrive coller"));
			parts[1].setQuantity(f.createPartQuantity("2"));
			parts[1].setAmout(f.createPartAmout("250"));
			
			ArrayOfPart arrayOfPart = f.createArrayOfPart();
			arrayOfPart.getPart().add(parts[0]);
			arrayOfPart.getPart().add(parts[1]);
			order.setParts(f.createOrderParts(arrayOfPart)); 
			
			String r = computerPartsSupplierHttpPort.purchaseOrder("xcomputerparts", "sathyan", order);
						
			assertTrue(r != null);
			assertTrue("ID1231".equals(r));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		} 	
	}

	public void testSuccesfullSoapCallToZComputerPartsSupplierAdapter() {
		/*try {
			
			ZComputerPartsSupplierLocator loc = new ZComputerPartsSupplierLocator();
			loc.setZComputerPartsSupplierHttpPortEndpointAddress(SERVICE_URL);
			ZComputerPartsSupplierPortType computerPartsSupplierHttpPort = loc.getZComputerPartsSupplierHttpPort();
			Order order = new Order();
			order.setEmail("raj@gmail.com");
			order.setTelephone("023232232");
			order.setId("1");
			order.setRequestdate("01/09/09");
			order.setTotal("400.00");
			
			Part[] parts = new Part[2];
			parts[0] = new Part(); 
			parts[0].setId("P232");
			parts[0].setDetails("WD hardrive");
			parts[0].setQuantity("2");
			parts[0].setAmout("250");
			parts[1] = new Part(); 
			parts[1].setId("P122");
			parts[1].setDetails("hardrive Cooler");
			parts[1].setQuantity("2");
			parts[1].setAmout("150");
			
			order.setParts(parts);
			
			String r = computerPartsSupplierHttpPort.purchaseOrder("xcomputerparts", "sathyan", order);
						
			assertTrue(r != null);
			assertTrue("ID1231".equals(r));
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
		*/
	}
	
	
	
	
	
}
