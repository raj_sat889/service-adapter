package com.jawise.serviceadapter.test;

import java.io.ByteArrayInputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

public class TestXmlToXmlAdapter extends TestCase{
	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=sathyan&service=FastFraudScreeningService&messageconversion=xmmsTotmfssConverter";

	private static final Logger logger = Logger
			.getLogger(TestXmlToNvpAdapter.class);

	public void testSuccessfulRequest() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	public void testInvalidRequest() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();

			String xml = postReq.getParameter("xml");			
			xml = xml.replaceFirst("</transaction_reference>", "<transaction_reference>");
			postReq.setParameter("xml", xml);
			
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("The following error occurred processing the request. error : The element type \"transaction_reference\" must be terminated by the matching end-tag \"</transaction_reference>\".")>=0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			ex.printStackTrace();
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}		

	private void assertNotSuccessResponse(Document doc)
			throws TransformerException {
		Node scoreNode = XPathAPI.selectSingleNode(doc,
				"RealTimeResponse/Score");
		assertNotNull(scoreNode);
		String score = scoreNode.getFirstChild().getNodeValue();
		assertTrue("69".equals(score));

		Node recommendationNode = XPathAPI.selectSingleNode(doc,
				"RealTimeResponse/recommendation");
		assertNotNull(recommendationNode);
		String recommendation = recommendationNode.getFirstChild()
				.getNodeValue();
		assertTrue("0".equals(recommendation));

	}

	private Document parseResponse(String text) throws Exception {

		DocumentBuilderFactory i = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = i.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(text.getBytes()));
		return doc;
	}

	private PostMethodWebRequest buildRequest() throws Exception {
		PostMethodWebRequest req = new PostMethodWebRequest(
				TestXmlToXmlAdapter.SERVICE_URL);
		String xmlmsg = buildXML();
		req.setParameter("xml", xmlmsg);
		return req;
	}

	private String buildXML() throws Exception {
		XmlComposer composer = new XmlComposer();
		HashMap<XmlPath, String> xmldata = new HashMap<XmlPath, String>();
		xmldata.put(new XmlPath("RealTime/transaction_reference"), new GUID()
				.toString());
		xmldata.put(new XmlPath("RealTime/transaction_date_time"),
				"010808 10:12:00");
		xmldata.put(new XmlPath("RealTime/aggregator_identifier"), "JAWISE");
		xmldata.put(new XmlPath("RealTime/merchant_identifier"),
				"2829827");
		xmldata.put(new XmlPath("RealTime/merchant_order_ref"), "ORDER121");
		xmldata.put(new XmlPath("RealTime/customer_ref"), "2829827");
		xmldata.put(new XmlPath("RealTime/sales_channel"), "E");
		xmldata.put(new XmlPath("RealTime/cardholder_title"), "Mr");
		xmldata.put(new XmlPath("RealTime/cardholder_first_name"), "Bill");
		xmldata.put(new XmlPath("RealTime/cardholder_surname"), "Gates");
		xmldata.put(new XmlPath("RealTime/card_bin"), "492900");
		xmldata.put(new XmlPath("RealTime/card_last_4_digits"), "0006");
		xmldata.put(new XmlPath("RealTime/card_number"), "4929000000006");
		xmldata.put(new XmlPath("RealTime/card_expiry_date"), "0109");
		xmldata.put(new XmlPath("RealTime/amount"), "0999");
		xmldata.put(new XmlPath("RealTime/currency"), "GBP");
		xmldata.put(new XmlPath("RealTime/transaction_type"), "Auth");
		xmldata.put(new XmlPath("RealTime/authorisation_code"), "ABC000");
		xmldata.put(new XmlPath("RealTime/bank_response_code"), "ABC000");
		xmldata.put(new XmlPath("RealTime/cv2_response"), "2");
		xmldata.put(new XmlPath("RealTime/avs_address_response"), "2");
		xmldata.put(new XmlPath("RealTime/avs_postcode_response"), "2");
		xmldata.put(new XmlPath("RealTime/threed_secure_eci_indicator"), "6");
		xmldata.put(new XmlPath("RealTime/threed_secure_cavv_avv"),
				"AAABARR5kwAAAAAAAAAAAAAAAAA=");
		xmldata.put(new XmlPath("RealTime/home_telephone_number"), "2332323");
		xmldata.put(new XmlPath("RealTime/delivery_telephone_number"), "");
		xmldata.put(new XmlPath("RealTime/mobile_telephone_number"),
				"040200223");
		xmldata.put(new XmlPath("RealTime/customer_email_address"),
				"bill@ms.com");
		xmldata.put(new XmlPath("RealTime/ip_address"), "123.232.232.232");
		xmldata.put(new XmlPath("RealTime/billing_street_address_1"),
				"45 Kuran Place");
		xmldata.put(new XmlPath("RealTime/billing_postcode_zipcode"),
				"QLD 4321");

		xmldata.put(new XmlPath("RealTime/billing_country"), "US");

		xmldata.put(new XmlPath("RealTime/delivery_street_address_1"),
				"45 Kuran Place");
		xmldata.put(new XmlPath("RealTime/billing_city"), "Brumbale");
		xmldata.put(new XmlPath("RealTime/delivery_postcode_zipcode"),
				"QLD 4321");
		xmldata.put(new XmlPath("RealTime/delivery_county"), "UK");

		xmldata.put(new XmlPath("RealTime/real_time_callback_format"), "HTTP");
		xmldata.put(new XmlPath("RealTime/real_time_callback"), "");
		xmldata.put(new XmlPath("RealTime/real_time_callback_options"), "1");
		xmldata.put(new XmlPath("RealTime/real_time_sha1"), "dsfgdgfdgdfrcvc");
		xmldata.put(new XmlPath("RealTime/merchant_attributes"), "");
		xmldata.put(new XmlPath("RealTime/merchant_attributes"), "");
		xmldata.put(new XmlPath("RealTime/http_header_fields"), "");
		xmldata.put(new XmlPath("RealTime/field_delimiter"), ">");
		String xmlmsg = composer.compose(xmldata);
		return xmlmsg;
	}

}
