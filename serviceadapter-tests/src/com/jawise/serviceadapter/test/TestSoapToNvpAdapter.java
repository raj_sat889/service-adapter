package com.jawise.serviceadapter.test;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.test.svc.soap.ArrayOfCar;
import com.jawise.serviceadapter.test.svc.soap.Car;
import com.jawise.serviceadapter.test.svc.soap.CarRentalServiceALocator;
import com.jawise.serviceadapter.test.svc.soap.CarRentalServiceAPortType;
import com.jawise.serviceadapter.test.svc.soap.cars.CarRentalServiceAClient;

public class TestSoapToNvpAdapter extends TestCase {

	protected TestFixture serviceAdapterTestFixture;
	private static final Logger logger = Logger
			.getLogger(TestNvpToSoapAdapter.class);

	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=CarRentalServiceA&messageconversion=listTosearchConverter";

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	/*
	 * xfire has a problem processing returned anytype. public void
	 * testSuccesfullCarRentalSoapCall() {
	 * 
	 * try { CarRentalServiceAClient c = new CarRentalServiceAClient();
	 * com.jawise.serviceadapter.test.svc.soap.cars.CarRentalServiceAPortType
	 * port = c .getCarRentalServiceAHttpPort();
	 * 
	 * ArrayOfCar cars = port.listRentalCars("Lodnon", "01/01/2009",
	 * "05/01/2009"); Object[] listRentalCars = cars.getCar().toArray();
	 * 
	 * assertTrue(listRentalCars != null); assertTrue("mazda".equals(((Car)
	 * listRentalCars[0]).getMake() .getValue())); } catch (Exception e) {
	 * e.printStackTrace(); fail(); } }
	 * 
	 * public void testSuccesfullCarRentalAdapterCall() {
	 * 
	 * try { CarRentalServiceAClient c = new CarRentalServiceAClient();
	 * com.jawise.serviceadapter.test.svc.soap.cars.CarRentalServiceAPortType
	 * port = c .getCarRentalServiceAHttpPort(SERVICE_URL);
	 * 
	 * ArrayOfCar cars = port.listRentalCars("Lodnon", "01/01/2009",
	 * "05/01/2009"); Object[] listRentalCars = cars.getCar().toArray();
	 * 
	 * assertTrue(listRentalCars != null); assertTrue("mazda".equals(((Car)
	 * listRentalCars[0]).getMake() .getValue())); } catch (Exception e) {
	 * e.printStackTrace(); fail(); } }
	 */

	public void testSuccesfullCarRentalSoapCall() {

		try {
			CarRentalServiceALocator loc = new CarRentalServiceALocator();
			CarRentalServiceAPortType port = loc.getCarRentalServiceAHttpPort();
			Car[] listRentalCars = port.listRentalCars("Lodnon", "01/01/2009",
					"05/01/2009");

			assertTrue(listRentalCars != null);
			assertTrue("Mazda".equals(listRentalCars[0].getMake()));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) { // TODOAuto-generated catch block
			e.printStackTrace();
		}

	}

	public void testSuccesfullCarRentalAdapterCall() {

		try {

			CarRentalServiceALocator loc = new CarRentalServiceALocator();
			loc.setCarRentalServiceAHttpPortEndpointAddress(SERVICE_URL);
			CarRentalServiceAPortType port = loc.getCarRentalServiceAHttpPort();
			Car[] listRentalCars = port.listRentalCars("Lodnon", "01/01/2009",
					"05/01/2009");

			assertTrue(listRentalCars != null);
			assertTrue("Mazda".equals(listRentalCars[0].getMake()));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) { // TODO catch block
			e.printStackTrace();
		}

	}

}
