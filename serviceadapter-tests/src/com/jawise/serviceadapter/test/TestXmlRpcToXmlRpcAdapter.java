package com.jawise.serviceadapter.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class TestXmlRpcToXmlRpcAdapter extends TestCase {

	private static final String HTTP_127_0_0_1_8090_XMLRPC = "http://127.0.0.1:8090/xmlrpc";
	protected TestFixture serviceAdapterTestFixture;
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(TestXmlRpcToXmlRpcAdapter.class);
	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=sathyan&service=YComputerPartsSupplier&messageconversion=purchaseOrderConversion";

	private static final String SERVICE_URL2 = "http://localhost:8080/serviceadapter/process/adapter?userid=sathyan&service=YComputerPartsSupplier&messageconversion=purchaseOrderStatusConversion";

	private static final String SERVICE_URL3 = "http://localhost:8080/serviceadapter/process/adapter?userid=sathyan&service=YComputerPartsSupplier&messageconversion=valueAddedTax";

	private static final String SERVICE_URL4 = "http://localhost:8080/serviceadapter/process/adapter?userid=sathyan&service=YComputerPartsSupplier&messageconversion=testValueAddedTax";

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	private XmlRpcClient getXmlRpcClient(String url)
			throws MalformedURLException {
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		config.setServerURL(new URL(url));
		XmlRpcClient client = new XmlRpcClient();
		client.setConfig(config);
		return client;
	}

	public void testHeartBeant() throws MalformedURLException, XmlRpcException {
		XmlRpcClient client = getXmlRpcClient(HTTP_127_0_0_1_8090_XMLRPC);
		Object[] params = new String[] { new String("") };
		String result = (String) client.execute(
				"YComputerPartsSupplier.checkHeartBeat", params);
		assertTrue("call success".equals(result));
	}

	// adapter data limits and validity for target

	@SuppressWarnings("unchecked")
	public void testEmptyArrayParameter() throws Exception {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
		ArrayList items = new ArrayList();
		Object[] itemsArray = new Object[items.size()];
		itemsArray = items.toArray(itemsArray);

		Object[] params = new Object[] { new String("xcomputerparts"),
				new String("password"), new String("PID_12345"),
				new String("raj@jawsie.com"), new String("010908"),
				new String("sathyavarathan"), new String("043232323"),
				itemsArray };
		String result = (String) client.execute(
				"YComputerPartsSupplier.processPurchaseOrder", params);
		assertTrue(result.length() == 38);
	}

	@SuppressWarnings("unchecked")
	public void testEmptyParameter() throws Exception {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			ArrayList items = new ArrayList();
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), new String("PID_12345"),
					new String("raj@jawsie.com"), new String("010908"),
					new String(""), new String("043232323"), itemsArray };

			String result = (String) client.execute(
					"YComputerPartsSupplier.processPurchaseOrder", params);
			assertTrue(result.length() == 38);
		} catch (XmlRpcException e) {
			assertTrue(e
					.getMessage()
					.indexOf(
							"message conversion exception, messages part contact not optional") >= 0);
		}

	}

	@SuppressWarnings("unchecked")
	public void testWrongTypeParameter() throws Exception {
		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			ArrayList items = new ArrayList();
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), new String("PID_12345"),
					new String("raj@jawsie.com"), new String("010908"),
					new Integer("5"), new String("043232323"), itemsArray };
			client.execute("YComputerPartsSupplier.processPurchaseOrder",
					params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
					"Parameter contact not correct type") >= 0);
		}
	}

	@SuppressWarnings("unchecked")
	public void testInvalidMethodName() throws Exception {
		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL2);

			Object[] params = new Object[] { new String("PID_12345") };
			client.execute("YComputerPartsSupplier.getPurchaseOrderStatus",
					params);
			fail();

		} catch (XmlRpcException e) {
			// TODO even if calling the wrong method since the name is already
			// mapped it goes to the correct
			// method.
			assertTrue(e.getMessage().equals(
					"No such handler: XComputerPartsSupplier.checkOrderStatus"));

		}
	}

	@SuppressWarnings("unchecked")
	public void testMethodSignature() throws Exception {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			ArrayList items = new ArrayList();
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Object[] params = new Object[] {
					new String("xcomputerparts"),
					new String("password"),
					new String("PID_12345"),
					new String("raj@jawsie.com"),
					new String("010908"),
					new String(
							"rajsathya12345678901234567890123456789012345678901234567890"),
					itemsArray };
			String result = (String) client.execute(
					"YComputerPartsSupplier.processPurchaseOrder", params);
			assertTrue(result.length() == 38);
		} catch (XmlRpcException e) {
			assertTrue(e
					.getMessage()
					.indexOf(
							"Parameter count mismatch. The operation requires 8 parameters") >= 0);
		}
	}

	@SuppressWarnings("unchecked")
	public void testNoValidationAdapteeParameters() {
		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL);
			ArrayList items = new ArrayList();
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), new String("PID_12345"),
					new String("raj@jawsie.com"), new String("010908"),
					new String("sathyavarathan"), new String("043232323"),
					itemsArray };
			String result = (String) client.execute(
					"YComputerPartsSupplier.processPurchaseOrder", params);
			assertTrue(result.length() == 38);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	// adapter transformation and script

	public void testCallWithInvalidScript() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL4);
			Object[] params = new Object[] { new Double(12.23),
					new String("UK"), new String("SIMPLE") };
			String result = (String) client.execute(
					"YComputerPartsSupplier.cacualteValueAddedTax", params);
			assertTrue(result.length() == 38);
		} catch (XmlRpcException e) {
			assertTrue(e.getMessage().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(e.getMessage().indexOf("Parse error") >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	public void testCallWithArithemeticExceptionThowingScript() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL3);
			Object[] params = new Object[] { new Double(12.23),
					new String("error"), new String("SIMPLE") };
			client.execute(
					"YComputerPartsSupplier.cacualteValueAddedTax", params);
			fail();
		} catch (XmlRpcException e) {
			e.printStackTrace();
			assertTrue(e.getMessage().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(e.getMessage().indexOf("Arithemetic Exception") >= 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testCallWithNullExceptionThowingScript() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL3);
			Object[] params = new Object[] { new Double(12.23),
					new String("throw error"), new String("SIMPLE") };
			client.execute(
					"YComputerPartsSupplier.cacualteValueAddedTax", params);
			fail();
		} catch (XmlRpcException e) {
			e.printStackTrace();
			assertTrue(e.getMessage().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(e.getMessage().indexOf("TargetError") >= 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// adaptee service interface

	// adaptee service behaviour

	public void testCacualteValueAddedTax() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL3);
			Object[] params = new Object[] { new Double(12.23),
					new String("UK"), new String("SIMPLE") };
			Double result = (Double) client.execute(
					"YComputerPartsSupplier.cacualteValueAddedTax", params);
			assertTrue(result == (12.23 * 0.10));
		} catch (XmlRpcException e) {
			e.printStackTrace();
			fail();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	public void testYProcessPurchaseOrder() throws MalformedURLException,
			XmlRpcException {
		String result = callProcessPurchaseOrder(HTTP_127_0_0_1_8090_XMLRPC);
		assertTrue(result.length() == 38);
	}

	public void testXsubmitPurchaseOrder() throws MalformedURLException,
			XmlRpcException {
		String result = callProcessPurchaseOrder(SERVICE_URL);
		assertTrue(result.length() == 38);
	}

	@SuppressWarnings("unchecked")
	private String callProcessPurchaseOrder(String url)
			throws MalformedURLException, XmlRpcException {
		XmlRpcClient client = getXmlRpcClient(url);

		ArrayList items = new ArrayList();
		addItems(items);

		Object[] itemsArray = new Object[items.size()];
		itemsArray = items.toArray(itemsArray);

		Object[] params = new Object[] { new String("xcomputerparts"),
				new String("password"), new String("PID_12345"),
				new String("raj@jawsie.com"), new String("010908"),
				new String("sathyavarathan"), new String("043232323"),
				itemsArray };
		String result = (String) client.execute(
				"YComputerPartsSupplier.processPurchaseOrder", params);
		return result;

	}

	public void testAdapteeValidationError1() {
		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL3);
			Object[] params = new Object[] { new Double(-0.23),
					new String("UK"), new String("SIMPLE") };
			@SuppressWarnings("unused")
			Double result = (Double) client.execute(
					"YComputerPartsSupplier.cacualteValueAddedTax", params);
			// assertTrue(result == (12.23 * 0.10));
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e.getMessage().indexOf("total value must be > 0") >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	public void testAdapteeValidationError2() {
		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL3);
			Object[] params = new Object[] { new Double(233.23),
					new String("Australia"), new String("SIMPLE") };
			@SuppressWarnings("unused")
			Double result = (Double) client.execute(
					"YComputerPartsSupplier.cacualteValueAddedTax", params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e.getMessage().indexOf("country format invalid") >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	public void testCallTimeout() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL3);
			Object[] params = new Object[] { new Double(233.23),
					new String("TI"), new String("SIMPLE") };
			@SuppressWarnings("unused")
			Double result = (Double) client.execute(
					"YComputerPartsSupplier.cacualteValueAddedTax", params);
			fail();
		} catch (XmlRpcException e) {
			e.printStackTrace();
			assertTrue(e
					.getMessage()
					.indexOf(
							"message communication error occurred. error : Read timed out") >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	// *****************************************************************************************
	// service adapter invocation
	// *****************************************************************************************

	public void testServiceAdapterInvocationWrongService() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL.replaceAll(
					"YComputerPartsSupplier", "TComputerPartsSupplier"));
			Object[] params = getParameters();
			client.execute(
					"YComputerPartsSupplier.processPurchaseOrder", params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e.getMessage().indexOf(
					"service requested is not found in the registry.") >= 0);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}

	}

	public void testServiceAdapterInvocationWrongConverter() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL.replaceAll(
					"purchaseOrderConversion", "ccpurchaseOrderConversion"));
			Object[] params = getParameters();
			client.execute(
					"YComputerPartsSupplier.processPurchaseOrder", params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e
					.getMessage()
					.indexOf(
							"message conversion requested is not found in the registry.") >= 0);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}

	}

	public void testServiceAdapterInvocationWithWrongUser() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL.replaceAll(
					"sathyan", "kuthiyan"));
			Object[] params = getParameters();
			client.execute(
					"YComputerPartsSupplier.processPurchaseOrder", params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e.getMessage().indexOf("userid is invalid") >= 0);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	private Object[] getParameters() {

		ArrayList items = new ArrayList();
		addItems(items);

		Object[] itemsArray = new Object[items.size()];
		itemsArray = items.toArray(itemsArray);

		Object[] params = new Object[] { new String("xcomputerparts"),
				new String("password"), new String("PID_12345"),
				new String("raj@jawsie.com"), new String("010908"),
				new String("sathyavarathan"), new String("043232323"),
				itemsArray };
		return params;
	}

	@SuppressWarnings("unchecked")
	private void addItems(ArrayList items) {
		for (int i = 0; i < 3; i++) {
			Map item = new HashMap();
			item.put("no", "" + (i + 1));
			item.put("description", "product" + (i + 1) + " is a dvd");
			item.put("count", 1);
			item.put("price", "" + ((i + 1) * 0.5));
			items.add(item);
		}
	}
}