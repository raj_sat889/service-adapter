package com.jawise.serviceadapter.test;

import java.util.HashMap;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

public class TestXmlComposer extends TestCase {

	protected TestFixture serviceAdapterTestFixture;
	private static final Logger logger = Logger
			.getLogger(TestXmlComposer.class);

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	public void testXMLForm() {
		try {
			XmlComposer c = new XmlComposer();
			HashMap<XmlPath, String> xmldata = new HashMap<XmlPath, String>();
			xmldata.put(new XmlPath("RealTime/transaction_reference"), "XX1");
			xmldata.put(new XmlPath("RealTime/transaction_date_time"),
					"010808 10:12:00");
			xmldata
					.put(new XmlPath("RealTime/aggregator_identifier"),
							"JAWISE");
			xmldata.put(new XmlPath("RealTime/merchant_identifier_identifier"),
					"2829827");
			xmldata.put(new XmlPath("RealTime/merchant_order_ref"), "ORDER121");
			xmldata.put(new XmlPath("RealTime/customer_ref"), "2829827");
			xmldata.put(new XmlPath("RealTime/products/catagor/name"),
					"AudioAndViedio");
			xmldata
					.put(new XmlPath("RealTime/products/product[1]/name"),
							"DVD");
			xmldata.put(new XmlPath("RealTime/products/product[1]/code"),
					"DVDCODE123343");
			xmldata.put(new XmlPath("RealTime/products/product[2]/code"),
					"BRCODE9909");
			xmldata.put(new XmlPath("RealTime/products/product[2]/name"),
					"BLURAY");
			xmldata.put(new XmlPath(
					"RealTime/products/product[2]/imdb/yearofrelease"), "1998");
			xmldata.put(
					new XmlPath("RealTime/products/product[2]/imdb/direct"),
					"Van Hasn Jr");
			xmldata.put(new XmlPath("RealTime/products@count"), "2");

			for (int i = 0; i < 5; i++) {
				long currentTimeMillis = System.currentTimeMillis();
				String data = c.compose(xmldata);
				logger.debug("duration "
						+ (System.currentTimeMillis() - currentTimeMillis));
				logger.debug(data);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			fail();
		}
	}

	public void testXMLFormtOfRecursiveNodes() {
		try {
			XmlComposer c = new XmlComposer();
			HashMap<XmlPath, String> xmldata = new HashMap<XmlPath, String>();
			xmldata.put(new XmlPath("RealTime/transaction_reference"), "XX1");
			xmldata.put(new XmlPath("RealTime/transaction_date_time"),
					"010808 10:12:00");
			xmldata
					.put(new XmlPath("RealTime/aggregator_identifier"),
							"JAWISE");
			xmldata.put(new XmlPath("RealTime/merchant_identifier_identifier"),
					"2829827");
			xmldata.put(new XmlPath("RealTime/merchant_order_ref"), "ORDER121");
			xmldata.put(new XmlPath("RealTime/customer_ref"), "2829827");				
			
			HashMap<XmlPath, String> recNodes = new HashMap<XmlPath, String>();
			recNodes.put(new XmlPath("RealTime/customer_ref"), "<products><product><name>DVD-CatsDogs</name><price>10.99</price></product><product><name>DVD-CatsDogs</name><price>10.99</price></product></products>");
			String data = c.compose(xmldata,recNodes);
			logger.debug(data);
			
			recNodes.clear();
			recNodes.put(new XmlPath(""), "<products><product><name>DVD-CatsDogs</name><price>10.99</price></product><product><name>DVD-CatsDogs</name><price>10.99</price></product></products>");
			data = c.compose(xmldata,recNodes);
			logger.debug(data);			

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			fail();
		}
	}
}
