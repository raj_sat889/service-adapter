package com.jawise.serviceadapter.test;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

public class TestNvpToNvpAdapter extends TestCase {

	protected TestFixture serviceAdapterTestFixture;
	private static final Logger logger = Logger
			.getLogger(TestNvpToNvpAdapter.class);
	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=ProtxDirectPaymentService&messageconversion=PayPalToProtxMessageConversion";
	private static final String SERVICE_URL1 = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=ProtxDirectPaymentService&messageconversion=testScriptConverter";

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	// adapter interface tests

	public void testInvalidNVPRequest() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			WebResponse res = null;
			try {
				res = wc.getResponse(SERVICE_URL + "&PaymentType&CardType");
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("CardType not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testAdapterParameterNoValidations() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("VendorTxCode");
			postReq.setParameter("VendorTxCode", new GUID().toString()
					+ "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testSuccessfulRequestWithMissingOptionalParmetres() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("StartDate");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testSuccessfulRequestWithOverridingConstant() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.setParameter("DoDirectPayment", "noop");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testMissingNoneOptionalParmeter1() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("CardNumber");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part CardNumber not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testMissingNoneOptionalParmeter2() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("CardNumber");
			postReq.setParameter("CardNumber", "");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part CardNumber not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	// adapter transformation and script

	public void testInvalidRecursiveValues() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq
					.setParameter(
							"Basket",
							"7:Pioneer NSDV99 DVD-Surround Sound System:1:�424.68:�74.32:�499.00: �499.00:Donnie Darko Director�s Cut:3:�11.91:�2.08:�13.99:�41.97: Finding Nemo:2:�11.05:�1.94:�12.99:�25.98: Delivery:---:---:---:---:�4.99");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithInvalidScript() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest req = new PostMethodWebRequest(
					TestNvpToNvpAdapter.SERVICE_URL1);
			req.setParameter("param1", "test");

			WebResponse res = null;
			try {
				res = wc.getResponse(req);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("Parse error") >= 0);

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithExceptionThrownInScript() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("ClientIPAddress");
			postReq.setParameter("ClientIPAddress", "error");
			postReq
					.setParameter(
							"Basket",
							"7:Pioneer NSDV99 DVD-Surround Sound System:1:�424.68:�74.32:�499.00: �499.00:Donnie Darko Director�s Cut:3:�11.91:�2.08:�13.99:�41.97: Finding Nemo:2:�11.05:�1.94:�12.99:�25.98: Delivery:---:---:---:---:�4.99");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("Arithemetic Exception") >= 0);

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	/*
	 * public void testCallWithInvalidTranformationNode(){
	 *  }
	 */

	// adaptee interface tests
	/*void testMethodSignature() {
		same as testMissingNoneOptionalParmeter1();
	}*/

	/*
	 * not applicable to NVP messages void testParamterType(){ }
	 */

	// adaptee service behavior tests
	public void testSuccessfulRequestWithBasket() {

		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq
					.setParameter(
							"Basket",
							"4:Pioneer NSDV99 DVD-Surround Sound System:1:�424.68:�74.32:�499.00: �499.00:Donnie Darko Director�s Cut:3:�11.91:�2.08:�13.99:�41.97: Finding Nemo:2:�11.05:�1.94:�12.99:�25.98: Delivery:---:---:---:---:�4.99");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testSuccessfulRequest() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceValidationFailure1() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("ExpiryDate");
			postReq.setParameter("ExpiryDate", "0101");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("Status=ERROR") >= 0);
			assertTrue(res.getText().indexOf(
					"StatusDetail=10562 : Invalid Data") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}

	}

	public void testServiceValidationFailure2() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("CardNumber");
			postReq.setParameter("CardNumber", "4988643962977060");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("Status=ERROR") >= 0);
			assertTrue(res.getText().indexOf(
					"StatusDetail=10527 : Invalid Data This transaction cannot be processed. Please enter a valid credit card number and type") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}

	}

	/*public void testTimeouts() {
	}*/

	/*public void testServiceUnavilability() {
	}*/

	// service adapter invocation

	public void testServiceAdapterInvocationWrongService() {
		try {

			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"ProtxDirectPaymentService", "TComputerPartsSupplier"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"service requested is not found in the registry.") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceAdapterInvocationWrongConverter() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"PayPalToProtxMessageConversion", "chaseOrderConverter"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion requested is not found in the registry.") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceAdapterInvocationWithWrongUser() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"raj", "sdfsdf"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("userid is invalid") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	private void assertNotSuccessResponse(Map<String, String> response) {
		assertTrue(response.get("Status").length() > 0);
		assertTrue("OK".equals(response.get("Status")));
		assertTrue(response.get("StatusDetail").length() > 0);
		assertTrue(response.get("TxAuthNo").length() > 0);
		assertTrue(response.get("VPSTxId").length() > 0);
		assertTrue(response.get("SecurityKey").length() == 10);
	}

	protected Map<String, String> parseResponse(String text) {

		Map<String, String> res = new HashMap<String, String>();
		StringTokenizer tokens = new StringTokenizer(text, "\r\n");
		while (tokens.hasMoreTokens()) {
			String t = tokens.nextToken();
			int midPos = t.indexOf("=");
			String key = t.substring(0, midPos);
			String val = t.substring(midPos + 1, t.length());
			res.put(key, val);
		}
		return res;
	}

	private PostMethodWebRequest buildRequest() {
		return buildRequest(TestNvpToNvpAdapter.SERVICE_URL);
	}

	private PostMethodWebRequest buildRequest(String url) {
		PostMethodWebRequest req = new PostMethodWebRequest(url);
		req.setParameter("VPSProtocol", "2.00");
		req.setParameter("PaymentType", "PAYMENT");
		req.setParameter("VendorName", "testvendor");
		req.setParameter("VendorTxCode", new GUID().toString());
		req.setParameter("Amount", "4560");
		req.setParameter("Currency", "GBP");
		req.setParameter("Description", "dvd movie purchace at 25% discount");
		req.setParameter("CardHolder", "Sathyan");
		req.setParameter("CardNumber", "4988643962977067");
		req.setParameter("ExpiryDate", "0919");
		req.setParameter("CardType", "VISA");
		req.setParameter("StartDate", "0101");
		return req;
	}

}
