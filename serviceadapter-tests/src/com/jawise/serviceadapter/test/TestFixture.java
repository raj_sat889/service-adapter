package com.jawise.serviceadapter.test;

import java.io.File;

import org.apache.log4j.Logger;

import com.meterware.httpunit.HttpUnitOptions;

public class TestFixture {

	String log4jfile = "E:/sathysoft/workspace/serviceadapter/WebContent/WEB-INF/log4j.properties";
	private static boolean configcompleted = false;
	//private static boolean serverStarted;
	//private XmlRpcServer xmlRpcServer;
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(TestFixture.class);

	public TestFixture(Object o) {
	}

	public void setUp() throws Exception {
		if (!configcompleted) {
			org.apache.log4j.BasicConfigurator.configure();
			//org.apache.log4j.PropertyConfigurator.configure(log4jfile);
			configcompleted = true;
		}
		/*
		if(!serverStarted) {
			xmlRpcServer = new XmlRpcServer();
			xmlRpcServer.start();
			serverStarted = true;
			
		}
		*/
		setupSSL();
		HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
		HttpUnitOptions.setExceptionsThrownOnScriptError(false);
	}

	public void tearDown() {
		HttpUnitOptions.reset();
	}

	@SuppressWarnings({ "deprecation", "deprecation" })
	public void setupSSL() {
		// System.setProperty("java.protocol.handler.pkgs","javax.net.ssl");
		// String truststr = System.getProperty("javax.net.ssl.trustStore");

		com.sun.net.ssl.HostnameVerifier hv = new com.sun.net.ssl.HostnameVerifier() {
			public boolean verify(String urlHostname, String certHostname) {
				System.out
						.println("WARNING: Hostname is not matched for cert.");
				return true;
			}
		};
		com.sun.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(hv);

		String filename = System.getProperty("java.home")
				+ "/lib/security/cacerts".replace('/', File.separatorChar);

		String password = "changeit";
		System.setProperty("javax.net.ssl.trustStore", filename);
		System.setProperty("javax.net.ssl.trustStorePassword", password);
		javax.net.ssl.HostnameVerifier hv2 = new javax.net.ssl.HostnameVerifier() {
			public boolean verify(String urlHostname,
					javax.net.ssl.SSLSession ses) {
				System.out
						.println("WARNING: Hostname is not matched for cert.");
				return true;
			}
		};

		javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(hv2);

	}
}
