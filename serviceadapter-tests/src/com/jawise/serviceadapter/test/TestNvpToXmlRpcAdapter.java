package com.jawise.serviceadapter.test;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

/**
 * 
 * The possible test area's 
 * adapter interface tests 1) adapter data limits and validity for target - (NOT
 * REQUIRED) 1.4) optional parameter 1.5) request message format i.e invalid xml
 * or nvp request 1.6) no validation for parameter data limits and validity for
 * source 1.7) try overiding constant value
 * 
 * 2) adapter transformation and script 2.1) conversion of empty and null values
 * 2.2) invalid array's 2.3) invalid script 2.4) illegal script 2.5) exception
 * throwing script 2.8) data overflow script 2.9) invalid transformation node
 * path 2.10) recursive transformation 2.11) recursive transformation with
 * errors
 * 
 * 3) adaptee  interface tests 3.1) method signature 3.2) parameter type 3.3)
 * parameter data limits and validity for target (NOT REQUIRED) 3.4) optional
 * parameter 3.5) response message format i.e invalid xml or nvp request 3.6) no
 * validation for parameter data limits and validity for source
 * 
 * 4) adaptee service behaviour 4.1) internal errors 4.2) validation errors 4.3)
 * timeouts 4.4) service unavailability 4.5) successfull test
 * 
 * 5) service adapter invocation 4.1) invalid service adapter call with wrong
 * service 4.2) invalid service adapter call with wrong message converter 4.3)
 * invalid service adapter call with invalid ip address 4.4) invalid service
 * adapter call with wrong userid/password 5.5) multiple requests
 * 
 */

public class TestNvpToXmlRpcAdapter extends TestCase {
	protected TestFixture serviceAdapterTestFixture;
	private static final Logger logger = Logger
			.getLogger(TestNvpToXmlRpcAdapter.class);
	private static final String SERVICE_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=WComputerPartsSupplier&messageconversion=PurchaseOrderConverter";
	private static final String VAT_SERVICE1_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=WComputerPartsSupplier&messageconversion=VatConverter";
	private static final String VAT_SERVICE2_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=WComputerPartsSupplier&messageconversion=VatConverterTypeError";
	private static final String VAT_SERVICE3_URL = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=WComputerPartsSupplier&messageconversion=VatConverterScriptTest";

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;

	}

	// *****************************************************************************************
	/* adapter interface teststs */
	// *****************************************************************************************
	public void testMethodSignatur() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL);
			postReq.removeParameter("orderid");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part orderid not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testNoneOptionalParameter() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL);
			postReq.removeParameter("emailaddress");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part emailaddress not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testOptionalParameter() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE1_URL);
			postReq.setParameter("taxcatagory", "none");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			Map<String, String> parseResponse = parseResponse(res.getText());
			assertTrue(parseResponse.get("taxvalue") != null);
			assertTrue(parseResponse.get("taxvalue").length() > 0);
			assertTrue(parseResponse.get("taxvalue").equals("14.535"));

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testNotUsedOptionalParameter() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE1_URL);
			postReq.setParameter("businesslocation", "home");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			Map<String, String> parseResponse = parseResponse(res.getText());
			assertTrue(parseResponse.get("taxvalue") != null);
			assertTrue(parseResponse.get("taxvalue").length() > 0);
			assertTrue(parseResponse.get("taxvalue").equals("14.535"));

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testSuccessfulCallWithoutSourceParamterValidation() {
		// do not validate long or invalid adapter parameters
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL);
			postReq.removeParameter("orderid");
			postReq
					.setParameter("orderid",
							"123456789012345678901234567890123456789012345678901234567890");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			Map<String, String> parseResponse = parseResponse(res.getText());
			assertTrue(parseResponse.get("OrderSubmissionID") != null);
			assertTrue(parseResponse.get("OrderSubmissionID").length() == 38);
			parseResponse.get("OrderSubmissionID");
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testInvalidRequestMessageFormat() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			String postReq = buildRequestString(SERVICE_URL);
			postReq = postReq.replaceAll("&", "&");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("message conversion exception") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	private String buildRequestString(String serviceUrl) {
		String query = serviceUrl
				+ "&orderid=ORDER1121emailaddress=pet@pan.comdateoforder=010807&contactname=methiew&totalitems=3&itemdescription0=disk drive&itemquantity0=2&amount0=50.00&itemdescription1=ram&itemquantity1=1&amount1=20.00&itemdescription2=cpu AMD Pantha 8393&itemquantity2=1&amount2=220.00";
		return query;
	}

	// *****************************************************************************************
	/* test transformation and scripts */
	// *****************************************************************************************

	public void testCallWithInvalidScript() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE3_URL);
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("Parse error") >= 0);

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithArithemeticExceptionThowingScript() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE2_URL);

			postReq.removeParameter("country");
			postReq.setParameter("country", "error");
			WebResponse res = null;
			try {

				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("Arithemetic Exception") >= 0);

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithNullExceptionThowingScript() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE2_URL);

			postReq.removeParameter("country");
			postReq.setParameter("country", "throw error");
			WebResponse res = null;
			try {

				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("TargetError") >= 0);

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithRecurciverError() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL);
			postReq.removeParameter("totalitems");
			postReq.setParameter("totalitems", "5");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithRecurciveElements() {
		testSuccessfulCallPurchaceOrder();
	}

	public void testCallWithEmptryParameter() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL);
			postReq.removeParameter("emailaddress");
			postReq.setParameter("emailaddress", "");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part emailaddress not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}
    /*
	public void testInvalidTransformationNodePath() {
		// not applicable
	}*/

	public void testTypeConverionScriptError() {
		// test automatic type conversion ? should that use scripted conversion
		// ?
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE1_URL);
			postReq.removeParameter("totalvalue");
			// double
			postReq.setParameter("totalvalue", "3894 GBP");

			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	// *****************************************************************************************
	// adaptee service interface tests
	// *****************************************************************************************

	public void testSuccessfulVATTestTypeError() {
		// test automacti type conversion ? should that use scripted conversion
		// ?
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE2_URL);
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("No method matching arguments") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testAdapterOptionalParamter() {
		// TODO there is proble her, when u have two optional parameter's the
		// order bcome important 2nd one is provided
		// the first one must be provided as well

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE1_URL);
			postReq.removeParameter("country");
			postReq.setParameter("taxcatagory", "SOLE TRADER");
			postReq.setParameter("businesslocation", "HOME");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part country not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	/**
	 * public void testInternalError() { //not applicable same as validation
	 * errors }
	 */
	// *****************************************************************************************
	// adaptee service behaviour tests
	// *****************************************************************************************
	public void testSuccessfulCallPurchaceOrder() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL);
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			Map<String, String> parseResponse = parseResponse(res.getText());
			assertTrue(parseResponse.get("OrderSubmissionID") != null);
			assertTrue(parseResponse.get("OrderSubmissionID").length() == 38);
			parseResponse.get("OrderSubmissionID");
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testAdapteeValidationError1() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE1_URL);
			postReq.removeParameter("totalvalue");
			postReq.setParameter("totalvalue", "0");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			// decoding xml encoded values
			assertTrue(res.getText().indexOf("total value must be > 0") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testAdapteeValidationError2() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE1_URL);
			postReq.removeParameter("country");
			postReq.setParameter("country", "Australia");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("country code format invalid") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallTimeout() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);

			WebConversation wc = new WebConversation();
			wc.set_connectTimeout(1);
			wc.set_readTimeout(1);
			PostMethodWebRequest postReq = buildVatRequest(VAT_SERVICE1_URL);
			postReq.removeParameter("country");
			postReq.setParameter("country", "TI");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message communication error occurred. error : Read timed out") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}
	

	public void testEcondedResponse() {
	}

	// *****************************************************************************************
	// service adapter invocation
	// *****************************************************************************************

	public void testServiceAdapterInvocationWrongService() {
		try {

			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"WComputerPartsSupplier", "TComputerPartsSupplier"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"service requested is not found in the registry.") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceAdapterInvocationWrongConverter() {

		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"PurchaseOrderConverter", "chaseOrderConverter"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion requested is not found in the registry.") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testServiceAdapterInvocationWithWrongUser() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest(SERVICE_URL.replaceAll(
					"raj", "sdfsdf"));
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("userid is invalid") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	protected Map<String, String> parseResponse(String text) {

		Map<String, String> res = new HashMap<String, String>();
		StringTokenizer tokens = new StringTokenizer(text, "&");
		while (tokens.hasMoreTokens()) {
			String t = tokens.nextToken();
			if (!t.isEmpty()) {
				try {
					int midPos = t.indexOf("=");
					String key = t.substring(0, midPos);
					String val = t.substring(midPos + 1, t.length());
					res.put(key, val);
				} catch (RuntimeException e) {
				}
			}
		}
		return res;
	}

	private PostMethodWebRequest buildVatRequest(String url) {
		PostMethodWebRequest req = new PostMethodWebRequest(url);
		req.setParameter("totalvalue", "145.35");
		req.setParameter("country", "US");
		return req;
	}

	private PostMethodWebRequest buildRequest(String url) {
		PostMethodWebRequest req = new PostMethodWebRequest(url);
		req.setParameter("orderid", "ORDER1121");
		req.setParameter("emailaddress", "pet@pan.com");
		req.setParameter("dateoforder", "010807");
		req.setParameter("contactname", "methiew");
		req.setParameter("totalitems", "3");
		req.setParameter("itemdescription0", "disk drive");
		req.setParameter("itemquantity0", "2");
		req.setParameter("amount0", "50.00");
		req.setParameter("itemdescription1", "ram");
		req.setParameter("itemquantity1", "1");
		req.setParameter("amount1", "20.00");
		req.setParameter("itemdescription2", "cpu AMD Pantha 8393");
		req.setParameter("itemquantity2", "1");
		req.setParameter("amount2", "220.00");
		return req;
	}

}
