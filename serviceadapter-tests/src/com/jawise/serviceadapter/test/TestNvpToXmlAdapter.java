package com.jawise.serviceadapter.test;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

import junit.framework.TestCase;

/**
 * 
 * The possible test area's adapter interface tests 1) adapter data limits and
 * validity for target - (NOT REQUIRED) 1.4) optional parameter 1.5) request
 * message format i.e invalid xml or nvp request 1.6) no validation for
 * parameter data limits and validity for source 1.7) try overiding constant
 * value
 * 
 * 2) adapter transformation and script 2.1) conversion of empty and null values
 * 2.2) invalid array's 2.3) invalid script 2.4) illegal script 2.5) exception
 * throwing script 2.8) data overflow script 2.9) invalid transformation node
 * path 2.10) recursive transformation 2.11) recursive transformation with
 * errors
 * 
 * 3) adaptee interface tests 3.1) method signature 3.2) parameter type 3.3)
 * parameter data limits and validity for target (NOT REQUIRED) 3.4) optional
 * parameter 3.5) response message format i.e invalid xml or nvp request 3.6) no
 * validation for parameter data limits and validity for source
 * 
 * 4) adaptee service behaviour 4.1) internal errors 4.2) validation errors 4.3)
 * timeouts 4.4) service unavailability 4.5) successfull test
 * 
 * 5) service adapter invocation 4.1) invalid service adapter call with wrong
 * service 4.2) invalid service adapter call with wrong message converter 4.3)
 * invalid service adapter call with invalid ip address 4.4) invalid service
 * adapter call with wrong userid/password 5.5) multiple requests
 * 
 */
public class TestNvpToXmlAdapter extends TestCase {
	protected TestFixture serviceAdapterTestFixture;
	private static final Logger logger = Logger
			.getLogger(TestNvpToXmlAdapter.class);
	private static final String SERVICE_URL1 = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=AllCarInsuranceService&messageconversion=quoteMessageConverter";
	private static final String SERVICE_URL2 = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=AllCarInsuranceService&messageconversion=inuranceProductsConverter";

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;

	}

	// adapter interface tests
	public void testInvalidNVPRequest() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			WebResponse res = null;
			try {
				res = wc.getResponse(SERVICE_URL1 + "&carvalue&carmodel");
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf("carvalue not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testAdapterParameterNoValidations() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("carvalue");
			postReq.setParameter("carvalue", "574834800000");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			Map<String, String> parseResponse = parseResponse(res.getText());
			assertTrue(parseResponse.get("quote").equals("344.34"));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testSuccessfulRequestWithMissingOptionalParmetres() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("phone");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testMissingNoneOptionalParmeter1() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("carmodel");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part carmodel not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testMissingNoneOptionalParmeter2() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("carmodel");
			postReq.setParameter("carmodel", "");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message conversion exception, messages part carmodel not optional") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	// adapter transformation and script

	public void testInvalidRecursiveValues() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("claimdetails3");			
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertNotSuccessResponse(parseResponse(res.getText()));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	public void testCallWithInvalidScript() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest req = buildRequest();
			req.removeParameter("carmodel");
			req.setParameter("carmodel", "error");

			WebResponse res = null;
			try {
				res = wc.getResponse(req);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);
			assertTrue(res.getText().indexOf("Arithemetic Exception") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}
	
	
	
	public void testCallWithExceptionThrownInScript() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("carmodel");
			postReq.setParameter("carmodel", "throw error");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res.getText().indexOf(
					"error occurred evaluating the script") >= 0);			

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}
	

	// adaptee service behavior tests
	public void testSuccessfulRequest() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			Map<String, String> parseResponse = parseResponse(res.getText());
			assertTrue(parseResponse.get("quote").equals("344.34"));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	
	public void testSuccessfulRequestWithRecursiveResponse() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildProductListRequest();
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			Map<String, String> parseResponse = parseResponse(res.getText());
			assertTrue(parseResponse.get("ProductName0").equals("Home Insurance"));
			assertTrue(parseResponse.get("ProductName1").equals("Motor Insurance"));
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}
	

	public void testServiceValidationFailure1() {
		try {

			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);
			WebConversation wc = new WebConversation();
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("yearofmanufacuring");
			postReq.setParameter("yearofmanufacuring", "1945");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			Map<String, String> parseResponse = parseResponse(res.getText());
			assertTrue(parseResponse.get("quote").equals("0"));			

			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}

	}

	
	public void testCallTimeout() {
		try {
			HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);

			WebConversation wc = new WebConversation();
			wc.set_connectTimeout(1);
			wc.set_readTimeout(1);
			PostMethodWebRequest postReq = buildRequest();
			postReq.removeParameter("carmodel");
			postReq.setParameter("carmodel", "TI");
			WebResponse res = null;
			try {
				res = wc.getResponse(postReq);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				fail(ex.getMessage());
			}
			logger.debug(res.getText());
			assertTrue(res.getResponseCode() == 200);
			assertTrue(res
					.getText()
					.indexOf(
							"message communication error occurred. error : Read timed out") >= 0);
			HttpUnitOptions.reset();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}	
	
	private void assertNotSuccessResponse(Map<String, String> parseResponse) {
		assertTrue(parseResponse.get("quote").equals("344.34"));

	}

	
	protected Map<String, String> parseResponse(String text) {

		Map<String, String> res = new HashMap<String, String>();
		StringTokenizer tokens = new StringTokenizer(text, "&");
		while (tokens.hasMoreTokens()) {
			String t = tokens.nextToken();
			if (!t.isEmpty()) {
				try {
					int midPos = t.indexOf("=");
					String key = t.substring(0, midPos);
					String val = t.substring(midPos + 1, t.length());
					res.put(key, val);
				} catch (RuntimeException e) {
				}
			}
		}
		return res;
	}	
	private PostMethodWebRequest buildProductListRequest() {
		return new PostMethodWebRequest(SERVICE_URL2);
	}	
	private PostMethodWebRequest buildRequest() {
		return buildRequest(TestNvpToXmlAdapter.SERVICE_URL1);
	}

	private PostMethodWebRequest buildRequest(String url) {
		PostMethodWebRequest req = new PostMethodWebRequest(url);
		req.setParameter("carvalue", "45990");
		req.setParameter("phone", "013456849");
		req.setParameter("yearofmanufacuring", "1998");
		req.setParameter("carmodel", "BMW 320i");
		req.setParameter("registerdaddress", "90 London Raod West Croydon");
		req.setParameter("registerdowner", "Mr Jolian");
		req.setParameter("noofclaims", "4");
		req.setParameter("claimdetails0", "breoken windows");
		req.setParameter("claimdetails1", "car caught fire");
		req.setParameter("claimdetails2", "accidend in the car pard");
		req.setParameter("claimdetails3", "storm damage");

		return req;
	}

}
