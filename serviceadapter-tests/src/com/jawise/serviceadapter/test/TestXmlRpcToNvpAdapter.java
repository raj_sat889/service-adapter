package com.jawise.serviceadapter.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class TestXmlRpcToNvpAdapter extends TestCase {
	protected TestFixture serviceAdapterTestFixture;
	private static final Logger logger = Logger
			.getLogger(TestXmlRpcToNvpAdapter.class);
	private static final String SERVICE_URL_1 = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=XComputerPartsSupplier&messageconversion=PurchaseOrderConversion";
	private static final String SERVICE_URL_2 = "http://localhost:8080/serviceadapter/process/adapter?userid=raj&service=XComputerPartsSupplier&messageconversion=PurchaseOrderConversionTest";

	protected void setUp() throws Exception {
		super.setUp();
		serviceAdapterTestFixture = new TestFixture(this);
		serviceAdapterTestFixture.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		serviceAdapterTestFixture.tearDown();
		serviceAdapterTestFixture = null;
	}

	private XmlRpcClient getXmlRpcClient(String url)
			throws MalformedURLException {
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		config.setServerURL(new URL(url));
		XmlRpcClient client = new XmlRpcClient();
		client.setConfig(config);
		return client;
	}

	// test adaptee service interface

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrderInvalidParmeters()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1);

		try {
			Map paramMap = getParameterMap();

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap, new String("") };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e
					.getMessage()
					.indexOf(
							"Parameter count mismatch. The operation requires 3 parameters.") >= 0);

		}

	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrderInvalidParameterType1()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1);

		try {
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "raj@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", 232);
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "PID_12345");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
					"Parameter po/orderplacer not correct type.") >= 0);
		}

	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrderInvalidParameterType2()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1);

		try {
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "raj@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "PID_12345");
			paramMap.put("items[]", 5);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			// TODO check types of arguments i.e arrays and maps
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
					"Parameter po/items[] should be an array type.") >= 0);

		}

	}

	/*
	 * public void testSubmitPurchaseOrderOptionalValue() { //xml rpc does not
	 * support optional parameter //it could besimulated method overloading }
	 */

	@SuppressWarnings("unchecked")
	public void testNoValidationAdapteeParameters() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1);
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			// long email address as parameter
			paramMap
					.put("email",
							"raj12345678901234567890123456789012345678901234567890@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "PID_12345");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			String result = (String) client.execute(
					"XComputerPartsSupplier.submitPurchaseOrder", params);
			assertTrue(result.length() == 38);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrderNoneOptionalValue()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1);

		try {
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "raj@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e
					.getMessage()
					.indexOf(
							"message conversion exception, messages part po/poid not optional") >= 0);

		}

	}

	// test adaptee transformation and script

	/*
	 * public void testIlegalScript() { should be common for all the adapter,
	 * this should throw parsing error }
	 */

	@SuppressWarnings("unchecked")
	public void testExceptionInScript() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL_2);
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "thorwexception");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "23232323");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
					"error occurred evaluating the script") >= 0);

		} catch (Exception e) {
			fail();
		}

	}

	/*
	 * public void testDataExceptionInScript() { /.same effect as the above test }
	 */

	@SuppressWarnings("unchecked")
	public void testCallWithInvalidScript() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL_2);
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "raj@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "23232323");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
					"error occurred evaluating the script") >= 0);

		} catch (Exception e) {
			fail();
		}

	}

	@SuppressWarnings("unchecked")
	public void testEmptryParamterValue() throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1);

		try {
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "raj@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e
					.getMessage()
					.indexOf(
							"message conversion exception, messages part po/orderplacer not optional") >= 0);

		}

	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrdersScriptError()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_2);

		try {
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "raj@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "23232323");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
					"error occurred evaluating the script") >= 0);

		}
	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrdersZeroLenArray()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_2);

		try {
			ArrayList items = new ArrayList();
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "raj@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "23232323");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);

		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			fail();
		}

	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrdersWrongParamterOrder()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_2);

		try {
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", 1221);
			paramMap.put("orderdate", "raj@jawsie.com");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "232323");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client.execute("XComputerPartsSupplier.submitPurchaseOrder", params);
			fail();
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			assertTrue(e.getMessage().indexOf(
					"Parameter po/email not correct type") >= 0);

		}

	}

	// adaptee service behaviour

	@SuppressWarnings("unchecked")
	public void testCalltimeout() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1);
			ArrayList items = new ArrayList();
			addItems(items);
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "raj@jawsie.com");
			paramMap.put("orderdate", "090802");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "timeout");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client.execute("XComputerPartsSupplier.submitPurchaseOrder", params);
			fail();
		} catch (XmlRpcException e) {
			e.printStackTrace();
			assertTrue(e
					.getMessage()
					.indexOf(
							"message communication error occurred. error : Read timed out") >= 0);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrder() throws MalformedURLException {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1);
			Map paramMap = getParameterMap();
			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			String result = (String) client.execute(
					"XComputerPartsSupplier.submitPurchaseOrder", params);
			assertTrue(result.length() == 38);
		} catch (XmlRpcException e) {
			e.printStackTrace();
			fail();
		}
	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrdersInvalidParamterLength1()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_2);

		try {
			ArrayList items = new ArrayList();
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "rajss@sds.com");
			paramMap.put("orderdate", "010109");
			paramMap.put("orderplacer", "raj");
			paramMap.put("totalprice", "80");
			paramMap.put("poid",
					"12345678901234567890123456789012345678901234567890ASDF");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			String result = (String) client.execute(
					"XComputerPartsSupplier.submitPurchaseOrder", params);
			assertTrue(result.indexOf("orderid length to big") >= 0);
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			fail();
		}
	}

	@SuppressWarnings("unchecked")
	public void testSubmitPurchaseOrdersInvalidParamterLength2()
			throws MalformedURLException {
		XmlRpcClient client = getXmlRpcClient(SERVICE_URL_2);

		try {
			ArrayList items = new ArrayList();
			Object[] itemsArray = new Object[items.size()];
			itemsArray = items.toArray(itemsArray);

			Map paramMap = new HashMap();
			paramMap.put("email", "rajss@sds.com");
			paramMap.put("orderdate", "010109");
			paramMap.put("orderplacer",
					"12345678901234567890123456789012345678901234567890ASDF");
			paramMap.put("totalprice", "80");
			paramMap.put("poid", "232DFSDF");
			paramMap.put("items[]", itemsArray);

			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			String result = (String) client.execute(
					"XComputerPartsSupplier.submitPurchaseOrder", params);
			assertTrue(result.indexOf("contactname length to big") >= 0);
			// TODO shoul there be a validaion by the converter
		} catch (XmlRpcException e) {
			logger.error(e.getMessage());
			fail();
		}
	}

	// *****************************************************************************************
	// service adapter invocation
	// *****************************************************************************************

	@SuppressWarnings("unchecked")
	public void testServiceAdapterInvocationWrongService() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1.replaceAll(
					"XComputerPartsSupplier", "TXComputerPartsSupplier"));
			Map paramMap = getParameterMap();
			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e.getMessage().indexOf(
					"service requested is not found in the registry.") >= 0);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	public void testServiceAdapterInvocationWrongConverter() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1.replaceAll(
					"PurchaseOrderConversion", "XPurchaseOrderConversion"));
			Map paramMap = getParameterMap();
			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e
					.getMessage()
					.indexOf(
							"message conversion requested is not found in the registry.") >= 0);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	public void testServiceAdapterInvocationWithWrongUser() {

		try {
			XmlRpcClient client = getXmlRpcClient(SERVICE_URL_1.replaceAll(
					"raj", "3kkaj"));
			Map paramMap = getParameterMap();
			Object[] params = new Object[] { new String("xcomputerparts"),
					new String("password"), paramMap };
			client
					.execute("XComputerPartsSupplier.submitPurchaseOrder",
							params);
			fail();
		} catch (XmlRpcException e) {
			assertTrue(e.getMessage().indexOf("userid is invalid") >= 0);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	private Map getParameterMap() {
		ArrayList items = new ArrayList();
		addItems(items);
		Object[] itemsArray = new Object[items.size()];
		itemsArray = items.toArray(itemsArray);

		Map paramMap = new HashMap();
		paramMap.put("email", "raj@jawsie.com");
		paramMap.put("orderdate", "090802");
		paramMap.put("orderplacer", "raj");
		paramMap.put("totalprice", "80");
		paramMap.put("poid", "PID_12345");
		paramMap.put("items[]", itemsArray);
		return paramMap;
	}

	// message conversion exception, messages part po/items[] not optional
	@SuppressWarnings("unchecked")
	private void addItems(ArrayList items) {
		for (int i = 0; i < 3; i++) {
			Map item = new HashMap();
			item.put("id", "" + (i + 1));
			item.put("details", "product" + (i + 1) + " is a dvd");
			item.put("quantity", 1);
			item.put("amout", "" + ((i + 1) * 0.5));
			items.add(item);
		}
	}
}
