package com.jawise.serviceadapter.test;

import junit.framework.Test;
import junit.framework.TestSuite;
 
public class AllTests {

	public static Test suite() {
		org.apache.log4j.BasicConfigurator.configure();
		TestSuite suite = new TestSuite(
				"Test for com.jawise.serviceadapter.test");
		//$JUnit-BEGIN$
		//protx
		//suite.addTestSuite(TestNvpToNvpAdapter.class);
		suite.addTestSuite(TestXmlToNvpAdapter.class);
		suite.addTestSuite(TestXmlComposer.class);
		suite.addTestSuite(TestXmlToXmlAdapter.class);
		suite.addTestSuite(TestXmlRpcToXmlRpcAdapter.class);
		suite.addTestSuite(TestXmlRpcToNvpAdapter.class);
		suite.addTestSuite(TestNvpToXmlRpcAdapter.class);
		suite.addTestSuite(TestNvpToXmlAdapter.class);
		suite.addTestSuite(TestXmlToXmlRpcAdapter.class);
		suite.addTestSuite(TestSoapToSoapAdapter.class);
		suite.addTestSuite(TestNvpToSoapAdapter.class);
		suite.addTestSuite(TestSoapToNvpAdapter.class);
		suite.addTestSuite(TestSoapToXmlAdapter.class);
		suite.addTestSuite(TestXMLToSoapAdapter.class);
		//$JUnit-END$
		return suite;
	}

}
