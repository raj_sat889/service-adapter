    Service Adapter v!.0
    Copyright (C) <2008>  <Raj Sathyavarathan>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    THIRD PARTY LICENSES

		A. apache-license.txt (xmlrpc,ws-common, commons*, spring, Castor,aximon)
		B. xstream-license.txt
		C. bsh - http://www.gnu.org/copyleft/lesser.html
		D. rhino - http://www.gnu.org/licenses/gpl-2.0.html
		E. ICEFaces - http://www.mozilla.org/MPL/MPL-1.1.html , icefaces-lib-licenses.html
		f. xfire - xfire-license.txt
		g. jwsdp - jwsdp-license.txt
		h. all other third party licenses included with this