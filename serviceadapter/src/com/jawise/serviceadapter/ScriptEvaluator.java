package com.jawise.serviceadapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;
import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.PartConversion;
import com.jawise.serviceadapter.core.ScriptType;

import bsh.EvalError;
import bsh.Interpreter;

public class ScriptEvaluator {

	private static Logger logger = Logger.getLogger(ScriptEvaluator.class);

	@SuppressWarnings("unchecked")
	public Object eval(Map sourceMap, Map targetMap, PartConversion pc,
			String targetName) throws MessageException {

		if (ScriptType.BEAN_SHELL_SCRIPT.toString().equals(pc.getScripttype())) {
			return evalBeanShellScript(sourceMap, targetMap, pc, targetName);
		} else if (ScriptType.JAVA_SCRIPT.toString().equals(pc.getScripttype())) {
			return evalJavaScript(sourceMap, targetMap, pc, targetName);
		} else {
			logger.debug("not script type");
			return "";
		}
	}

	@SuppressWarnings("unchecked")
	private Object evalBeanShellScript(Map sourceMap, Map targetMap,
			PartConversion pc, String targetName) throws MessageException {
		try {

			logger.debug("processing script to evaluate " + targetName);
			ArrayList<String> referances = pc.getReferances();
			String script = pc.getScript();

			Interpreter i = new Interpreter();
			for (String referance : referances) {
				Object value = getReferanceValue(referance, sourceMap,
						targetMap);
				setInterpreterParam(i, referance, value);
			}

			Object doc = getReferanceValue("sourceDoc", sourceMap, targetMap);
			if (doc != null) {
				setInterpreterParam(i, "sourceDoc", doc);
			}

			// pass source map as well for dynamic acces
			setInterpreterParam(i, "sourceMap", sourceMap);

			if (isScriptUsesDynamicLookup(script)) {
				Iterator iterator = sourceMap.keySet().iterator();
				while (iterator.hasNext()) {
					String key = (String) iterator.next();
					Object value = getReferanceValue(key, sourceMap, targetMap);
					setInterpreterParam(i, key, value);
				}
			}
			i.eval(script);

			Object result = i.get(targetName);
			if (pc.getLog().allowed()) {
				logger.debug("script evaluated to " + targetName + " = "
						+ result);
			}
			return result;
		} catch (EvalError e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1006", e.getMessage());
		}
	}

	private void setInterpreterParam(Interpreter i, String referance,
			Object value) throws EvalError {
		if (value != null) {
			if (value instanceof String[]) {
				i.set(referance, ((String[]) value)[0]);
			} else if (value instanceof String) {
				i.set(referance, (String) value);
			} else {
				i.set(referance, value);
			}
		} else {
			logger.debug("referance value is null for " + referance);
			i.set(referance, "");
		}
	}

	private boolean isScriptUsesDynamicLookup(String script) {
		if (script.indexOf("interpreter.get") >= 0) {
			return true;
		} else {
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	private Object getReferanceValue(String referance, Map sourceMap,
			Map targetMap) {

		Object objectvalue = sourceMap.get(referance);
		if (objectvalue == null)
			objectvalue = targetMap.get(referance);

		return objectvalue;
	}

	@SuppressWarnings("unchecked")
	private Object evalJavaScript(Map sourceMap, Map targetMap,
			PartConversion pc, String targetName) throws MessageException {
		try {
			logger.debug("processing script to evaluate " + targetName);
			ArrayList<String> referances = pc.getReferances();
			String script = pc.getScript();

			// Map parameterMap = toLowerCaseKeys(params);
			BSFManager manager = new BSFManager();

			for (String referance : referances) {
				Object value = getReferanceValue(referance, sourceMap,
						targetMap);

				if (value != null) {

					if (value instanceof String[]) {
						logger.debug("added value for " + referance);
						manager.declareBean(referance, ((String[]) value)[0],
								String.class);

					} else {
						logger.debug("added value for " + referance);
						manager.declareBean(referance, value,
								getValueClass(value));
					}
				} else {
					logger.debug("referance value is null for " + referance);
					manager
							.declareBean(referance, new String(""),
									String.class);
				}
			}
			manager.registerBean(targetName, "");

			Object doc = getReferanceValue("sourceDoc", sourceMap, targetMap);
			if (doc != null) {
				manager.declareBean("sourceDoc", doc,
						org.w3c.dom.Document.class);
			}

			manager.declareBean("sourceMap", sourceMap, java.util.Map.class);

			Object result = manager.eval("javascript", "(java)", 1, 1, script);
			if (result == null)
				result = (String) manager.lookupBean(targetName);

			logger.debug("script evaluated to " + targetName + " = " + result);
			return result;
		} catch (BSFException e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1006", e.getMessage());
		}
	}

	private Class<?> getValueClass(Object value) {
		if (value instanceof String) {
			return String.class;
		}
		return value.getClass();
	}
}
