package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.swing.tree.DefaultMutableTreeNode;

import com.jawise.serviceadapter.core.Binding;

public class BindingPanel extends Panel {
	@Override
	public void startEdit(ActionEvent event) {
		backup(service.getPort().getBinding());
		super.startEdit(event);
	}

	@Override
	public void cancelEdit(ActionEvent event) {
		restore(service.getPort().getBinding());
		super.cancelEdit(event);
	}

	@Override
	public void save(ActionEvent event) throws Exception {
		Binding b = service.getPort().getBinding();
		ValidationErrors errors = b.validate();
		if (errors.isEmpty()) {
			ServiceRegistry.getInstance().save();
			super.save(event);
			DefaultMutableTreeNode selectednode = serviceRegistryBrowser
					.getSelectednode();
			BrowserUserObject userObject = (BrowserUserObject) selectednode
					.getUserObject();
			if (!userObject.getText().equals(b.getName())) {
				userObject.setText(b.getName());
				userObject.setServicename(service.getName());
			}

		} else {
			error("bindingForm", errors);
		}
	}

	@SuppressWarnings("unused")
	public String addOperation() throws IOException {

		serviceRegistryBrowser.setSelectedPanel("operationsPanel");
		Map<String, Object> requestMap = getRequestMap();		
		Map<String, Object> sessionMap = getSessionMap();
		requestMap.remove("newoperation");		 
		requestMap.remove("operationsPanel");
		return "";
	}
}
