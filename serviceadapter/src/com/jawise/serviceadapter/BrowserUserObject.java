package com.jawise.serviceadapter;

import javax.swing.tree.DefaultMutableTreeNode;

import com.icesoft.faces.component.tree.IceUserObject;

public class BrowserUserObject extends IceUserObject {

	private int id;
	private String servicename;
	private Object data;

	public BrowserUserObject(DefaultMutableTreeNode wrapper) {
		super(wrapper);
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
