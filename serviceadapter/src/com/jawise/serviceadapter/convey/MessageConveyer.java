package com.jawise.serviceadapter.convey;

import java.net.URL;

import com.jawise.serviceadapter.core.MessageException;

public interface MessageConveyer {		
	public Object convey(URL url, Object msg) throws MessageException;

	public long getConnectionTimeout();

	public void setConnectionTimeout(long connectionTimeout);

	public long getReadTimeout();

	public void setReadTimeout(long readTimeout);

	public boolean getCheckCertificates();

	public void setCheckCertificates(boolean checkCertificates);

	public String getContentType();

	public void setContentType(String contentType);
}
