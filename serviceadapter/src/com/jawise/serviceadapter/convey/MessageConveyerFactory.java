package com.jawise.serviceadapter.convey;

import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Protocol;
import com.jawise.serviceadapter.core.Service;

public class MessageConveyerFactory {
	public static MessageConveyer getMessageConveyer(Service service)
			throws MessageException {

		Binding toBinding = service.getAdapter().getService().getPort()
				.getBinding();

		if (Protocol.NVP_HTTP.toString().equals(toBinding.getProtocol())) {
			return new HttpMessageConveyer();
		} else if (Protocol.XML_HTTP.toString().equals(toBinding.getProtocol())) {
			return new HttpMessageConveyer();
		} else if (Protocol.XML_RPC.toString().equals(toBinding.getProtocol())) {
			return new HttpMessageConveyer();
		} else if (Protocol.SOAP11.toString().equals(toBinding.getProtocol())) {
			return new SoapMessageConveyer();
		} else {
			throw new MessageException("1004");
		}

	}
}
