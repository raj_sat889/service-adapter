package com.jawise.serviceadapter.convey;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.MessageException;

public class SoapMessageConveyer implements MessageConveyer {

	protected static Logger logger = Logger
			.getLogger(SoapMessageConveyer.class);

	private long connectionTimeout = 15000;

	private long readTimeout = 15000;

	private boolean checkCertificates = false;

	@Override
	public Object convey(URL url, Object msg) throws MessageException {
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection connection = soapConnectionFactory
					.createConnection();
			SOAPMessage response = connection.call(
					(javax.xml.soap.SOAPMessage) msg, url);
			connection.close();

			ByteArrayOutputStream str = new ByteArrayOutputStream();
			response.writeTo(str);
			String r = new String(str.toByteArray());
			logger.info("response  : " + r);
			
			return r;
		} catch (Exception e) {
			throw new MessageException("1013", e.getMessage());
		}
	}

	public long getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(long connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public long getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(long readTimeout) {
		this.readTimeout = readTimeout;
	}

	public boolean isCheckCertificates() {
		return checkCertificates;
	}

	public void setCheckCertificates(boolean checkCertificates) {
		this.checkCertificates = checkCertificates;
	}

	@Override
	public boolean getCheckCertificates() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getContentType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setContentType(String contentType) {
		// TODO Auto-generated method stub

	}

}
