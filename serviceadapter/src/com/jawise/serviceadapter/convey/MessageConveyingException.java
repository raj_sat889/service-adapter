package com.jawise.serviceadapter.convey;

import java.util.ResourceBundle;

import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.ResourceBundleSupport;

public class MessageConveyingException extends MessageException implements ResourceBundleSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 468307201506344605L;
	public String value;

	public MessageConveyingException() {
		super();
	}

	public MessageConveyingException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageConveyingException(String message) {
		super(message);
	}

	public MessageConveyingException(String message, String value) {
		super(message);
		setValue(value);
	}

	public MessageConveyingException(Throwable cause) {
		super(cause);
	}

	public String getMessage(ResourceBundle bundle) {
		String m = bundle.getString(super.getMessage());
		if(value != null && !value.isEmpty()) {
			m = m.replaceAll("#value", value);
		}
		else {
			m = m.replaceAll("#value","");
		}
		return m;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
