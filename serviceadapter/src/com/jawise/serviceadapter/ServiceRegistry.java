package com.jawise.serviceadapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Adapter;
import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.PartConversion;
import com.jawise.serviceadapter.core.Port;
import com.jawise.serviceadapter.core.PortType;
import com.jawise.serviceadapter.core.Service;
import com.jawise.serviceadapter.core.Services;
import com.thoughtworks.xstream.XStream;

public class ServiceRegistry {
	private static Logger logger = Logger.getLogger(ServiceRegistry.class);
	private String localuploaddir;
	private String filename;
	private String backupfilename;
	private File file;
	private XStream xstream;
	private Services services;

	private static ServiceRegistry serviceRegistry;

	public void initialise() throws IOException {

		file = new File(filename);
		if (!file.exists()) {
			file.createNewFile();
		}

		xstream = new XStream();
		xstream.alias("binding", Binding.class);
		xstream.alias("service", Service.class);
		xstream.alias("port", Port.class);
		xstream.alias("services", Services.class);
		xstream.alias("porttype", PortType.class);
		xstream.alias("operation", Operation.class);
		xstream.alias("message", Message.class);
		xstream.alias("part", Part.class);
		xstream.alias("messageconversion", MessageConversion.class);
		xstream.alias("partconversion", PartConversion.class);
		xstream.omitField(java.util.List.class, "default");
		xstream.omitField(java.util.List.class, "int");
		xstream.omitField(ServiceRegistry.class, "filename");
		load();
	}

	public static ServiceRegistry getInstance() throws IOException {
		if (serviceRegistry == null) {
			serviceRegistry = new ServiceRegistry();
		}
		return serviceRegistry;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Services getServices() {
		return services;
	}

	public void setServices(Services services) {
		this.services = services;
	}

	public void register(Service svc) throws IOException {
		services.add(svc);
		save();
	}

	public String exportServiceAsString(String service) throws IOException {

		Service s = findService(service);
		if (s == null) {
			throw new IllegalArgumentException("service not found");
		}
		Services expservices = new Services();
		expservices.add(s);
		String xml = xstream.toXML(expservices);
		return xml;
	}

	public void save() throws IOException {
		backup();
		FileOutputStream os = new FileOutputStream(file);
		String xml = xstream.toXML(services);
		os.write(xml.getBytes());
		os.flush();
		os.close();
		logger.debug("service registry saved");
	}

	public void backup() throws IOException {
		String registrydata = null;
		FileInputStream is = new FileInputStream(file);
		if (file.length() != 0) {
			byte fileContent[] = new byte[(int) file.length()];
			is.read(fileContent);
			registrydata = new String(fileContent);
			is.close();

			File backupfile = new File(backupfilename);
			if (!backupfile.exists()) {
				backupfile.createNewFile();
			}
			FileOutputStream os = new FileOutputStream(backupfile);
			os.write(registrydata.getBytes());
			os.flush();
			os.close();
			logger.debug("service registry saved to backup file");
		}

	}

	public void load() throws FileNotFoundException, IOException {
		FileInputStream is = new FileInputStream(file);
		if (file.length() == 0) {
			services = new Services();
		} else {
			byte fileContent[] = new byte[(int) file.length()];
			is.read(fileContent);
			String xml = new String(fileContent);
			logRegistry(xml);
			if (services != null)
				services.clear();
			services = (Services) xstream.fromXML(xml);
			is.close();
		}
	}

	private void logRegistry(String xml) {
		// logger.debug(xml);
	}

	public Services loadServices(File file) throws FileNotFoundException,
			IOException {

		Services loadedServices = new Services();
		FileInputStream is = new FileInputStream(file);

		byte fileContent[] = new byte[(int) file.length()];
		is.read(fileContent);
		String xml = new String(fileContent);
		logRegistry(xml);
		if (loadedServices != null)
			loadedServices.clear();
		loadedServices = (Services) xstream.fromXML(xml);
		is.close();
		logger.debug("service registry loaded");
		return loadedServices;
	}

	public Service findService(String servicename) throws IOException {
		for (Service s : services) {
			if (servicename.equals(s.getName())) {
				return s;
			}
		}
		return null;
	}

	public Adapter findAdapter(String adapter) throws IOException {
		for (Service s : services) {
			if (adapter.equals(s.getAdapter().getName())) {
				return s.getAdapter();
			}
		}
		return null;
	}

	public String getBackupfilename() {
		return backupfilename;
	}

	public void setBackupfilename(String backupfilename) {
		this.backupfilename = backupfilename;
	}

	public String getLocaluploaddir() {
		return localuploaddir;
	}

	public void setLocaluploaddir(String localuploaddir) {
		this.localuploaddir = localuploaddir;
	}
}
