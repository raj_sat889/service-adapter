package com.jawise.serviceadapter.imp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jawise.serviceadapter.ServiceRegistry;
import com.jawise.serviceadapter.ValidationErrors;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.Type;

public class XmlMessagePartsImporter implements MessagePartsImporter {
	private static Logger logger = Logger
			.getLogger(XmlMessagePartsImporter.class);

	private Map<String, String> completedElements;

	public XmlMessagePartsImporter() {
		completedElements = new HashMap<String, String>();
	}

	@Override
	public ValidationErrors importMessageParts(Message message,
			String importData, String importSourceType) throws IOException {

		ValidationErrors errors = new ValidationErrors();
		completedElements.clear();
		try {
			if (importData != null && !importData.isEmpty()) {
				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder newDB = factory.newDocumentBuilder();
				ByteArrayInputStream is = new ByteArrayInputStream(importData
						.getBytes());
				Document doc = newDB.parse(is);
				Element root = doc.getDocumentElement();
				processNode(message, root, "");
				ServiceRegistry.getInstance().save();

			} else {
				errors.add("importData is required");
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			errors.add(e.getMessage());
		}
		
		completedElements.clear();
		return errors;

	}

	private void processNode(Message m, Node n, String p) {

		List<Part> parts = m.getParts();
		String name = n.getNodeName();
		String cp = "";

		switch (n.getNodeType()) {
		case Node.DOCUMENT_NODE:
			cp = p.isEmpty() ? name : "/" + name;
			processNode(m, n, cp);
			break;
		case Node.ELEMENT_NODE:
			cp = p + (p.isEmpty() ? name : "/" + name);
			NamedNodeMap attributes = n.getAttributes();
			for (int i = 0; i < attributes.getLength(); i++) {
				Node current = attributes.item(i);
				Part part = createNewPart(current, cp + "/@");
				if (!completedElements.containsKey(part.getName())) {
					parts.add(part);
					completedElements.put(part.getName(), part.getId());
				}
			}

			// recurse on each child
			int partssize = parts.size();
			NodeList children = n.getChildNodes();
			if (children != null) {
				for (int i = 0; i < children.getLength(); i++) {
					processNode(m, children.item(i), cp);
				}
			}
			if (partssize == parts.size()) {
				Part part = createNewPart(n, p + "/");
				parts.add(part);
			}
			break;
		case Node.TEXT_NODE:
			break;
		case Node.CDATA_SECTION_NODE:
			break;
		case Node.COMMENT_NODE:
			break;
		case Node.PROCESSING_INSTRUCTION_NODE:
			break;
		case Node.ENTITY_REFERENCE_NODE:
			break;
		case Node.DOCUMENT_TYPE_NODE:
			break;
		}
	}

	private Part createNewPart(Node item, String currentpath) {

		Part p = new Part();
		p.setName(currentpath + item.getNodeName());
		p.setDescription("");
		p.setContentformat("Alphanumeric");
		p.setOptional(true);
		Type t = new Type();
		t.setName(getTypeName(item));
		t.setMaximumlength(0);
		p.setType(t);
		p.setId(p.getName() + "_" + p.getType());
		return p;
	}

	private String getTypeName(Node item) {
		String textContent = item.getTextContent();
		String value = item.getNodeValue();

		textContent = textContent == null ? "" : textContent;
		value = value == null ? "" : value;
		if (textContent.indexOf("string") >= 0 || value.indexOf("string") >= 0) {
			return "String";
		} else if (textContent.indexOf("<string>") >= 0
				|| value.indexOf("<string>") >= 0) {
			return "String";
		} else if (textContent.indexOf("<i4>") >= 0
				|| value.indexOf("<i4>") >= 0) {
			return "i4";
		} else if (textContent.indexOf("<int>") >= 0
				|| value.indexOf("<int>") >= 0) {
			return "int";
		} else if (textContent.indexOf("<boolean>") >= 0
				|| value.indexOf("<boolean>") >= 0) {
			return "boolean";
		} else if (textContent.indexOf("<double>") >= 0
				|| value.indexOf("<double>") >= 0) {
			return "double";
		} else if (textContent.indexOf("<dateTime.iso8601>") >= 0
				|| value.indexOf("<dateTime.iso8601>") >= 0) {
			return "dateTime.iso8601";
		} else if (textContent.indexOf("<base64>") >= 0
				|| value.indexOf("<base64>") >= 0) {
			return "base64";
		} else if (textContent.indexOf("<struct>") >= 0
				|| value.indexOf("<struct>") >= 0) {
			return "struct";
		} else if (textContent.indexOf("<array>") >= 0
				|| value.indexOf("<array>") >= 0) {
			return "array";
		} else {
			return "String";
		}
	}
}
