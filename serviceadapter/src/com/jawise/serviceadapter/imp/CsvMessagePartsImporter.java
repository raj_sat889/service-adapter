package com.jawise.serviceadapter.imp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.ServiceRegistry;
import com.jawise.serviceadapter.ValidationErrors;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.Type;

public class CsvMessagePartsImporter implements MessagePartsImporter {
	private static Logger logger = Logger
			.getLogger(CsvMessagePartsImporter.class);
	private static Pattern lengthPattern = Pattern.compile("\\d+");

	/**
	 *  Import csv formated part definition. 
	 *  name , description, format
	 */
	public ValidationErrors importMessageParts(Message message,
			String importData, String importSourceType) throws IOException {

		ValidationErrors errors = new ValidationErrors();
		StringReader sr = new StringReader(importData);
		BufferedReader br = new BufferedReader(sr);
		String line = null;

		List<Part> parts = new ArrayList<Part>();

		while ((line = br.readLine()) != null) {
			StringTokenizer st = new StringTokenizer(line, ",");
			int tokens = st.countTokens();

			//name , description, format
			if (tokens == 3) {
				Part p = new Part();
				p.setName(st.nextToken().trim());
				p.setDescription(st.nextToken().trim());
				String format = st.nextToken().trim();
				p.setContentformat(parsePartTypeFormat(format));
				p.setOptional(true);
				Type t = new Type();
				t.setName("String");
				t.setMaximumlength(parsePartTypeLength(format));
				p.setType(t);
				p.setId(p.getName() + "_" + p.getType());
				
				ValidationErrors partErrors = p.validate();
				if(partErrors.size() == 0) {
					parts.add(p);
				}
				else {
					errors.add("errors in line : " + line);
					break;
				}
			} else {
				errors.add("incorrect number of comma's in line : " + line);
				break;
			}

		}
		if (errors.size() == 0) {
			for (Part p : parts) {
				message.getParts().add(p);
			}
			ServiceRegistry.getInstance().save();
			importData = "";
			importSourceType = "";
		}
		return errors;
	}

	private int parsePartTypeLength(String format) {
		try {
			Matcher matcher = lengthPattern.matcher(format);
			int ret = 0;
			if (matcher.find()) {
				String len = format.substring(matcher.start(), matcher.end());
				ret = Integer.valueOf(len).intValue();
			}
			return ret;
		} catch (RuntimeException e) {
			logger.error(e.getMessage());
			return 0;
		}
	}

	private String parsePartTypeFormat(String format) {

		String lf = format.toLowerCase();
		if (lf.indexOf("alphanumeric") >= 0 || lf.indexOf("string") >= 0
				|| lf.indexOf("characters") >= 0) {
			return "Alphanumeric";
		} else if (lf.indexOf("numeric") >= 0 || lf.indexOf("number") >= 0
				|| lf.indexOf("digits") >= 0 || lf.indexOf("integer") >= 0) {
			return "Numeric";
		} else if (lf.indexOf("base64") >= 0) {
			return "Base64Encode";
		}
		return "Alphanumeric";
	}
}
