package com.jawise.serviceadapter.imp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jawise.serviceadapter.ServiceRegistry;
import com.jawise.serviceadapter.ValidationErrors;
import com.jawise.serviceadapter.convert.NVPHelper;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.Type;

public class NvpMessagePartsImporter implements MessagePartsImporter {

	@SuppressWarnings("unchecked")
	@Override
	public ValidationErrors importMessageParts(Message message,
			String importData, String importSourceType) throws IOException {
		ValidationErrors errors = new ValidationErrors();
		List<Part> parts = new ArrayList<Part>();

		if (message.getSeparator() == null || message.getSeparator().isEmpty()) {
			errors.add("NVP seperator must be setup first");
		} else {
			Map nvp = NVPHelper.parseNvpMessage(importData, true, message
					.getSeparator());
			Iterator iter = nvp.keySet().iterator();
			while (iter.hasNext()) {
				String key = (String) iter.next();

				if (key != null) {
					@SuppressWarnings("unused")
					Object v = nvp.get(key);
					String val = "";
					if(v instanceof Object[])
						val = ((String[])v)[0];
					else
						val = (String)v;						
					Part p = new Part();
					p.setName(key.trim());
					p.setContentformat("Alphanumeric");
					p.setOptional(true);
					Type t = new Type();
					t.setName("String");
					t.setMaximumlength(0);
					p.setType(t);
					p.setId(p.getName() + "_" + p.getType());
					parts.add(p);
				}

			}
		}
		if (errors.size() == 0) {
			for (Part p : parts) {
				message.getParts().add(p);
			}
			ServiceRegistry.getInstance().save();
			importData = "";
			importSourceType = "";
		}
		return errors;
	}

}
