package com.jawise.serviceadapter.imp;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.wsdl.Binding;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Input;
import javax.wsdl.Operation;
import javax.wsdl.Output;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.ExtensibilityElement;
import javax.wsdl.extensions.UnknownExtensibilityElement;
import javax.wsdl.extensions.http.HTTPAddress;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.wsdl.extensions.soap12.SOAP12Address;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;

import org.exolab.castor.xml.schema.ComplexType;
import org.exolab.castor.xml.schema.ElementDecl;
import org.exolab.castor.xml.schema.Group;
import org.exolab.castor.xml.schema.Particle;
import org.exolab.castor.xml.schema.Schema;
import org.exolab.castor.xml.schema.Structure;
import org.exolab.castor.xml.schema.XMLType;
import org.exolab.castor.xml.schema.reader.SchemaReader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.DOMBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.xml.sax.InputSource;

public class WsdlImporter {
	Definition definition;
	Service service;
	private Port port;
	
	//private Binding binding;
	//private Operation operation;	
	private Schema schema;
	

	public void importWsdl(String url) throws WSDLException {
		WSDLFactory factory = WSDLFactory.newInstance();
		WSDLReader reader = factory.newWSDLReader();
		definition = reader.readWSDL(url);
		schema = createSchemaFromTypes(definition);
		
	}
	
	public Map getServices() {
		Map services = definition.getServices();
		return services;
	}
	
	protected void importService(String servicename , String portname) throws WSDLException {

		extractService(servicename);
		System.out.println("service : " + service.getQName().getLocalPart());
		System.out.println("namespace : "
				+ service.getQName().getNamespaceURI());
		// 2nd identify the Port

		extractPort(portname);
		// only 1 extensibility element

		String locationURI = getServiceLocation();
		System.out.println("address : " + locationURI);

		// 3 get binding and op names
		Binding binding = port.getBinding();

		List operations = binding.getBindingOperations();

		for (int i = 0; i < operations.size(); i++) {
			BindingOperation bop = (BindingOperation) operations.get(i);

			System.out.println("binding : " + bop.getName());
			Operation operation = bop.getOperation();

			System.out.println("operation: " + operation.getName());
			System.out.println("stype: " + operation.getStyle());

			// 4th identify the the Message and get Part information

			Input input = operation.getInput();
			System.out.println("msg name: " + input.getName());
			Map inputparts = input.getMessage().getParts();

			getParameters(operation,input.getMessage().getQName().getLocalPart(),inputparts);
			Output output = operation.getOutput();
			System.out.println("msg name: " + output.getName());
			Map outputparts = output.getMessage().getParts();
			getParameters(operation,output.getMessage().getQName().getLocalPart(),outputparts);
			// 5th identify the Part, if there is only 1 part then the parts are
			// defined types sections

		}

	}

	public void getParameters(Operation operation, String name, Map parts) {
		Element rootElem = new Element(name);
		Iterator iter = parts.keySet().iterator();
		while (iter.hasNext()) {
			// Get each part
			Part part = (Part) parts.get(iter.next());

			// Add content for each message part
			String partName = part.getName();

			if (partName != null) {
				// Determine if this message part's type is complex
				XMLType xmlType = getXMLType(part);

				if (xmlType != null && xmlType.isComplexType()) {
					// Build the message structure
					buildComplexPart((ComplexType) xmlType, rootElem);
				} else {
					// Build the element that will be added to the message
					Element partElem = new Element(partName);

					// Add some default content as just a place holder
					partElem.addContent("0");

					if (operation.getStyle().toString().equalsIgnoreCase("rpc")) {
						// If this is an RPC style operation, we need to
						// include some type information
						partElem.setAttribute("type", part.getTypeName()
								.getLocalPart());
					} else {
						partElem.setAttribute("type", part.getTypeName()
								.getLocalPart());
					}

				}
			}
		}
		System.out.println(outputString(rootElem));
	}

	/**
	 * Populate a JDOM element using the complex XML type passed in
	 * 
	 * @param complexType
	 *            The complex XML type to build the element for
	 * @param partElem
	 *            The JDOM element to build content for
	 */
	protected void buildComplexPart(ComplexType complexType, Element partElem) {
		// Find the group
		Enumeration particleEnum = complexType.enumerate();
		Group group = null;

		while (particleEnum.hasMoreElements()) {
			Particle particle = (Particle) particleEnum.nextElement();

			if (particle instanceof Group) {
				group = (Group) particle;
				break;
			}
		}

		if (group != null) {
			Enumeration groupEnum = group.enumerate();

			while (groupEnum.hasMoreElements()) {
				Structure item = (Structure) groupEnum.nextElement();

				if (item.getStructureType() == Structure.ELEMENT) {
					ElementDecl elementDecl = (ElementDecl) item;
					Element childElem = new Element(elementDecl.getName());
					XMLType xmlType = elementDecl.getType();

					childElem.setAttribute("type", xmlType.getName() == null ? "" : xmlType.getName());

					if (xmlType != null && xmlType.isComplexType()) {
						buildComplexPart((ComplexType) xmlType, childElem);
					} else {
						childElem.addContent("0");
					}

					partElem.addContent(childElem);

				}
			}
		}
	}

	public XMLType getXMLType(Part part) {
		if (schema == null) {
			// No defined types, Nothing to do
			return null;
		}

		// Find the XML type
		XMLType xmlType = null;

		// First see if there is a defined element
		if (part.getElementName() != null) {
			// Get the element name
			String elemName = part.getElementName().getLocalPart();

			// Find the element declaration
			ElementDecl elemDecl = schema.getElementDecl(elemName);

			if (elemDecl != null) {
				// From the element declaration get the XML type
				xmlType = elemDecl.getType();
			}
		}

		return xmlType;
	}

	public void extractService(String servicename) {
		Map services = definition.getServices();
		Iterator iter = services.keySet().iterator();
		while (iter.hasNext()) {
			Service s = (Service) services.get(iter.next());
			if (s.getQName().getLocalPart().equals(servicename)) {
				service = s;
			}
		}
	}

	public void extractPort(String portname) {
		// assuming only 1 port, but more then one port can be present
		Map ports = service.getPorts();
		Iterator iter = ports.keySet().iterator();
		while (iter.hasNext()) {
			Port p = (Port) ports.get(iter.next());
			if (p.getName().indexOf(portname) >= 0) {
				port = p;
			}
		}
	}

	/**
	 * Creates a castor schema based on the types defined by a WSDL document
	 * 
	 * @param wsdlDefinition
	 *            The WSDL4J instance of a WSDL definition.
	 * 
	 * @return A castor schema is returned if the WSDL definition contains a
	 *         types element. null is returned otherwise.
	 */
	protected Schema createSchemaFromTypes(Definition wsdlDefinition) {
		// Get the schema element from the WSDL definition
		org.w3c.dom.Element schemaElement = null;

		if (wsdlDefinition.getTypes() != null) {
			ExtensibilityElement schemaExtElem = findExtensibilityElement(
					wsdlDefinition.getTypes().getExtensibilityElements(),
					"schema");

			if (schemaExtElem != null
					&& (schemaExtElem instanceof UnknownExtensibilityElement)) {
				schemaElement = ((UnknownExtensibilityElement) schemaExtElem)
						.getElement();
			} else {

				Class<? extends ExtensibilityElement> class1 = schemaExtElem
						.getClass();
				schemaElement = ((com.ibm.wsdl.extensions.schema.SchemaImpl) schemaExtElem)
						.getElement();
			}
		}

		if (schemaElement == null) {
			// No schema to read
			System.err
					.println("Unable to find schema extensibility element in WSDL");
			return null;
		}

		// Convert from DOM to JDOM
		DOMBuilder domBuilder = new DOMBuilder();
		org.jdom.Element jdomSchemaElement = domBuilder.build(schemaElement);
		;

		if (jdomSchemaElement == null) {
			System.err.println("Unable to read schema defined in WSDL");
			return null;
		}

		// Add namespaces from the WSDL
		Map namespaces = wsdlDefinition.getNamespaces();

		if (namespaces != null && !namespaces.isEmpty()) {
			Iterator nsIter = namespaces.keySet().iterator();

			while (nsIter.hasNext()) {
				String nsPrefix = (String) nsIter.next();
				String nsURI = (String) namespaces.get(nsPrefix);

				if (nsPrefix != null && nsPrefix.length() > 0) {
					org.jdom.Namespace nsDecl = org.jdom.Namespace
							.getNamespace(nsPrefix, nsURI);
					jdomSchemaElement.addNamespaceDeclaration(nsDecl);
				}
			}
		}

		// Make sure that the types element is not processed
		jdomSchemaElement.detach();

		// Convert it into a Castor schema instance
		Schema schema = null;

		try {
			schema = convertElementToSchema(jdomSchemaElement);
		}

		catch (Exception e) {
			System.err.println(e.getMessage());
		}

		// Return it
		return schema;
	}

	public static void main(String args[]) throws WSDLException {
		WsdlImporter i = new WsdlImporter();
		i.importWsdl("http://www.ecubicle.net/iptocountry.asmx?wsdl");
		i.importService("iptocountry","iptocountrySoap");
		
		
		//i.importWsdl("E://sathysoft//workspace//serviceadapter-tests//zcomp.wsdl");
		//i.importService("ZComputerPartsSupplier");
	}

	private static ExtensibilityElement findExtensibilityElement(
			List extensibilityElements, String elementType) {
		if (extensibilityElements != null) {
			Iterator iter = extensibilityElements.iterator();

			while (iter.hasNext()) {
				ExtensibilityElement element = (ExtensibilityElement) iter
						.next();

				if (element.getElementType().getLocalPart().equalsIgnoreCase(
						elementType)) {
					// Found it
					return element;
				}
			}
		}

		return null;
	}

	public Schema convertElementToSchema(Element element) throws IOException {
		// get the string content of the element
		String content = outputString(element);

		// check for null value
		if (content != null) {
			// create a schema from the string content
			return readSchema(new StringReader(content));
		}

		// otherwise return null
		return null;
	}

	/**
	 * Returns a string representation of the given jdom document.
	 * 
	 * @param doc
	 *            The jdom document to be converted into a string.
	 * 
	 * @return Ths string representation of the given jdom document.
	 */
	public static String outputString(Document doc) {
		XMLOutputter xmlWriter = new XMLOutputter(Format.getPrettyFormat());

		return xmlWriter.outputString(doc);

	}

	/**
	 * Returns a string representation of the given jdom element.
	 * 
	 * @param elem
	 *            The jdom element to be converted into a string.
	 * 
	 * @return The string representation of the given jdom element.
	 */
	public static String outputString(Element elem) {
		XMLOutputter xmlWriter = new XMLOutputter(Format.getPrettyFormat());

		return xmlWriter.outputString(elem);
	}

	/**
	 * It reads the given reader and returns the castor schema.
	 * 
	 * @param reader
	 *            The reader to read.
	 * 
	 * @return The castor schema created from the reader.
	 * 
	 * @throws IOException
	 *             If the schema could not be read from the reader.
	 */
	public static Schema readSchema(Reader reader) throws IOException {
		// create the sax input source
		InputSource inputSource = new InputSource(reader);

		// create the schema reader
		SchemaReader schemaReader = new SchemaReader(inputSource);
		schemaReader.setValidation(false);

		// read the schema from the source
		Schema schema = schemaReader.read();

		return schema;
	}
	
	public String getServiceLocation() {
		Object address = port.getExtensibilityElements().get(0);
	
		if(address instanceof SOAPAddress) {
			return ((SOAPAddress)address).getLocationURI();
		}
		if(address instanceof SOAP12Address) {
			return ((SOAP12Address)address).getLocationURI();
		}		
		else if(address instanceof HTTPAddress) {
			return ((HTTPAddress)address).getLocationURI();
		}
		
		else
			return address.toString();
		
	}


	public Definition getDefinition() {
		return definition;
	}

	public Service getService() {
		return service;
	}

	public Port getPort() {
		return port;
	}

}
