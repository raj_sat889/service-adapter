package com.jawise.serviceadapter.imp;

import java.io.IOException;

import com.jawise.serviceadapter.ValidationErrors;
import com.jawise.serviceadapter.core.Message;

public interface MessagePartsImporter {

	public abstract ValidationErrors importMessageParts(Message message,
			String importData, String importSourceType) throws IOException;

}