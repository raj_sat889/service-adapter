package com.jawise.serviceadapter;



public class PopUp {

	private String title;
	private String message = "";
	private String message2 = "";
	private String message3 = "";
    private boolean rendered;
    private boolean okonly;
    private boolean multiline;
    

	public boolean getMultiline() {
		
		if((message2 != null && !message2.isEmpty()) ||
				(message3 != null && !message3.isEmpty())) {
			multiline = true;
		}
		return multiline;
	}

	public void setMultiline(boolean multiline) {
		this.multiline = multiline;
	}

	public String getMessage2() {
		return message2;
	}

	public void setMessage2(String message2) {
		this.message2 = message2;
	}

	public String getMessage3() {
		return message3;
	}

	public void setMessage3(String message3) {
		this.message3 = message3;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isRendered() {
		return rendered;
	}

	public void setRendered(boolean rendered) {
		this.rendered = rendered;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean getOkonly() {
		return okonly;
	}

	public void setOkonly(boolean okonly) {
		this.okonly = okonly;		
	}
	public void clear(){
		message = "";
		message2 = "";
		message3 = "";
		okonly = false;
		
	}
}
