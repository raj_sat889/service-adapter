package com.jawise.serviceadapter;

import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.Message;

public class MessagePanel extends Panel {
	
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(MessagePartsPanel.class);

	private Message message;
	

	@Override
	public void startEdit(ActionEvent event) {

		backup(message);
		super.startEdit(event);
	}

	@Override
	public void save(ActionEvent event) throws Exception {
		
		Binding binding = service.getPort().getBinding();
		
		
		ValidationErrors errors = message.validateFor(binding);
		if (errors.isEmpty()) {
			ServiceRegistry.getInstance().save();
			super.save(event);
		} else {
			error("messageForm", errors);
		}
	}

	@Override
	public void cancelEdit(ActionEvent event) {
		restore(message);
		super.cancelEdit(event);
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

}
