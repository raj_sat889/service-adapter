package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.component.UIData;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.ImportSourceType;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.Protocol;
import com.jawise.serviceadapter.imp.CsvMessagePartsImporter;
import com.jawise.serviceadapter.imp.MessagePartsImporter;
import com.jawise.serviceadapter.imp.NvpMessagePartsImporter;
import com.jawise.serviceadapter.imp.XmlMessagePartsImporter;

public class MessagePartsPanel extends Panel {
	private static Logger logger = Logger.getLogger(MessagePartsPanel.class);
	private Part newpart;
	private Part selectedPart;
	private Message message;
	private boolean renderImporter;
	private String importData;
	private String importSourceType;

	private SelectItem[] types;
	
	public SelectItem[] getTypes() {
		
		if(types == null) {
			Collection<SelectItem> items = new ArrayList<SelectItem>();	
			
			Binding binding = service.getPort().getBinding();
			String protocol = binding.getProtocol();
			if(Protocol.XML_RPC.toString().equals(protocol)) {
				items.add(new SelectItem("String", "String"));
				items.add(new SelectItem("int", "int"));
				items.add(new SelectItem("i4", "i4"));
				items.add(new SelectItem("double", "double"));
				items.add(new SelectItem("dateTime.iso8601", "dateTime.iso8601"));
				items.add(new SelectItem("base64", "base64"));
				items.add(new SelectItem("boolean", "boolean"));
				items.add(new SelectItem("struct", "struct"));
				items.add(new SelectItem("array", "array"));												
			}			
			else if(Protocol.SOAP11.toString().equals(protocol)) {
				items.add(new SelectItem("string", "string"));
				items.add(new SelectItem("long", "long"));
				items.add(new SelectItem("float", "float"));
				items.add(new SelectItem("double", "double"));
				items.add(new SelectItem("int", "int"));
				items.add(new SelectItem("short", "short"));
				items.add(new SelectItem("boolean", "boolean"));
				items.add(new SelectItem("dateTime", "dateTime"));
				items.add(new SelectItem("base64", "base64Binary"));
				items.add(new SelectItem("decimal", "decimal"));
				items.add(new SelectItem("integer", "integer"));
				items.add(new SelectItem("anyURI", "anyURI"));
				items.add(new SelectItem("anyType", "anyType"));
				items.add(new SelectItem("struct", "struct"));
			
			}			
			else
				items.add(new SelectItem("String", "String"));
			
			types = new SelectItem[items.size()];
			types = items.toArray(types);			
		}
		return types;
	}

	public void setTypes(SelectItem[] types) {
		this.types = types;
	}
	
	public Part getNewpart() {
		return newpart;
	}

	public void setNewpart(Part newpart) {
		this.newpart = newpart;
	}

	protected Part selectRow(ActionEvent event) {
		UIData uiData = (UIData) event.getComponent().getParent().getParent();
		Part part = (Part) uiData.getRowData();
		logger.debug("selected part id: " + part.getId());
		return part;
	}

	public void deletePart(ActionEvent event) throws IOException {
		selectedPart = selectRow(event);
		popup.setTitle("Remove Part Conversion");
		popup.setMessage("Do you want to remove the part conversion ?");
		popup.setRendered(true);
	}

	@Override
	public String yes() throws IOException {
		deletePart();
		super.yes();
		return null;
	}

	public void deletePart() throws IOException {

		List<Part> parts = message.getParts();
		for (Part p : parts) {
			if (p.equals(selectedPart)) {
				parts.remove(p);
				ServiceRegistry.getInstance().save();
				break;
			}
		}
	}

	@Override
	public void cancelEdit(ActionEvent event) {
		restore(newpart);
		newpart = new Part();
		super.cancelEdit(event);
	}

	@Override
	public void save(ActionEvent event) throws Exception {

		ValidationErrors errors = newpart.validate();
		if (errors.isEmpty()) {
			ServiceRegistry.getInstance().save();
			super.save(event);
			getRequestMap().remove("newpart");
			newpart = new Part();
		} else {
			error("PartForm", errors);
		}
	}

	@Override
	public void startEdit(ActionEvent event) {
		newpart = selectRow(event);
		backup(newpart);
		super.startEdit(event);
	}

	public void addPart(ActionEvent event) {
		try {
			if (newpart != null) {
				newpart.setId(newpart.getName() + "_" + newpart.getType());
				ValidationErrors errors = newpart.validate();
				if (errors.size() == 0) {
					message.getParts().add(newpart);
					newpart = new Part();
					ServiceRegistry.getInstance().save();
				} else {
					error("PrtForm", errors);
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public void importParts(ActionEvent e) {
		renderImporter = true;
		return;
	}

	public String processImport() {

		ValidationErrors errors = new ValidationErrors();
		try {
			if (importData == null || importData.isEmpty()) {
				errors.add("import data is required");
			} else if (ImportSourceType.CSV.toString().equals(importSourceType)) {
				MessagePartsImporter i = new CsvMessagePartsImporter();
				errors = i.importMessageParts(message, importData,
						importSourceType);
			}
			else if (ImportSourceType.NVP.toString().equals(importSourceType)) {
				MessagePartsImporter i = new NvpMessagePartsImporter();
				errors = i.importMessageParts(message, importData,
						importSourceType);
			}			
			else if (ImportSourceType.XML.toString().equals(importSourceType)) {
				MessagePartsImporter i = new XmlMessagePartsImporter();
				errors = i.importMessageParts(message, importData,
						importSourceType);
			}					
			else {
				errors.add("source type support not implemented");
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			errors.add(e.getMessage());
		}

		if (errors.isEmpty()) {
			renderImporter = false;
			importData = "";
			importSourceType = "";			
		} else {
			error("importFM", errors);
		}
		
		return null;
	}

	public String cancelImport() {
		renderImporter = false;
		return null;
	}

	public boolean getRenderImporter() {
		return renderImporter;
	}

	public void setRenderImporter(boolean renderImporter) {
		this.renderImporter = renderImporter;
	}

	public String getImportData() {
		return importData;
	}

	public void setImportData(String importData) {
		this.importData = importData;
	}

	public String getImportSourceType() {
		return importSourceType;
	}

	public void setImportSourceType(String importSourceType) {
		this.importSourceType = importSourceType;
	}
}
