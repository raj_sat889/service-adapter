package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Adapter;
import com.jawise.serviceadapter.core.Service;
import com.jawise.serviceadapter.core.Services;

public class ServicePanel extends Panel {
	private static Logger logger = Logger.getLogger(ServicePanel.class);
	private Adapter newadapter;
	private String adapteename;
	private SelectItem[] servicenames;
	private Services services;

	public ServicePanel() throws IOException {

		Services services = ServiceRegistry.getInstance().getServices();
		Collection<SelectItem> items = new ArrayList<SelectItem>();
		for (Service service : services) {
			String name = service.getName();
			items.add(new SelectItem(name, name));
		}

		servicenames = new SelectItem[items.size()];
		items.toArray(servicenames);
	}

	public Adapter getNewadapter() {
		return newadapter;
	}

	public void setNewadapter(Adapter newadapter) {
		this.newadapter = newadapter;
	}

	public SelectItem[] getServicenames() {
		return servicenames;
	}

	public void setServicenames(SelectItem[] servicenames) {
		this.servicenames = servicenames;
	}

	public String getAdapteename() {
		return adapteename;
	}

	public void setAdapteename(String adapteename) {
		this.adapteename = adapteename;
	}

	private String doRegisterAdapter() {
		try {
			
			ValidationErrors errors = newadapter.validate();
			
			if(errors.size() != 0) {
				error("apForm", errors);
				return "";
			}			
			
			Service adaptee = ServiceRegistry.getInstance().findService(
					adapteename);

			if (adaptee == null) {
				error("adpterForm", "can not finds the requested adaptee");

			}

			newadapter.setService(adaptee);
			Map<String, Object> sessionMap = getSessionMap();
			Service service = (Service) sessionMap.get("service");

			Adapter oldAdapter = service.getAdapter();
			service.setAdapter(null);
			ServiceRegistry.getInstance().save();
			service.setAdapter(newadapter);
			ServiceRegistry.getInstance().save();
			DefaultMutableTreeNode node = serviceRegistryBrowser
					.getSelectednode();
			if (oldAdapter != null) {
				DefaultMutableTreeNode oldNode = serviceRegistryBrowser
						.findChildNode(node, service.getName(), oldAdapter);
				oldNode.removeFromParent();
			}

			serviceRegistryBrowser.addAdapter(service, node, newadapter);
			serviceRegistryBrowser.setSelectedPanel("emptyPanel");
			Map<String, Object> requestMap = getRequestMap();
			requestMap.remove("newadapter");
			
			newadapter = new Adapter();

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}

	public String registerAdapter() {
		if (service.getAdapter() == null) {
			doRegisterAdapter();
		} else {
			popup.setTitle("Confirm Action");
			popup.setMessage("This action will replace the current adapter ?");
			popup.setRendered(true);
		}
		return "";
	}

	@Override
	public String yes() throws IOException {
		if (popup.getMessage().indexOf("adapter") >= 0) {
			doRegisterAdapter();
			popup.setRendered(false);
		} else {
			removeService();
			popup.setRendered(false);
		}
		return null;
	}

	@Override
	public void startEdit(ActionEvent event) {

		backup(service);
		super.startEdit(event);
	}

	@Override
	public void save(ActionEvent event) throws Exception {

		ValidationErrors errors = service.validate();
		if (errors.isEmpty()) {
			ServiceRegistry.getInstance().save();
			super.save(event);
			// selected service node
			DefaultMutableTreeNode selectednode = serviceRegistryBrowser
					.getSelectednode();
			BrowserUserObject userObject = (BrowserUserObject) selectednode
					.getUserObject();
			userObject.setText(service.getName());
			String oldServiceName = userObject.getServicename();
			userObject.setServicename(service.getName());

			DefaultMutableTreeNode child = selectednode;
			while ((child = child.getNextNode()) != null) {
				if (child.getUserObject() instanceof BrowserUserObject) {

					BrowserUserObject userObj = (BrowserUserObject) child
							.getUserObject();
					if (oldServiceName.equals(userObj.getServicename())) {
						userObj.setServicename(service.getName());
					}
				}
			}

		} else {
			error("portForm", errors);
		}
	}

	@Override
	public void cancelEdit(ActionEvent event) {
		restore(service);
		super.cancelEdit(event);
	}

	public void remove(ActionEvent event) {

		try {
			if (isRemoveable(service)) {
				popup.setTitle("Remove Service");
				popup
						.setMessage("This action will remove the current service ?");
				popup.setRendered(true);
			} else {
				popup.setTitle("Remove Service");
				popup
						.setMessage("Can not remove this service due to dependency's.");
				popup.setOkonly(true);
				popup.setRendered(true);
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public boolean isRemoveable(Service service) throws IOException {

		ServiceRegistry registry = ServiceRegistry.getInstance();
		services = registry.getServices();
		for (Service s : services) {
			if (!s.equals(service)) {
				Adapter adapter = s.getAdapter();
				if (adapter != null) {
					Service adaptee = adapter.getService();
					if (adaptee.equals(service)
							|| adaptee.getName().equals(service.getName())) {
						return false;
					}
				}

			}
		}
		return true;
	}

	private void removeService() throws IOException {
		ServiceRegistry registry = ServiceRegistry.getInstance();
		services = registry.getServices();
		services.remove(service);
		ServiceRegistry.getInstance().save();

		service = null;
		Map<String, Object> sessionMap = getSessionMap();
		sessionMap.remove("service");
		serviceRegistryBrowser.setSelectedPanel("emptyPanel");
		serviceRegistryBrowser.getSelectednode().removeFromParent();
	}

}
