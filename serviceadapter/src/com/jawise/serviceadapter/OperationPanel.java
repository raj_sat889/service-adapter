package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Adapter;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Service;
import com.jawise.serviceadapter.core.Services;

public class OperationPanel extends Panel {
	private static Logger logger = Logger.getLogger(OperationPanel.class);
	private Operation operation;

	@Override
	public String yes() throws IOException {
		removeOperation();
		super.yes();
		return null;
	}

	public void remove(ActionEvent event) {

		try {
			if (isRemoveable(operation)) {
				popup.setTitle("Remove Operation");
				popup.setMessage("Do you want to remove the operation ?");
				popup.setRendered(true);
			} else {
				popup.setTitle("Remove Operation");
				popup
						.setMessage("Can not remove this operation due to dependancy's.");
				popup.setOkonly(true);
				popup.setRendered(true);
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}

	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	private void removeOperation() {
		try {
			Collection<Operation> operations = service.getPort().getBinding()
					.getPortyype().getOperations();
			operations.remove(operation);
			ServiceRegistry.getInstance().save();

			operation = null;
			ExternalContext externalContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			Map<String, String> requestParameterMap = externalContext
					.getRequestParameterMap();
			Map<String, Object> sessionMap = externalContext.getSessionMap();
			requestParameterMap.remove("operation");
			sessionMap.remove("operation");
			serviceRegistryBrowser.setSelectedPanel("emptyPanel");
			serviceRegistryBrowser.getSelectednode().removeFromParent();

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public boolean isRemoveable(Operation op) throws IOException {

		ServiceRegistry registry = ServiceRegistry.getInstance();
		Services services = registry.getServices();
		for (Service s : services) {		
			if (!s.equals(service)) {
				Adapter adapter = s.getAdapter();
				if (adapter != null) {
					Service adaptee = adapter.getService();
					if (adaptee.equals(service)
							|| adaptee.getName().equals(service.getName())) {						
						Collection<MessageConversion> mcs = adapter.getMessageConversions().values();
						for(MessageConversion mc : mcs) {							
							if(mc.getAdapteeoperation().equals(op.getName()) ||
									mc.getAdapteroperation().equals(op.getName())) {
								return false;
							}
						}						
						
					}
				}

			}
		}
		return true;
	}
}
