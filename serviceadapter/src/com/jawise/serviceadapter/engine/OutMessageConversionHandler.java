package com.jawise.serviceadapter.engine;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.MessageConverter;
import com.jawise.serviceadapter.convert.MessageConverterFactory;
import com.jawise.serviceadapter.convert.NVPHelper;
import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Protocol;
import com.jawise.serviceadapter.core.Service;

public class OutMessageConversionHandler extends AbstractHandler {

	@Override
	public void handleException(MessageContext ctx, Exception ex) {
		ctx.setException(ex);
		logerror(ctx, ex);
	}

	@Override
	public void invoke(MessageContext ctx) throws Exception {

		try {
			if (ctx.isError()) {
				return;
			}

			Service adapterService = (Service) ctx.get("adapterService");
			MessageConversion mc = (MessageConversion) ctx
					.get("messageConversion");
			Object response = ctx.get("remotereponse");
			Object obj = readResponse(adapterService, mc, response);

			ctx.put("messagse", obj);
			ctx.put("responsemessage", response);
			ctx.put("input", Boolean.FALSE);

			if (obj instanceof String) {
				if (((String) obj).trim().length() == 0) {
					throw new MessageException("1015");
				}
			}

			MessageConverter messageConverter = MessageConverterFactory
					.getMessageConverter(ctx);
			Object convertedMsg = messageConverter.doConvertion();

			String encodingStyle = adapterService.getPort().getBinding()
					.getEncodingStyle();
			boolean urlencode = false;
			if (encodingStyle.indexOf("UrlEncode") >= 0) {
				urlencode = true;
			}
			if (urlencode && convertedMsg instanceof String) {
				convertedMsg = URLEncoder
						.encode((String) convertedMsg, "UTF-8");
			}

			ctx.put("outmessage", convertedMsg);
		} catch (Exception ex) {
			handleException(ctx, ex);
		}

	}

	@SuppressWarnings("unchecked")
	private Object readResponse(Service adapterService, MessageConversion mc,
			Object response) throws UnsupportedEncodingException,
			MessageException {

		if (response == null) {
			throw new MessageException("1015");
		}

		Service adapteeService = adapterService.getAdapter().getService();
		Binding binding = adapteeService.getPort().getBinding();
		String encodingStyle = binding.getEncodingStyle();

		boolean urldecode = false;
		if (encodingStyle.indexOf("UrlEncodeRequest") >= 0) {
			urldecode = true;
		}

		Operation op = binding.getPortyype().findOperation(
				mc.getAdapteeoperation());
		String separator = op.getOutput().getSeparator();

		Protocol protocol = Protocol.valueOf(binding.getProtocol());
		if (Protocol.NVP_HTTP.equals(protocol)) {
			Map nvpMap = NVPHelper.parseNvpMessage((String) response,
					urldecode, separator);
			return nvpMap;
		}

		return response;
	}
}
