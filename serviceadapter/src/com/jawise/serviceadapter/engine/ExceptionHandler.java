package com.jawise.serviceadapter.engine;

import java.util.ResourceBundle;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.rpc.XmlRpcMessageFault;
import com.jawise.serviceadapter.convert.rpc.XmlRpcSerialiser;
import com.jawise.serviceadapter.convert.soap.binding.SoapFaultMessageBuilder;
import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.ResourceBundleSupport;
import com.jawise.serviceadapter.core.Service;
import com.jawise.serviceadapter.sec.AccessControlException;

public class ExceptionHandler extends AbstractHandler {

	@Override
	public void invoke(MessageContext ctx) throws Exception {

		if (ctx.isError()) {
			handleException(ctx, ctx.getException());
		}
	}

	@Override
	public void handleException(MessageContext ctx, Exception ex) {
		try {
			Object message = (Object) ctx.get("messagse");
			ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");

			if (ex instanceof XmlRpcMessageFault) {
				handleXmlRpcFault(ctx, (XmlRpcMessageFault) ex);
			} else if (isXmlRpc(ctx) || isXmlRpcMessage(message)) {
				handleXmlRpcError(ctx, ex);
			} else if (isSoap(ctx)) {
				handleSoapException(ctx, ex);
			} else if (ex instanceof MessageException) {
				handleMessageException(ctx, (MessageException) ex);
			} else if (ex instanceof AccessControlException) {
				handleAccessControlException(ctx, (AccessControlException) ex);
			} else {
				String msg = bundle.getString("1016");
				ctx.put("outmessage", msg);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	private void handleXmlRpcError(MessageContext ctx, Exception ex) {
		ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");
		String message = getLocalErrorMessage(ex, bundle);
		String rpcmsg = getXmlRpcError(ex.getMessage(), message);
		ctx.put("outmessage", rpcmsg);
	}

	private String getLocalErrorMessage(Exception ex, ResourceBundle bundle) {
		String message = ex.getMessage();
		if (ex instanceof ResourceBundleSupport) {
			message = ((ResourceBundleSupport) ex).getMessage(bundle);
		}
		return message;
	}

	private void handleXmlRpcFault(MessageContext ctx, XmlRpcMessageFault ex) {
		String msg = ex.getMessage();
		ctx.put("outmessage", msg);
	}

	private void handleSoapException(MessageContext ctx, Exception ex)
			throws Exception {
		SoapFaultMessageBuilder soapFaultBuilder = new SoapFaultMessageBuilder(
				ctx);
		String msg = soapFaultBuilder.buildFaultMessage();
		ctx.put("outmessage", msg);
	}

	public void handleMessageException(MessageContext ctx, MessageException ex)
			throws Exception {

		ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");
		String msg = ((MessageException) ex).getMessage(bundle);
		ctx.put("outmessage", msg);
	}

	public void handleAccessControlException(MessageContext ctx,
			AccessControlException ex) throws Exception {
		ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");
		String msg = ((AccessControlException) ex).getMessage(bundle);
		ctx.put("outmessage", msg);
	}

	private boolean isXmlRpcMessage(Object message) {
		return message != null && message instanceof String
				&& ((String) message).indexOf("<methodCall>") >= 0
				&& ((String) message).indexOf("<methodName>") >= 0;
	}

	private boolean isXmlRpc(MessageContext ctx) {
		Service adapterService = (Service) ctx.get("adapterService");
		if (adapterService != null) {
			Binding fromBinding = adapterService.getPort().getBinding();
			if (fromBinding.isXmlRpc()) {
				return true;
			}
		}
		return false;
	}

	private boolean isSoap(MessageContext ctx) {

		Object message = (Object) ctx.get("messagse");
		Service adapterService = (Service) ctx.get("adapterService");
		if (adapterService != null) {
			Binding fromBinding = adapterService.getPort().getBinding();
			if (fromBinding.isSoap()) {
				return true;
			}
		}
		if (message != null
				&& message.toString().indexOf("<soapenv:Envelope") >= 0) {
			return true;
		}
		return false;
	}

	private String getXmlRpcError(String errorcode, String errormsg) {
		String msg;
		int error;
		try {
			error = Integer.valueOf(errorcode).intValue();
		} catch (NumberFormatException e1) {
			error = 2000;
		}
		msg = new XmlRpcSerialiser().serialise(error, errormsg);
		return msg;
	}
}
