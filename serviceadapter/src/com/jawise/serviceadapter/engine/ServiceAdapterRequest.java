package com.jawise.serviceadapter.engine;

public class ServiceAdapterRequest {
	private String userid;
	private String service;
	private String messageconversion;
	private String remoteaddress;
	
	public ServiceAdapterRequest(String userid, String service, String mcname, String remoteaddress) {
		super();
		this.userid = userid;
		this.service = service;
		this.messageconversion = mcname;
		this.remoteaddress = remoteaddress;
	}

	public String getUserid() {
		return userid;
	}

	public String getService() {
		return service;
	}

	public boolean isValid() {
		boolean missing = userid == null || userid.isEmpty() || service == null
				|| service.isEmpty() || messageconversion == null || messageconversion.isEmpty();
		return !missing;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("userid=");
		buffer.append(userid);
		buffer.append(" service=");
		buffer.append(service);
		buffer.append(" messageconversion=");
		buffer.append(messageconversion);	
		buffer.append(" remoteaddress=");
		buffer.append(remoteaddress);			
		return buffer.toString();
	}

	public String getMessageconversion() {
		return messageconversion;
	}

	public String getRemoteaddress() {
		return remoteaddress;
	}
}
