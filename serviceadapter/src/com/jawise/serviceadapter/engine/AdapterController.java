package com.jawise.serviceadapter.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.sec.LicenseValidator;

public class AdapterController extends AbstractController {

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(AdapterController.class);
	private List<Handler> handlers = new ArrayList<Handler>();
	private String licensekey = "";
	private String userid = "";
	private String company = "";
	private String endtime = "" + (getMiliseconds() + Long.valueOf("3600000"));
	private boolean licensevalidity = false;
	private boolean trialnotifcation;

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		try {
			isLicenceValid();
			processRequest(req, res);
		} catch (LicenseException e) {
			processLicenseException(req, res);
		}
		return null;
	}

	private void processLicenseException(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		if (!trialnotifcation) {
			logger
					.info("license invalid, running in trial mode and will stop in 1 hour");
			trialnotifcation = true;
		}
		if (getMiliseconds() > Long.valueOf(endtime)) {
			logger.error("license invalid, can not process the requests");
		} else {
			processRequest(req, res);
		}
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		MessageContext ctx = createContext(req, res);
		setLoggingContext(ctx.getId());
		sendRequestToHandler(ctx);
	}

	private void sendRequestToHandler(MessageContext ctx) throws Exception {

		for (Handler h : handlers) {

			List<Handler> preHandlers = h.getPreInvokeHandlers();
			for (Handler post : preHandlers) {
				post.invoke(ctx);
			}

			h.invoke(ctx);

			List<Handler> postHandlers = h.getPostInvokeHandlers();
			for (Handler pre : postHandlers) {
				pre.invoke(ctx);
			}
		}
	}

	private long getMiliseconds() {
		return System.currentTimeMillis();
	}

	private void setLoggingContext(String ctxvalue) {
		NDC.clear();
		if (ctxvalue != null) {
			ctxvalue = ctxvalue.replace('{', ' ');
			ctxvalue = ctxvalue.replace('}', ' ');
			ctxvalue = ctxvalue.trim();
		}
		NDC.push(ctxvalue);
	}

	private void isLicenceValid() throws Exception {
		return;
	}

	private MessageContext createContext(HttpServletRequest req,
			HttpServletResponse res) {
		MessageContext ctx = new MessageContext();
		ctx.put(MessageContext.REQUEST, req);
		ctx.put(MessageContext.RESPONSE, res);
		ctx.put("licensevalidity", licensevalidity);
		ResourceBundle bundle = ResourceBundle
				.getBundle("com.jawise.serviceadapter.engine.messages");
		ctx.put("bundle", bundle);
		return ctx;
	}

	public List<Handler> getHandlers() {
		return handlers;
	}

	public void setHandlers(List<Handler> handlers) {
		this.handlers = handlers;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getLicensekey() {
		return licensekey;
	}

	public void setLicensekey(String licensekey) {
		this.licensekey = licensekey;
	}
}