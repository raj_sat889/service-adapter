package com.jawise.serviceadapter.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.ResourceBundleSupport;

public abstract class AbstractHandler implements Handler {
	protected static Logger logger = Logger.getLogger(AbstractHandler.class);
	private List<Handler> postInvokeHandlers = new ArrayList<Handler>();
	private List<Handler> preInvokeHandlers = new ArrayList<Handler>();

	public void setPostInvokeHandlers(List<Handler> postInvokeHandlers) {
		this.postInvokeHandlers = postInvokeHandlers;
	}

	public void setPreInvokeHandlers(List<Handler> preInvokeHandlers) {
		this.preInvokeHandlers = preInvokeHandlers;
	}

	@Override
	public List<Handler> getPostInvokeHandlers() {
		return postInvokeHandlers;
	}

	@Override
	public List<Handler> getPreInvokeHandlers() {

		return preInvokeHandlers;
	}

	protected void logerror(MessageContext ctx, Exception ex) {

		String message = ex.getMessage();
		if (message != null && message.length() == 4
				&& ex instanceof ResourceBundleSupport) {
			ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");
			if (bundle != null) {
				logger.error(((ResourceBundleSupport) ex).getMessage(bundle),
						ex);
				return;
			}

		}

		logger.error(ex.getMessage(), ex);
	}
}
