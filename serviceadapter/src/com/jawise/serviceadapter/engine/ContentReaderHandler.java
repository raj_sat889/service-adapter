package com.jawise.serviceadapter.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Protocol;
import com.jawise.serviceadapter.core.Service;

public class ContentReaderHandler extends AbstractHandler {

	@Override
	public void handleException(MessageContext ctx, Exception ex) {
		ctx.setException(ex);
		logger.error(ex.getMessage(), ex);
	}

	@Override
	public void invoke(MessageContext ctx) throws Exception {
		try {
			readAndStoreContent(ctx);
		} catch (Exception ex) {
			handleException(ctx, ex);
		}

	}

	private void readAndStoreContent(MessageContext ctx) throws IOException,
			MessageException {
		HttpServletRequest req = (HttpServletRequest) ctx
				.get(MessageContext.REQUEST);
		Service adapterService = (Service) ctx
				.get(MessageContext.ADAPTER_SERVICE);
		Object messagse = null;

		if (adapterService != null)
			messagse = readContent(adapterService, req);
		else
			messagse = readRawContent(req);
		logger.debug(messagse);
		ctx.put("messagse", messagse);
	}

	private Object readContent(Service adapterService, HttpServletRequest req)
			throws IOException, MessageException {

		Protocol protocol = Protocol.valueOf(adapterService.getPort()
				.getBinding().getProtocol());

		if (Protocol.NVP_HTTP.equals(protocol)) {
			return req.getParameterMap();
		} else if (Protocol.XML_HTTP.equals(protocol)) {
			String xml = req.getParameter("xml");
			if (xml != null && !xml.isEmpty()) {
				return xml;
			}
		}
		String content = readRawContent(adapterService, req);
		content = applyContentEncoding(adapterService, content);
		return content;

	}

	private String applyContentEncoding(Service adapterService, String content)
			throws UnsupportedEncodingException {
		Binding binding = adapterService.getPort().getBinding();
		if (binding.isUrlEncodingRequired()) {
			content = URLEncoder.encode(content, "UTF-8");
		}
		return content;
	}

	private String readRawContent(Service adapterService, HttpServletRequest req)
			throws IOException, MessageException, UnsupportedEncodingException {
		String content = readRawContent(req);
		if (content == null || content.isEmpty()) {
			throw new MessageException("1014");
		}
		return content;
	}

	private String readRawContent(HttpServletRequest request)
			throws IOException {
		StringBuffer buf = new StringBuffer();
		BufferedReader reader = request.getReader();
		int contentLength = request.getContentLength();

		if (contentLength <= 0) {
			return "";
		}
		char data[] = new char[contentLength];
		while (reader.read(data, 0, contentLength) != -1) {
			buf.append(data);
		}
		reader.read(data);
		return buf.toString();
	}
}
