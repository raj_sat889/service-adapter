package com.jawise.serviceadapter.engine;

import java.util.ResourceBundle;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.MessageConverter;
import com.jawise.serviceadapter.convert.MessageConverterFactory;
import com.jawise.serviceadapter.core.MessageException;

public class InMessageConversionHandler extends AbstractHandler {

	@Override
	public void handleException(MessageContext ctx, Exception ex) {
		ctx.setException(ex);

		ResourceBundle bundle = ResourceBundle
				.getBundle("com.jawise.serviceadapter.core.messages");

		if(ex instanceof MessageException) {			
			logger.error(((MessageException)ex).getMessage(bundle), ex);
		}
		logger.error(ex.getMessage(), ex);
	}

	@Override
	public void invoke(MessageContext ctx) throws Exception {

		try {
			if (ctx.isError()) {
				return;
			}

			logger.debug("processing conversion");
			ctx.put(MessageContext.INPUT, Boolean.TRUE);
			MessageConverter messageConverter = MessageConverterFactory
					.getMessageConverter(ctx);
			Object convertedMsg = messageConverter.doConvertion();
			ctx.put(MessageContext.CONVERTEDMESSAGE, convertedMsg);

		} catch (Exception ex) {
			handleException(ctx, ex);
		}

	}

}
