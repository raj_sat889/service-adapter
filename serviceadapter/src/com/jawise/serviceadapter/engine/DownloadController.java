package com.jawise.serviceadapter.engine;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.jawise.serviceadapter.ServiceRegistry;

public class DownloadController extends AbstractController {

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		ServiceRegistry registry = ServiceRegistry.getInstance();
		String service = req.getParameter("service");
		InputStream exportstream = null;

		if ("All Services".equals(service)) {
			File file = new File(registry.getFilename());
			exportstream = new FileInputStream(file);
			if (!file.exists()) {
				logger.debug("file does not exist");
				return null;
			}
		} else {
			if (registry.findService(service) == null) {
				logger.debug("service does not exist");
				return null;
			}
			String services = registry.exportServiceAsString(service);
			exportstream = new ByteArrayInputStream(services.getBytes());
		}

		String contentType = URLConnection.guessContentTypeFromName(registry
				.getFilename());
		if (contentType == null) {
			contentType = "text/xml";
		}

		// Prepare streams.
		BufferedInputStream input = null;
		BufferedOutputStream output = null;

		try {
			// Open file.
			input = new BufferedInputStream(exportstream);
			int contentLength = input.available();

			// Init servlet response.
			res.reset();
			res.setContentLength(contentLength);
			res.setContentType(contentType);
			// res.setHeader("Content-disposition", "attachment; filename=\""
			// + registtryfile + "\"");
			res.setHeader("Content-disposition",
					"attachment; filename=registry.xml");
			output = new BufferedOutputStream(res.getOutputStream());

			// Write file contents to response.
			int data;
			while ((data = input.read()) != -1) {
				output.write(data);
			}

			// Finalize task.
			output.flush();
		} catch (IOException e) {
			// Something went wrong?
			e.printStackTrace();
		} finally {
			// Gently close streams.
			close(output);
			close(input);
		}

		return null;
	}

	private static void close(Closeable resource) {
		if (resource != null) {
			try {
				resource.close();
			} catch (IOException e) {
				// Do your thing with the exception. Print it, log it or mail
				// it.
				e.printStackTrace();
			}
		}
	}

}
