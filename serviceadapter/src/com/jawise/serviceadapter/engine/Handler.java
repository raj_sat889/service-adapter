package com.jawise.serviceadapter.engine;

import java.util.List;

import com.jawise.serviceadapter.convert.MessageContext;

public interface Handler {
	public void invoke(MessageContext ctx) throws Exception;;

	void handleException(MessageContext ctx, Exception ex);

	public List<Handler> getPostInvokeHandlers();

	public List<Handler> getPreInvokeHandlers();
}
