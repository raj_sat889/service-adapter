package com.jawise.serviceadapter.engine;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.Service;

public class ContentWriterHandler extends AbstractHandler {

	@Override
	public void handleException(MessageContext ctx, Exception ex) {
		logerror(ctx, ex);
	}

	@Override
	public void invoke(MessageContext ctx) throws Exception {

		writeContent(ctx);
	}

	private void writeContent(MessageContext ctx) throws IOException,
			SOAPException {

		HttpServletResponse res = (HttpServletResponse) ctx.get("response");		
		PrintWriter writer = res.getWriter();
		String message = getContentToWrite(ctx);

		setResponseContentType(ctx, res);
		logger.debug(message);
		writeContent(writer, message);
	}

	private void setResponseContentType(MessageContext ctx,
			HttpServletResponse res) {
		Service adapterService = (Service) ctx.get("adapterService");
		if (ctx.isError() || adapterService.getPort().getBinding().isSoap()) {
			res.setContentType("text/xml");
		}
	}

	private void writeContent(PrintWriter writer, String content) {
		writer.write(content);
		writer.write("\r\n");
	}

	private String getContentToWrite(MessageContext ctx) throws SOAPException,
			IOException {

		Object outmessage = ctx.get("outmessage");
		if (outmessage instanceof SOAPMessage) {
			ByteArrayOutputStream str = new ByteArrayOutputStream();
			((SOAPMessage) outmessage).writeTo(str);
			return new String(str.toByteArray());
		} else {
			return (String) outmessage;
		}

	}

}
