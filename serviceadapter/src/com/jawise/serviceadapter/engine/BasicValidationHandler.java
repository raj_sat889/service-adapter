package com.jawise.serviceadapter.engine;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import com.jawise.serviceadapter.ServiceRegistry;
import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.Adapter;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Service;
import com.jawise.serviceadapter.sec.AccessControlException;
import com.jawise.serviceadapter.sec.AccessControlService;

public class BasicValidationHandler extends AbstractHandler {

	private static final String MESSAGECONVERSION = "messageconversion";
	private static final String SERVICE = "service";
	private static final String USERID = "userid";

	private ServiceAdapterRequest createServiceAdapterRequest(MessageContext ctx)
			throws MessageException {
		HttpServletRequest req = (HttpServletRequest) ctx.get("request");
		ServiceAdapterRequest serviceAdapterRequest = new ServiceAdapterRequest(
				req.getParameter(USERID), req.getParameter(SERVICE), req
						.getParameter(MESSAGECONVERSION), req.getRemoteAddr());

		return serviceAdapterRequest;
	}

	@Override
	public void handleException(MessageContext ctx, Exception ex) {
		ctx.setException(ex);
		logerror(ctx, ex);
	}

	@Override
	public void invoke(MessageContext ctx) throws Exception {
		try {
			logger.info("handling request " + ctx.getId());
			validateAndSetupMessageContext(ctx);

		} catch (Exception ex) {
			handleException(ctx, ex);
		}
	}

	private Service loadAdapterService(
			ServiceAdapterRequest serviceAdapterRequest) throws IOException,
			MessageException, AccessControlException {
		ServiceRegistry registry = ServiceRegistry.getInstance();
		String servicename = serviceAdapterRequest.getService();
		Service adapterService = registry.findService(servicename);

		if (adapterService == null) {
			throw new MessageException("1025");
		}

		return adapterService;
	}

	private MessageConversion loadMessageConversion(
			ServiceAdapterRequest serviceAdapterRequest, Service adapterService)
			throws MessageException {
		Adapter adapter = adapterService.getAdapter();

		if (adapter == null) {
			throw new MessageException("1002");
		}
		MessageConversion mc = null;
		mc = adapter.getMessageConversions().get(
				serviceAdapterRequest.getMessageconversion());
		if (mc == null) {
			throw new MessageException("1003");
		}
		return mc;
	}

	private void setupMessageContext(MessageContext ctx,
			ServiceAdapterRequest serviceAdapterRequest, Service adapterService)
			throws MessageException {
		MessageConversion messageConversion = loadMessageConversion(
				serviceAdapterRequest, adapterService);
		ctx.put(MessageContext.SERVICE_ADAPTER_REQUEST, serviceAdapterRequest);
		ctx.put(MessageContext.ADAPTER_SERVICE, adapterService);
		ctx.put(MessageContext.MESSAGE_CONVERSION, messageConversion);
	}

	private void validateAccesToService(
			ServiceAdapterRequest serviceAdapterRequest, Service adapterService)
			throws AccessControlException {
		AccessControlService acs = new AccessControlService();
		acs.validateAccess(serviceAdapterRequest, adapterService);
	}

	private void validateAndSetupMessageContext(MessageContext ctx)
			throws MessageException, IOException, AccessControlException {

		ServiceAdapterRequest serviceAdapterRequest = createServiceAdapterRequest(ctx);
		logger.debug("processing request " + serviceAdapterRequest.toString());
		validateServiceAdapterRequest(ctx, serviceAdapterRequest);

		Service adapterService = loadAdapterService(serviceAdapterRequest);
		validateAccesToService(serviceAdapterRequest, adapterService);

		setupMessageContext(ctx, serviceAdapterRequest, adapterService);
	}

	public void validateServiceAdapterRequest(MessageContext ctx,
			ServiceAdapterRequest serviceAdapterRequest)
			throws MessageException {
		ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");
		if (!serviceAdapterRequest.isValid()) {
			throw new MessageException(bundle.getString("1001"));
		}
	}

}
