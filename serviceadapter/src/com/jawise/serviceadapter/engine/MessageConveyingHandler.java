package com.jawise.serviceadapter.engine;

import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ResourceBundle;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.rpc.XmlRpcMessageFault;
import com.jawise.serviceadapter.convert.rpc.XmlRpcSerialiser;
import com.jawise.serviceadapter.convey.MessageConveyer;
import com.jawise.serviceadapter.convey.MessageConveyerFactory;
import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Protocol;
import com.jawise.serviceadapter.core.Service;

public class MessageConveyingHandler extends AbstractHandler {

	@Override
	public void handleException(MessageContext ctx, Exception ex) {
		ctx.setException(ex);
		logger.error(ex.getMessage(), ex);
	}

	@Override
	public void invoke(MessageContext ctx) throws Exception {

		if (ctx.isError()) {
			return;
		}

		ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");
		Service adapterService = (Service) ctx.get("adapterService");
		Object convertedmessage = ctx.get("convertedmessage");
		try {

			String location = adapterService.getAdapter().getService()
					.getPort().getLocation();
			URL url = new URL(location);
			MessageConveyer messageConveyer = MessageConveyerFactory
					.getMessageConveyer(adapterService);

			// apply any content encoding
			Service adapteeService = adapterService.getAdapter().getService();
			Binding toBinding = adapteeService.getPort().getBinding();

			long readtimeout = toBinding.getReadtimeout();
			if (readtimeout > 0)
				messageConveyer.setReadTimeout(readtimeout);

			Protocol protocol = Protocol.valueOf(toBinding.getProtocol());
			if (Protocol.XML_HTTP.equals(protocol)
					|| Protocol.XML_RPC.equals(protocol)) {
				messageConveyer.setContentType("text/xml");
			}
			String encodingStyle = toBinding.getEncodingStyle();
			if (convertedmessage instanceof String &&
					encodingStyle.indexOf("UrlEncodeRequest") >= 0) {
				convertedmessage = URLEncoder.encode((String)convertedmessage, "UTF-8");
			}
			Object response = messageConveyer.convey(url, convertedmessage);

			if (response instanceof String
					&& encodingStyle.indexOf("UrlEncodeRequest") >= 0) {
				response = URLDecoder.decode((String)response, "UTF-8");
			}

			ctx.put("remotereponse", response);
		} catch (MessageException ex) {
			Binding fromBinding = adapterService.getPort().getBinding();
			Protocol protocol = Protocol.valueOf(fromBinding.getProtocol());
			if (Protocol.XML_RPC.equals(protocol)) {
				String fault = new XmlRpcSerialiser().serialise(1017, ex
						.getMessage(bundle));
				handleException(ctx, new XmlRpcMessageFault(fault));
			} else {
				handleException(ctx, ex);
			}
		} catch (Exception ex) {
			handleException(ctx, ex);
		}

	}

}
