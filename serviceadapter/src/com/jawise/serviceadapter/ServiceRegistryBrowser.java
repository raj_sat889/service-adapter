package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Adapter;
import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Port;
import com.jawise.serviceadapter.core.Service;
import com.jawise.serviceadapter.core.Services;

public class ServiceRegistryBrowser {
	@SuppressWarnings("unused")
	private static Logger logger = Logger
			.getLogger(ServiceRegistryBrowser.class);

	private DefaultTreeModel registryModel;
	private DefaultMutableTreeNode rootTreeNode;
	private BrowserUserObject selectedBrowserNode;
	private String selectedPanel = "servicesPanel";
	private DefaultMutableTreeNode selectednode;
	int id;

	/*
	 * private Service service; private Operation operation; private Message
	 * message; private Part part; private Adapter adapter;
	 */

	public BrowserUserObject getSelectedUserObject() {
		return selectedBrowserNode;
	}

	public ServiceRegistryBrowser() throws IOException {
		rootTreeNode = new DefaultMutableTreeNode();
		BrowserUserObject rootObject = new BrowserUserObject(rootTreeNode);
		rootObject.setText("Services");
		rootObject.setExpanded(true);
		setImages(rootObject);
		rootObject.setServicename("Services");
		rootObject.setId(0);
		rootTreeNode.setUserObject(rootObject);
		registryModel = new DefaultTreeModel(rootTreeNode);

		addServices();

	}

	@SuppressWarnings("unused")
	private void addServices() throws IOException {
		rootTreeNode.removeAllChildren();
		Services services = ServiceRegistry.getInstance().getServices();
		id = 0;
		for (Service s : services) {
			addService(s);

		}
	}

	public void addService(Service s) {
		id++;
		DefaultMutableTreeNode serviceNode = addNode(rootTreeNode, s.getName(),
				s.getName(), id, s, false);
		id++;
		Port port = s.getPort();
		DefaultMutableTreeNode portNode = addNode(serviceNode, s.getName(),
				port.getName(), id, port, false);
		// port or porttype
		id++;
		Binding binding = port.getBinding();
		DefaultMutableTreeNode bindingNode = addNode(portNode, s.getName(),
				binding.getName(), id, binding, false);

		// add operations
		Collection<Operation> operations = binding.getPortyype()
				.getOperations();
		for (Operation o : operations) {
			addOperation(s, bindingNode, o);
		}

		id++;

		Adapter adapter = s.getAdapter();
		if (adapter != null) {
			addAdapter(s, serviceNode, adapter);
		}
	}

	public void addAdapter(Service s, DefaultMutableTreeNode serviceNode,
			Adapter adapter) {
		DefaultMutableTreeNode adapterNode = addNode(serviceNode, s.getName(),
				adapter.getName(), id, adapter, false);

		Map<String, MessageConversion> messageConversions = adapter
				.getMessageConversions();
		Set<String> keySet = messageConversions.keySet();
		for (String k : keySet) {
			MessageConversion mc = messageConversions.get(k);
			addMessageConversion(s, adapterNode, mc);

		}
	}

	public void addMessageConversion(Service s,
			DefaultMutableTreeNode adapterNode, MessageConversion mc) {
		id++;
		DefaultMutableTreeNode msgTransNode = addNode(adapterNode, s.getName(),
				mc.getName(), id, mc, false);
		id++;
		addNode(msgTransNode, s.getName(), "InputPartConversion", id, mc, true);
		id++;
		addNode(msgTransNode, s.getName(), "OutputPartConversion", id, mc, true);
	}

	public void addOperation(Service s, DefaultMutableTreeNode bindingNode,
			Operation o) {
		id++;
		DefaultMutableTreeNode opNode = addNode(bindingNode, s.getName(), o
				.getName(), id, o, false);
		id++;
		Message input = o.getInput();
		if (input != null) {
			addNode(opNode, s.getName(), input.getName(), id, input, true);
		}

		id++;
		Message output = o.getOutput();
		if (output != null) {
			addNode(opNode, s.getName(), output.getName(), id, output, true);
		}
	}

	public DefaultMutableTreeNode addNode(DefaultMutableTreeNode parent,
			String servicename, String name, int id, Object data, boolean leaf) {
		DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode();

		BrowserUserObject nodeObject = new BrowserUserObject(parent);
		nodeObject.setText(name);
		nodeObject.setLeaf(leaf);
		nodeObject.setServicename(servicename);
		nodeObject.setId(id);
		nodeObject.setData(data);

		setImages(nodeObject);
		leafNode.setUserObject(nodeObject);
		parent.add(leafNode);
		return leafNode;
	}

	private void setImages(BrowserUserObject nodeObject) {

		String leafIcon = "tree_document.gif";
		String branchExpandedIcon = "tree_folder_open.gif";
		String branchContractedIcon = "tree_folder_close.gif";

		if (nodeObject.getData() instanceof Service) {
			leafIcon = "service.gif";
			branchExpandedIcon = "service.gif";
			branchContractedIcon = "service.gif";
		} else if (nodeObject.getData() instanceof Port) {
			leafIcon = "port.gif";
			branchExpandedIcon = "port.gif";
			branchContractedIcon = "port.gif";
		} else if (nodeObject.getData() instanceof Operation) {
			leafIcon = "operation.gif";
			branchExpandedIcon = "operation.gif";
			branchContractedIcon = "operation.gif";
		} else if (nodeObject.getData() instanceof Binding) {
			leafIcon = "binding.gif";
			branchExpandedIcon = "binding.gif";
			branchContractedIcon = "binding.gif";
		} else if (nodeObject.getData() instanceof Message) {
			leafIcon = "message.gif";
			branchExpandedIcon = "message.gif";
			branchContractedIcon = "message.gif";
		} else if (nodeObject.getData() instanceof Adapter) {
			leafIcon = "adapter.gif";
			branchExpandedIcon = "adapter.gif";
			branchContractedIcon = "adapter.gif";
		} else if ("InputPartConversion".equals(nodeObject.getText())
				|| "OutputPartConversion".equals(nodeObject.getText())) {
			leafIcon = "partconversion.gif";
			branchExpandedIcon = "partconversion.gif";
			branchContractedIcon = "partconversion.gif";
		} else if (nodeObject.getData() instanceof MessageConversion) {
			leafIcon = "conversion.gif";
			branchExpandedIcon = "conversion.gif";
			branchContractedIcon = "conversion.gif";
		} else if ("Services".equals(nodeObject.getText())) {
			leafIcon = "services.gif";
			branchExpandedIcon = "services.gif";
			branchContractedIcon = "services.gif";
		}

		nodeObject.setLeafIcon(leafIcon);
		nodeObject.setBranchExpandedIcon(branchExpandedIcon);
		nodeObject.setBranchContractedIcon(branchContractedIcon);
	}

	public void nodeSelected(ActionEvent event) throws IOException {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		Map<String, String> parameterMap = externalContext
				.getRequestParameterMap();
		String serviceName = parameterMap.get("servicename");
		String nodeId = parameterMap.get("nodeid");

		selectednode = findTreeNode(serviceName, nodeId);
		if (selectednode != null) {
			selectedBrowserNode = (BrowserUserObject) selectednode
					.getUserObject();
			selectCurrentRenderer(serviceName, selectedBrowserNode.getData());

		}
	}

	private void selectCurrentRenderer(String serviceName, Object data)
			throws IOException {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		Map<String, Object> requestMap = externalContext.getRequestMap();

		String text = selectedBrowserNode.getText();
		if (data instanceof Service) {
			requestMap.remove("servicePanel");
			selectedPanel = "servicePanel";
		} else if (data instanceof Port) {
			requestMap.remove("portPanel");
			selectedPanel = "portPanel";
		} else if (data instanceof Binding) {
			requestMap.remove("bindingPanel");
			selectedPanel = "bindinPanel";
		} else if (data instanceof Operation) {
			requestMap.remove("operationPanel");
			sessionMap.put("operation", data);
			selectedPanel = "operationPanel";
		} else if (data instanceof Message) {
			requestMap.remove("newpart");
			requestMap.remove("messagePanel");
			requestMap.remove("messagePartsPanel");
			sessionMap.put("message", data);
			selectedPanel = "messagePanel";
		} else if (data instanceof Adapter) {
			sessionMap.put("adapter", data);
			requestMap.remove("adapterPanel");
			selectedPanel = "adapterPanel";
		} else if ("InputPartConversion".equals(text)) {
			requestMap.remove("partConversionPanel");
			selectedPanel = "partsConversionPanel";
			if (!((MessageConversion) data).getName().equals(text)) {
				sessionMap.put("conversionnodename", text);
			}
			sessionMap.put("conversion", data);
		} else if ("OutputPartConversion".equals(text)) {
			requestMap.remove("partConversionPanel");
			selectedPanel = "partsConversionPanel";
			if (!((MessageConversion) data).getName().equals(text)) {
				sessionMap.put("conversionnodename", text);
			}
			sessionMap.put("conversion", data);
		} else if (data instanceof MessageConversion) {
			requestMap.remove("conversionPanel");
			sessionMap.put("conversion", data);
			selectedPanel = "conversionPanel";
		} else if ("Services".equals(text)) {
			requestMap.remove("newservice");
			requestMap.remove("service");
			requestMap.remove("servicesPanel");
			selectedPanel = "servicesPanel";
		} else {
			selectedPanel = "emptyPanel";
		}

		Service service = ServiceRegistry.getInstance()
				.findService(serviceName);
		sessionMap.put("service", service);
	}

	@SuppressWarnings("unchecked")
	private DefaultMutableTreeNode findTreeNode(String servicename,
			String nodeid) {

		if (servicename == null || nodeid == null)
			return null;

		DefaultMutableTreeNode rootNode = rootTreeNode;
		DefaultMutableTreeNode node;
		BrowserUserObject tmp;
		Enumeration nodes = rootNode.depthFirstEnumeration();
		while (nodes.hasMoreElements()) {
			node = (DefaultMutableTreeNode) nodes.nextElement();
			if (node.getUserObject() instanceof BrowserUserObject) {
				tmp = (BrowserUserObject) node.getUserObject();
				if (servicename.equals(tmp.getServicename())
						&& nodeid.equals("" + tmp.getId())) {
					return node;
				}
			}
		}
		return null;

	}

	@SuppressWarnings("unchecked")
	public DefaultMutableTreeNode findChildNode(DefaultMutableTreeNode parent,
			String service, Object data) {
		Enumeration nodes = parent.children();
		while (nodes.hasMoreElements()) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) nodes
					.nextElement();
			if (node.getUserObject() instanceof BrowserUserObject) {
				BrowserUserObject tmp = (BrowserUserObject) node
						.getUserObject();
				if (service.equals(tmp.getServicename())
						&& data.equals(tmp.getData())) {
					return node;
				}
			}
		}
		return null;
	}

	public DefaultTreeModel getRegistryModel() {
		return registryModel;
	}

	public void setRegistryModel(DefaultTreeModel registryModel) {
		this.registryModel = registryModel;
	}

	public String getSelectedPanel() {
		return selectedPanel;
	}

	public void setSelectedPanel(String selectedPanel) {
		this.selectedPanel = selectedPanel;
	}

	public String refresh() throws IOException {
		addServices();
		return "";
	}

	public String refreshAll() throws IOException {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		Map<String, Object> requestMap = externalContext.getRequestMap();
		requestMap.remove(selectedPanel);

		Map<String, Object> sessionMap = externalContext.getSessionMap();
		sessionMap.remove(selectedPanel);
		sessionMap.remove("service");

		ServiceRegistry.getInstance().load();
		addServices();
		selectedPanel = "emptyPanel";
		return "";
	}

	public String newService() {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		Map<String, Object> requestMap = externalContext.getRequestMap();
		requestMap.remove("newservice");
		requestMap.remove("service");
		requestMap.remove("servicesPanel");
		selectedPanel = "servicesPanel";
		return "";
	}

	public String importWebService() {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		Map<String, Object> requestMap = externalContext.getRequestMap();
		requestMap.remove("newservice");
		requestMap.remove("service");
		requestMap.remove("webServiceImportPanel");

		selectedPanel = "webServiceImportPanel";
		return "";
	}

	public String exportRegistry() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, Object> map = context.getExternalContext().getSessionMap();
		map.put("registtryfile", ServiceRegistry.getInstance().getFilename());
		map.put("filenametosend", "registry.xml");
		return "exportc";
	}

	public DefaultMutableTreeNode getSelectednode() {
		return selectednode;
	}

	public void setSelectednode(DefaultMutableTreeNode selectednode) {
		this.selectednode = selectednode;
	}

	public BrowserUserObject getSelectedBrowserNode() {
		return selectedBrowserNode;
	}

	public void setSelectedBrowserNode(BrowserUserObject selectedBrowserNode) {
		this.selectedBrowserNode = selectedBrowserNode;
	}
}
