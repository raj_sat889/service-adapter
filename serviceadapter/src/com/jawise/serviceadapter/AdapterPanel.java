package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Adapter;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Service;

public class AdapterPanel extends Panel {
	private static Logger logger = Logger.getLogger(AdapterPanel.class);
	private Adapter adapter;
	private MessageConversion messageConversion;
	private SelectItem[] adapterOperations;
	private SelectItem[] adapteeOperations;

	public String registerMessageConversion() {
		try {
			Map<String, MessageConversion> messageConversions = adapter
					.getMessageConversions();
			ValidationErrors errors = messageConversion.validate();
			if (errors.size() == 0) {
				
				if(messageConversions.containsKey(messageConversion.getName())) {
					error("mcForm","message conversion name exist already");
					return "";
				}
				messageConversions.put(messageConversion.getName(),
						messageConversion);
				ServiceRegistry.getInstance().save();
				DefaultMutableTreeNode selectednode = serviceRegistryBrowser
						.getSelectednode();
				serviceRegistryBrowser.addMessageConversion(service,
						selectednode, messageConversion);
				Map<String, Object> requestMap = getRequestMap();
				requestMap.remove("newconversion");
				messageConversion = new MessageConversion();
				
			} else {
				error("mcForm", errors);
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public ServiceRegistryBrowser getServiceRegistryBrowser() {
		return serviceRegistryBrowser;
	}

	public void setServiceRegistryBrowser(
			ServiceRegistryBrowser serviceRegistryBrowser) {
		this.serviceRegistryBrowser = serviceRegistryBrowser;
	}

	public MessageConversion getMessageConversion() {
		return messageConversion;
	}

	public void setMessageConversion(MessageConversion messageConversion) {
		this.messageConversion = messageConversion;
	}

	public SelectItem[] getAdapterOperations() {

		if (adapterOperations == null) {
			adapterOperations = getOperation(service);
		}

		return adapterOperations;
	}

	public void setAdapterOperations(SelectItem[] adapterOperations) {
		this.adapterOperations = adapterOperations;
	}

	public SelectItem[] getAdapteeOperations() {

		if (adapteeOperations == null) {
			adapteeOperations = getOperation(service.getAdapter().getService());
		}
		return adapteeOperations;
	}

	public void setAdapteeOperations(SelectItem[] adapteeOperations) {
		this.adapteeOperations = adapteeOperations;
	}

	SelectItem[] getOperation(Service service) {
		SelectItem[] ops;
		Collection<Operation> operations = service.getPort().getBinding()
				.getPortyype().getOperations();

		Collection<SelectItem> items = new ArrayList<SelectItem>();
		for (Operation o : operations) {
			String name = o.getName();
			String value = o.getName();
			items.add(new SelectItem(value, name));
		}
		ops = new SelectItem[items.size()];
		items.toArray(ops);
		return ops;
	}

	public Adapter getAdapter() {
		return adapter;
	}

	public void setAdapter(Adapter adapter) {
		this.adapter = adapter;
	}

	public void remove(ActionEvent event) {

		popup.setTitle("Remove Adapter" + adapter.getName());
		popup.setMessage("Do you want to remove Adapter ?");
		popup.setRendered(true);

	}

	@Override
	public String yes() throws IOException {
		removeAdapter();
		super.yes();
		return null;
	}

	private void removeAdapter() {
		try {
			service.setAdapter(null);			
			ServiceRegistry.getInstance().save();
			adapter = null;
			Map<String, Object> sessionMap = getSessionMap();
			Map<String, Object> requestMap = getRequestMap();
			requestMap.remove("adapter");
			sessionMap.remove("adapter");
			serviceRegistryBrowser.setSelectedPanel("emptyPanel");
			serviceRegistryBrowser.getSelectednode().removeFromParent();

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

}
