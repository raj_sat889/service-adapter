package com.jawise.serviceadapter.core;

import java.util.HashMap;
import java.util.Map;

import com.jawise.serviceadapter.ValidationErrors;

public class Adapter {
	private String name;

	private Service service; // adaptee

	private Map<String, MessageConversion> messageConversions = new HashMap<String, MessageConversion>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * The adaptee service
	 * @return
	 */
	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Map<String, MessageConversion> getMessageConversions() {
		return messageConversions;
	}

	public void setMessageConversions(
			Map<String, MessageConversion> messageConversions) {
		this.messageConversions = messageConversions;
	}
	
	
	public ValidationErrors validate() {
		ValidationErrors errors = new ValidationErrors();
		
		if(name == null || name.isEmpty()) {
			errors.add("adapter name is required");
		}	
		
		return errors;
	}		

}