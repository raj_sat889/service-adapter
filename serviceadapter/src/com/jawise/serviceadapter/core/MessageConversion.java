package com.jawise.serviceadapter.core;

import com.jawise.serviceadapter.ValidationErrors;


public class MessageConversion {
	private String name;

	private String adapteroperation;

	private String adapteeoperation;

	private PartConversions inputPartConversions = new PartConversions();

	private PartConversions outputPartConversions = new PartConversions();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PartConversions getInputPartConversions() {
		return inputPartConversions;
	}

	public void setInputPartConversions(PartConversions inputPartConversions) {
		this.inputPartConversions = inputPartConversions;
	}

	public PartConversions getOutputPartConversions() {
		return outputPartConversions;
	}

	public void setOutputPartConversions(
			PartConversions outputPartConversions) {
		this.outputPartConversions = outputPartConversions;
	}

	public String getAdapteroperation() {
		return adapteroperation;
	}

	public void setAdapteroperation(String adapteroperation) {
		this.adapteroperation = adapteroperation;
	}

	public String getAdapteeoperation() {
		return adapteeoperation;
	}

	public void setAdapteeoperation(String adapteeoperation) {
		this.adapteeoperation = adapteeoperation;
	}

	public ValidationErrors validate() {
		ValidationErrors errors = new ValidationErrors();
		
		if(name == null || name.isEmpty()) {
			errors.add("message conversion name is required");
		}

		if(adapteroperation == null || adapteroperation.isEmpty()) {
			errors.add("adapteroperation value is required");
		}

		if(adapteeoperation == null || adapteeoperation.isEmpty()) {
			errors.add("adapteeoperation; value is required");
		}

		
		
		return errors;
	}

}
