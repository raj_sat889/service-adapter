package com.jawise.serviceadapter.core;

import com.jawise.serviceadapter.ValidationErrors;

public class Part {
	String id;
	Type type = new Type();
	String name;
	boolean optional;
	String contentformat;
	String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Part() {
	}

	public Part(Part p) {
		this.id = p.id;
		this.type = new Type(p.type);
		this.name = p.name;
		this.optional = p.optional;
		this.contentformat = p.contentformat;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		if (id == null) {
			id = getName() + "_" + getType();
		}
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean getOptional() {
		return optional;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public String getContentformat() {
		return contentformat;
	}

	public void setContentformat(String contentformat) {
		this.contentformat = contentformat;
	}

	public ValidationErrors validate() {
		ValidationErrors errors = new ValidationErrors();
		if(id == null || id.isEmpty()) {
			errors.add("id value is required");
		}
		
		if(name == null || name.isEmpty()) {
			errors.add("name value is required");
		}
		
		if(contentformat == null || contentformat.isEmpty()) {
			errors.add("content format value is required");
		}
		
		return errors;
	}


}
