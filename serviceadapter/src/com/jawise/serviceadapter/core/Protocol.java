package com.jawise.serviceadapter.core;

public enum Protocol {
	NVP_HTTP("NVP_HTTP"),XML_RPC("XML_RPC"), XML_HTTP("XML_HTTP"),SOAP11("SOAP11");
	String protocol;	
	Protocol(String protocol){
		this.protocol = protocol;
	}
	public String toString() {
		return protocol;
	}

}
