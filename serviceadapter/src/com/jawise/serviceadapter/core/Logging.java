package com.jawise.serviceadapter.core;

public enum Logging {

	ALLOW_LOGGING("ALLOW_LOGGING"), DISALLOW_LOGGING("DISALLOW_LOGGING"), MASK_LOGGING(
			"MASK_LOGGING");
	String value;

	Logging(String value) {
		this.value = value;
	}

	public String toString() {
		return value;
	}

	public boolean allowed() {
		return ALLOW_LOGGING.toString().equals(value);
	}
}
