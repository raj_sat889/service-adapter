package com.jawise.serviceadapter.core;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.soap.binding.SoapResponseMessageBuilder;

@SuppressWarnings("serial")
public class MessageException extends Exception implements ResourceBundleSupport {
	private static Logger logger = Logger.getLogger(MessageException.class);
	public String value;

	public MessageException() {
		super();
	}

	public MessageException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageException(String message) {
		super(message);
	}

	public MessageException(String message, String value) {
		super(message);
		setValue(value);
	}

	public MessageException(Throwable cause) {
		super(cause);
	}

	public String getMessage(ResourceBundle bundle) {

		String m = "";
		try {
			m = bundle.getString(super.getMessage());
		} catch (Exception e) {
			logger.debug(e.getMessage());
			m = getMessage();
		}

		if (value != null && !value.isEmpty()) {
			m = m.replaceAll("#value", value);
		} else {
			m = m.replaceAll("#value", "");
		}
		return m;

	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
