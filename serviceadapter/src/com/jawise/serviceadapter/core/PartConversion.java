package com.jawise.serviceadapter.core;

import java.util.ArrayList;

import com.jawise.serviceadapter.ValidationErrors;

public class PartConversion {
	private String id;
	private Part adapteePart;
	private Part adapterPart;
	private String constantvalue;
	private String script;
	private String logging;
	private String scripttype;
	private boolean recursive;
	private ArrayList<String> referances = new ArrayList<String>();

	public Part getAdapteePart() {
		return adapteePart;
	}

	public void setAdapteePart(Part adapteePart) {
		this.adapteePart = adapteePart;
	}

	public Part getAdapterPart() {
		return adapterPart;
	}

	public void setAdapterPart(Part adapterPart) {
		this.adapterPart = adapterPart;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getConstantvalue() {
		return constantvalue;
	}

	public void setConstantvalue(String constantvalue) {
		this.constantvalue = constantvalue;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public ArrayList<String> getReferances() {
		if (referances == null) {
			referances = new ArrayList<String>();
		}
		return referances;
	}

	public void setReferances(ArrayList<String> referances) {
		this.referances = referances;
	}

	public String getLogging() {
		if (logging == null || logging.isEmpty()) {
			logging = "ALLOW_LOGGING";
		}
		return logging;
	}

	public Logging getLog() {
		return Logging.valueOf(logging);
	}	
	
	public void setLogging(String logging) {
		this.logging = logging;
	}

	public String getScripttype() {
		if (scripttype == null || scripttype.isEmpty()) {
			scripttype = "BeanShellScript";
		}
		return scripttype;
	}

	public void setScripttype(String scripttype) {
		this.scripttype = scripttype;
	}

	public ValidationErrors validate() {
		ValidationErrors errors = new ValidationErrors();

		if (adapterPart == null && constantvalue == null) {
			errors.add("adapter part or constant value is required");
		}
		if (adapteePart == null && constantvalue == null) {
			errors.add("adaptee part or constant value is required");
		}

		if (script != null && !script.isEmpty()
				&& (scripttype == null || scripttype.isEmpty())) {
			errors.add("scripttype is required");
		}
		return errors;
	}

	public boolean getRecursive() {
		return recursive;
	}

	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}

	public String getAdapterValue() {
		return getPDDispayValue(true);
	}
	public String getAdapteeValue() {
		return getPDDispayValue(false);
	}
	
	private String getPDDispayValue(boolean adapter) {
		Part part = adapter ? adapterPart :  adapteePart;
		String name = part != null ? part.getName() : "";
		if (name != null && !name.isEmpty()) {
			return name;
		} else if (recursive) {
			return "Recursive Part";
		} else if (constantvalue != null 
				&& !constantvalue.isEmpty()) {
			return constantvalue;
		}
		return "";		
	}

	public String getIdentification() {
		String identification = "";
		if(adapteePart != null) {
			identification += "AdapteePart - " + adapteePart.getName();
		}
		else {
			identification += "AdapteePart - NotUsed";
		}

		if(!identification.isEmpty()) {
			identification += " ";
		}
		
		if(adapterPart != null) {
			identification += "AdapterPart - " + adapterPart.getName();
		}
		else {
			identification += "AdapterPart - NotUsed";
		}		
		return identification;
	}
}
