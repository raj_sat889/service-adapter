package com.jawise.serviceadapter.core;

import com.jawise.serviceadapter.ValidationErrors;

/**
 * operations that a service provides
 * 
 * @author sathyan
 * 
 */
public class Operation {
	String name;
	Message input;
	Message output;
	String operationtype;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOperationtype() {
		return operationtype;
	}

	public void setOperationtype(String operationtype) {
		this.operationtype = operationtype;
	}

	public Message getInput() {
		return input;
	}

	public void setInput(Message input) {
		this.input = input;
	}

	public Message getOutput() {
		return output;
	}

	public void setOutput(Message output) {
		this.output = output;
	}

	public void initMessages() {
		Message in = new Message();
		Message out = new Message();
		in.setName(getName() + "InputMessage");
		out.setName(getName() + "OutputMessage");
		if ("REQUEST_RESPONSE".equals(getOperationtype())) {
			setInput(in);
			setOutput(out);
		} else if ("NOTIFICATION".equals(getOperationtype())) {
			setInput(out);
		} else {
			setOutput(out);
			setInput(in);
		}
	}
	
	public ValidationErrors validate() {
		ValidationErrors errors = new ValidationErrors();
		
		if(name == null || name.isEmpty()) {
			errors.add("operation name is required");
		}

		if(operationtype == null || operationtype.isEmpty()) {
			errors.add("operationtype is required");
		}		
		
		return errors;
	}	
}
