package com.jawise.serviceadapter.core;

import java.util.ArrayList;
import java.util.List;

import com.jawise.serviceadapter.ValidationErrors;

/**
 * describe the messages that the service expects to receive or send
 * 
 * @author sathyan
 * 
 */
public class Message {
	String name;
	List<Part> parts = new ArrayList<Part>();
	String separator = "&";

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public Message() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Part> getParts() {
		if (parts == null) {
			parts = new ArrayList<Part>();
		}
		return parts;
	}

	public void setParts(List<Part> parts) {
		this.parts = parts;
	}

	public Part findPart(String partid) {
		for (Part p : parts) {
			if (p.getId().equals(partid)) {
				return p;
			}
		}
		return null;
	}
	
	public Part findPartByName(String name) {
		for (Part p : parts) {
			if (p.getName().equals(name) ||
					p.getName().indexOf("/" + name) >0 ) {
				return p;
			}
		}
		return null;
	}	

	public ValidationErrors validateFor(Binding binding) {
		ValidationErrors errors = new ValidationErrors();
		Protocol protocol = Protocol.valueOf(binding.getProtocol());
		if (Protocol.NVP_HTTP.equals(protocol)
				&& (separator == null || separator.isEmpty())) {
			errors.add("sepearator value must be specified");
		}

		return errors;
	}

}
