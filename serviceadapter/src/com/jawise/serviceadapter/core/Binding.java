package com.jawise.serviceadapter.core;

import com.jawise.serviceadapter.ValidationErrors;

/**
 * Associates the abstract descriptions of a portType with a network protocol
 * 
 * @author sathyan
 * 
 */
public class Binding {
	private String name;
	private String tranport;
	private String protocol;
	private String use;
	private String encodingStyle;
	private PortType porttype;
	private long readtimeout;
	private long connectiontimeout;

	public long getReadtimeout() {
		return readtimeout;
	}

	public void setReadtimeout(long readtimeout) {
		this.readtimeout = readtimeout;
	}

	public long getConnectiontimeout() {
		return connectiontimeout;
	}

	public void setConnectiontimeout(long connectiontimeout) {
		this.connectiontimeout = connectiontimeout;
	}

	public Binding() {
		initPortType();

	}

	private void initPortType() {
		porttype = new PortType();
		porttype.setName(name + "PortType");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTranport() {
		return tranport;
	}

	public void setTranport(String tranport) {
		this.tranport = tranport;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getUse() {
		return use;
	}

	public void setUse(String use) {
		this.use = use;
	}

	public String getEncodingStyle() {
		return encodingStyle;
	}

	public boolean isUrlEncodingRequired() {
		return encodingStyle.indexOf("UrlEncodeRequest") >= 0;
	}

	public void setEncodingStyle(String encodingStyle) {
		this.encodingStyle = encodingStyle;
	}

	public PortType getPortyype() {
		if (porttype == null)
			initPortType();
		return porttype;
	}

	public void setPortyype(PortType portyype) {
		this.porttype = portyype;
	}

	public ValidationErrors validate() {

		ValidationErrors errors = new ValidationErrors();
		if (tranport == null || tranport.isEmpty()) {
			errors.add("binding transport required");
		}

		if (protocol == null || protocol.isEmpty()) {
			errors.add("binding style required");
		}

		if (encodingStyle == null || encodingStyle.isEmpty()) {
			errors.add("encoding style required");
		}

		return errors;
	}

	public boolean isSoap() {
		Protocol protocol = Protocol.valueOf(getProtocol());
		if (Protocol.SOAP11.equals(protocol)) {
			return true;
		}
		return false;
	}

	public boolean isXmlRpc() {
		Protocol protocol = Protocol.valueOf(getProtocol());
		if (Protocol.XML_RPC.equals(protocol)) {
			return true;
		}
		return false;
	}
}
