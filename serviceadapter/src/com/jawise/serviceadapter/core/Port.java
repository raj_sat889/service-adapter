package com.jawise.serviceadapter.core;

import java.net.MalformedURLException;
import java.net.URL;

import com.jawise.serviceadapter.ValidationErrors;

public class Port {
	String name;
	String location;
	Binding binding;

	public Port() {
		binding = new Binding();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Binding getBinding() {
		return binding;
	}

	public void setBinding(Binding binding) {
		this.binding = binding;
	}

	public ValidationErrors validate() {
		ValidationErrors errors = new ValidationErrors();
		if (location == null || location.isEmpty()) {
			errors.add("location value required");
		}

		try {
			URL url = new URL(location);
		} catch (MalformedURLException e) {
			errors.add("location value invalid : " + e.getMessage());
		}

		return errors;
	}

}
