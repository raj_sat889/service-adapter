package com.jawise.serviceadapter.core;

import java.util.ResourceBundle;

public interface ResourceBundleSupport {
	public String getMessage(ResourceBundle bundle);
}
