package com.jawise.serviceadapter.core;

import java.util.Date;
import java.util.HashMap;

import javax.xml.namespace.QName;

/**
 * The data types that are used in the messages exchanged by a service
 * 
 * @author sathyan
 * 
 */
public class Type {
	String name;
	int maximumlength = 0;

	public Type() {
	}

	
	public Type(String name, int maximumlength) {
		super();
		this.name = name;
		this.maximumlength = maximumlength;
	}


	public Type(Type t) {
		this.name = t.name;
		this.maximumlength = t.maximumlength;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaximumlength() {
		return maximumlength;
	}

	public void setMaximumlength(int maximumlength) {
		this.maximumlength = maximumlength;
	}

	public boolean isNumeric() {
		if ("int,i4,double".indexOf(name) >= 0) {
			return true;
		}

		return false;
	}

	public boolean isString() {
		if ("String,string,base64".indexOf(name) >= 0) {
			return true;
		}
		return false;
	}

	public boolean isArray() {
		if ("array".indexOf(name) >= 0) {
			return true;
		}
		return false;
	}

	public boolean isBoolean() {
		if ("boolean,Boolean".indexOf(name) >= 0) {
			return true;
		}
		return false;
	}

	public boolean isStruct() {
		if ("struct".indexOf(name) >= 0) {
			return true;
		}
		return false;
	}

	public boolean isDate() {
		if ("dateTime.iso8601".indexOf(name) >= 0) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public Class getTypeClass() {
		if (isString()) {
			return String.class;
		}

		if ("int".indexOf(name) >= 0) {
			return Integer.class;
		}

		if ("double".indexOf(name) >= 0) {
			return Double.class;
		}

		if ("i4".indexOf(name) >= 0) {
			return Integer.class;
		}

		if (isArray()) {
			return Object[].class;
		}

		if(isStruct()) {
			return HashMap.class;
		}
		if (isDate()) {
			return Date.class;
		}

		if (isBoolean()) {
			return Boolean.class;
		}

		return null;
	}
}
