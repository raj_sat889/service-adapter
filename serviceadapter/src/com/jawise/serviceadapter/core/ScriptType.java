package com.jawise.serviceadapter.core;

public enum ScriptType {
	JAVA_SCRIPT("javascript"),BEAN_SHELL_SCRIPT("BeanShellScript"), XSL("XSL");
	String type;	
	ScriptType(String type){
		this.type = type;
	}
	public String toString() {
		return type;
	}
}
