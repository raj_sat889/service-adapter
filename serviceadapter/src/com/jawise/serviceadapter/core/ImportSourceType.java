package com.jawise.serviceadapter.core;

public enum ImportSourceType {
	CSV("CSV"),XML("XML"), NVP("NVP");
	String type;	
	ImportSourceType(String type){
		this.type = type;
	}
	public String toString() {
		return type;
	}
}
