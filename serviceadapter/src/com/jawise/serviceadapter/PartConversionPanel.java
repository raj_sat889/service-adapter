package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;

import javax.faces.component.UIData;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;
import org.apache.log4j.Logger;
import org.dom4j.DocumentFactory;

import bsh.EvalError;
import bsh.Interpreter;
import bsh.ParseException;
import bsh.TargetError;

import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.Adapter;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.PartConversion;
import com.jawise.serviceadapter.core.PartConversions;
import com.jawise.serviceadapter.core.Protocol;
import com.jawise.serviceadapter.core.ScriptType;
import com.jawise.serviceadapter.core.Service;

public class PartConversionPanel extends Panel {

	private static Logger logger = Logger.getLogger(PartConversionPanel.class);

	private String adapteePartName;
	private String adapterPartName;
	private String pageindex = "1";
	private String name;
	private Adapter adapter;
	private MessageConversion messageConversion;
	private PartConversions partConversions;
	private SelectItem[] adapterParts;
	private SelectItem[] adapteeParts;
	private Part selectedPart;
	private boolean renderPartDetailsPopup = false;
	private boolean renderPartConversionDetailsPopup = false;
	private PartConversion partConversion;

	public SelectItem[] getAdapterParts() {

		if (adapterParts == null) {
			adapterParts = getParts(service, messageConversion
					.getAdapteroperation(), true);
		}

		return adapterParts;
	}

	public void setAdapterParts(SelectItem[] adapterParts) {
		this.adapterParts = adapterParts;
	}

	public SelectItem[] getAdapteeParts() {

		if (adapteeParts == null) {
			adapteeParts = getParts(adapter.getService(), messageConversion
					.getAdapteeoperation(), false);
		}

		return adapteeParts;
	}

	private SelectItem[] getParts(Service service, String opname,
			boolean adapter) {
		SelectItem[] partsitems;
		Operation op = service.getPort().getBinding().getPortyype()
				.findOperation(opname);

		Message m = isInputConversion() ? op.getInput() : op.getOutput();
		List<Part> parts = m.getParts();
		Collection<SelectItem> items = new ArrayList<SelectItem>();
		for (Part p : parts) {
			String name = p.getName();
			String value = p.getId() + ":" + p.getName();
			items.add(new SelectItem(value, name));
		}
		if ((adapter && isInputConversion())
				|| (!adapter && !isInputConversion())) {
			items.add(new SelectItem("NotUsed", "NotUsed"));
		} else {
			items.add(new SelectItem("NotUsed", "NotUsed"));
		}

		partsitems = new SelectItem[items.size()];
		partsitems = items.toArray(partsitems);
		return partsitems;
	}

	public void setAdapteeParts(SelectItem[] adapteeParts) {
		this.adapteeParts = adapteeParts;
	}

	private void populateReferances(Service service, String opname,
			PartConversion pc) {
		populateReferances(service, opname, pc.getScript(), pc.getReferances());
	}

	private void populateReferances(Service service, String opname,
			String script, ArrayList<String> referances) {
		// first check adapters
		Operation op = service.getPort().getBinding().getPortyype()
				.findOperation(opname);
		Message msg = isInputConversion() ? op.getInput() : op.getOutput();
		List<Part> parts = msg.getParts();

		Protocol protocol = Protocol.valueOf(service.getPort().getBinding()
				.getProtocol());
		boolean xml = false;
		if (Protocol.XML_HTTP.equals(protocol)
				|| Protocol.XML_RPC.equals(protocol)
				|| Protocol.SOAP11.equals(protocol)) {
			xml = true;
		}

		for (Part p : parts) {

			String name = p.getName();
			if (xml) {
				name = new XmlPath(name).getName();
			}
			if (name.indexOf("[]") >= 0) {
				name = name.replaceAll("\\x5b\\x5d", "");
			}

			if (script.indexOf(name) >= 0) {
				referances.add(name);
			}
		}
		return;

	}

	private Part findSelectedPart(Service service, String partname,
			String opname) {

		Operation op = service.getPort().getBinding().getPortyype()
				.findOperation(opname);
		StringTokenizer tokens = new StringTokenizer(partname, ":");
		Message msg = isInputConversion() ? op.getInput() : op.getOutput();
		Part part = msg.findPart(tokens.nextToken());
		return part;
	}

	protected PartConversion selectRow(ActionEvent event) {
		UIData uiData = (UIData) event.getComponent().getParent().getParent();
		PartConversion partConversion = (PartConversion) uiData.getRowData();
		logger.debug("selected part conversion id: " + partConversion.getId());
		return partConversion;
	}

	public void deletePartConversion(ActionEvent event) throws IOException {
		partConversion = selectRow(event);
		popup.clear();
		popup.setTitle("Remove Part Conversion");
		popup.setMessage("Do you want to remove the part conversion ?");
		popup.setRendered(true);

	}

	@Override
	public String yes() throws IOException {
		deletePartConversion();
		popup.setRendered(false);
		return null;
	}

	private void deletePartConversion() throws IOException {
		for (PartConversion pc : partConversions) {
			if (pc.equals(partConversion)) {
				partConversions.remove(pc);
				ServiceRegistry.getInstance().save();
				break;
			}
		}
	}

	@Override
	public void startEdit(ActionEvent event) {

		partConversion = selectRow(event);
		backup(partConversion);
		Part p = partConversion.getAdapteePart();
		if (p != null) {
			adapteePartName = p.getId() + ":" + p.getName();
		} else {
			adapteePartName = "NotUsed";
		}

		p = partConversion.getAdapterPart();
		if (p != null) {
			adapterPartName = p.getId() + ":" + p.getName();
		} else {
			adapterPartName = "NotUsed";

		}
		super.startEdit(event);
	}

	@Override
	public void cancelEdit(ActionEvent event) {
		adapteePartName = "";
		adapterPartName = "";
		restore(partConversion);
		partConversion = new PartConversion();
		super.cancelEdit(event);
	}

	@Override
	public void save(ActionEvent event) throws Exception {

		PartConversions partConversions = getPartConversions();
		PartConversion pc = partConversion;

		if (!"NotUsed".equals(adapteePartName)) {
			Part part = findSelectedPart(service.getAdapter().getService(),
					adapteePartName, messageConversion.getAdapteeoperation());
			pc.setAdapteePart(part);
		} else {
			pc.setAdapteePart(null);
		}

		if (!"NotUsed".equals(adapterPartName)) {

			Part part = findSelectedPart(service, adapterPartName,
					messageConversion.getAdapteroperation());
			pc.setAdapterPart(part);
		} else {
			pc.setAdapterPart(null);
		}

		pc.getReferances().clear();
		populateReferances(service, messageConversion.getAdapteroperation(), pc);
		populateReferances(adapter.getService(), messageConversion
				.getAdapteeoperation(), pc);

		ValidationErrors errors = pc.validate();
		if (errors.size() > 0) {
			error("pcForm", errors);
		}

		if (!edit) {
			pc.setId("" + partConversions.size() + 1);
			partConversions.add(pc);
		} else {
			super.save(event);
		}

		ServiceRegistry.getInstance().save();
		partConversion = new PartConversion();
		getRequestMap().remove("partConversion");
	}

	public void showPartConversion(ActionEvent event) {
		renderPartConversionDetailsPopup = true;
		partConversion = selectRow(event);
	}

	public String closePartConversionDetailsPopup() {
		renderPartConversionDetailsPopup = false;
		partConversion = new PartConversion();
		return null;
	}

	public void showAdapteePartDetails(ActionEvent event) {
		selectedPart = findSelectedPart(service.getAdapter().getService(),
				adapteePartName, messageConversion.getAdapteeoperation());
		renderPartDetailsPopup = true;
	}

	public void showAdapterPartDetails(ActionEvent event) {
		selectedPart = findSelectedPart(service, adapterPartName,
				messageConversion.getAdapteroperation());
		renderPartDetailsPopup = true;
	}

	public String closePartDetailsPopup() {
		renderPartDetailsPopup = false;
		return null;
	}

	public boolean isInputConversion() {
		if (name.toLowerCase().indexOf("input") >= 0) {
			return true;
		} else {
			return false;
		}
	}

	public Adapter getAdapter() {
		return adapter;
	}

	public void setAdapter(Adapter adapter) {
		this.adapter = adapter;
	}

	public MessageConversion getMessageConversion() {
		return messageConversion;
	}

	public void setMessageConversion(MessageConversion messageConversion) {
		this.messageConversion = messageConversion;
	}

	public String getAdapteePartName() {
		return adapteePartName;
	}

	public void setAdapteePartName(String adapteePartName) {
		this.adapteePartName = adapteePartName;
	}

	public String getAdapterPartName() {
		return adapterPartName;
	}

	public void setAdapterPartName(String adapterPartName) {
		this.adapterPartName = adapterPartName;
	}

	public PartConversions getPartConversions() {
		if (isInputConversion())
			partConversions = messageConversion.getInputPartConversions();
		else
			partConversions = messageConversion.getOutputPartConversions();

		return partConversions;
	}

	public void setPartConversions(PartConversions partConversions) {
		this.partConversions = partConversions;
	}

	public Part getSelectedPart() {
		return selectedPart;
	}

	public void setSelectedPart(Part selectedPart) {
		this.selectedPart = selectedPart;
	}

	public boolean isRenderPartDetailsPopup() {
		return renderPartDetailsPopup;
	}

	public void setRenderPartDetailsPopup(boolean renderPartDetailsPopup) {
		this.renderPartDetailsPopup = renderPartDetailsPopup;
	}

	public boolean isRenderPartConversionDetailsPopup() {
		return renderPartConversionDetailsPopup;
	}

	public void setRenderPartConversionDetailsPopup(
			boolean renderPartConversionDetailsPopup) {
		this.renderPartConversionDetailsPopup = renderPartConversionDetailsPopup;
	}

	public PartConversion getPartConversion() {
		return partConversion;
	}

	public void setPartConversion(PartConversion partConversion) {
		this.partConversion = partConversion;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void evaluateScript(ActionEvent event) {

		logger.debug("evaluating script");
		String script = partConversion.getScript();
		if (script == null || script.isEmpty()) {
			return;
		}

		if (ScriptType.BEAN_SHELL_SCRIPT.toString().equals(
				partConversion.getScripttype())) {
			evaluateBeanShellScript(script);
		} else if (ScriptType.JAVA_SCRIPT.toString().equals(
				partConversion.getScripttype())) {
			evaluateJavaScript(script);
		}
		return;

	}

	private void evaluateJavaScript(String script) {
		try {
			BSFManager manager = new BSFManager();
			ArrayList<String> refs = new ArrayList<String>();
			populateReferances(service,
					messageConversion.getAdapteroperation(), script, refs);
			populateReferances(service.getAdapter().getService(),
					messageConversion.getAdapteeoperation(), script, refs);

			if (script.indexOf("sourceDoc") >= 0) {
				manager.declareBean("sourceDoc", null,
						org.w3c.dom.Document.class);
			}

			for (String r : refs) {

				Part p = getReferancePart(r);
				if (p != null) {
					if (p.getType().isStruct()) {
						manager.declareBean(r, null, String.class);
					} else if (p.getType().isNumeric()) {
						manager.declareBean(r, new Integer(0), Integer.class);
					} else {
						manager.declareBean(r, new String(""), String.class);
					}
				}

			}
			manager.eval("javascript", "(java)", 1, 1, script);
			showScriptEvalSuccess();
			return;
		} catch (BSFException e) {
			logger.error(e.getMessage());
			String details = "error = " + e.getMessage();
			showScriptEvalFailure(details);

		}

	}

	private void evaluateBeanShellScript(String script) {
		try {
			Interpreter i = new Interpreter();
			ArrayList<String> refs = new ArrayList<String>();

			if (script.indexOf("sourceDoc") >= 0) {
				i.set("sourceDoc", null);
			}

			populateReferances(service,
					messageConversion.getAdapteroperation(), script, refs);

			populateReferances(service.getAdapter().getService(),
					messageConversion.getAdapteeoperation(), script, refs);

			for (String r : refs) {

				Part p = getReferancePart(r);
				if (p != null) {
					if (p.getType().isStruct()) {
						i.set(r, null);
					} else if (p.getType().isNumeric()) {
						i.set(r, 0);
					} else {
						i.set(r, "");
					}
				}
			}
			i.eval(script);
			showScriptEvalSuccess();
		} catch (ParseException e) {
			showError(e, "Script Parsing Failure");
		} catch (TargetError e) {
			showError(e, "Script Execution Failure");
		} catch (EvalError e) {
			showError(e, "Script Evaluation Failure");
		}
	}

	private void showError(EvalError e, String title) {
		logger.error(e.getMessage());
		String error = "error = " + e.getMessage();
		try {
			if (e.getErrorText() != null) {
				popup.setMessage2("error text = " + e.getErrorText());
			}
			if (e.getErrorLineNumber() >= 0) {
				popup.setMessage3("line no = " + e.getErrorLineNumber());
			}
		} catch (RuntimeException e1) {
		}
		showScriptEvalFailure(error, title);
	}

	private Part getReferancePart(String r) {
		Operation op1 = service.getPort().getBinding().getPortyype()
				.findOperation(messageConversion.getAdapteroperation());
		Message msg1 = isInputConversion() ? op1.getInput() : op1.getOutput();

		Operation op2 = service.getAdapter().getService().getPort()
				.getBinding().getPortyype().findOperation(
						messageConversion.getAdapteeoperation());
		Message msg2 = isInputConversion() ? op2.getInput() : op2.getOutput();
		Part p = msg1.findPartByName(r);
		if (p == null) {
			p = msg2.findPartByName(r);
		}
		return p;
	}

	private void showScriptEvalFailure(String error) {
		showScriptEvalFailure(error, "Script Evaluation Failure");
	}

	private void showScriptEvalFailure(String error, String tittle) {
		popup.clear();
		popup.setOkonly(true);
		popup.setRendered(true);
		popup.setTitle(tittle);
		popup.setMessage(error);
	}

	private void showScriptEvalSuccess() {
		popup.clear();
		popup.setOkonly(true);
		popup.setRendered(true);
		popup.setTitle("Script Evaluation");
		popup.setMessage("Script evaluated successfully");
	}

	public String getPageindex() {
		return pageindex;
	}

	public void setPageindex(String pageindex) {
		this.pageindex = pageindex;
	}

	public void validateMessageConversion(ActionEvent event) {

		ValidationErrors errors = new ValidationErrors();

		List<Part> parts = null;
		PartConversions partConversions = null;
		boolean input = isInputConversion();

		if (input) {
			Service adaptee = service.getAdapter().getService();
			Operation op = adaptee.getPort().getBinding().getPortyype()
					.findOperation(messageConversion.getAdapteeoperation());
			parts = op.getInput().getParts();
			partConversions = messageConversion.getInputPartConversions();
		} else {
			Service adapter = service;
			Operation op = adapter.getPort().getBinding().getPortyype()
					.findOperation(messageConversion.getAdapteroperation());
			parts = op.getOutput().getParts();
			partConversions = messageConversion.getOutputPartConversions();
		}

		for (Part p : parts) {
			if (!p.getOptional()) {
				boolean present = false;
				for (PartConversion pc : partConversions) {
					Part part = input ? pc.getAdapteePart() : pc
							.getAdapterPart();

					if (part != null && part.getName().equals(p.getName())) {
						present = true;
						break;
					}
				}
				if (!present) {
					errors.add(p.getName());
				}
			}
		}

		if (!errors.isEmpty()) {
			StringBuffer msg = new StringBuffer(
					"Following fields are required : ");
			int size = errors.size();
			for (String s : errors) {
				msg.append(s);
				size--;
				if (size > 0)
					msg.append(", ");
			}
			error("pcForm", msg.toString());
		}

		return;
	}
}
