package com.jawise.serviceadapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.icesoft.faces.component.inputfile.FileInfo;
import com.icesoft.faces.component.inputfile.InputFile;
import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.Port;
import com.jawise.serviceadapter.core.Service;
import com.jawise.serviceadapter.core.Services;

public class ServicesPanel extends Panel {

	private static Logger logger = Logger.getLogger(ServicesPanel.class);
	private SelectItem[] services;
	private String servicename;
	private String localuploaddir;
	
	public ServicesPanel() throws IOException {
		localuploaddir = ServiceRegistry.getInstance().getLocaluploaddir();
	}
	public String registerService() {
		try {
			Port port = service.getPort();
			port.setName(service.getName() + "Port");

			Binding binding = port.getBinding();
			binding.setName(service.getName() + "Binding");

			ServiceRegistry.getInstance().register(service);

			FacesContext ctx = FacesContext.getCurrentInstance();
			Map<String, Object> requestMap = ctx.getExternalContext()
					.getRequestMap();
			requestMap.remove("newservice");

			serviceRegistryBrowser.addService(service);
			serviceRegistryBrowser.setSelectedPanel("emptyPanel");

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}

	public void uploadFile(ActionEvent event) {
		try {
			InputFile inputFile = (InputFile) event.getSource();

			if (inputFile.getStatus() == InputFile.SAVED) {
				FileInfo fileInfo = inputFile.getFileInfo();
				logger.debug("file uploaded " + fileInfo.getFileName());
				File file = inputFile.getFile();
				ServiceRegistry registry = ServiceRegistry.getInstance();
				Services services = registry.loadServices(file);
				for (Service s : services) {
					registry.register(s);
				}
				serviceRegistryBrowser.refresh();
				serviceRegistryBrowser.setSelectedPanel("emptyPanel");
			}
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
			error("ieForm", e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			error("ieForm", "can't process the file uploaded");
		}

	}

	public SelectItem[] getServices() throws IOException {
		if (services == null) {
			intservices();
		}
		return services;
	}

	private void intservices() throws IOException {
		Collection<SelectItem> serviceitems = new ArrayList<SelectItem>();
		
		serviceitems.add(new SelectItem("All Services","All Services"));
		
		
		ServiceRegistry registry = ServiceRegistry.getInstance();
		Iterator<Service> iter = registry.getServices().iterator();
		while (iter.hasNext()) {
			Service s = iter.next();
			serviceitems.add(new SelectItem(s.getName(), s.getName()));

		}

		services = new SelectItem[serviceitems.size()];
		services = serviceitems.toArray(services);
		if (services.length > 0) {
			servicename = services[0].getLabel();
		}		
	}
	public void serviceChange(ValueChangeEvent e) {
		Object newValue = e.getNewValue();
		servicename = (String) newValue;
	}	

	public void setServices(SelectItem[] services) {
		this.services = services;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public String getLocaluploaddir() {
		return localuploaddir;
	}

	public void setLocaluploaddir(String localuploaddir) {
		this.localuploaddir = localuploaddir;
	}

}
