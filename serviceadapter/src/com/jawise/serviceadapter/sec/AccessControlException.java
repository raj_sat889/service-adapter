package com.jawise.serviceadapter.sec;

import java.util.ResourceBundle;

import com.jawise.serviceadapter.core.ResourceBundleSupport;

public class AccessControlException extends Exception implements ResourceBundleSupport {

	private static final long serialVersionUID = 1L;
	public String value;

	public AccessControlException() {
		super();
	}

	public AccessControlException(String message, Throwable cause) {
		super(message, cause);
	}

	public AccessControlException(String message) {
		super(message);
	}

	public AccessControlException(String message, String value) {
		super(message);
		setValue(value);
	}

	public AccessControlException(Throwable cause) {
		super(cause);
	}

	public String getMessage(ResourceBundle bundle) {
		String m = bundle.getString(super.getMessage());
		if(value != null && !value.isEmpty()) {
			m = m.replaceAll("#value", value);
		}
		else {
			m = m.replaceAll("#value","");
		}
		return m;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
