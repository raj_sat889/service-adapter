package com.jawise.serviceadapter.sec;

import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Service;
import com.jawise.serviceadapter.engine.AbstractHandler;
import com.jawise.serviceadapter.engine.ServiceAdapterRequest;

public class AccessControlService {
	protected static Logger logger = Logger
			.getLogger(AccessControlService.class);

	public void validateAccess(ServiceAdapterRequest serviceAdapterRequest,
			Service adapterService) throws AccessControlException {
		validateIPAddress(adapterService, serviceAdapterRequest
				.getRemoteaddress());
		validateUser(adapterService, serviceAdapterRequest.getUserid());

	}

	private void validateIPAddress(Service service, String ip)
			throws AccessControlException {
		if (ip == null || ip.isEmpty()) {
			throw new AccessControlException("1010");
		}

		if (ip.indexOf(":") >= 0) {
			logger.info("ip6 format not supported");
			return;
		}
		String filter = service.getPermittedIPAddresses();
		if (filter == null || filter.isEmpty()) {
			throw new AccessControlException("1011");
		}

		IPAddress reqip = new IPAddress(ip);

		StringTokenizer tokens = new StringTokenizer(service
				.getPermittedIPAddresses(), ",");
		while (tokens.hasMoreTokens()) {
			IPAddress iprange = new IPAddress(tokens.nextToken());
			if (!iprange.contains(reqip)) {
				throw new AccessControlException("1012", ip);
			} else {
				break;
			}

		}
	}

	private void validateUser(Service service, String userid)
			throws AccessControlException {
		if (userid == null || userid.isEmpty()) {
			throw new AccessControlException("1009");
		}
		StringTokenizer users = new StringTokenizer(service.getUserid(), ",");
		boolean uservalid = false;
		while (users.hasMoreTokens()) {
			String nextToken = users.nextToken();
			if (nextToken.trim().equals(userid)) {
				uservalid = true;
				break;
			}
		}
		if (!uservalid) {
			throw new AccessControlException("1026");
		}
	}

}
