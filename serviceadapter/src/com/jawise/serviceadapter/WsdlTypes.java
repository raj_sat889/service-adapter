package com.jawise.serviceadapter;

import java.util.HashMap;
import java.util.Map;

import com.jawise.serviceadapter.convert.soap.encoding.XMLXsdTypes;

public class WsdlTypes {

	private Map<String,String> types = new HashMap();
	
	public WsdlTypes() {
		types.put(XMLXsdTypes.STRING.toString(),"string"); 
		types.put(XMLXsdTypes.LONG.toString(),"long"); 
		types.put(XMLXsdTypes.FLOAT.toString(),"float");
		types.put(XMLXsdTypes.DOUBLE.toString(),"double");
		types.put(XMLXsdTypes.INT.toString(),"int");
		types.put(XMLXsdTypes.SHORT.toString(),"short");
		types.put(XMLXsdTypes.BOOLEAN.toString(),"boolean");
		types.put(XMLXsdTypes.DATETIME.toString().toLowerCase(),"dateTime");
		types.put(XMLXsdTypes.TIME.toString().toLowerCase(),"dateTime");
		types.put(XMLXsdTypes.BASE64.toString().toLowerCase(),"base64Binary");
		types.put(XMLXsdTypes.DECIMAL.toString(),"decimal");
		types.put(XMLXsdTypes.INTEGER.toString(),"integer");
		types.put(XMLXsdTypes.URI.toString().toLowerCase(),"anyURI");
		types.put(XMLXsdTypes.ANY.toString().toLowerCase(),"anyType");
		types.put(XMLXsdTypes.DATE.toString(),"date");
		types.put(XMLXsdTypes.DURATION.toString(),"duration");
		types.put(XMLXsdTypes.G_YEAR_MONTH.toString(),"string");
		types.put(XMLXsdTypes.G_MONTH_DAY.toString(),"string");
		types.put(XMLXsdTypes.G_YEAR.toString(),"string");
		types.put(XMLXsdTypes.G_MONTH.toString(),"string");
		types.put(XMLXsdTypes.G_DAY.toString(),"string");	
	}
	
	public String getType(String v ) {
		String t = types.get(v);
		if(t == null) {
			t = types.get(v.toLowerCase());
		}
		return t;
	}
}
