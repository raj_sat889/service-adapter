package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIData;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Service;

public class Panel {
	private static Logger logger = Logger.getLogger(Panel.class);

	protected ServiceRegistryBrowser serviceRegistryBrowser;

	protected Service service;

	protected Object backupData;

	protected PopUp popup = new PopUp();

	protected boolean edit;

	public boolean getEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public ServiceRegistryBrowser getServiceRegistryBrowser() {
		return serviceRegistryBrowser;
	}

	public void setServiceRegistryBrowser(
			ServiceRegistryBrowser serviceRegistryBrowser) {
		this.serviceRegistryBrowser = serviceRegistryBrowser;
	}

	protected Object selectRow(ActionEvent event) {
		UIData uiData = (UIData) event.getComponent().getParent().getParent();
		return uiData.getRowData();
	}

	public void startEdit(ActionEvent event) {
		edit = true;
	}

	public void cancelEdit(ActionEvent event) {
		edit = false;
	}

	public void save(ActionEvent event) throws Exception {
		edit = false;
	}

	protected void error(String t, String m) {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage message = new FacesMessage();
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		message.setSummary(m);
		message.setDetail(m);
		context.addMessage(t, message);
	}

	protected void error(String t, ValidationErrors ve) {
		FacesContext context = FacesContext.getCurrentInstance();
		for (String m : ve) {
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary(m);
			message.setDetail(m);
			context.addMessage(t, message);
		}
	}

	protected void backup(Object orig) {
		try {
			backupData = orig.getClass().newInstance();
			BeanUtils.copyProperties(backupData, orig);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	protected void restore(Object orig) {
		try {
			BeanUtils.copyProperties(orig, backupData);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public PopUp getPopup() {
		return popup;
	}

	public void setPopup(PopUp popup) {
		this.popup = popup;
	}

	public String yes() throws IOException {
		popup.setRendered(false);
		popup.clear();
		return null;
	}

	public String no() {
		popup.setRendered(false);
		popup.clear();
		return null;
	}

	public String ok() {
		popup.setRendered(false);
		popup.clear();
		return null;
	}

	public ExternalContext getJsfContext() {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		return externalContext;
	}

	public Map<String, Object> getSessionMap() {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		return externalContext.getSessionMap();
	}

	public Map<String, Object> getRequestMap() {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		return externalContext.getRequestMap();
	}
}
