package com.jawise.serviceadapter.convert;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.ScriptEvaluator;
import com.jawise.serviceadapter.core.Binding;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.PartConversion;
import com.jawise.serviceadapter.core.PartConversions;
import com.jawise.serviceadapter.core.Service;

public abstract class MessageConverter {
	private static Logger logger = Logger.getLogger(MessageConverter.class);
	private MessageContext ctx;
	private Service adapterService;
	private MessageConversion msgConversion;
	private Boolean input;
	private boolean urlEncodeParameter;

	public MessageConverter(MessageContext ctx) throws MessageException {
		this.setCtx(ctx);
		setAdapterService((Service) ctx.get("adapterService"));
		setMsgConversion((MessageConversion) ctx.get("messageConversion"));
		setInput((Boolean) ctx.get("input"));
		// check the adapte service
		Service adapteeService = getAdapterService().getAdapter().getService();
		Binding toBinding = adapteeService.getPort().getBinding();
		String encodingStyle = toBinding.getEncodingStyle();
		urlEncodeParameter = encodingStyle.indexOf("UrlEncodeParameter") >= 0;
	}

	public abstract Object doConvertion() throws MessageException;

	@SuppressWarnings("unchecked")
	protected abstract Map getTargetReferances() throws MessageException;

	@SuppressWarnings("unchecked")
	protected abstract Map getSourceReferences() throws MessageException;

	protected abstract void setTargetValue(String targetName,
			Object targetValue, boolean recursive) throws MessageException;

	protected abstract Object getSourceValue(String sourceName)
			throws MessageException;

	protected void convertParts() throws MessageException {

		PartConversions partConversions = selectPartConversions();
		ScriptEvaluator evaluator = new ScriptEvaluator();
		for (PartConversion pc : partConversions) {
			String sourceName = "";
			Object sourceValue = "";

			Part sourcePart = isInput() ? pc.getAdapterPart() : pc
					.getAdapteePart();
			if (sourcePart != null) {
				sourceName = sourcePart.getName();
				logger.debug("getting value for : " + sourceName);
				sourceValue = getSourceValue(sourceName);
				if (!sourcePart.getOptional() && (isSourceEmpty(sourceValue))) {
					throw new MessageException("1005", sourceName);
				}
			} else {
				sourceValue = pc.getConstantvalue();
			}

			// calculate target value
			Object targetValue = "";
			String targetName = "";

			Part targetPart = isInput() ? pc.getAdapteePart() : pc
					.getAdapterPart();

			if (targetPart != null) {
				targetName = targetPart.getName();
			}

			if (pc.getRecursive()) {
				targetName = "recursiveValue";
			}

			if (targetName.isEmpty()) {
				throw new MessageException("1024", pc.getIdentification());
			}

			String script = pc.getScript();
			if (script == null || script.isEmpty()) {
				targetValue = sourceValue;
			} else {
				targetValue = evaluator.eval(getSourceReferences(),
						getTargetReferances(), pc,
						getScriptTargetName(targetName));
			}
			if (pc.getRecursive() && targetPart != null) {
				targetName = targetPart.getName();
			}

			if (isOptionalAndEmptry(targetPart, targetValue)) {
				logger.info("not pasing the optional part "
						+ targetPart.getName());
				continue;
			}
			setTargetValue(targetName, targetValue, pc.getRecursive());
		}
	}

	private boolean isSourceEmpty(Object sourceValue) {
		if (sourceValue == null) {
			return true;
		} else if (sourceValue instanceof String
				&& ((String) sourceValue).isEmpty()) {
			return true;
		} else
			return false;
	}

	private boolean isOptionalAndEmptry(Part targetPart, Object targetValue) {
		if (targetPart != null && targetPart.getOptional()) {
			if (targetValue == null)
				return true;
			else if (targetValue instanceof String && "".equals(targetValue)) {
				return true;
			}
		}
		return false;
	}

	protected String getScriptTargetName(String targetName) {
		return targetName;
	}

	protected String buildNvpMessage(Map<String, String> result)
			throws UnsupportedEncodingException {

		Binding binding = getTargetBinding();
		String encodingStyle = binding.getEncodingStyle();
		boolean urlencode = false;
		if (encodingStyle.indexOf("UrlEncode") >= 0) {
			urlencode = true;
		}
		String seperator = getTargetSeperator();
		String nvpMsg = NVPHelper
				.formatNvpMessage(result, urlencode, seperator);
		return nvpMsg;
	}

	protected String getTargetSeperator() {
		Binding binding = getTargetBinding();
		String operation = isInput() ? getMsgConversion().getAdapteeoperation()
				: getMsgConversion().getAdapteroperation();
		Operation op = binding.getPortyype().findOperation(operation);
		String seperator = isInput() ? op.getInput().getSeparator() : op
				.getOutput().getSeparator();
		return seperator;
	}

	protected Binding getTargetBinding() {
		Service targetService = null;
		if (isInput())
			targetService = getAdapterService().getAdapter().getService();// adaptee
		else
			targetService = getAdapterService();// adapter
		return targetService.getPort().getBinding();
	}

	protected PartConversions selectPartConversions() {
		PartConversions partConversions;
		if (isInput()) {
			partConversions = getMsgConversion().getInputPartConversions();
		} else {
			partConversions = getMsgConversion().getOutputPartConversions();
		}
		return partConversions;
	}

	protected String applyEncoding(String value)
			throws UnsupportedEncodingException {
		if (value == null || value.isEmpty())
			return value;

		if (urlEncodeParameter) {
			return URLDecoder.decode(value, "UTF-8");
		} else {
			return value;
		}
	}

	public boolean getUrlEncodeParameter() {
		return urlEncodeParameter;
	}

	protected void setInput(Boolean input) {
		this.input = input;
	}

	public Boolean isInput() {
		return input;
	}

	public void setMsgConversion(MessageConversion msgConversion) {
		this.msgConversion = msgConversion;
	}

	public MessageConversion getMsgConversion() {
		return msgConversion;
	}

	public void setAdapterService(Service adapterService) {
		this.adapterService = adapterService;
	}

	public Service getAdapterService() {
		return adapterService;
	}

	protected void setCtx(MessageContext ctx) {
		this.ctx = ctx;
	}

	protected MessageContext getCtx() {
		return ctx;
	}

}
