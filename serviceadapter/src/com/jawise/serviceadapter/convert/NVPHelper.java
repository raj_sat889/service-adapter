package com.jawise.serviceadapter.convert;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class NVPHelper {

	public static String formatNvpMessage(Map<String, String> nvp,
			boolean urlencode, String seperator)
			throws UnsupportedEncodingException {

		StringBuffer message = new StringBuffer();
		boolean begin = true;

		if (nvp != null) {

			Set<String> keys = nvp.keySet();
			for (String key : keys) {
				if (begin) {
					begin = false;
				} else {
					if (seperator.equals("CRLF")) {
						message.append("\r\n");
					} else
						message.append(seperator);

				}
				message.append(key);
				message.append('=');

				String val = nvp.get(key);
				if (val != null) {
					if (urlencode) {
						message.append(URLEncoder.encode(val, "UTF-8"));
					} else {
						message.append(val);

					}
				}

			}
		}
		if (!message.toString().isEmpty()) {
			if (seperator.equals("CRLF")) {
				message.append("\r\n");
			} else
				message.append(seperator);
		}

		return message.toString();
	}

	@SuppressWarnings("unchecked")
	public static Map parseNvpMessage(String msg, boolean urldecode,
			String separator) throws UnsupportedEncodingException {

		if (urldecode) {
			msg = URLDecoder.decode(msg, "UTF-8");
		}

		if (separator == null || separator.isEmpty()) {
			separator = "&";
		}
		
		if (separator.equals("CRLF")) {
			separator = "\r\n";
		}
		StringTokenizer nvpTokens = new StringTokenizer(msg, separator);
		Map<String, String[]> nvpMap = new HashMap<String, String[]>();

		String nvp;
		String name;
		String value;

		while (nvpTokens.hasMoreTokens()) {
			nvp = nvpTokens.nextToken();
			int eqi = nvp.indexOf("=");
			if (eqi == -1) {
				name = nvp;
				value = "";
			} else {
				name = nvp.substring(0, eqi);
				value = nvp.substring(eqi + 1, nvp.length());
			}

			String[] values = nvpMap.get(name);

			if (values == null) {
				values = new String[1];
			} else {
				String[] pastvalues = values;
				values = new String[values.length + 1];
				System.arraycopy(pastvalues, 0, values, 0, pastvalues.length);

			}
			values[values.length - 1] = value;
			nvpMap.put(name, values);

		}
		return nvpMap;
	}
}
