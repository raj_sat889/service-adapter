package com.jawise.serviceadapter.convert.soap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.TargetValue;
import com.jawise.serviceadapter.convert.XmlTargetValueAccepter;
import com.jawise.serviceadapter.convert.soap.binding.SoapMessageParser;
import com.jawise.serviceadapter.convert.xml.XmlComposer;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;

public class SoapToXmlConverter extends AbstractSoapConverter {
	private static Logger logger = Logger.getLogger(SoapToXmlConverter.class);
	@SuppressWarnings("unchecked")
	private XmlPath lastAddedTargetPath;
	@SuppressWarnings("unchecked")
	private Map recursiveNodesMap;

	public SoapToXmlConverter(MessageContext ctx) throws MessageException {
		super(ctx);

		recursiveNodesMap = new HashMap<XmlPath, String>();
	}

	@Override
	public Object doConvertion() throws MessageException {
		try {
			SoapMessageParser parser = new SoapMessageParser(getCtx());

			// the main parsing is done here
			List parameters = parser.getParameters();
			// now populate source map;
			populateSourceMap(parameters);

			// do conversion
			convertParts();

			getCtx().put("paramterslist", trargetParameters);
			getCtx().put("paramtersmap", targetMap);
			XmlComposer composer = new XmlComposer();
			String xml = composer.compose((HashMap) targetMap,
					(HashMap) recursiveNodesMap);
			return xml;
		} catch (MessageException e) {
			logger.error(e.getMessage(), e);
			ResourceBundle bundle = ResourceBundle
					.getBundle("com.jawise.serviceadapter.core.messages");
			throw new MessageException("1013", e.getMessage(bundle));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}
	}

	@Override
	protected Map getTargetReferances() throws MessageException {
		return targetReferences;
	}

	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		XmlTargetValueAccepter accepter = new XmlTargetValueAccepter(this,
				targetReferences, targetMap, null, recursiveNodesMap,
				lastAddedTargetPath);
		accepter.acceptTargetValue(new TargetValue(targetName, targetValue,
				recursive));
		lastAddedTargetPath = accepter.getLastAddedTargetPath();

	}

}
