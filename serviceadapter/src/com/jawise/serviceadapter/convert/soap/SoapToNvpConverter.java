package com.jawise.serviceadapter.convert.soap;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.NvpTargetValueAccepter;
import com.jawise.serviceadapter.convert.TargetValue;
import com.jawise.serviceadapter.convert.soap.binding.SoapMessageParser;
import com.jawise.serviceadapter.core.MessageException;

public class SoapToNvpConverter extends AbstractSoapConverter {
	private static Logger logger = Logger.getLogger(SoapToNvpConverter.class);


	public SoapToNvpConverter(MessageContext ctx) throws MessageException {
		super(ctx);
	}
	
	@Override
	public Object doConvertion() throws MessageException {
		try {
			SoapMessageParser parser = new SoapMessageParser(getCtx());

			// the main parsing is done here
			List parameters = parser.getParameters();
			// now populate source map;
			populateSourceMap(parameters);

			// do conversion
			convertParts();

			getCtx().put("paramterslist", trargetParameters);
			getCtx().put("paramtersmap", targetMap);
			String targgetMsg = buildNvpMessage(getTargetReferances());
			logger.debug("finished conversion");
			return targgetMsg;
		} 
		catch (MessageException e) {
			throw e;
		}		
		catch (Exception e) {
			throw new MessageException(e.getMessage(), e);
		}
	}



	@Override
	protected Map getTargetReferances() throws MessageException {
		return targetReferences;
	}

	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
				
		NvpTargetValueAccepter accepter = new NvpTargetValueAccepter(this,targetReferences,targetMap,null);
		accepter.acceptTargetValue(new TargetValue(targetName,targetValue,recursive));
		
	}

}
