package com.jawise.serviceadapter.convert.soap.unused;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;
import org.codehaus.xfire.util.jdom.StaxBuilder;
import org.codehaus.xfire.util.stax.DepthXMLStreamReader;
import org.codehaus.xfire.util.stax.FragmentStreamReader;
import org.jdom.Element;
import org.w3c.dom.Document;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.soap.binding.SoapException;
import com.jawise.serviceadapter.convert.soap.binding.SoapMessageSerializer;
import com.jawise.serviceadapter.convert.soap.binding.SoapXmlUtil;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.PartConversion;
import com.jawise.serviceadapter.core.PartConversions;

public class MessageTypeBinding implements SoapMessageSerializer {
	private static Logger logger = Logger.getLogger(MessageTypeBinding.class);
	@SuppressWarnings("unchecked")
	@Override
	public void readMessage(MessageContext ctx) throws Exception {

		XMLStreamReader reader = (XMLStreamReader) 
			ctx.get("xmlstreamreader");
		List trargetParameters = new ArrayList();
		SoapMessageReader soapmessahereader = new SoapMessageReader();
		soapmessahereader.readToHeader(ctx);

		String sourceName = "";
		int param = 0;

		PartConversions conversions = getPartConversions(ctx);
		Boolean input = (Boolean) ctx.get("input");
		DepthXMLStreamReader dr = new DepthXMLStreamReader(reader);
		while (SoapXmlUtil.toNextElement(dr)) {

			PartConversion pc = conversions.get(param);

			if (pc == null) {
				throw new MessageException("1027", "" + param);
			}
			Part sourcePart = input ? pc.getAdapterPart() : pc.getAdapteePart();
			if (sourcePart != null) {
				sourceName = sourcePart.getName();

			}
			trargetParameters.add(readParameter(sourcePart, dr, ctx));

			if (dr.getEventType() == XMLStreamReader.END_ELEMENT)
				dr.next();
			param++;
		}

		return;

	}

	public Object readParameter(Part p, XMLStreamReader reader,
			MessageContext context) throws Exception {
		if (p.getType().getTypeClass().isAssignableFrom(XMLStreamReader.class)) {
			return reader;
		} else if (Element.class.isAssignableFrom(p.getType().getTypeClass())) {
			StaxBuilder builder = new StaxBuilder();
			try {
				org.jdom.Document doc = builder.build(new FragmentStreamReader(
						reader));

				if (doc.hasRootElement())
					return doc.getRootElement();
				else
					return null;
			} catch (XMLStreamException e) {
				throw new SoapException("Couldn't parse stream.", e);
			}
		} else if (Document.class.isAssignableFrom(p.getType().getTypeClass())) {
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();

				return SoapXmlUtil.read(builder, reader, true);
			} catch (Exception e) {
				throw new SoapException("Couldn't read message.", e);
			}
		} else if (p.getType().getTypeClass().isAssignableFrom(
				MessageContext.class)) {
			return context;
		} else {
			logger.warn("Unknown type for serialization: "
					+ p.getType().getTypeClass());
			return null;
		}

	}

	@Override
	public void writeMessage(MessageContext context) {
		throw new UnsupportedOperationException("not yet coded");

	}
	
	protected PartConversions getPartConversions(MessageContext ctx) {
		MessageConversion msgConversion = (MessageConversion) ctx.get("messageConversion");
		Boolean input = (Boolean) ctx.get("input");		
		PartConversions partConversions;
		if (input) {
			partConversions = msgConversion.getInputPartConversions();
		} else {
			partConversions = msgConversion.getOutputPartConversions();
		}
		return partConversions;
	} 

}
