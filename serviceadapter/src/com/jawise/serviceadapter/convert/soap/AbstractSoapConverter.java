package com.jawise.serviceadapter.convert.soap;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.MessageConverter;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.Type;

public abstract class AbstractSoapConverter extends MessageConverter {

	private static Logger logger = Logger
			.getLogger(AbstractSoapConverter.class);

	@SuppressWarnings("unchecked")
	protected Map sourceReferences;
	@SuppressWarnings("unchecked")
	protected Map sourceMap;
	@SuppressWarnings("unchecked")
	protected Map targetReferences;
	@SuppressWarnings("unchecked")
	protected Map targetMap;
	protected List trargetParameters;

	public AbstractSoapConverter(MessageContext ctx) throws MessageException {
		super(ctx);

		targetMap = new HashMap<String, String>();
		sourceMap = new HashMap<String, Object>();
		sourceReferences = new HashMap<String, Object>();
		targetReferences = new HashMap<String, String>();
		trargetParameters = new ArrayList<String>();
	}

	protected void populateSourceMap(List parameters) throws Exception {
		logger.debug("populating source map");

		List<Part> parts = null;
		Operation op = null;

		if (isInput()) {
			String opname = getMsgConversion().getAdapteroperation();
			op = getAdapterService().getPort().getBinding().getPortyype()
					.findOperation(opname);

			if (op == null) {
				throw new MessageException("1019");
			}
			parts = op.getInput().getParts();
		} else {
			String opname = getMsgConversion().getAdapteeoperation();
			op = getAdapterService().getAdapter().getService().getPort()
					.getBinding().getPortyype().findOperation(opname);

			if (op == null) {
				throw new MessageException("1019");
			}
			parts = op.getOutput().getParts();
		}

		int expected = getActualPramsetersSize(parts);
		if (parameters.size() != expected) {
			throw new MessageException("1020", "" + expected);
		}

		populateSourceMap(parameters, parts, 0, parameters.size(), null, null);

	}

	private void populateSourceMap(List params, List<Part> parts,
			final int spos, final int epos, final String pn, final Object val)
			throws MessageException {

		for (int i = spos; i < epos; i++) {
			Object o = val == null ? params.get(i) : val;
			String partname = pn == null ? parts.get(i).getName() : pn;

			Part part = findPart(parts, partname);
			if (o instanceof Object[]) {
				// array
				if(partname.indexOf("[]") == -1) {
					partname = partname + "[]";
				}
				addSourceEntry(part, partname, o);
			} else if (o instanceof Number) {
				addSourceEntry(part, partname, o);
			} else if (o instanceof Map) {
				// struct
				Map m = (Map) o;
				Iterator iter = m.keySet().iterator();
				int j = i;
				while (iter.hasNext()) {
					String key = (String) iter.next();
					int lastIndexOf = partname.lastIndexOf("/");
					String partnamePrefix = partname;
					if (lastIndexOf != -1) {
						partnamePrefix = partname.substring(0, lastIndexOf);
					}

					String thispartname = partnamePrefix + "/" + key;
					populateSourceMap(params, parts, j, j + 1, thispartname, m
							.get(key));
					j++;
				}
				//the struct its self must be added as well
				Part structpart = new Part();			
				int p = partname.indexOf("/");
				String structname = partname; 
				if(p >= 0) {	
					structname = partname.substring(0, p);
				}
				structpart.setId(structname);
				structpart.setType(new Type("struct",0));
				addSourceEntry(structpart, structname, o);

			} else {
				addSourceEntry(part, partname, o);
			}
		}
	}

	private Part findPart(List<Part> parts, String name) {
		for (Part p : parts) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private void addSourceEntry(Part part, String partname, Object o)
			throws MessageException {
		String referanceName = new XmlPath(partname).getName();
		String sourceName = partname;
		validateSourceEntry(part, partname, o);
		if (o instanceof Object[]) {
			referanceName = referanceName.replaceAll("\\x5b\\x5d", "");
			sourceReferences.put(referanceName, o);
			sourceMap.put(sourceName, o);
		} else if (o instanceof Number) {
			sourceReferences.put(referanceName, o);
			sourceMap.put(sourceName, o);
		}
		else if (o instanceof Map){
			sourceReferences.put(referanceName, o);
			sourceMap.put(sourceName, o);			
		}
		else {
			String value = o == null ? null : o.toString();
			sourceReferences.put(referanceName, value);
			sourceMap.put(sourceName, value);
		}

		logger.debug(sourceName + " = " + o.toString());
	}

	private void validateSourceEntry(Part part, String partname, Object o)
			throws MessageException {
		if (partname.indexOf("[]") > 0 && !(o instanceof Object[])) {
			throw new MessageException("1022", partname);
		}

		if (part != null) {
			if (part.getType().isArray() && !(o instanceof Object[])) {
				throw new MessageException("1022", partname);
			} else if (part.getType().isNumeric() && !(o instanceof Number)) {
				throw new MessageException("1023", partname);
			} else if (part.getType().isBoolean() && !(o instanceof Boolean)) {
				throw new MessageException("1023", partname);
			} else if (part.getType().isString() && !(o instanceof String)) {
				throw new MessageException("1023", partname);
			} else if (part.getType().isStruct() && !(o instanceof Map)) {
				throw new MessageException("1023", partname);
			} else if (part.getType().isDate() && !(o instanceof Date)) {
				throw new MessageException("1023", partname);
			}
		}
	}

	private int getActualPramsetersSize(List<Part> parts) {
		int parmSize = 0;
		boolean inGroup = false;

		for (Part p : parts) {
			if (p.getName().indexOf("/") >= 0) {
				if (!inGroup) {
					parmSize++;
				}
				inGroup = true;
			} else {
				parmSize++;
				inGroup = false;
			}
		}
		return parmSize;
	}

	@Override
	protected Map getSourceReferences() throws MessageException {
		return sourceReferences;
	}

	@Override
	protected Object getSourceValue(String sourceName) throws MessageException {
		Object val = sourceMap.get(sourceName);
		if (val == null) {
			return null;
		} else if (val instanceof String) {
			return val;
		} else if (val instanceof Number) {
			return val;
		} else {
			return val.toString();
		}
	}
	
	@Override
	protected String getScriptTargetName(String targetName) {
		return new XmlPath(targetName).getName();
	}	
}