package com.jawise.serviceadapter.convert.soap.encoding;

import javax.xml.namespace.QName;

import org.codehaus.xfire.soap.Soap11;
import org.codehaus.xfire.soap.SoapConstants;

public enum SOAPEncodingTypes {
	
	STRING("string"), LONG("long"), FLOAT("float"), DOUBLE("double"), INT("int"), SHORT(
			"short"), BOOLEAN("boolean"), DATETIME("dateTime"), TIME("dateTime"), BASE64(
			"base64Binary"), DECIMAL("decimal"), INTEGER("integer"),CHAR("char");
	String value;
	public static final String ENCODED_NS = Soap11.getInstance().getSoapEncodingStyle();
	
	SOAPEncodingTypes(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public QName getQName() {
		return new QName(ENCODED_NS, value);
	}
}
