package com.jawise.serviceadapter.convert.soap.encoding;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;

import org.jdom.Element;
import org.w3c.dom.Document;

import com.jawise.serviceadapter.convert.soap.binding.SoapException;
import com.jawise.serviceadapter.convert.soap.type.ArrayType;
import com.jawise.serviceadapter.convert.soap.type.Base64Type;
import com.jawise.serviceadapter.convert.soap.type.BooleanType;
import com.jawise.serviceadapter.convert.soap.type.DateType;
import com.jawise.serviceadapter.convert.soap.type.DecimalType;
import com.jawise.serviceadapter.convert.soap.type.DocumentType;
import com.jawise.serviceadapter.convert.soap.type.DoubleType;
import com.jawise.serviceadapter.convert.soap.type.FloatType;
import com.jawise.serviceadapter.convert.soap.type.IntType;
import com.jawise.serviceadapter.convert.soap.type.IntegerType;
import com.jawise.serviceadapter.convert.soap.type.JDOMDocumentType;
import com.jawise.serviceadapter.convert.soap.type.JDOMElementType;
import com.jawise.serviceadapter.convert.soap.type.LongType;
import com.jawise.serviceadapter.convert.soap.type.ObjectType;
import com.jawise.serviceadapter.convert.soap.type.ShortType;
import com.jawise.serviceadapter.convert.soap.type.SoapType;
import com.jawise.serviceadapter.convert.soap.type.SourceType;
import com.jawise.serviceadapter.convert.soap.type.SqlDateType;
import com.jawise.serviceadapter.convert.soap.type.StringType;
import com.jawise.serviceadapter.convert.soap.type.StructType;
import com.jawise.serviceadapter.convert.soap.type.TimeType;
import com.jawise.serviceadapter.convert.soap.type.TimestampType;
import com.jawise.serviceadapter.convert.soap.type.URIType;
import com.jawise.serviceadapter.convert.soap.type.XMLStreamReaderType;

public class SoapTypeMappingRegistry {

	@SuppressWarnings("unchecked")
	private Map<String, SoapTypeMapping> mappings;

	public SoapTypeMappingRegistry() throws SoapException {
		mappings = Collections
				.synchronizedMap(new HashMap<String, SoapTypeMapping>());
		initialise();
	}

	public Map<String, SoapTypeMapping> getMappings() {
		return mappings;
	}

	@SuppressWarnings("unchecked")
	private void register(SoapTypeMapping tm, Class c, XMLXsdTypes et,
			SoapType st) {
		tm.add(c, et.getQName(), st);
	}

	@SuppressWarnings("unchecked")
	private void register(SoapTypeMapping tm, Class c, SOAPEncodingTypes et,
			SoapType st) {
		tm.add(c, et.getQName(), st);
	}

	@SuppressWarnings("unchecked")
	private void register(SoapTypeMapping tm, Class c, QName qn, SoapType st) {
		tm.add(c, qn, st);
	}

	private void initialise() throws SoapException {

		SoapTypeMapping tm = new SoapTypeMapping();

		tm.add(boolean.class, SOAPEncodingTypes.BOOLEAN.getQName(),
				new BooleanType());

		register(tm, boolean.class, SOAPEncodingTypes.BOOLEAN,
				new BooleanType());

		register(tm, boolean[].class, createCollectionQName(new BooleanType(),
				SOAPEncodingTypes.BOOLEAN), new ArrayType());

		register(tm, int.class, SOAPEncodingTypes.INT, new IntType());
		register(tm, int[].class, createCollectionQName(new IntType(),
				SOAPEncodingTypes.INT), new ArrayType());

		register(tm, short.class, SOAPEncodingTypes.SHORT, new ShortType());

		register(tm, short[].class, createCollectionQName(new ShortType(),
				SOAPEncodingTypes.SHORT), new ArrayType());

		register(tm, double.class, SOAPEncodingTypes.DOUBLE, new DoubleType());
		register(tm, double[].class, createCollectionQName(new DoubleType(),
				SOAPEncodingTypes.DOUBLE), new ArrayType());

		register(tm, float.class, SOAPEncodingTypes.FLOAT, new FloatType());
		register(tm, float[].class, createCollectionQName(new FloatType(),
				SOAPEncodingTypes.FLOAT), new ArrayType());

		register(tm, long.class, SOAPEncodingTypes.LONG, new LongType());
		register(tm, long[].class, createCollectionQName(new LongType(),
				SOAPEncodingTypes.LONG), new ArrayType());

		register(tm, String.class, SOAPEncodingTypes.STRING, new StringType());
		register(tm, String[].class, createCollectionQName(new StringType(),
				SOAPEncodingTypes.STRING), new ArrayType());

		register(tm, Boolean.class, SOAPEncodingTypes.BOOLEAN,
				new BooleanType());
		register(tm, Boolean[].class, collectionQName(new BooleanType(),
				XMLXsdTypes.BOOLEAN), new ArrayType());

		register(tm, Integer.class, SOAPEncodingTypes.INT, new IntType());
		register(tm, Integer[].class, collectionQName(new IntType(),
				XMLXsdTypes.INT), new ArrayType());

		register(tm, Short.class, SOAPEncodingTypes.SHORT, new ShortType());

		register(tm, Short[].class, createCollectionQName(new ShortType(),
				SOAPEncodingTypes.SHORT), new ArrayType());

		register(tm, Double.class, SOAPEncodingTypes.DOUBLE, new DoubleType());
		register(tm, Double[].class, createCollectionQName(new DoubleType(),
				SOAPEncodingTypes.DOUBLE), new ArrayType());

		register(tm, Float.class, SOAPEncodingTypes.FLOAT, new FloatType());

		register(tm, Float[].class, createCollectionQName(new FloatType(),
				SOAPEncodingTypes.FLOAT), new ArrayType());
		register(tm, Long.class, SOAPEncodingTypes.LONG, new LongType());
		register(tm, Long[].class, createCollectionQName(new LongType(),
				SOAPEncodingTypes.LONG), new ArrayType());

		register(tm, java.sql.Date.class, SOAPEncodingTypes.DATETIME,
				new SqlDateType());
		register(tm, Date[].class, createCollectionQName(new DateType(),
				SOAPEncodingTypes.DATETIME), new ArrayType());

		register(tm, byte[].class, SOAPEncodingTypes.BASE64, new Base64Type());

		// stm.add(BigDecimal.class, SOAPEncodingTypes.DECIMAL.getQName(), new
		// BigDecimalType());
		// stm.add(BigInteger.class, SOAPEncodingTypes.INTEGER.getQName(), new
		// BigIntegerType());
		// stm.add(charegister(tm,,SOAPEncodingTypes.CHAR.getQName(),new
		// CharacterType());
		// stm.add(Character.class,SOAPEncodingTypes.CHAR.getQName(),new
		// CharacterType());
		// stm.add(Date.class, SOAPEncodingTypes.DATETIME.getQName(), new
		// DateTimeType());

		// stm.add(Calendar.class, SOAPEncodingTypes.DATETIME.getQName(), new
		// CalendarType());

		register(tm, String.class, XMLXsdTypes.STRING, new StringType());
		register(tm, String[].class, collectionQName(new StringType(),
				XMLXsdTypes.STRING), new ArrayType());

		register(tm, Long.class, XMLXsdTypes.LONG, new LongType());
		register(tm, Long[].class, collectionQName(new LongType(),
				XMLXsdTypes.LONG), new ArrayType());

		register(tm, long.class, XMLXsdTypes.LONG, new LongType());
		register(tm, long[].class, collectionQName(new LongType(),
				XMLXsdTypes.LONG), new ArrayType());

		register(tm, Float.class, XMLXsdTypes.FLOAT, new FloatType());
		register(tm, Float[].class, collectionQName(new FloatType(),
				XMLXsdTypes.FLOAT), new ArrayType());

		register(tm, float.class, XMLXsdTypes.FLOAT, new FloatType());
		register(tm, float[].class, collectionQName(new FloatType(),
				XMLXsdTypes.FLOAT), new ArrayType());

		register(tm, Short.class, XMLXsdTypes.SHORT, new ShortType());
		register(tm, Short[].class, collectionQName(new ShortType(),
				XMLXsdTypes.SHORT), new ArrayType());

		register(tm, short.class, XMLXsdTypes.SHORT, new ShortType());
		register(tm, short[].class, collectionQName(new ShortType(),
				XMLXsdTypes.SHORT), new ArrayType());

		register(tm, Double.class, XMLXsdTypes.DOUBLE, new DoubleType());
		register(tm, Double[].class, collectionQName(new DoubleType(),
				XMLXsdTypes.DOUBLE), new ArrayType());
		register(tm, double.class, XMLXsdTypes.DOUBLE, new DoubleType());
		register(tm, double[].class, collectionQName(new DoubleType(),
				XMLXsdTypes.DOUBLE), new ArrayType());

		register(tm, BigDecimal.class, XMLXsdTypes.DECIMAL, new DecimalType());
		register(tm, BigDecimal[].class, collectionQName(new DecimalType(),
				XMLXsdTypes.DECIMAL), new ArrayType());

		register(tm, Integer.class, XMLXsdTypes.INT, new IntType());
		register(tm, Integer[].class, collectionQName(new IntType(),
				XMLXsdTypes.INT), new ArrayType());
		register(tm, int.class, XMLXsdTypes.INT, new IntType());
		register(tm, int[].class, collectionQName(new IntType(),
				XMLXsdTypes.INT), new ArrayType());

		register(tm, BigInteger.class, XMLXsdTypes.INTEGER, new IntegerType());
		register(tm, BigInteger[].class, collectionQName(new IntegerType(),
				XMLXsdTypes.INTEGER), new ArrayType());

		register(tm, Boolean.class, XMLXsdTypes.BOOLEAN, new BooleanType());
		register(tm, Boolean[].class, collectionQName(new BooleanType(),
				XMLXsdTypes.BOOLEAN), new ArrayType());

		register(tm, boolean.class, XMLXsdTypes.BOOLEAN, new BooleanType());
		register(tm, boolean[].class, collectionQName(new BooleanType(),
				XMLXsdTypes.BOOLEAN), new ArrayType());

		register(tm, Date.class, XMLXsdTypes.DATETIME, new DateType());
		register(tm, Date[].class, collectionQName(new DateType(),
				XMLXsdTypes.DATETIME), new ArrayType());

		register(tm, java.sql.Date.class, XMLXsdTypes.DATETIME,
				new SqlDateType());
		register(tm, java.sql.Date[].class, collectionQName(new SqlDateType(),
				XMLXsdTypes.DATETIME), new ArrayType());

		register(tm, Time.class, XMLXsdTypes.TIME, new TimeType());
		register(tm, Time[].class, collectionQName(new TimeType(),
				XMLXsdTypes.TIME), new ArrayType());

		register(tm, Timestamp.class, XMLXsdTypes.TIME, new TimestampType());
		register(tm, Timestamp[].class, collectionQName(new TimestampType(),
				XMLXsdTypes.TIME), new ArrayType());

		register(tm, URI.class, XMLXsdTypes.URI, new URIType());
		register(tm, URI[].class, collectionQName(new URIType(),
				XMLXsdTypes.URI), new ArrayType());

		register(tm, byte[].class, XMLXsdTypes.BASE64, new Base64Type());

		register(tm, Document.class, XMLXsdTypes.ANY, new DocumentType());
		register(tm, Source.class, XMLXsdTypes.ANY, new SourceType());
		register(tm, XMLStreamReader.class, XMLXsdTypes.ANY,
				new XMLStreamReaderType());
		register(tm, Element.class, XMLXsdTypes.ANY, new JDOMElementType());
		register(tm, org.jdom.Document.class, XMLXsdTypes.ANY,
				new JDOMDocumentType());
		register(tm, Object.class, XMLXsdTypes.ANY, new ObjectType());

		register(tm, Map.class, XMLXsdTypes.ANY, new StructType(this));
		register(tm, Map[].class, collectionQName(new StructType(this),
				XMLXsdTypes.ANY), new ArrayType());

		register(tm, HashMap.class, XMLXsdTypes.ANY, new StructType(this));
		register(tm, HashMap[].class, collectionQName(new StructType(this),
				XMLXsdTypes.ANY), new ArrayType());

		register(tm, Hashtable.class, XMLXsdTypes.ANY, new StructType(this));
		register(tm, Hashtable[].class, collectionQName(new StructType(this),
				XMLXsdTypes.ANY), new ArrayType());

		register(tm, LinkedHashMap.class, XMLXsdTypes.ANY, new StructType(this));
		register(tm, LinkedHashMap[].class, collectionQName(
				new StructType(this), XMLXsdTypes.ANY), new ArrayType());

		register(tm, Properties.class, XMLXsdTypes.ANY, new StructType(this));
		register(tm, Properties[].class, collectionQName(new StructType(this),
				XMLXsdTypes.ANY), new ArrayType());

		register(tm, TreeMap.class, XMLXsdTypes.ANY, new StructType(this));
		register(tm, TreeMap[].class, collectionQName(new StructType(this),
				XMLXsdTypes.ANY), new ArrayType());

		mappings.put("default", tm);
	}

	protected QName collectionQName(SoapType compounttype, XMLXsdTypes type) {
		return collectionQName(compounttype, type.getQName());

	}

	protected QName createCollectionQName(SoapType compounttype,
			SOAPEncodingTypes type) {
		return collectionQName(compounttype, type.getQName());
	}

	protected QName collectionQName(SoapType compounttype, QName qn) {

		String ns;

		if (compounttype.isComplex()) {
			ns = compounttype.getXmlSchemaType().getNamespaceURI();
		} else {
			ns = "http://schemas.xmlsoap.org/soap/encoding/";
		}

		String first = qn.getLocalPart().substring(0, 1);
		String last = qn.getLocalPart().substring(1);
		String localName = "ArrayOf" + first.toUpperCase() + last;

		return new QName(ns, localName);
	}

	public QName createCollectionQName(String ns, QName qn) {

		if (ns == null || ns.isEmpty()) {
			ns = "http://schemas.xmlsoap.org/soap/encoding/";
		}
		String first = qn.getLocalPart().substring(0, 1);
		String last = qn.getLocalPart().substring(1);
		String localName = "ArrayOf" + first.toUpperCase() + last;

		return new QName(ns, localName);
	}
}
