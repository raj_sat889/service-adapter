package com.jawise.serviceadapter.convert.soap.encoding;

import javax.xml.namespace.QName;

import org.codehaus.xfire.soap.SoapConstants;

public enum XMLXsdTypes {
	STRING("string"), LONG("long"), FLOAT("float"), DOUBLE("double"), INT("int"), SHORT(
			"short"), BOOLEAN("boolean"), DATETIME("dateTime"), TIME("dateTime"), BASE64(
			"base64Binary"), DECIMAL("decimal"), INTEGER("integer"), URI(
			"anyURI"), ANY("anyType"),
	DATE("date"), DURATION("duration"), G_YEAR_MONTH("gYearMonth"), G_MONTH_DAY(
			"gMonthDay"), G_YEAR("gYear"), G_MONTH("gMonth"), G_DAY("gDay");
	String value;

	XMLXsdTypes(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public QName getQName() {
		return new QName(SoapConstants.XSD, value, SoapConstants.XSD_PREFIX);
	}
}
