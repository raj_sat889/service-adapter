package com.jawise.serviceadapter.convert.soap.encoding;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import com.jawise.serviceadapter.convert.soap.type.SoapType;

public class SoapTypeMapping {
	
	@SuppressWarnings("unchecked")
	Map<Class,QName> classToXml;
	@SuppressWarnings("unchecked")
	Map<Class,SoapType> classToSoapType;
	@SuppressWarnings("unchecked")
	Map<QName,Class> xmlToClass;
	
	@SuppressWarnings("unchecked")
	public SoapTypeMapping() {
		classToXml = Collections.synchronizedMap(new HashMap<Class,QName>());
		classToSoapType = Collections.synchronizedMap(new HashMap<Class,SoapType>());
		xmlToClass = Collections.synchronizedMap(new HashMap<QName,Class>());		
	}
	
	@SuppressWarnings("unchecked")
	public void add(Class javaType, QName xmlType, SoapType type) {
		type.setJavaType(javaType);
		type.setXmlSchemaType(xmlType);
		type.setSoapTypeMapping(this);
		classToXml.put(javaType, xmlType);
		classToSoapType.put(javaType, type);
		xmlToClass.put(xmlType, javaType);
	}
	
	@SuppressWarnings("unchecked")
	public boolean isMapped(Class javaType) {
		boolean mapped = classToSoapType.containsKey(javaType);
		return mapped;
	}
	
	public boolean isMapped(QName xmlType) {
		boolean mapped = xmlToClass.containsKey(xmlType);
		return mapped;
	}	
	
    @SuppressWarnings("unchecked")
	public SoapType getSoapType(Class javaType) {
    	return classToSoapType.get(javaType);
    }

    @SuppressWarnings("unchecked")
	public SoapType getSoapType(QName xmlType) {
    	Class clazz = xmlToClass.get(xmlType);
    	return classToSoapType.get(clazz);    	
    }

    @SuppressWarnings("unchecked")
	public QName getSoapTypeQName(Class clazz) {
    	return classToXml.get(clazz);
    }
}
