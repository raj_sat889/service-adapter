package com.jawise.serviceadapter.convert.soap;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfig;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcClientRequestImpl;
import org.apache.xmlrpc.client.XmlRpcSunHttpTransportFactory;
import org.xml.sax.SAXException;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.nvp.NvpToXmlRpcConverter;
import com.jawise.serviceadapter.convert.rpc.XmlRpcConversionHelper;
import com.jawise.serviceadapter.convert.rpc.XmlRpcSerialiser;
import com.jawise.serviceadapter.convert.soap.binding.SoapMessageParser;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;

public class SoapToXmlRpcConverter extends AbstractSoapConverter {
	private static Logger logger = Logger
			.getLogger(SoapToXmlRpcConverter.class);

	public SoapToXmlRpcConverter(MessageContext ctx) throws MessageException {
		super(ctx);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object doConvertion() throws MessageException {
		try {
			SoapMessageParser parser = new SoapMessageParser(getCtx());

			// the main parsing is done here
			List parameters = parser.getParameters();
			// now populate source map;
			populateSourceMap(parameters);

			// do conversion
			convertParts();

			getCtx().put("paramterslist", trargetParameters);
			getCtx().put("paramtersmap", targetMap);
			XmlRpcConversionHelper xmlrpcHelper = new XmlRpcConversionHelper();
			String convertedMsg = "";
			if (isInput()) {
				List parmeters = xmlrpcHelper.convertTargetToXmlRpcParmeters(
						this, targetMap, trargetParameters);
				Object[] parmeterArray = new Object[parmeters.size()];
				parmeterArray = parmeters.toArray(parmeterArray);
				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
				String method = xmlrpcHelper.getAdapteeMethod(this);
				XmlRpcRequest request = new XmlRpcClientRequestImpl(config,
						method, parmeterArray);
				XmlRpcClient client = new XmlRpcClient();
				client.setTransportFactory(new XmlRpcSunHttpTransportFactory(
						client));
				client.setConfig((XmlRpcClientConfig) config);
				convertedMsg = new XmlRpcSerialiser()
						.serialise(client, request);
			} else {
				List result = xmlrpcHelper.convertTargetToXmlRpcResult(this,
						targetMap, trargetParameters);
				Object[] resultArray = new Object[result.size()];
				resultArray = result.toArray(resultArray);

				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
				XmlRpcClient client = new XmlRpcClient();
				client.setTransportFactory(new XmlRpcSunHttpTransportFactory(
						client));
				client.setConfig((XmlRpcClientConfig) config);
				convertedMsg = new XmlRpcSerialiser().serialise(client, config,
						result.get(0));
			}
			logger.debug("finished conversion");
			return convertedMsg;
		} catch (MessageException e) {
			throw e;
		} catch (Exception e) {
			throw new MessageException(e.getMessage(), e);
		}

	}

	@Override
	protected Map getTargetReferances() throws MessageException {
		// TODO Auto-generated method stub
		return targetReferences;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		XmlPath p = new XmlPath(targetName);
		targetMap.put(targetName, targetValue);
		targetReferences.put(p.getName(), targetValue);
		trargetParameters.add(targetName);

	}

}
