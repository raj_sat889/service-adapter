package com.jawise.serviceadapter.convert.soap.type;

import java.net.URI;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class URIType extends SoapType {

	@Override
	public Object read(MessagePartReader reader) throws Exception {
		final String value = reader.getValue();

		return null == value ? null : URI.create(value);
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
		writer.writeValue(((URI) object).toASCIIString());
	}

}
