package com.jawise.serviceadapter.convert.soap.type;

import java.sql.Time;
import java.text.ParseException;
import java.util.Calendar;

import org.apache.ws.commons.util.XsTimeFormat;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;
import com.jawise.serviceadapter.convert.soap.binding.SoapException;

public class TimeType extends SoapType {
	private static XsTimeFormat format = new XsTimeFormat();

	@Override
	public Object read(MessagePartReader reader) throws Exception {
		String value = reader.getValue();

		if (value == null)
			return null;

		try {
			Calendar c = (Calendar) format.parseObject(value);
			return new Time(c.getTimeInMillis());
		} catch (ParseException e) {
			throw new SoapException("Could not parse xs:dateTime: "
					+ e.getMessage(), e);
		}
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
		Calendar c = Calendar.getInstance();
		c.setTime((Time) object);
		writer.writeValue(format.format(c));
	}

}
