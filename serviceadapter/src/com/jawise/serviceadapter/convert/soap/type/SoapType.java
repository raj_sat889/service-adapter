package com.jawise.serviceadapter.convert.soap.type;

import java.util.Set;

import javax.xml.namespace.QName;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;
import com.jawise.serviceadapter.convert.soap.encoding.SoapTypeMapping;

public abstract class SoapType {
	@SuppressWarnings("unchecked")
	Class javaType;	
	private QName xmlSchemaType;	
	private boolean writeOuter = true;
	private boolean abstrct = true;
	private boolean nillable = true;
	@SuppressWarnings("unchecked")
	
	private SoapTypeMapping soapTypeMapping;

	@SuppressWarnings("unchecked")

	public Class getJavaType() {
		return javaType;
	}
	
	public void setJavaType(Class javaType) {
		this.javaType = javaType;

		if (javaType.isPrimitive()) {
			setNillable(false);
		}
	}	
	
	public abstract Object read(MessagePartReader reader)
			throws Exception;

	public abstract void write(Object object, MessagePartWriter writer)
			throws Exception;

	public boolean isComplex() {
		return false;
	}

	public boolean isAbstract() {
		return abstrct;
	}

	public void setAbstract(boolean abstrct) {
		this.abstrct = abstrct;
	}

	public boolean isNillable() {
		return nillable;
	}

	public void setNillable(boolean nillable) {
		this.nillable = nillable;
	}


	public Set getDependencies() {
		return null;
	}

	public boolean equals(Object obj) {
		if (obj == this)
			return true;

		if (obj instanceof SoapType) {
			SoapType type = (SoapType) obj;

			if (type.getXmlSchemaType().equals(getXmlSchemaType())
					&& type.getJavaType().equals(getJavaType())) {
				return true;
			}
		}

		return false;
	}



	public int hashCode() {
		int hashcode = 0;

		if (getJavaType() != null) {
			hashcode ^= getJavaType().hashCode();
		}

		if (getXmlSchemaType() != null) {
			hashcode ^= getXmlSchemaType().hashCode();
		}

		return hashcode;
	}

	/**
	 * @return Get the schema type.
	 */
	public QName getXmlSchemaType() {
		return xmlSchemaType;
	}

	/**
	 * @param name
	 *            The qName to set.
	 */
	public void setXmlSchemaType(QName name) {
		xmlSchemaType = name;
	}

	public boolean isWriteOuter() {
		return writeOuter;
	}

	public void setWriteOuter(boolean writeOuter) {
		this.writeOuter = writeOuter;
	}

	@SuppressWarnings("unchecked")
	public String toString() {
		StringBuffer sb = new StringBuffer(getClass().getName());
		sb.append("[class=");
		Class c = getJavaType();
		sb.append((c == null) ? ("<null>") : (c.getName()));
		sb.append(",\nQName=");
		QName q = getXmlSchemaType();
		sb.append((q == null) ? ("<null>") : (q.toString()));
		sb.append("]");
		return sb.toString();
	}

	public SoapTypeMapping getSoapTypeMapping() {
		return soapTypeMapping;
	}

	public void setSoapTypeMapping(SoapTypeMapping soapTypeMapping) {
		this.soapTypeMapping = soapTypeMapping;
	}
}
