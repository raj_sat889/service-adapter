package com.jawise.serviceadapter.convert.soap.type;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;
import org.codehaus.xfire.soap.SoapConstants;
import org.codehaus.xfire.util.NamespaceHelper;

public class BindingHelper {
	private static Logger logger = Logger.getLogger(BindingHelper.class);
	public static SoapType getReadType(XMLStreamReader xsr,
			SoapType type) {
    	
        String overrideType = xsr.getAttributeValue(SoapConstants.XSI_NS, "type");
        if (overrideType != null)
        {
            QName overrideTypeName = NamespaceHelper.createQName(xsr.getNamespaceContext(),
                                                                 overrideType);
            if (!overrideTypeName.equals(type.getXmlSchemaType()))
            {
                SoapType type2 = type.getSoapTypeMapping().getSoapType(overrideTypeName);
                if (type2 == null)
                {
                	logger.info("xsi:type=\"" + overrideTypeName
                             + "\" was requested, but no corresponding Type was registered. Defaulting to "
                             + type.getXmlSchemaType());
                }
                else
                {
                    type = type2;
                }
            }
        }
        return type;
	}

	public static SoapType getWriteType(Object value, SoapType type) {

//        if (value != null && type != null && type.getTypeClass() != value.getClass()
//                && context.getService() != null)
//        {
//            List l = (List) context.getService().getProperty(OVERRIDE_TYPES_KEY);
//            if (l != null && l.contains(value.getClass().getName()))
//            {
//                type = type.getTypeMapping().getType(value.getClass());
//            }
//        }
        return type;
	}

}
