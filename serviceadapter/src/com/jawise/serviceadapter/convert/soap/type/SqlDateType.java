package com.jawise.serviceadapter.convert.soap.type;

import java.sql.Date;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class SqlDateType extends DateType {
	private static Logger logger = Logger.getLogger(SqlDateType.class);
	@Override
	public Object read(MessagePartReader reader) throws Exception {
        Date date = ((Date) super.read(reader));
        if (date == null) return null;
        
        return new java.sql.Date(date.getTime());
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
        try {
			java.sql.Date date = (java.sql.Date) object;
			
			super.write(new Date(date.getTime()), writer);
		} catch (RuntimeException e) {
			logger.error(e.getMessage(),e);
		}		
	}

}
