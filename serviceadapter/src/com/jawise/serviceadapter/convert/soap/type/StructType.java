package com.jawise.serviceadapter.convert.soap.type;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.soap.binding.MessageElementReader;
import com.jawise.serviceadapter.convert.soap.binding.MessageElementWriter;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;
import com.jawise.serviceadapter.convert.soap.binding.RpcTypeBinding;
import com.jawise.serviceadapter.convert.soap.binding.SoapException;
import com.jawise.serviceadapter.convert.soap.encoding.SoapTypeMapping;
import com.jawise.serviceadapter.convert.soap.encoding.SoapTypeMappingRegistry;
import com.jawise.serviceadapter.convert.soap.encoding.XMLXsdTypes;

/**
 * The complext types in ServiceAdapter are map to Java.util.Map types.
 * 
 * @author sathyan
 * 
 */
public class StructType extends SoapType {
	
	private static Logger logger = Logger.getLogger(StructType.class);
	
	SoapTypeMappingRegistry soapTypeMappingRegistry;

	public StructType(SoapTypeMappingRegistry soapTypeMappingRegistry) {
		this.soapTypeMappingRegistry = soapTypeMappingRegistry;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object read(MessagePartReader reader) throws Exception {

		Map map = new HashMap();
		try {

			SoapType valueType = null;

			Object key = null;
			Object value = null;

			SoapTypeMapping soapTypeMapping = soapTypeMappingRegistry
					.getMappings().get("default");

			while (reader.hasMoreElementReaders()) {

				MessageElementReader structMemberReader = (MessageElementReader) reader
						.getNextElementReader();

				structMemberReader.extractXsiType();
				QName xsiType = structMemberReader.getXsiType();

				if (xsiType == null) {
					xsiType = XMLXsdTypes.STRING.getQName();
					valueType = soapTypeMapping.getSoapType(xsiType);
				} else if (xsiType != null && structMemberReader.isArray()) {
					QName arrayType = soapTypeMappingRegistry.createCollectionQName("",
							structMemberReader.getArrayType());
					valueType = soapTypeMapping.getSoapType(arrayType);
				}
				else if(xsiType != null) {
					valueType = soapTypeMapping.getSoapType(xsiType);					
				}

				if (valueType == null) {
					if(structMemberReader.isArray()) {
						valueType = soapTypeMapping.getSoapType(Map[].class);	
					}
					else{
						valueType = soapTypeMapping.getSoapType(Map.class);
					}
				}
				
				key = structMemberReader.getName().getLocalPart();
				logger.debug("reading complex type members : " + key);
				value = valueType.read(structMemberReader);
				map.put(key, value);
				// readToEnd(structMemberReader);

			}

			return map;
		} catch (IllegalArgumentException e) {
			throw new SoapException("Illegal argument.", e);
		}

	}

	private void readToEnd(MessagePartReader childReader) throws SoapException {
		while (childReader.hasMoreElementReaders()) {
			readToEnd(childReader.getNextElementReader());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void write(Object object, MessagePartWriter writer) throws Exception {

		if (object == null)
			return;

		// ((MessageElementWriter) writer).writeXsiType(getXmlSchemaType());

		try {
			Map map = (Map) object;

			for (Iterator itr = map.entrySet().iterator(); itr.hasNext();) {
				Map.Entry entry = (Map.Entry) itr.next();
				SoapTypeMapping soapTypeMapping = soapTypeMappingRegistry
						.getMappings().get("default");
				Object value = entry.getValue();
				SoapType valueType = null;
				if (value == null) {
					valueType = new StringType();
				} else {
					valueType = soapTypeMapping.getSoapType(value.getClass());
				}

				valueType.setXmlSchemaType(new QName(
						"http://soap.svc.test.serviceadapter.jawise.com",
						(String) entry.getKey()));				
				
				writeStructMember((MessageElementWriter) writer, valueType,
						entry);
			}
		} catch (IllegalArgumentException e) {
			throw new SoapException("Illegal argument.", e);
		}

	}

	@SuppressWarnings("unchecked")
	private void writeStructMember(MessageElementWriter writer,
			SoapType valueType, Entry entry) throws Exception {
		// valueType.getXmlSchemaType().getNamespaceURI()
		QName name = new QName(valueType.getXmlSchemaType().getNamespaceURI(),
				(String) entry.getKey());
		Object value = entry.getValue();

		MessageElementWriter cwriter;

		// Write the value if it is not null.
		if (value != null) {
			cwriter = getWriter(writer, name, valueType);

			if (valueType == null)
				throw new SoapException("Couldn't find type for "
						+ value.getClass() + " for property " + name);

			valueType.write(value, cwriter);

			cwriter.close();
		} else if (valueType.isNillable()) {
			cwriter = getWriter(writer, name, valueType);
			// Write the xsi:nil if it is null.
			cwriter.writeXsiNil();

			cwriter.close();
		}
	}

	private MessageElementWriter getWriter(MessageElementWriter writer,
			QName name, SoapType type) throws SoapException {
		MessageElementWriter cwriter;
		if (type.isAbstract()) {
			cwriter = writer.getElementWriter(name);
		} else {
			cwriter = writer.getElementWriter(name);
		}

		return cwriter;
	}
}