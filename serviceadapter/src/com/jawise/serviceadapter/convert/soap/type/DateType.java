package com.jawise.serviceadapter.convert.soap.type;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.codehaus.xfire.util.date.XsDateFormat;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;
import com.jawise.serviceadapter.convert.soap.binding.SoapException;

public class DateType extends SoapType {

	private static XsDateFormat format = new XsDateFormat();

	public Object read(MessagePartReader reader) throws Exception {
		String value = reader.getValue();

		if (value == null)
			return null;

		try {
			Calendar c = (Calendar) format.parseObject(value);
			return c.getTime();
		} catch (ParseException e) {
			throw new SoapException(
					"Could not parse xs:dat: " + e.getMessage(), e);
		}
	}

	public void write(Object object, MessagePartWriter writer) throws Exception{
		Calendar c = Calendar.getInstance();
		c.setTime((Date) object);
		writer.writeValue(format.format(c));
	}

}
