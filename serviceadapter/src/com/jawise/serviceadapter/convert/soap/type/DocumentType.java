package com.jawise.serviceadapter.convert.soap.type;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.codehaus.xfire.util.stax.FragmentStreamReader;
import org.w3c.dom.Document;

import com.jawise.serviceadapter.convert.soap.binding.MessageAttributeWriter;
import com.jawise.serviceadapter.convert.soap.binding.MessageElementReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;
import com.jawise.serviceadapter.convert.soap.binding.SoapException;
import com.jawise.serviceadapter.convert.soap.binding.SoapXmlUtil;

public class DocumentType extends SoapType {



	private DocumentBuilder builder;

	public DocumentType() throws SoapException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new SoapException("Couldn't load document builder.",
					e);
		}
		setWriteOuter(false);
	}

	public DocumentType(DocumentBuilder builder) {
		this.builder = builder;
		setWriteOuter(false);
	}

	public Object read(MessagePartReader mreader) throws SoapException {
		try {
			XMLStreamReader reader = ((MessageElementReader) mreader)
					.getXMLStreamReader();
			return SoapXmlUtil.read(builder, new FragmentStreamReader(reader),
					true);
		} catch (XMLStreamException e) {
			throw new SoapException("Could not parse xml.", e);
		}
	}

	public void write(Object object, MessagePartWriter writer)
			throws Exception {
		
		Document doc = (Document) object;
		try {
			
			SoapXmlUtil.writeElement(doc.getDocumentElement(),
					((MessageAttributeWriter) writer).getWriter(), false);
		} catch (XMLStreamException e) {
			throw new SoapException("Could not write xml.", e);
		}
		
		throw new UnsupportedOperationException();
	}

}
