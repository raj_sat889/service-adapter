package com.jawise.serviceadapter.convert.soap.type;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class DoubleType extends SoapType {

	@Override
	public Object read(MessagePartReader reader) throws Exception {
		return new Double( reader.getValueAsDouble() );
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
        if (object instanceof Double)
        {
            writer.writeValueAsDouble( (Double) object );
        }
        else
        {
            writer.writeValueAsDouble(new Double(((Number)object).doubleValue()));
        }
		
	}

}
