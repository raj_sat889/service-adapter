package com.jawise.serviceadapter.convert.soap.type;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class ShortType extends SoapType {

	@Override
	public Object read(MessagePartReader reader) throws Exception {
		return new Short( reader.getValue() );
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
        if (object instanceof Short)
        {
            writer.writeValueAsShort( (Short) object );
        }
        else
        {
            writer.writeValueAsShort(new Short(((Number)object).shortValue()));
        }
		
	}

}
