package com.jawise.serviceadapter.convert.soap.type;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class StringType extends SoapType {

	@Override
	public Object read(MessagePartReader reader) throws Exception {
		return reader.getValue();
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
		writer.writeValue( object );		
	}

}
