package com.jawise.serviceadapter.convert.soap.type;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.codehaus.xfire.soap.SoapConstants;
import org.codehaus.xfire.util.NamespaceHelper;
import org.jdom.Attribute;
import org.jdom.Element;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;
import com.jawise.serviceadapter.convert.soap.binding.SoapException;

public class ArrayType extends SoapType {

	private QName componentName;
	private static final Logger logger = Logger.getLogger(ArrayType.class);
	private long minOccurs = 0;
	private long maxOccurs = Long.MAX_VALUE;
	private boolean flat;

	public ArrayType() {
	}

	@SuppressWarnings("unchecked")
	public Object read(MessagePartReader reader) throws Exception {
		try {
			Collection values = readCollection(reader);
			return makeArray(getComponentType().getJavaType(), values);
		} catch (IllegalArgumentException e) {
			throw new SoapException("Illegal argument.", e);
		}
	}

	@SuppressWarnings("unchecked")
	protected Collection createCollection() {
		return new ArrayList();
	}

	@SuppressWarnings("unchecked")
	protected Collection readCollection(MessagePartReader reader)
			throws Exception {
		Collection values = createCollection();

		while (reader.hasMoreElementReaders()) {
			MessagePartReader creader = reader.getNextElementReader();
			//AegisBindingProvider
			SoapType compType = BindingHelper.getReadType(creader
					.getXMLStreamReader(), getComponentType());

			if (creader.isXsiNil()) {
				values.add(null);
				creader.readToEnd();
			} else {
				values.add(compType.read(creader));
			}

			// check max occurs
			int size = values.size();
			if (size > maxOccurs)
				throw new SoapException("The number of elements in "
						+ getXmlSchemaType() + " exceeds the maximum of "
						+ maxOccurs);

		}

		// check min occurs
		if (values.size() < minOccurs)
			throw new SoapException("The number of elements in "
					+ getXmlSchemaType() + " does not meet the minimum of "
					+ minOccurs);
		return values;
	}

	protected Object makeArray(Class arrayType, Collection values) {
		if (Integer.TYPE.equals(arrayType)) {
			Object[] objects = values.toArray();
			Object array = Array.newInstance(Integer.TYPE, objects.length);
			for (int i = 0, n = objects.length; i < n; i++) {
				Array.set(array, i, objects[i]);
			}
			return array;
		} else if (Long.TYPE.equals(arrayType)) {
			Object[] objects = values.toArray();
			Object array = Array.newInstance(Long.TYPE, objects.length);
			for (int i = 0, n = objects.length; i < n; i++) {
				Array.set(array, i, objects[i]);
			}
			return array;
		} else if (Short.TYPE.equals(arrayType)) {
			Object[] objects = values.toArray();
			Object array = Array.newInstance(Short.TYPE, objects.length);
			for (int i = 0, n = objects.length; i < n; i++) {
				Array.set(array, i, objects[i]);
			}
			return array;
		} else if (Double.TYPE.equals(arrayType)) {
			Object[] objects = values.toArray();
			Object array = Array.newInstance(Double.TYPE, objects.length);
			for (int i = 0, n = objects.length; i < n; i++) {
				Array.set(array, i, objects[i]);
			}
			return array;
		} else if (Float.TYPE.equals(arrayType)) {
			Object[] objects = values.toArray();
			Object array = Array.newInstance(Float.TYPE, objects.length);
			for (int i = 0, n = objects.length; i < n; i++) {
				Array.set(array, i, objects[i]);
			}
			return array;
		} else if (Byte.TYPE.equals(arrayType)) {
			Object[] objects = values.toArray();
			Object array = Array.newInstance(Byte.TYPE, objects.length);
			for (int i = 0, n = objects.length; i < n; i++) {
				Array.set(array, i, objects[i]);
			}
			return array;
		} else if (Boolean.TYPE.equals(arrayType)) {
			Object[] objects = values.toArray();
			Object array = Array.newInstance(Boolean.TYPE, objects.length);
			for (int i = 0, n = objects.length; i < n; i++) {
				Array.set(array, i, objects[i]);
			}
			return array;
		} else if (Character.TYPE.equals(arrayType)) {
			Object[] objects = values.toArray();
			Object array = Array.newInstance(Character.TYPE, objects.length);
			for (int i = 0, n = objects.length; i < n; i++) {
				Array.set(array, i, objects[i]);
			}
			return array;
		}
		return values.toArray((Object[]) Array.newInstance(getComponentType()
				.getJavaType(), values.size()));
	}

	@Override
	public void write(Object values, MessagePartWriter writer) throws Exception {    
    
        if (values == null)
            return;

        SoapType type = getComponentType();

        String ns = null;
        if (type.isAbstract())
            ns = getXmlSchemaType().getNamespaceURI();
        else
            ns = type.getXmlSchemaType().getNamespaceURI();
        
        String name = type.getXmlSchemaType().getLocalPart();

        if ( type == null )
            throw new SoapException( "Couldn't find type for " + type.getJavaType() + "." );

        Class arrayType = type.getJavaType();
        
        if (Object.class.isAssignableFrom(arrayType))
        {
            Object[] objects = (Object[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(objects[i], writer, type, name, ns);
            }
        }
        else if (Integer.TYPE.equals(arrayType))
        {
            int[] objects = (int[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(new Integer(objects[i]), writer, type, name, ns);
            }
        }
        else if (Long.TYPE.equals(arrayType))
        {
            long[] objects = (long[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(new Long(objects[i]), writer, type, name, ns);
            }
        }
        else if (Short.TYPE.equals(arrayType))
        {
            short[] objects = (short[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(new Short(objects[i]), writer, type, name, ns);
            }
        }
        else if (Double.TYPE.equals(arrayType))
        {
            double[] objects = (double[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(new Double(objects[i]), writer, type, name, ns);
            }
        }
        else if (Float.TYPE.equals(arrayType))
        {
            float[] objects = (float[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(new Float(objects[i]), writer, type, name, ns);
            }
        }
        else if (Byte.TYPE.equals(arrayType))
        {
            byte[] objects = (byte[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(new Byte(objects[i]), writer, type, name, ns);
            }
        }
        else if (Boolean.TYPE.equals(arrayType))
        {
            boolean[] objects = (boolean[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(new Boolean(objects[i]), writer, type, name, ns);
            }
        }
        else if (Character.TYPE.equals(arrayType))
        {
            char[] objects = (char[]) values;
            for (int i = 0, n = objects.length; i < n; i++)
            {
                writeValue(new Character(objects[i]), writer, type, name, ns);
            }
        }        
    }

	protected void writeValue(Object value, MessagePartWriter writer,
			SoapType type, String name, String ns) throws Exception {
		type = BindingHelper.getWriteType(value, type);
		MessagePartWriter cwriter;
		if (type.isWriteOuter()) {
			cwriter = writer.getElementWriter(name, ns);
		} else {
			cwriter = writer;
		}

		if (value == null && type.isNillable())
			cwriter.writeXsiNil();
		else
			type.write(value, cwriter);

		cwriter.close();
	}

	public void writeSchema(Element root) throws Exception {
		try {
			Element complex = new Element("complexType",
					SoapConstants.XSD_PREFIX, SoapConstants.XSD);
			complex.setAttribute(new Attribute("name", getXmlSchemaType()
					.getLocalPart()));
			root.addContent(complex);

			Element seq = new Element("sequence", SoapConstants.XSD_PREFIX,
					SoapConstants.XSD);
			complex.addContent(seq);

			Element element = new Element("element", SoapConstants.XSD_PREFIX,
					SoapConstants.XSD);
			seq.addContent(element);

			SoapType componentType = getComponentType();
			String prefix = NamespaceHelper.getUniquePrefix((Element) root
					.getParent(), componentType.getXmlSchemaType()
					.getNamespaceURI());

			String typeName = prefix + ":"
					+ componentType.getXmlSchemaType().getLocalPart();

			element.setAttribute(new Attribute("name", componentType
					.getXmlSchemaType().getLocalPart()));
			element.setAttribute(new Attribute("type", typeName));

			if (componentType.isNillable()) {
				element.setAttribute(new Attribute("nillable", "true"));
			}

			element.setAttribute(new Attribute("minOccurs", new Long(
					getMinOccurs()).toString()));

			if (maxOccurs == Long.MAX_VALUE)
				element.setAttribute(new Attribute("maxOccurs", "unbounded"));
			else
				element.setAttribute(new Attribute("maxOccurs", new Long(
						getMaxOccurs()).toString()));

		} catch (IllegalArgumentException e) {
			throw new SoapException("Illegal argument.", e);
		}
	}

	public boolean isComplex() {
		return true;
	}

	public QName getComponentName() {
		return componentName;
	}

	public void setComponentName(QName componentName) {
		this.componentName = componentName;
	}

	public Set getDependencies() {
		Set deps = new HashSet();

		deps.add(getComponentType());

		return deps;
	}

	public SoapType getComponentType() {
		Class compType = getJavaType().getComponentType();

		SoapType type;

		if (componentName == null) {
			type = getSoapTypeMapping().getSoapType(compType);
		} else {
			type = getSoapTypeMapping().getSoapType(componentName);

			// We couldn't find the type the user specified. One is created
			// below instead.
			if (type == null) {
				logger.debug("Couldn't find array component type "
						+ componentName + ". Creating one instead.");
			}
		}

		if (type == null) {
			//type = getSoapTypeMapping().getTypeCreator().createType(compType);
			//getSoapTypeMapping().register(type);
		}

		return type;
	}


	public long getMaxOccurs() {
		return maxOccurs;
	}

	public void setMaxOccurs(long maxOccurs) {
		this.maxOccurs = maxOccurs;
	}

	public long getMinOccurs() {
		return minOccurs;
	}

	public void setMinOccurs(long minOccurs) {
		this.minOccurs = minOccurs;
	}

	public boolean isFlat() {
		return flat;
	}

	public void setFlat(boolean flat) {
		setWriteOuter(!flat);
		this.flat = flat;
	}
}
