package com.jawise.serviceadapter.convert.soap.type;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.codec.binary.Base64;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;
import com.jawise.serviceadapter.convert.soap.binding.SoapException;

public class Base64Type extends SoapType {

	@Override
	public Object read(MessagePartReader mpreader) throws Exception {
		XMLStreamReader reader = mpreader.getXMLStreamReader();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			int event = reader.next();
			while (!reader.isCharacters() && !reader.isEndElement()
					&& !reader.isStartElement()) {
				event = reader.next();
			}

			if (reader.isEndElement()) {
				reader.next();
				return new byte[0];
			}

			int length = reader.getTextLength();

			char[] databuffer = new char[length];
			for (int sourceStart = 0;; sourceStart += length) {
				int nCopied = reader.getTextCharacters(sourceStart, databuffer,
						0, length);
				Base64 b64 = new Base64();
				char[] decodeable = new char[nCopied];
				for (int i = 0; i < decodeable.length; i++) {
					decodeable[i] = databuffer[i];
				}
				String val = new String(decodeable);
				byte[] decoded = b64.decode(val.getBytes());
				bos.write(decoded);

				if (nCopied < length)
					break;
			}

			while (reader.getEventType() != XMLStreamReader.END_ELEMENT) {
				reader.next();
			}

			reader.next();

			return bos.toByteArray();
		} catch (IOException e) {
			throw new SoapException("Could not parse base64Binary data.", e);
		} catch (XMLStreamException e) {
			throw new SoapException("Could not parse base64Binary data.", e);
		}

	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
		byte[] data = (byte[]) object;
		Base64 b64 = new Base64();
		writer.writeValue(b64.encode(data));
	}

}
