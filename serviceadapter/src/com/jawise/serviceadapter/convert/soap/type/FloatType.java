package com.jawise.serviceadapter.convert.soap.type;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class FloatType extends SoapType {

	@Override
	public Object read(MessagePartReader reader) throws Exception {
		return new Float( reader.getValueAsFloat() );
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
        if (object instanceof Float)
        {
            writer.writeValueAsFloat((Float) object);
        }
        else
        {
            writer.writeValueAsFloat(new Float(((Number)object).floatValue()));
        }
		
	}

}
