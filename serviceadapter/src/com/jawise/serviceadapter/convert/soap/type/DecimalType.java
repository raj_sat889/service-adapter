package com.jawise.serviceadapter.convert.soap.type;

import java.math.BigDecimal;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class DecimalType extends SoapType {

	@Override
	public Object read(MessagePartReader reader) throws Exception {
        final String value = reader.getValue();

        return null == value ? null : new BigDecimal( value );
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
		writer.writeValue( object.toString() );
		
	}

}
