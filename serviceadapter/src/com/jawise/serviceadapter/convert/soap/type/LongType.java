package com.jawise.serviceadapter.convert.soap.type;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class LongType extends SoapType {

	@Override
	public Object read(MessagePartReader reader) throws Exception {
		return new Long( reader.getValueAsLong() );
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
        if (object instanceof Long)
        {
            writer.writeValueAsLong( (Long) object );
        }
        else
        {
            writer.writeValueAsLong(new Long(((Number)object).longValue()));
        }
		
	}

}
