package com.jawise.serviceadapter.convert.soap.type;

import com.jawise.serviceadapter.convert.soap.binding.MessagePartReader;
import com.jawise.serviceadapter.convert.soap.binding.MessagePartWriter;

public class IntType extends SoapType {

	@Override
	public Object read(MessagePartReader reader) throws Exception {
		return new Integer(reader.getValueAsInt());
	}

	@Override
	public void write(Object object, MessagePartWriter writer)
			throws Exception {
		if (object instanceof Integer) {
			writer.writeValueAsInt((Integer) object);
		} else {
			writer.writeValueAsInt(new Integer(((Number) object).intValue()));
		}

	}

}
