package com.jawise.serviceadapter.convert.soap.binding;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

import org.codehaus.xfire.soap.SoapConstants;

public abstract class AbstractMessagePartReader implements MessagePartReader {

	protected static final QName XSI_NIL = new QName(SoapConstants.XSI_NS, "nil",
			SoapConstants.XSI_PREFIX);

	public AbstractMessagePartReader() {
	}

	public void readToEnd() throws SoapException {
		readToEnd(this);
	}

	private void readToEnd(MessagePartReader childReader) throws SoapException {
		while (childReader.hasMoreElementReaders()) {
			readToEnd(childReader.getNextElementReader());
		}
	}

	public boolean isXsiNil() throws SoapException {
		MessagePartReader nilReader = getAttributeReader(XSI_NIL);
		boolean nil = false;
		if (nilReader != null) {
			String value = nilReader.getValue();
			if (value != null && (value.equals("true") || value.equals("1")))
				return true;
		}

		return nil;
	}

	public boolean hasValue() throws SoapException {
		return getValue() != null;
	}

	public char getValueAsCharacter() throws SoapException {
		if (getValue() == null)
			return 0;
		return getValue().charAt(0);
	}

	public int getValueAsInt() throws SoapException {
		if (getValue() == null)
			return 0;

		return Integer.parseInt(getValue());
	}

	public long getValueAsLong() throws SoapException {
		if (getValue() == null)
			return 0l;

		return Long.parseLong(getValue());
	}

	public double getValueAsDouble() throws SoapException {
		if (getValue() == null)
			return 0d;

		return Double.parseDouble(getValue());
	}

	public float getValueAsFloat() throws SoapException {
		if (getValue() == null)
			return 0f;

		return Float.parseFloat(getValue());
	}

	public boolean getValueAsBoolean() throws SoapException {
		String value = getValue();
		if (value == null)
			return false;

		if ("true".equalsIgnoreCase(value) || "1".equalsIgnoreCase(value))
			return true;

		if ("false".equalsIgnoreCase(value) || "0".equalsIgnoreCase(value))
			return false;

		throw new SoapException("Invalid boolean value: " + value);
	}

	public abstract XMLStreamReader getXMLStreamReader();
}
