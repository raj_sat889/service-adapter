package com.jawise.serviceadapter.convert.soap.binding;

import org.codehaus.xfire.soap.Soap11;
import org.codehaus.xfire.soap.Soap12;
import org.codehaus.xfire.soap.SoapVersion;

import com.jawise.serviceadapter.convert.MessageContext;

public class SoapFaultSerializerFactory {

	public static SoapMessageSerializer getSoapFaultSerializer(
			MessageContext ctx) throws SoapException {

		SoapVersion soapVersion = (SoapVersion) ctx
				.get(MessageContext.SOAP_VERSION);
		if (soapVersion instanceof Soap11)
			return new Soap11FaultSerializer();
		else if (soapVersion instanceof Soap12)
			return new Soap12FaultSerializer();
		else
			throw new SoapException("Unrecognized soap version.");
	}
}
