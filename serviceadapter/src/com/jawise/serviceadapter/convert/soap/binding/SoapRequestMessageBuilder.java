package com.jawise.serviceadapter.convert.soap.binding;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Service;

public class SoapRequestMessageBuilder {
	private static Logger logger = Logger
			.getLogger(SoapRequestMessageBuilder.class);
	@SuppressWarnings("unused")
	private MessageContext ctx = null;

	public SoapRequestMessageBuilder(MessageContext ctx) {
		this.ctx = ctx;
	}

	@SuppressWarnings("unchecked")
	public SOAPMessage buildRequestMessage() throws SOAPException, MessageException {
		
		List trargetParameters = (List)ctx.get("paramterslist");
		Map targetMap = (Map)ctx.get("paramtersmap");	
		MessageConversion msgConversion = (MessageConversion) ctx.get("messageConversion");
		Service adapterservice = (Service) ctx.get("adapterService");
		Service targetsvc = adapterservice.getAdapter().getService();
		
		SOAPFactory soapFactory = SOAPFactory.newInstance();
		MessageFactory factory = MessageFactory.newInstance();
		SOAPMessage message = factory.createMessage();

		SOAPHeader header = message.getSOAPHeader();
		SOAPBody body = message.getSOAPBody();
		header.detachNode();

		Name bodyName = soapFactory.createName(msgConversion
				.getAdapteeoperation(), "m", targetsvc.getPort().getLocation());
		SOAPBodyElement bodyElement = body.addBodyElement(bodyName);

		addParameters(trargetParameters, targetMap, soapFactory, bodyElement);

		return message;
	}

	/*
	 * public SOAPMessage buildResponseMessage(Service adapterService,
	 * MessageConversion msgConversion, List trargetParameters, Map targetMap)
	 * throws SOAPException, MessageException {
	 * 
	 * SOAPFactory soapFactory = SOAPFactory.newInstance(); MessageFactory
	 * factory = MessageFactory.newInstance(); SOAPMessage message =
	 * factory.createMessage();
	 * 
	 * SOAPHeader header = message.getSOAPHeader(); SOAPBody body =
	 * message.getSOAPBody(); header.detachNode();
	 * 
	 * String loc = "http://soap.svc.test.serviceadapter.jawise.com"; Name
	 * bodyName = soapFactory.createName(msgConversion .getAdapteroperation() +
	 * "Response", "ns1", loc); SOAPBodyElement bodyElement =
	 * body.addBodyElement(bodyName);
	 * 
	 * SOAPElement returnelemnt = soapFactory.createElement(soapFactory
	 * .createName(msgConversion.getAdapteroperation() + "Return", "", loc));
	 * SOAPElement addChildElement = bodyElement.addChildElement(returnelemnt);
	 * addParameters(trargetParameters, targetMap, soapFactory,
	 * addChildElement);
	 * 
	 * return message; }
	 */
	private void addParameters(List trargetParameters, Map targetMap,
			SOAPFactory soapFactory, SOAPElement element) throws SOAPException,
			MessageException {
		// now add parameters
		for (int i = 0; i < trargetParameters.size(); i++) {
			String pn = (String) trargetParameters.get(i);
			Object v = (Object) targetMap.get(pn);

			// decide what type of paramter
			addSoapPrameter(soapFactory, element, pn, v);

		}
	}

	@SuppressWarnings("unchecked")
	private void addSoapPrameter(SOAPFactory soapFactory, SOAPElement parentEl,
			String pn, Object v) throws SOAPException, MessageException {
		if (v instanceof String) {
			Name name = soapFactory.createName(pn);
			SOAPElement el = parentEl.addChildElement(name);
			el.addTextNode((String) v);
		} else if (v instanceof Number) {
			Name name = soapFactory.createName(pn);
			SOAPElement el = parentEl.addChildElement(name);
			el.addTextNode(((Number) v).toString());
		} else if (v instanceof Map) {

			Name name = soapFactory.createName(pn);
			SOAPElement complexEl = parentEl.addChildElement(name);

			Map complex = (Map) v;
			Iterator iter = complex.keySet().iterator();
			while (iter.hasNext()) {
				String cn = (String) iter.next();
				Object cv = complex.get(cn);
				addSoapPrameter(soapFactory, complexEl, cn, cv);
			}

		} else {
			logger.info("name : " + pn);
			logger.info("value : " + v);
			throw new MessageException("1013", "Type not supported");
		}
	}

}
