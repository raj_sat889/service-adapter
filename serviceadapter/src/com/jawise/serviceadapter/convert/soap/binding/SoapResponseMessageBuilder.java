package com.jawise.serviceadapter.convert.soap.binding;

import java.io.ByteArrayOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.log4j.Logger;
import org.codehaus.xfire.soap.SoapConstants;
import org.codehaus.xfire.soap.SoapVersion;

import com.jawise.serviceadapter.convert.MessageContext;

public class SoapResponseMessageBuilder {

	public static final String DECLARED_NAMESPACES = "declared.namespaces";
	private static Logger logger = Logger
			.getLogger(SoapResponseMessageBuilder.class);
	private SoapMessageSerializer serializer = new RpcTypeBinding();
	private MessageContext ctx = null;
	private String encoding = null;
	private XMLStreamWriter writer = null;
	private ByteArrayOutputStream outputmessage = null;

	public SoapResponseMessageBuilder(MessageContext ctx) throws Exception {
		this.ctx = ctx;
		HttpServletRequest req = (HttpServletRequest) ctx.get("request");
		encoding = req.getCharacterEncoding();
		encoding = SoapHelper.dequote(encoding);
		outputmessage = new ByteArrayOutputStream();
		writer = SoapXmlUtil
				.createXMLStreamWriter(outputmessage, encoding, ctx);
		this.ctx.put("xmlstreamwriter", writer);
	}

	public String buildResponseMessage() throws Exception {

		writeHeaders();
		writeBody();
		writeMessage();
		String response = new String(outputmessage.toByteArray());
		return response;
	}

	private void writeBody() throws Exception {

		try {
			SoapVersion soapversion = (SoapVersion) ctx.get("SoapVersion");
			QName env = soapversion.getEnvelope();

			boolean serializeProlog = Boolean.TRUE.equals(ctx
					.get("serialiseprolog"));
			if (Boolean.TRUE.equals(ctx.get("serialiseprolog")))
				writer.writeStartDocument(encoding, "1.0");

			writer.setPrefix(env.getPrefix(), env.getNamespaceURI());
			writer.setPrefix(SoapConstants.XSD_PREFIX, SoapConstants.XSD);
			writer.setPrefix(SoapConstants.XSI_PREFIX, SoapConstants.XSI_NS);
			writer.writeStartElement(env.getPrefix(), env.getLocalPart(), env
					.getNamespaceURI());
			writer.writeNamespace(env.getPrefix(), env.getNamespaceURI());

			writer.writeNamespace(SoapConstants.XSD_PREFIX, SoapConstants.XSD);
			writer.writeNamespace(SoapConstants.XSI_PREFIX,
					SoapConstants.XSI_NS);

			QName body = soapversion.getBody();
			writer.writeStartElement(body.getPrefix(), body.getLocalPart(),
					body.getNamespaceURI());

			serializer.writeMessage(ctx);

			writer.writeEndElement();
			writer.writeEndElement();

			if (serializeProlog)
				writer.writeEndDocument();

			writer.flush();
		} catch (XMLStreamException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}

	}

	// ServiceInvocationHandler
	void writeHeaders() {
	}

	//
	// SoapSerializer
	void writeMessage() {

	}

}
