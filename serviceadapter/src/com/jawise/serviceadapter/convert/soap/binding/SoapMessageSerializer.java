package com.jawise.serviceadapter.convert.soap.binding;

import com.jawise.serviceadapter.convert.MessageContext;

public interface SoapMessageSerializer {
	
	void readMessage(MessageContext context) throws Exception;

	void writeMessage(MessageContext context) throws Exception;

}
