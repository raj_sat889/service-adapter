package com.jawise.serviceadapter.convert.soap.binding;

import javax.xml.namespace.QName;

import org.codehaus.xfire.soap.SoapConstants;


public abstract class AbstractMessagePartWriter implements MessagePartWriter {

	public AbstractMessagePartWriter() {
	}

	public void writeXsiType(QName type) throws SoapException {
		/*
		 * Do not assume that the prefix supplied with the QName should be used
		 * in this case.
		 */
		
		if(type == null)
			return;
		
		String prefix = getPrefixForNamespace(type.getNamespaceURI(), type
				.getPrefix());
		String value;
		if (prefix != null && prefix.length() > 0) {
			StringBuffer sb = new StringBuffer(prefix.length() + 1
					+ type.getLocalPart().length());
			sb.append(prefix);
			sb.append(':');
			sb.append(type.getLocalPart());
			value = sb.toString();
		} else {
			value = type.getLocalPart();
		}
		getAttributeWriter("type", SoapConstants.XSI_NS).writeValue(value);
	}

	public void writeXsiNil() throws SoapException {
		MessagePartWriter attWriter = getAttributeWriter("nil",
				SoapConstants.XSI_NS);
		attWriter.writeValue("true");
		attWriter.close();
	}

	public void writeValueAsInt(Integer i) throws SoapException {
		writeValue(i.toString());
	}

	public void writeValueAsDouble(Double d) throws SoapException {
		writeValue(d.toString());
	}

	public void writeValueAsCharacter(Character char1) throws SoapException {
		writeValue(char1.toString());
	}

	public void writeValueAsLong(Long l) throws SoapException {
		writeValue(l.toString());
	}

	public void writeValueAsFloat(Float f) throws SoapException {
		writeValue(f.toString());
	}

	public void writeValueAsBoolean(boolean b) throws SoapException {
		writeValue(b ? "true" : "false");
	}

	public void writeValueAsShort(Short s) throws SoapException {
		writeValue(s.toString());
	}
}
