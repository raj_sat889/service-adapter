package com.jawise.serviceadapter.convert.soap.binding;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

public class MessageAttributeReader extends AbstractMessagePartReader {
	
	private QName name;
	private String value;

	public MessageAttributeReader(QName name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public boolean hasMoreAttributeReaders() {
		return false;
	}

	public MessagePartReader getNextAttributeReader() {
		throw new IllegalStateException();
	}

	public MessagePartReader getAttributeReader(QName qName) {
		throw new IllegalStateException();
	}

	public MessagePartReader getAttributeReader(String name, String namespace) {
		throw new IllegalStateException();
	}

	public boolean hasMoreElementReaders() {
		return false;
	}

	public MessagePartReader getNextElementReader() {
		throw new IllegalStateException();
	}

	public QName getName() {
		return name;
	}

	public String getLocalName() {
		return name.getLocalPart();
	}

	public String getNamespace() {
		return name.getNamespaceURI();
	}

	public String getNamespaceForPrefix(String prefix) {
		throw new IllegalStateException();
	}

	@Override
	public XMLStreamReader getXMLStreamReader() {
		throw new UnsupportedOperationException();
	}

}
