package com.jawise.serviceadapter.convert.soap.binding;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;
import org.codehaus.xfire.soap.SoapConstants;
import org.codehaus.xfire.soap.SoapVersion;
import org.codehaus.xfire.soap.SoapVersionFactory;
import org.codehaus.xfire.util.jdom.StaxBuilder;
import org.codehaus.xfire.util.stax.FragmentStreamReader;
import org.jdom.Element;

import com.jawise.serviceadapter.convert.MessageContext;

public final class SoapMessageParser {
	
	public static final String DECLARED_NAMESPACES = "declared.namespaces";
	private static Logger logger = Logger.getLogger(SoapMessageParser.class);
	private String soapAction;
	private String encoding;
	private MessageContext ctx;
	private XMLStreamReader reader;	
	private SoapMessageSerializer binding = new RpcTypeBinding();

	public SoapMessageParser(MessageContext ctx) throws Exception {
		this.ctx = ctx;
		HttpServletRequest req = (HttpServletRequest) ctx.get("request");
		soapAction = getSoapAction(req);
		encoding = req.getCharacterEncoding();
		encoding = SoapHelper.dequote(encoding);
		String m = (String) ctx.get("messagse");
		logger.debug(m);
		reader = SoapXmlUtil.createXMLStreamReader(new ByteArrayInputStream(m
				.getBytes()), encoding, ctx);
		ctx.put("xmlstreamreader", reader);
	}


	protected String getSoapAction(HttpServletRequest request) {
		String action = request.getHeader(SoapConstants.SOAP_ACTION);

		if (action != null && action.startsWith("\"") && action.endsWith("\"")
				&& action.length() >= 2) {
			action = action.substring(1, action.length() - 1);
		}

		return action;
	}

	@SuppressWarnings("unchecked")
	public List getParameters() throws Exception {
		//read the head and faults and possition net main parameters
		readHeaderAndFaults();
		binding.readMessage(ctx);
		List parameters = (List) ctx.get("body");
		return parameters;

	}
	
	@SuppressWarnings("unchecked")
	public void readVersion() throws Exception {
		readHeaderAndFaults();
	}	

	/**
	 * reads the soap message until the body part
	 * 
	 * @param ctx
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void readHeaderAndFaults() throws Exception {
		
		Map namespaces = new HashMap();
		ctx.put(DECLARED_NAMESPACES, namespaces);

		boolean end = !reader.hasNext();
		while (!end && reader.hasNext()) {
			int event = reader.next();
			switch (event) {
			case XMLStreamReader.START_DOCUMENT:
				String encoding = reader.getCharacterEncodingScheme();
				// message.setEncoding(encoding);
				break;
			case XMLStreamReader.END_DOCUMENT:
				end = true;
				return;
			case XMLStreamReader.END_ELEMENT:
				break;
			case XMLStreamReader.START_ELEMENT:
				if (reader.getLocalName().equals("Header")) {
					readHeaders(ctx, namespaces);
				} else if (reader.getLocalName().equals("Body")) {
					readNamespaces(reader, namespaces);
					event = reader.nextTag();
					checkForFault(ctx);
					return;
				} else if (reader.getLocalName().equals("Envelope")) {
					readNamespaces(reader, namespaces);
					String version = reader.getNamespaceURI();
					SoapVersion soapVersion = SoapVersionFactory.getInstance()
							.getSoapVersion(version);
					ctx.put("SoapVersion", soapVersion);
					if (version == null) {
						throw new SoapException("Invalid SOAP version: ");
					}
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * read name space values and populate
	 * 
	 * @param reader
	 * @param namespaces
	 */
	@SuppressWarnings("unchecked")
	private void readNamespaces(XMLStreamReader reader, Map namespaces) {
		for (int i = 0; i < reader.getNamespaceCount(); i++) {
			String prefix = reader.getNamespacePrefix(i);
			if (prefix == null)
				prefix = "";

			namespaces.put(prefix, reader.getNamespaceURI(i));
		}
	}

	/*
	 * Check the boady for a soap fault
	 */
	private void checkForFault(MessageContext ctx) throws Exception {

		XMLStreamReader reader = (XMLStreamReader) ctx.get("xmlstreamreader");
		SoapVersion soapVersion = (SoapVersion) ctx.get("SoapVersion");
		if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
			if (reader.getName().equals(soapVersion.getFault())) {
				SoapMessageSerializer serializer = SoapFaultSerializerFactory.getSoapFaultSerializer(ctx);
				serializer.readMessage(ctx);
				SoapFault fault = (SoapFault) ctx.get("soapboady");
				throw fault;
			}
		}
	}

	/**
	 * Read in the headers as a YOM Element and create a response Header.
	 * 
	 * @param context
	 * @throws XMLStreamException
	 */
	private void readHeaders(MessageContext ctx, Map namespaces)
			throws XMLStreamException {

		XMLStreamReader reader = (XMLStreamReader) ctx.get("xmlstreamreader");
		StaxBuilder builder = new StaxBuilder();
		FragmentStreamReader fsr = new FragmentStreamReader(reader);
		fsr.setAdvanceAtEnd(false);
		builder.setAdditionalNamespaces(namespaces);
		Element header = builder.build(fsr).getRootElement();
		ctx.put("header", header);
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}
}