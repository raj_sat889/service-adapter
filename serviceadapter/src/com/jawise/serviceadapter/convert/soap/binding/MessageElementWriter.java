package com.jawise.serviceadapter.convert.soap.binding;

import java.io.OutputStream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.codehaus.xfire.util.NamespaceHelper;


public class MessageElementWriter extends AbstractMessagePartWriter {

	private XMLStreamWriter writer;

	private String namespace;

	private String name;

	private String prefix;

	/**
	 * Create a LiteralWriter but without writing an element name.
	 * 
	 * @param writer
	 */
	public MessageElementWriter(XMLStreamWriter writer) {
		this.writer = writer;
	}

	public MessageElementWriter(XMLStreamWriter writer, String name,
			String namespace) throws SoapException {
		this(writer, name, namespace, null);
	}

	public MessageElementWriter(XMLStreamWriter streamWriter, QName name)
			throws SoapException {
		this(streamWriter, name.getLocalPart(), name.getNamespaceURI());
	}

	public MessageElementWriter(XMLStreamWriter writer, String name,
			String namespace, String prefix) throws SoapException {
		this.writer = writer;
		this.namespace = namespace;
		this.name = name;
		this.prefix = prefix;

		try {
			writeStartElement();
		} catch (XMLStreamException e) {
			throw new SoapException("Error writing document.", e);
		}
	}

	/**
	 * @param os
	 * @throws XMLStreamException
	 */
	public MessageElementWriter(OutputStream os, String name, String namespace)
			throws SoapException {
		try {
			XMLOutputFactory ofactory = XMLOutputFactory.newInstance();
			this.writer = ofactory.createXMLStreamWriter(os);
			this.namespace = namespace;
			this.name = name;
			writeStartElement();
		} catch (XMLStreamException e) {
			throw new SoapException("Error writing document.", e);
		}
	}

	private void writeStartElement() throws XMLStreamException {
		if (namespace != null) {
			boolean declare = false;

			String decPrefix = writer.getNamespaceContext()
					.getPrefix(namespace);

			// If the user didn't specify a prefix, create one
			if (prefix == null && decPrefix == null) {
				declare = true;
				prefix = NamespaceHelper.getUniquePrefix(writer);
			} else if (prefix == null) {
				prefix = decPrefix;
			} else if (!prefix.equals(decPrefix)) {
				declare = true;
			}

			writer.writeStartElement(prefix, name, namespace);

			if (declare) {
				writer.setPrefix(prefix, namespace);
				writer.writeNamespace(prefix, namespace);
			}
		} else {
			writer.writeStartElement(name);
		}
	}

	/**
	 * @throws SoapException
	 * @see org.codehaus.xfire.aegis.MessageWriter#writeValue(java.lang.Object)
	 */
	public void writeValue(Object value) throws SoapException {
		try {
			if (value != null)
				writer.writeCharacters(value.toString());
		} catch (XMLStreamException e) {
			throw new SoapException("Error writing document.", e);
		}
	}

	/**
	 * @throws SoapException
	 * @see org.codehaus.xfire.aegis.MessageWriter#getWriter(java.lang.String)
	 */
	public MessageElementWriter getElementWriter(String name) throws SoapException {
		return new MessageElementWriter(writer, name, namespace);
	}

	public MessageElementWriter getElementWriter(String name, String ns)
			throws SoapException {
		return new MessageElementWriter(writer, name, ns);
	}

	public MessageElementWriter getElementWriter(QName qname) throws SoapException {
		return new MessageElementWriter(writer, qname.getLocalPart(), qname
				.getNamespaceURI(), qname.getPrefix());
	}

	public String getNamespace() {
		return namespace;
	}

	public void close() throws SoapException {
		try {
			writer.writeEndElement();
		} catch (XMLStreamException e) {
			throw new SoapException("Error writing document.", e);
		}
	}

	public void flush() throws XMLStreamException {
		writer.flush();
	}

	public XMLStreamWriter getXMLStreamWriter() {
		return writer;
	}

	public MessagePartWriter getAttributeWriter(String name)
			throws SoapException {
		return new MessageAttributeWriter(writer, name, namespace);
	}

	public MessagePartWriter getAttributeWriter(String name, String namespace)
			throws SoapException {
		return new MessageAttributeWriter(writer, name, namespace);
	}

	public MessagePartWriter getAttributeWriter(QName qname)
			throws SoapException {
		return new MessageAttributeWriter(writer, qname.getLocalPart(), qname
				.getNamespaceURI());
	}

	public String getPrefixForNamespace(String namespace) throws SoapException {
		try {
			String prefix = writer.getPrefix(namespace);

			if (prefix == null) {
				prefix = NamespaceHelper.getUniquePrefix(writer);

				writer.setPrefix(prefix, namespace);
				writer.writeNamespace(prefix, namespace);
			}

			return prefix;
		} catch (XMLStreamException e) {
			throw new SoapException("Error writing document.", e);
		}
	}

	public String getPrefixForNamespace(String namespace, String hint)
			throws SoapException {
		try {
			String prefix = writer.getPrefix(namespace);

			if (prefix == null) {
				String ns = writer.getNamespaceContext().getNamespaceURI(hint);
				if (ns == null) {
					prefix = hint;
				} else if (ns.equals(namespace)) {
					return prefix;
				} else {
					prefix = NamespaceHelper.getUniquePrefix(writer);
				}

				writer.setPrefix(prefix, namespace);
				writer.writeNamespace(prefix, namespace);
			}

			return prefix;
		} catch (XMLStreamException e) {
			throw new SoapException("Error writing document.", e);
		}
	}

}
