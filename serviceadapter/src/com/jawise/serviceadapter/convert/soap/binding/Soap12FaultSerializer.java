package com.jawise.serviceadapter.convert.soap.binding;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.codehaus.xfire.util.NamespaceHelper;
import org.codehaus.xfire.util.jdom.StaxBuilder;
import org.codehaus.xfire.util.jdom.StaxSerializer;
import org.codehaus.xfire.util.stax.DepthXMLStreamReader;
import org.codehaus.xfire.util.stax.FragmentStreamReader;
import org.jdom.Element;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.ResourceBundleSupport;

public class Soap12FaultSerializer implements SoapMessageSerializer {

	@Override
	public void readMessage(MessageContext ctx) throws SoapException {

		XMLStreamReader xmlstreamreader = (XMLStreamReader) ctx
				.get("xmlstreamreader");
		SoapFault fault = new SoapFault();
		DepthXMLStreamReader reader = new DepthXMLStreamReader(xmlstreamreader);

		try {
			boolean end = false;

			while (!end && reader.hasNext()) {
				int event = reader.next();
				switch (event) {
				case XMLStreamReader.START_DOCUMENT:
					String encoding = reader.getCharacterEncodingScheme();
					// message.setEncoding(encoding);
					break;
				case XMLStreamReader.END_DOCUMENT:
					end = true;
					break;
				case XMLStreamReader.END_ELEMENT:
					break;
				case XMLStreamReader.START_ELEMENT:
					if (reader.getLocalName().equals("Code")) {
						reader.next();
						SoapXmlUtil.toNextElement(reader);

						if (reader.getLocalName().equals("Value")) {
							fault.setFaultCode(NamespaceHelper
									.readQName(reader));
						}
					} else if (reader.getLocalName().equals("SubCode")) {
						reader.next();
						SoapXmlUtil.toNextElement(reader);

						if (reader.getLocalName().equals("Value")) {
							fault.setSubCode(NamespaceHelper.readQName(reader));
						}

					} else if (reader.getLocalName().equals("Reason")) {
						reader.next();
						SoapXmlUtil.toNextElement(reader);

						if (reader.getLocalName().equals("Text")) {
							fault.setMessage(reader.getElementText());
						}
					} else if (reader.getLocalName().equals("Actor")) {
						fault.setRole(reader.getElementText());
					} else if (reader.getLocalName().equals("Detail")) {
						StaxBuilder builder = new StaxBuilder();
						fault.setDetail(builder.build(
								new FragmentStreamReader(reader))
								.getRootElement());
					}
					break;
				default:
					break;
				}
			}
		} catch (XMLStreamException e) {
			throw new SoapException("1028", e);
		}
		ctx.put("soapboady", fault);

	}

	@Override
	public void writeMessage(MessageContext ctx) throws Exception {

		XMLStreamWriter writer = (XMLStreamWriter) ctx.get("xmlstreamwriter");
		Exception exception = ctx.getException();
		SoapFault fault = (SoapFault) ctx.get("soapboady");

		try {
			Map namespaces = null;// fault.getNamespaces();
			for (Iterator itr = namespaces.keySet().iterator(); itr.hasNext();) {
				String prefix = (String) itr.next();
				writer.writeAttribute("xmlns:" + prefix, (String) namespaces
						.get(prefix));
			}

			writer.writeStartElement("soap:Fault");

			writer.writeStartElement("soap:Code");

			writer.writeStartElement("soap:Value");
			if (fault != null) {
				writeQName(writer, fault.getFaultCode());
			}
			writer.writeEndElement(); // Value

			if (fault != null) {
				if (fault.getSubCode() != null) {
					writer.writeStartElement("soap:SubCode");
					writer.writeStartElement("soap:Value");
					writeQName(writer, fault.getSubCode());
					writer.writeEndElement(); // Value
					writer.writeEndElement(); // SubCode
				}
			}
			writer.writeEndElement(); // Code

			writer.writeStartElement("soap:Reason");
			writer.writeStartElement("soap:Text");
			if (exception == null && fault != null) {
				if (fault.getReason() != null)
					writer.writeCharacters(fault.getReason());
			}
			else {
				writer.writeCharacters(getMessage(ctx,exception));
			}

			writer.writeEndElement(); // Text
			writer.writeEndElement(); // Reason

			if (fault != null) {
				if (fault.getRole() != null) {
					writer.writeStartElement("soap:Role");
					writer.writeCharacters(fault.getRole());
					writer.writeEndElement();
				}

				if (fault.hasDetails()) {
					Element detail = fault.getDetail();

					writer.writeStartElement("soap:Detail");

					StaxSerializer serializer = new StaxSerializer();
					List details = detail.getChildren();
					for (int i = 0; i < details.size(); i++) {
						serializer.writeElement((Element) details.get(i),
								writer);
					}

					writer.writeEndElement(); // Details
				}
			}
			writer.writeEndElement(); // Fault
		} catch (XMLStreamException xe) {
			throw new SoapException("Couldn't create fault.", xe);
		}

	}

	protected void writeQName(XMLStreamWriter writer, QName qname)
			throws XMLStreamException {
		String ns = qname.getNamespaceURI();
		String prefix = qname.getPrefix();
		if (ns.length() > 0 && prefix.length() == 0) {
			prefix = NamespaceHelper.getUniquePrefix(writer, ns, true) + ":";
		} else if (prefix.length() > 0) {
			writer.writeNamespace(prefix, ns);
			prefix = prefix + ":";
		}

		writer.writeCharacters(prefix + qname.getLocalPart());
	}
	
	private String getMessage(MessageContext ctx, Exception ex) {
		String message = ex.getMessage();
		if (message != null && message.length() == 4
				&& ex instanceof ResourceBundleSupport) {
			ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");
			if (bundle != null) {
				return ((ResourceBundleSupport) ex).getMessage(bundle);
			}

		}		
		return ex.getMessage();
	}	
}
