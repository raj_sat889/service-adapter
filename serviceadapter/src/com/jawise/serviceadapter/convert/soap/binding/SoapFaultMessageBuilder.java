package com.jawise.serviceadapter.convert.soap.binding;

import java.io.ByteArrayOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamWriter;

import org.apache.log4j.Logger;
import org.codehaus.xfire.soap.SoapConstants;
import org.codehaus.xfire.soap.SoapVersion;

import com.jawise.serviceadapter.convert.MessageContext;

public class SoapFaultMessageBuilder {
	private static Logger logger = Logger
			.getLogger(SoapFaultMessageBuilder.class);
	@SuppressWarnings("unused")
	private XMLStreamWriter writer = null;
	private ByteArrayOutputStream outputmessage = null;
	private SoapMessageSerializer serialiser = new Soap11FaultSerializer();
	private final MessageContext ctx;
	private String encoding;

	public SoapFaultMessageBuilder(MessageContext ctx) throws Exception {
		this.ctx = ctx;

		if (ctx.get("SoapVersion") == null) {
			SoapMessageParser p = new SoapMessageParser(ctx);
			p.readVersion();
		}

		serialiser = SoapFaultSerializerFactory.getSoapFaultSerializer(ctx);
		outputmessage = new ByteArrayOutputStream();
		HttpServletRequest req = (HttpServletRequest) ctx.get("request");
		encoding = req.getCharacterEncoding();
		encoding = SoapHelper.dequote(encoding);
		writer = SoapXmlUtil
				.createXMLStreamWriter(outputmessage, encoding, ctx);
		this.ctx.put("xmlstreamwriter", writer);
	}

	public String buildFaultMessage() throws Exception {

		writeHeaders();
		writeBody();
		writeMessage();
		String response = new String(outputmessage.toByteArray());
		return response;
	}

	private void writeMessage() {

	}

	private void writeBody() throws Exception {

		SoapVersion soapversion = (SoapVersion) ctx.get("SoapVersion");
		QName env = soapversion.getEnvelope();

		boolean serializeProlog = Boolean.TRUE.equals(ctx
				.get("serialiseprolog"));
		if (Boolean.TRUE.equals(ctx.get("serialiseprolog")))
			writer.writeStartDocument(encoding, "1.0");

		writer.setPrefix(env.getPrefix(), env.getNamespaceURI());
		writer.setPrefix(SoapConstants.XSD_PREFIX, SoapConstants.XSD);
		writer.setPrefix(SoapConstants.XSI_PREFIX, SoapConstants.XSI_NS);
		writer.writeStartElement(env.getPrefix(), env.getLocalPart(), env
				.getNamespaceURI());
		writer.writeNamespace(env.getPrefix(), env.getNamespaceURI());

		writer.writeNamespace(SoapConstants.XSD_PREFIX, SoapConstants.XSD);
		writer.writeNamespace(SoapConstants.XSI_PREFIX, SoapConstants.XSI_NS);

		QName body = soapversion.getBody();
		writer.writeStartElement(body.getPrefix(), body.getLocalPart(), body
				.getNamespaceURI());

		serialiser.writeMessage(ctx);

		writer.writeEndElement();
		writer.writeEndElement();

		if (serializeProlog)
			writer.writeEndDocument();

		writer.flush();
	}

	private void writeHeaders() {

	}

}
