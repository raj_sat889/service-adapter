package com.jawise.serviceadapter.convert.soap.binding;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.codehaus.xfire.fault.XFireFault;
import org.codehaus.xfire.util.NamespaceHelper;
import org.codehaus.xfire.util.jdom.StaxBuilder;
import org.codehaus.xfire.util.jdom.StaxSerializer;
import org.codehaus.xfire.util.stax.FragmentStreamReader;
import org.jdom.Element;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.ResourceBundleSupport;

public class Soap11FaultSerializer implements SoapMessageSerializer {

	@Override
	public void readMessage(MessageContext ctx) throws SoapException {

		XMLStreamReader reader = (XMLStreamReader) ctx.get("xmlstreamreader");
		SoapFault fault = new SoapFault();

		try {
			boolean end = false;
			while (!end && reader.hasNext()) {
				int event = reader.next();
				switch (event) {
				case XMLStreamReader.START_DOCUMENT:
					String encoding = reader.getCharacterEncodingScheme();
					// reader.setEncoding(encoding);
					break;
				case XMLStreamReader.END_DOCUMENT:
					end = true;
					break;
				case XMLStreamReader.END_ELEMENT:
					break;
				case XMLStreamReader.START_ELEMENT:
					if (reader.getLocalName().equals("faultcode")) {
						fault.setValue(NamespaceHelper.readQName(reader));
					} else if (reader.getLocalName().equals("faultstring")) {
						fault.setMessage(reader.getElementText());
					} else if (reader.getLocalName().equals("faultactor")) {
						fault.setRole(reader.getElementText());
					} else if (reader.getLocalName().equals("detail")) {
						StaxBuilder builder = new StaxBuilder();
						fault.setDetail(builder.build(
								new FragmentStreamReader(reader))
								.getRootElement());
					}
					break;
				default:
					break;
				}
			}
		} catch (XMLStreamException e) {
			throw new SoapException("1028", e);
		}

		ctx.put("soapboady", fault);

	}

	@Override
	public void writeMessage(MessageContext ctx) throws Exception {

		XMLStreamWriter writer = (XMLStreamWriter) ctx.get("xmlstreamwriter");

		SoapFault fault = (SoapFault) ctx.get("soapboady");
		Exception exception = fault == null ? ctx.getException() : null;

		try {
			Map namespaces = null; // fault.getNamespaces();
			if (namespaces != null) {
				for (Iterator itr = namespaces.keySet().iterator(); itr
						.hasNext();) {
					String prefix = (String) itr.next();
					writer.writeAttribute("xmlns:" + prefix,
							(String) namespaces.get(prefix));
				}
			}

			writer.writeStartElement("soap:Fault");

			writer.writeStartElement("faultcode");

			QName faultCode = XFireFault.RECEIVER;
			if (fault != null &&  fault.getFaultCode() != null) {
				faultCode = fault.getFaultCode();
			}

			
			String codeString;
			if (faultCode.equals(XFireFault.RECEIVER)) {
				codeString = "soap:Server";
			} else if (faultCode.equals(XFireFault.SENDER)) {
				codeString = "soap:Client";
			} else if (faultCode.equals(XFireFault.VERSION_MISMATCH)) {
				codeString = "soap:VersionMismatch";
			} else if (faultCode.equals(XFireFault.MUST_UNDERSTAND)) {
				codeString = "soap:MustUnderstand";
			} else if (faultCode.equals(XFireFault.DATA_ENCODING_UNKNOWN)) {
				codeString = "soap:Client";
			} else {
				String ns = faultCode.getNamespaceURI();
				String prefix = faultCode.getPrefix();
				if (ns.length() > 0 && prefix.length() == 0) {
					prefix = NamespaceHelper.getUniquePrefix(writer, ns, true)
							+ ":";
				} else if (prefix.length() > 0) {
					writer.writeNamespace(prefix, ns);
					prefix = prefix + ":";
				}

				codeString = prefix + faultCode.getLocalPart();
			}

			writer.writeCharacters(codeString);
			writer.writeEndElement();

			writer.writeStartElement("faultstring");
			writer.writeCharacters(fault == null ? getMessage(ctx,exception)
					: fault.getMessage());
			writer.writeEndElement();

			if (fault != null) {
				if (fault.hasDetails()) {
					Element detail = fault.getDetail();

					writer.writeStartElement("detail");

					StaxSerializer serializer = new StaxSerializer();
					List details = detail.getContent();
					for (int i = 0; i < details.size(); i++) {
						serializer.writeElement((Element) details.get(i),
								writer);
					}

					writer.writeEndElement(); // Details

				}

				if (fault.getRole() != null) {
					writer.writeStartElement("faultactor");
					writer.writeCharacters(fault.getRole());
					writer.writeEndElement();
				}
			}
			writer.writeEndElement(); // Fault
		} catch (XMLStreamException xe) {
			throw new SoapException("Couldn't create fault.", xe);
		}
	}

	private String getMessage(MessageContext ctx, Exception ex) {
		String message = ex.getMessage();
		if (message != null && message.length() == 4
				&& ex instanceof ResourceBundleSupport) {
			ResourceBundle bundle = (ResourceBundle) ctx.get("bundle");
			if (bundle != null) {
				return ((ResourceBundleSupport) ex).getMessage(bundle);
			}

		}		
		return ex.getMessage();
	}

}
