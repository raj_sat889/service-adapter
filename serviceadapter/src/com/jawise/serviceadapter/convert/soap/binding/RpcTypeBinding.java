package com.jawise.serviceadapter.convert.soap.binding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.apache.log4j.Logger;
import org.codehaus.xfire.util.NamespaceHelper;
import org.codehaus.xfire.util.stax.DepthXMLStreamReader;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.soap.encoding.SoapTypeMapping;
import com.jawise.serviceadapter.convert.soap.encoding.SoapTypeMappingRegistry;
import com.jawise.serviceadapter.convert.soap.type.SoapType;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.PartConversions;
import com.jawise.serviceadapter.core.Service;

//RPCBinding
public class RpcTypeBinding implements SoapMessageSerializer {

	private static Logger logger = Logger.getLogger(RpcTypeBinding.class);

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * read message and update the body
	 */
	public void readMessage(MessageContext ctx) throws Exception {

		logger.debug("reading message");
		// the stream reader has be navigated down to point where Boday starts
		XMLStreamReader reader = (XMLStreamReader) ctx
				.get(MessageContext.XMLSTREAMREADER);
		Boolean input = (Boolean) ctx.get(MessageContext.INPUT);

		Service adapterService = (Service) ctx
				.get(MessageContext.ADAPTER_SERVICE);
		MessageConversion msgConversion = (MessageConversion) ctx
				.get(MessageContext.MESSAGE_CONVERSION);

		List parameters = new ArrayList();
		DepthXMLStreamReader dr = new DepthXMLStreamReader(reader);

		if (!SoapXmlUtil.toNextElement(dr)) {
			throw new SoapFault("There must be a method name element.");
		}

		String opname = dr.getLocalName();

		if (!input) {
			opname = opname.substring(0, opname.lastIndexOf("Response"));
		}

		Operation operation = findOperation(adapterService, msgConversion,
				input, opname);

		// Move from operation element to whitespace or start element
		dr.next();

		Message msg = input ? operation.getInput() : operation.getOutput();
		ctx.put(MessageContext.MSG_DEF,msg);		
		
		int i = 0;
		String structName = "";
		List<Part> parts = msg.getParts();
		while (SoapXmlUtil.toNextElement(dr)) {
			Part p = parts.get(i);
			if (p == null) {
				throw new SoapFault("Parameter " + dr.getName()
						+ " does not exist!");
			}

			// skip all strcut parts like struct/structmemer etc
			int x = p.getName().indexOf("/");
			if (x >= 0) {
				structName = p.getName().substring(0,x);
				for (int y = i + 1; y < parts.size(); y++) {
					Part nextPart = parts.get(y);
					if (nextPart != null
							&& nextPart.getName().indexOf(structName) >= 0) {
						i++;
					}
				}
			}

			QName name;
			/*
			 * if (p.getSchemaType().isAbstract()) { name = new
			 * QName(service.getTargetNamespace(), dr .getLocalName()); } else {
			 * name = dr.getName(); }
			 */
			name = dr.getName();

			logger.debug("reading paramter :" + name);

			if (input
					&& (!p.getName().equals(name.getLocalPart()) && !structName
							.equals(name.getLocalPart()))) {
				throw new SoapFault("Parameter " + dr.getName()
						+ " does not exist!");
			}

			parameters.add(readParameter(p, dr, ctx));
			i++;
		}

		ctx.put(MessageContext.BODY, parameters);
	}

	private Operation findOperation(Service adapterService,
			MessageConversion msgConversion, Boolean input, String opname)
			throws SoapFault {
		Operation operation;
		if (input) {
			String adapteroperation = msgConversion.getAdapteroperation();
			if (!adapteroperation.equals(opname))
				throw new SoapFault("Could not find operation: " + opname);

			operation = adapterService.getPort().getBinding().getPortyype()
					.findOperation(adapteroperation);
		} else {
			String adapteeoperation = msgConversion.getAdapteeoperation();
			if (!adapteeoperation.equals(opname))
				throw new SoapFault("Could not find operation: " + opname);

			operation = adapterService.getAdapter().getService().getPort()
					.getBinding().getPortyype().findOperation(adapteeoperation);
		}
		return operation;
	}

	@SuppressWarnings("unchecked")
	public Object readParameter(Part p, XMLStreamReader xsr,
			MessageContext context) throws Exception {

		// get type mapping
		SoapTypeMappingRegistry reg = new SoapTypeMappingRegistry();
		// get type
		SoapTypeMapping soapTypeMapping = reg.getMappings().get("default");
		// decide how to read i.e read type may be different from type
		// read using readtype
		MessagePartReader reader = new MessageElementReader(xsr,context);
		SoapType soapType = null;
		if(p.getName().indexOf("/") >=0) {
			//so this structure element HashMap.class
			soapType = soapTypeMapping.getSoapType(HashMap.class);			
		}
		else {
			soapType = soapTypeMapping.getSoapType(p.getType()
				.getTypeClass());
		}
		if (soapType == null) {
			throw new SoapFault("the message part " + p.getName()
					+ " not mapped in type registry");
		}

		Object readObject = soapType.read(reader);
		logger
				.debug("paramter type read : "
						+ readObject.getClass().toString());
		logger.debug("paramter value read : " + readObject);

		return readObject;
	}

	private static Document getDocument(Node parent) {
		return (parent instanceof Document) ? (Document) parent : parent
				.getOwnerDocument();
	}

	/*
	 * protected String getBoundNamespace(MessageContext context,
	 * MessagePartInfo p) { if (p.isSchemaElement() || ((AbstractSoapBinding)
	 * context.getBinding()).getUse() .equals(SoapConstants.USE_ENCODED)) return
	 * p.getName().getNamespaceURI(); else return ""; }
	 */
	public Object clone() {
		return new RpcTypeBinding();
	}

	@Override
	public void writeMessage(MessageContext context) throws Exception {
		logger.debug("writing message");

		XMLStreamWriter writer = (XMLStreamWriter) context
				.get("xmlstreamwriter");
		Boolean input = (Boolean) context.get(MessageContext.INPUT);
		Service adapterService = (Service) context
				.get(MessageContext.ADAPTER_SERVICE);
		MessageConversion msgConversion = (MessageConversion) context
				.get(MessageContext.MESSAGE_CONVERSION);

		List parameters = (List) context.get(MessageContext.PARAMTERSLIST);
		Map parametermap = (Map) context.get(MessageContext.PARAMTERSMAP);

		if (parameters == null || parametermap == null) {
			throw new SoapFault("Parameter list missing!");
		}

		String name = getOperationElementName(input, msgConversion);
		String opname = getOperationName(input, msgConversion);

		Operation operation = findOperation(adapterService, msgConversion,
				!input, opname);

		writeStartElement(writer, name, adapterService.getNamespace());
		// get message to process
		Message msg = input ? operation.getInput() : operation.getOutput();

		// iterrate over the paramters or parts and
		// write them out
		int i = 0;
		List<Part> parts = msg.getParts();
		Iterator<Part> iterator = parts.iterator();

		while (iterator.hasNext()) {

			Part p = iterator.next();
			Object value = parametermap.get(p.getName());

			String pn = (String) parameters.get(i);

			if (input && !p.getName().equals(pn)) {
				throw new SoapFault("Parameter " + pn
						+ " does not match with partname " + p.getName());
			}
			String ns = adapterService.getNamespace();
			writeParameter(writer, p, value, ns, input);
			i++;
		}

		writer.writeEndElement();

	}

	private String getOperationName(Boolean input,
			MessageConversion msgConversion) {
		if (!input) {
			return msgConversion.getAdapteroperation();
		} else {
			return msgConversion.getAdapteeoperation();
		}
	}

	private String getOperationElementName(boolean input,
			MessageConversion msgConversion) {
		if (!input) {
			return msgConversion.getAdapteroperation() + "Response";
		} else {
			return msgConversion.getAdapteeoperation();
		}
	}

	private void writeParameter(XMLStreamWriter writer, Part p, Object value,
			String ns, Boolean input) throws SoapException, Exception {

		// get type mapping
		SoapTypeMappingRegistry reg = new SoapTypeMappingRegistry();

		// get type
		SoapTypeMapping soapTypeMapping = reg.getMappings().get("default");

		SoapType soapType = soapTypeMapping.getSoapType(value.getClass());

		if (soapType == null) {
			throw new MessageException("Type not mapped : " + value.getClass());
		}

		if (p.getType().isStruct()) {
			if (input) {
				soapType.setXmlSchemaType(new QName(ns, p.getName()));
			} else {

			}
		}

		String name = p.getName();
		if (!input) {
			name = "out";
			// writer.writeStartElement("", "ns1:out", ns);
		}

		// write the parameter's start element
		if (soapType.isWriteOuter()) {
			if (ns != null && ns.length() > 0) {
				String prefix = writer.getPrefix(ns);
				boolean declare = false;
				if (prefix == null || "".equals(prefix)) {
					prefix = "";
					declare = true;
					writer.setDefaultNamespace(ns);
				}

				writer.writeStartElement(prefix, name, ns);
				if (declare)
					writer.writeDefaultNamespace(ns);
			} else {
				// ns = "ns1";
				writer.writeStartElement(name);
				// writer.writeDefaultNamespace(ns);
			}
		}

		writeParameterValue(writer, soapType, p, value);

		// write the parameter's end element
		if (soapType.isWriteOuter()) {
			writer.writeEndElement();
		}

		if (!input) {
			// name = "ns1:out";
			// writer.writeEndElement();
		}

	}

	private void writeParameterValue(XMLStreamWriter writer, SoapType soapType,
			Part p, Object value) throws Exception {

		MessageElementWriter mw = new MessageElementWriter(writer);

		if (soapType.isNillable() && soapType.isWriteOuter() && value == null) {
			mw.writeXsiNil();
			return;
		}
		soapType.write(value, mw);
	}

	public void writeStartElement(XMLStreamWriter writer, String name,
			String namespace) throws XMLStreamException {
		String prefix = "";
		prefix = NamespaceHelper.getUniquePrefix(writer);

		if (namespace.length() > 0) {
			writer.setPrefix(prefix, namespace);
			writer.writeStartElement(prefix, name, namespace);
			writer.writeNamespace(prefix, namespace);
		} else {
			writer.setDefaultNamespace("");
			writer.writeStartElement(name);
			writer.writeDefaultNamespace("");
		}
	}

	protected PartConversions getPartConversions(MessageContext ctx) {
		MessageConversion msgConversion = (MessageConversion) ctx
				.get(MessageContext.MESSAGE_CONVERSION);
		Boolean input = (Boolean) ctx.get(MessageContext.INPUT);
		PartConversions partConversions;
		if (input) {
			partConversions = msgConversion.getInputPartConversions();
		} else {
			partConversions = msgConversion.getOutputPartConversions();
		}
		return partConversions;
	}
}
