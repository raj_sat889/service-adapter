package com.jawise.serviceadapter.convert.soap.binding;

import com.jawise.serviceadapter.core.MessageException;

@SuppressWarnings("serial")
public class SoapException extends MessageException {

	public SoapException() {
		super();
	}

	public SoapException(String message, String value) {
		super(message, value);
	}

	public SoapException(String message, Throwable cause) {
		super(message, cause);
	}

	public SoapException(String message) {
		super(message);
	}

	public SoapException(Throwable cause) {
		super(cause);
	}

}
