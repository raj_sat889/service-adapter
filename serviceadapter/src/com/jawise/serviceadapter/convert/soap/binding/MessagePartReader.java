package com.jawise.serviceadapter.convert.soap.binding;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

public interface MessagePartReader {

	public void readToEnd() throws SoapException;

	public String getValue() throws SoapException;

	public boolean hasMoreElementReaders() throws SoapException;

	public MessagePartReader getNextElementReader() throws SoapException;

	public MessagePartReader getAttributeReader(QName qName);

	public boolean hasMoreAttributeReaders();

	public MessagePartReader getNextAttributeReader();

	public char getValueAsCharacter() throws SoapException;

	public int getValueAsInt() throws SoapException;

	public long getValueAsLong() throws SoapException;

	public double getValueAsDouble() throws SoapException;

	public float getValueAsFloat() throws SoapException;

	public boolean getValueAsBoolean() throws SoapException;

	public XMLStreamReader getXMLStreamReader();

	public QName getName();

	public boolean isXsiNil() throws SoapException;
}