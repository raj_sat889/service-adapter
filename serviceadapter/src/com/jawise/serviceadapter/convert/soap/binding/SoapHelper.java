package com.jawise.serviceadapter.convert.soap.binding;

public class SoapHelper {

	public static String dequote(String charEncoding) {
		if (charEncoding != null && charEncoding.length() > 0) {
			if ((charEncoding.charAt(0) == '"' && charEncoding
					.charAt(charEncoding.length() - 1) == '"')
					|| (charEncoding.charAt(0) == '\'' && charEncoding
							.charAt(charEncoding.length() - 1) == '\'')) {
				charEncoding = charEncoding.substring(1,
						charEncoding.length() - 1);
			}
		}
		return charEncoding;
	}



}
