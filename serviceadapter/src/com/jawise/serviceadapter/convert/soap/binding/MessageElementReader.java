package com.jawise.serviceadapter.convert.soap.binding;

import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.codehaus.xfire.soap.SoapConstants;
import org.codehaus.xfire.util.stax.DepthXMLStreamReader;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.Service;

public class MessageElementReader extends AbstractMessagePartReader {
	private static final Pattern QNAME_PATTERN = Pattern
			.compile("([^:]+):([^:]+)");

	private String localName;
	private DepthXMLStreamReader root;
	private QName name;
	private String namespace;
	private int depth;
	private QName xsiType;
	private QName arrayType;
	private boolean array;
	private String value;
	private boolean hasCheckedChildren;
	private boolean hasChildren;
	private int currentAttribute = 0;
	private MessageContext context;

	public MessageElementReader(XMLStreamReader reader, MessageContext context) {
		this(new DepthXMLStreamReader(reader), context);

	}

	public MessageElementReader(DepthXMLStreamReader root,
			MessageContext context) {
		this.context = context;
		this.root = root;
		this.localName = root.getLocalName();
		this.name = root.getName();
		this.namespace = root.getNamespaceURI();

		extractXsiType();

		depth = root.getDepth();
	}

	public MessageElementReader(InputStream is) throws Exception {
		XMLStreamReader xmlReader = SoapXmlUtil.createXMLStreamReader(is, null,
				null);

		xmlReader.nextTag();

		this.root = new DepthXMLStreamReader(xmlReader);
		this.localName = root.getLocalName();
		this.name = root.getName();
		this.namespace = root.getNamespaceURI();

		extractXsiType();

		depth = root.getDepth();
	}

	public void extractXsiType() {
		/*
		 * We're making a conscious choice here -- garbage in == garbate out.
		 */
		String xsiTypeQname = root.getAttributeValue(SoapConstants.XSI_NS,
				"type");
		if (xsiTypeQname != null) {
			Matcher m = QNAME_PATTERN.matcher(xsiTypeQname);
			if (m.matches()) {
				NamespaceContext nc = root.getNamespaceContext();
				this.xsiType = new QName(nc.getNamespaceURI(m.group(1)), m
						.group(2), m.group(1));
			} else {
				this.xsiType = new QName(this.namespace, xsiTypeQname, "");
			}

			// so this is a array type and it should be decode
			if ("Array".equals(xsiType.getLocalPart())
					|| isArrayType(localName)) {
				extractArrayInfo(xsiTypeQname);

			}
		}
		else if(isArrayType(localName)){
			this.arrayType = new QName(this.namespace, localName, "");
			this.xsiType=new QName(this.namespace, localName, "");
			this.array = true;

		}
	}

	private void extractArrayInfo(String xsiTypeQname) {
		Matcher m;
		array = true;

		String at = root.getAttributeValue(
				"http://schemas.xmlsoap.org/soap/encoding/",
				"arrayType");
		
		if(at == null)
			at = "";
		
		m = QNAME_PATTERN.matcher(at);
		if (m.matches()) {
			NamespaceContext nc = root.getNamespaceContext();
			this.arrayType = new QName(nc.getNamespaceURI(m.group(1)),
					m.group(2), m.group(1));
		} else {
			this.arrayType = new QName(this.namespace, xsiTypeQname, "");
		}
	}

	public void readToEnd() throws SoapException {
		readToEnd(this);
	}

	public String getValue() throws SoapException {
		if (value == null) {
			try {
				value = root.getElementText();

				while (checkHasMoreChildReaders()) {
				}
			} catch (XMLStreamException e) {
				throw new SoapException("Could not read XML stream.", e);
			}

			if (value == null) {
				value = "";
			}
		}

		return value.trim();
	}

	public String getValue(String ns, String attr) {
		return root.getAttributeValue(ns, attr);
	}

	public boolean hasMoreElementReaders() throws SoapException {
		// Check to see if we checked before,
		// so we don't mess up the stream position.
		if (!hasCheckedChildren)
			checkHasMoreChildReaders();

		return hasChildren;
	}

	public boolean hasMoreAttributeReaders() {
		if (!root.isStartElement())
			return false;

		return currentAttribute < root.getAttributeCount();
	}

	public MessagePartReader getAttributeReader(QName qName) {
		return new MessageAttributeReader(qName, root.getAttributeValue(qName
				.getNamespaceURI(), qName.getLocalPart()));
	}

	public MessagePartReader getNextAttributeReader() {
		MessagePartReader reader = new MessageAttributeReader(root
				.getAttributeName(currentAttribute), root
				.getAttributeValue(currentAttribute));
		currentAttribute++;

		return reader;
	}

	private boolean checkHasMoreChildReaders() throws SoapException {
		try {
			int event = root.getEventType();
			while (root.hasNext()) {
				switch (event) {
				case XMLStreamReader.START_ELEMENT:
					if (root.getDepth() > depth) {
						hasCheckedChildren = true;
						hasChildren = true;

						return true;
					}
					break;
				case XMLStreamReader.END_ELEMENT:
					if (root.getDepth() <= depth + 1) {
						hasCheckedChildren = true;
						hasChildren = false;

						if (root.hasNext()) {
							root.next();
						}
						return false;
					}
					break;
				case XMLStreamReader.END_DOCUMENT:
					// We should never get here...
					hasCheckedChildren = true;
					hasChildren = false;
					return false;
				default:
					break;
				}

				if (root.hasNext())
					event = root.next();
			}

			hasCheckedChildren = true;
			hasChildren = false;
			return false;
		} catch (XMLStreamException e) {
			throw new SoapException("Error parsing document.", e);
		}
	}

	public MessagePartReader getNextElementReader() throws SoapException {
		if (!hasCheckedChildren)
			checkHasMoreChildReaders();

		if (!hasChildren)
			return null;

		hasCheckedChildren = false;

		return new MessageElementReader(root, context);
	}

	private void readToEnd(MessagePartReader childReader) throws SoapException {
		while (childReader.hasMoreElementReaders()) {
			readToEnd(childReader.getNextElementReader());
		}
	}

	public boolean isXsiNil() throws SoapException {
		MessagePartReader nilReader = getAttributeReader(XSI_NIL);
		boolean nil = false;
		if (nilReader != null) {
			String value = nilReader.getValue();
			if (value != null && (value.equals("true") || value.equals("1")))
				return true;
		}

		return nil;
	}

	@Override
	public XMLStreamReader getXMLStreamReader() {
		return root;
	}

	public QName getName() {
		return name;
	}

	public QName getXsiType() {
		return xsiType;
	}

	public QName getArrayType() {
		return arrayType;
	}

	public boolean isArray() {
		return array;
	}

	public MessageContext getContext() {
		return context;
	}

	public void setContext(MessageContext context) {
		this.context = context;
	}

	public boolean isArrayType(String partname) {
		if (context != null) {
			Message msg = (Message) context.get(MessageContext.MSG_DEF);
			Part p = msg.findPartByName(partname);
			if (p != null) {
				if (p.getType().isArray() || p.getName().indexOf("[]") > 0) {
					return true;
				}
			}

		}

		return false;
	}
}
