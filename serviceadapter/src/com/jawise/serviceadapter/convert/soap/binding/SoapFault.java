package com.jawise.serviceadapter.convert.soap.binding;

import javax.xml.namespace.QName;

import org.jdom.Element;

public class SoapFault extends SoapException {
	private static final long serialVersionUID = 1L;
	private QName value;
	private Element detail;
	private String message;
	private String role;
	private QName faultCode;
	private QName subCode;
	
	public SoapFault() {
		super();
	}
	public SoapFault(String message, String value) {
		super(message, value);
	}
	public SoapFault(String message, Throwable cause) {
		super(message, cause);
	}
	public SoapFault(String message) {
		super(message);
	}
	public SoapFault(Throwable cause) {
		super(cause);
		
	}
	public QName getFaultCode() {
		return faultCode;
	}
	public void setFaultCode(QName faultCode) {
		this.faultCode = faultCode;
	}
	public QName getSubCode() {
		return subCode;
	}
	public void setSubCode(QName subCode) {
		this.subCode = subCode;
	}
	public String getValue() {
		return value.toString();
	}
	public void setValue(QName value) {
		this.value = value;
	}
	public Element getDetail() {
		return detail;
	}
	public void setDetail(Element detail) {
		this.detail = detail;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public boolean hasDetails() {
		return detail == null ? false : true;
	}

    public String getReason()
    {
        return getMessage();
    }
}
