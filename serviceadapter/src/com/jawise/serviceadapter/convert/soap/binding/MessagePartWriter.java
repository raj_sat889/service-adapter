package com.jawise.serviceadapter.convert.soap.binding;


public interface MessagePartWriter {
	public void writeValueAsBoolean(boolean booleanValue) throws SoapException;

	public void writeValue(Object format) throws SoapException;

	public void writeValueAsDouble(Double object) throws SoapException;

	public void writeValueAsFloat(Float object) throws SoapException;

	public void writeValueAsInt(Integer object) throws SoapException;

	public void writeValueAsLong(Long object) throws SoapException;

	public void writeValueAsShort(Short object) throws SoapException;

	public void close() throws SoapException;

	public MessagePartWriter getAttributeWriter(String string, String xsiNs)
			throws SoapException;

	public String getPrefixForNamespace(String namespaceURI, String prefix)
			throws SoapException;

	public MessagePartWriter getElementWriter(String name, String ns)
			throws SoapException;

	void writeXsiNil() throws SoapException;
}
