package com.jawise.serviceadapter.convert.soap;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.soap.binding.SoapResponseMessageBuilder;
import com.jawise.serviceadapter.convert.soap.binding.SoapMessageParser;
import com.jawise.serviceadapter.convert.soap.binding.SoapRequestMessageBuilder;
import com.jawise.serviceadapter.core.MessageException;

public class SoapToSoapConverter extends AbstractSoapConverter {
	private static Logger logger = Logger.getLogger(SoapToSoapConverter.class);

	public SoapToSoapConverter(MessageContext ctx) throws MessageException {
		super(ctx);
	}

	@Override
	public Object doConvertion() throws MessageException {
		try {
			SoapMessageParser parser = new SoapMessageParser(getCtx());

			// the main parsing is done here
			List parameters = parser.getParameters();
			// now populate source map;
			populateSourceMap(parameters);

			// do conversion
			convertParts();

			getCtx().put("paramterslist", trargetParameters);
			getCtx().put("paramtersmap", targetMap);


			if (isInput()) {

				SoapRequestMessageBuilder soapMessageBuilder = new SoapRequestMessageBuilder(getCtx());
				SOAPMessage msg = soapMessageBuilder.buildRequestMessage();
				ByteArrayOutputStream str = new ByteArrayOutputStream();
				msg.writeTo(str);
				logger.info("converted messages : "
						+ new String(str.toByteArray()));
				return msg;
			} else {

				SoapResponseMessageBuilder soapMessageBuilder = new SoapResponseMessageBuilder(
						getCtx());
				String str = soapMessageBuilder.buildResponseMessage();
				logger.info("converted messages : " + str);
				return str;
			}
		} catch (MessageException e) {
			throw e;
		} catch (Exception e) {
			throw new MessageException(e.getMessage(), e);
		}
	}

	@Override
	protected Map getTargetReferances() throws MessageException {
		return targetReferences;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {

		targetMap.put(targetName, targetValue);
		targetReferences.put(targetValue, targetValue);
		trargetParameters.add(targetName);

	}

}
