package com.jawise.serviceadapter.convert;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xml.serializer.Method;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.PartConversion;
import com.jawise.serviceadapter.core.PartConversions;

public abstract class AbstractXmlConverter extends MessageConverter {

	private static Logger logger = Logger.getLogger(AbstractXmlConverter.class);
	protected Document sourceDoc;
	@SuppressWarnings("unchecked")
	protected Map sourceReferences;
	@SuppressWarnings("unchecked")
	protected Map sourceMap;
	@SuppressWarnings("unchecked")
	protected Map targetReferences;
	@SuppressWarnings("unchecked")
	protected Map targetMap;

	public AbstractXmlConverter(MessageContext ctx) throws MessageException {
		super(ctx);
		targetMap = new HashMap<String, String>();
		sourceMap = new HashMap<String, Object>();
		sourceReferences = new HashMap<String, Object>();
		targetReferences = new HashMap<String, String>();
	}

	public void createSourceDoc() throws ParserConfigurationException,
			SAXException, IOException {
		String xml = (String) getCtx().get("messagse");
		DocumentBuilderFactory i = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = i.newDocumentBuilder();
		sourceDoc = db.parse(new ByteArrayInputStream(xml.getBytes()));
	}

	/**
	 * Builds the source name value map
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings( { "unchecked" })
	protected void populateSourceMap() throws Exception {

		logger.debug("populating source map");

		if (sourceDoc == null) {
			throw new MessageException("1013", "sourceDoc is not initialised");
		}

		PartConversions conversions = selectPartConversions();
		sourceReferences.put("sourceDoc", sourceDoc);
		for (PartConversion pc : conversions) {
			String sourceValue = "";
			String sourceName = "";
			Part sourcePart = isInput() ? pc.getAdapterPart() : pc
					.getAdapteePart();
			if (sourcePart != null) {
				sourceName = sourcePart.getName();
				NodeList sourceNodes = XPathAPI.selectNodeList(sourceDoc,
						XmlPath.removeArrayPrefix(sourceName));
				if (sourceNodes != null) {
					int length = sourceNodes.getLength();
					if (length == 1) {
						Node item = sourceNodes.item(0);
						if (item.getFirstChild() != null) {
							sourceValue = item.getFirstChild().getNodeValue();
						}
						sourceValue = applyEncoding(sourceValue);
						sourceMap.put(sourceName, sourceValue);
						sourceReferences.put(item.getNodeName(), sourceValue);
					} else if (length > 1) {
						if (sourceName.indexOf("[]") >= 0) {

							Node parentNode = sourceNodes.item(0).getParentNode();
							StringWriter writer = new StringWriter();
							OutputFormat format = new OutputFormat(Method.XML,
									"UTF-8", true);							
							format.setOmitXMLDeclaration(true);
							XMLSerializer serializer = new XMLSerializer(
									writer, format);
							serializer.serialize((Element) parentNode);
							String xml = writer.toString();
							logger.debug(xml);
							sourceReferences.put(sourceName, xml);
							sourceMap.put(sourceName, xml);							
							

						} else {
							throw new MessageException("1029", sourceName);
						}
					}

				}
			}
		}

	}

	@Override
	protected String getScriptTargetName(String targetName) {
		return new XmlPath(targetName).getName();
	}

}
