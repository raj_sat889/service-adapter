package com.jawise.serviceadapter.convert;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;

public class XmlTargetValueAccepter extends TargetValueAccepter {
	protected XmlPath lastAddedTargetPath;
	@SuppressWarnings("unchecked")
	protected Map recursiveNodesMap;

	@SuppressWarnings("unchecked")
	public XmlTargetValueAccepter(MessageConverter mc, Map targetReferences,
			Map targetMap, List trargetParameters, Map recursiveNodesMap, XmlPath lastAddedTargetPath) {
		super(mc, targetReferences, targetMap, trargetParameters);
		this.recursiveNodesMap = recursiveNodesMap;
		this.lastAddedTargetPath = lastAddedTargetPath;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void acceptTargetValue(TargetValue value) throws MessageException {
		try {

			if (value.getRecursive()) {
				if (lastAddedTargetPath == null) {
					lastAddedTargetPath = new XmlPath("");
				}
				recursiveNodesMap.put(lastAddedTargetPath, value
						.getTargetValue());
			} else {
				XmlPath p = new XmlPath(value.getTargetName());
				String encodedvalue = mc.applyEncoding((String) value
						.getTargetValue());
				targetMap.put(p, encodedvalue);
				targetReferences.put(p.getName(), encodedvalue);
				lastAddedTargetPath = p;
			}
		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}

	}

	public XmlPath getLastAddedTargetPath() {
		return lastAddedTargetPath;
	}

	public Map getRecursiveNodesMap() {
		return recursiveNodesMap;
	}

}
