package com.jawise.serviceadapter.convert.rpc;

import com.jawise.serviceadapter.core.MessageException;

public class XmlRpcMessageFault extends MessageException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2530037600703754518L;

	public XmlRpcMessageFault() {
		super();
	}

	public XmlRpcMessageFault(String message, String value) {
		super(message, value);
	}

	public XmlRpcMessageFault(String message, Throwable cause) {
		super(message, cause);
	}

	public XmlRpcMessageFault(String message) {
		super(message);
	}

	public XmlRpcMessageFault(Throwable cause) {
		super(cause);
	}

}
