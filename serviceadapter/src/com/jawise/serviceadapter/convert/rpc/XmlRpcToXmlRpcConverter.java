package com.jawise.serviceadapter.convert.rpc;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfig;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcClientRequestImpl;
import org.apache.xmlrpc.client.XmlRpcSunHttpTransportFactory;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.core.MessageException;

public class XmlRpcToXmlRpcConverter extends AbstractXmlRpcConverter {

	private static Logger logger = Logger
			.getLogger(XmlRpcToXmlRpcConverter.class);
	@SuppressWarnings( { "unchecked", "unused" })	

	public XmlRpcToXmlRpcConverter(MessageContext ctx)
			throws MessageException {
		super(ctx);

	}

	@SuppressWarnings( { "unused", "unchecked" })
	@Override
	public Object doConvertion() throws MessageException {
		try {
			logger.debug("started conversion");

			if (fault) {
				return faultmessage;
			}
			populateSourceMap();
			convertParts();
			String convertedMsg = "";
			XmlRpcConversionHelper xmlrpcHelper = new XmlRpcConversionHelper();
			if (isInput()) {
				List parmeters = xmlrpcHelper.convertTargetToXmlRpcParmeters(
						this, targetMap, trargetParameters);				
				Object[] parmeterArray = new Object[parmeters.size()];
				parmeterArray = parmeters.toArray(parmeterArray);
				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
				
				String method = xmlrpcHelper.getAdapteeMethod(this);
				XmlRpcRequest request = new XmlRpcClientRequestImpl(config,
						method, parmeterArray);
				XmlRpcClient client = new XmlRpcClient();
				client.setTransportFactory(new XmlRpcSunHttpTransportFactory(
						client));
				client.setConfig((XmlRpcClientConfig) config);
				convertedMsg = new XmlRpcSerialiser().serialise(client, request);
			} else {
				List result = xmlrpcHelper.convertTargetToXmlRpcResult(this,
						targetMap, trargetParameters);

				Object[] resultArray = new Object[result.size()];
				resultArray = result.toArray(resultArray);

				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
				XmlRpcClient client = new XmlRpcClient();
				client.setTransportFactory(new XmlRpcSunHttpTransportFactory(
						client));
				client.setConfig((XmlRpcClientConfig) config);
				convertedMsg = new XmlRpcSerialiser().serialise(client, config, result.get(0));
			}
			logger.debug("finished conversion");
			return convertedMsg;
		} catch (MessageException e) {
			logger.error(e.getMessage(), e);
			ResourceBundle bundle = ResourceBundle
					.getBundle("com.jawise.serviceadapter.core.messages");
			XmlRpcSerialiser xmlrpc = new XmlRpcSerialiser();
			String fault = xmlrpc.serialise(2000, e.getMessage(bundle));
			throw new XmlRpcMessageFault(fault);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			XmlRpcSerialiser xmlrpc = new XmlRpcSerialiser();
			String fault = xmlrpc.serialise(2000, e.getMessage());
			throw new XmlRpcMessageFault(fault);
		}
	}

}
