package com.jawise.serviceadapter.convert.rpc;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.TargetValue;
import com.jawise.serviceadapter.convert.XmlTargetValueAccepter;
import com.jawise.serviceadapter.convert.xml.XmlComposer;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;

public class XmlRpcToXmlConverter extends AbstractXmlRpcConverter{
	private static Logger logger = Logger.getLogger(XmlRpcToXmlConverter.class);
	private XmlPath lastAddedTargetPath;
	@SuppressWarnings("unchecked")
	private Map recursiveNodesMap;
	
	@SuppressWarnings("unchecked")
	public XmlRpcToXmlConverter(MessageContext ctx)
			throws MessageException {
		super(ctx);
		recursiveNodesMap = new HashMap();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object doConvertion() throws MessageException {
		XmlComposer composer = new XmlComposer();
		try {
			
			if (fault) {
				return getFaultString();
			}				
			populateSourceMap();			
			convertParts();
			String compose = composer.compose((HashMap) targetMap,
					(HashMap) recursiveNodesMap);
			return compose;
		}

		catch (MessageException e) {
			logger.error(e.getMessage(), e);
			ResourceBundle bundle = ResourceBundle
					.getBundle("com.jawise.serviceadapter.core.messages");
			throw new MessageException("1013", e.getMessage(bundle));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		XmlTargetValueAccepter accepter = new XmlTargetValueAccepter(this,
				targetReferences, targetMap, null, recursiveNodesMap,
				lastAddedTargetPath);
		accepter.acceptTargetValue(new TargetValue(targetName, targetValue,
				recursive));
		lastAddedTargetPath = accepter.getLastAddedTargetPath();
	}
	@Override
	protected String getScriptTargetName(String targetName) {
		return new XmlPath(targetName).getName();
	}
}
