package com.jawise.serviceadapter.convert.rpc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageConverter;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageConversion;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.PartConversion;
import com.jawise.serviceadapter.core.PartConversions;
import com.jawise.serviceadapter.core.Type;

public class XmlRpcConversionHelper {

	private static Logger logger = Logger
			.getLogger(XmlRpcConversionHelper.class);

	public String getAdapteeMethod(MessageConverter mc) {
		String rpcmethod = mc.getAdapterService().getAdapter().getService()
				.getName()
				+ "." + mc.getMsgConversion().getAdapteeoperation();
		return rpcmethod;
	}

	@SuppressWarnings("unchecked")
	public List convertTargetToXmlRpcParmeters(MessageConverter mc,
			Map targetMap, List trargetParameters) throws MessageException {

		List parmeters = new ArrayList();
		Iterator iter = trargetParameters.iterator();
		int index = 0;
		while (iter.hasNext()) {
			String key = (String) iter.next();
			Object value = targetMap.get(key);
			addParameter(mc, parmeters, key, value, index);
			index++;
		}
		return parmeters;
	}

	@SuppressWarnings("unchecked")
	public List convertTargetToXmlRpcResult(MessageConverter mc, Map targetMap,
			List trargetParameters) throws MessageException {
		List result = new ArrayList();
		Iterator iter = trargetParameters.iterator();
		int index = 0;
		while (iter.hasNext()) {
			String key = (String) iter.next();
			Object value = targetMap.get(key);
			addParameter(mc, result, key, value, index);
			index++;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private void addParameter(MessageConverter mc, List parmeters,
			final String key, final Object val, final int index)
			throws MessageException {

		Part part = findPart(mc, key, index);
		Object value = val;
		if (value == null)
			value = getNullReplacement(part);

		if (part.getName().indexOf("/") >= 0) {
			XmlPath p = new XmlPath(key);
			int lastparam = parmeters.size() == 0 ? 0 : parmeters.size() - 1;
			Object param = parmeters.get(lastparam);
			if (param instanceof Map) {
				((Map) param).put(p.getName(), value);
			} else {
				Map sturct = new HashMap();
				sturct.put(p.getName(), value);
				parmeters.add(sturct);
			}
		} else if (value instanceof String) {
			parmeters.add(value);
		} else if (value instanceof Byte) {
			parmeters.add(value);
		} else if (value instanceof Short) {
			parmeters.add(value);
		} else if (value instanceof Integer) {
			parmeters.add(value);
		} else if (value instanceof Long) {
			parmeters.add(value);
		} else if (value instanceof Double) {
			parmeters.add(value);
		} else if (value instanceof Boolean) {
			parmeters.add(value);
		} else if (value instanceof Date) {
			parmeters.add(value);
		} else if (value instanceof Calendar) {
			parmeters.add(value);
		} else if (value instanceof Byte[]) {
			parmeters.add(value);
		} else if (value instanceof Object[]) {
			parmeters.add(value);
		} else if (value instanceof Map) {
			parmeters.add(value);
		} else if (value instanceof List) {
			parmeters.add(value);
		} else if (value == null) {
			parmeters.add(value);
		} else {
			throw new MessageException("1020", value != null ? value.getClass()
					.toString() : "Null");
		}
	}

	@SuppressWarnings("unchecked")
	private Object getNullReplacement(Part part) {
		Type type = part.getType();
		if (type.isNumeric()) {
			return 0;
		} else if (type.isString()) {
			return "";
		} else if (type.isArray()) {
			return new Object[0];
		} else if (type.isBoolean()) {
			return false;
		} else if (type.isStruct()) {
			return new HashMap();
		} else if (type.isDate()) {
			return new Date();
		} else
			return new Object();
	}

	@SuppressWarnings("unused")
	private Part findPart(MessageConverter mc, String key, int index) {
		logger.debug("finding part for :" + key);

		MessageConversion msgConversion = mc.getMsgConversion();
		PartConversions pcs = mc.isInput() ? msgConversion
				.getInputPartConversions() : msgConversion
				.getOutputPartConversions();

		PartConversion pc = pcs.get(index);
		Part part = mc.isInput() ? pc.getAdapteePart() : pc.getAdapterPart();

		String referanceName = new XmlPath(part.getName()).getName();
		if (!key.equals(referanceName) && key.indexOf(referanceName) == -1) {
			logger.debug("part key miss match for " + key + " - "
					+ referanceName + " - using index " + index);
		}
		return part;
	}

}
