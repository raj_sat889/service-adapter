package com.jawise.serviceadapter.convert.rpc;

import java.io.ByteArrayOutputStream;
import java.util.ResourceBundle;

import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.soap.binding.SoapResponseMessageBuilder;
import com.jawise.serviceadapter.convert.soap.binding.SoapRequestMessageBuilder;
import com.jawise.serviceadapter.core.MessageException;

public class XmlRpcToSoapConverter extends AbstractXmlRpcConverter {
	private static Logger logger = Logger.getLogger(XmlRpcToSoapConverter.class);
	public XmlRpcToSoapConverter(MessageContext ctx) throws MessageException {
		super(ctx);
	}

	@Override
	public Object doConvertion() throws MessageException {
		try {
			
			if (fault) {
				return getFaultString();
			}				
			populateSourceMap();			
			convertParts();
			getCtx().put("paramterslist", trargetParameters);
			getCtx().put("paramtersmap", targetMap);			
			
			if (isInput()) {
				SoapRequestMessageBuilder soapMessageBuilder = new SoapRequestMessageBuilder(getCtx());
				SOAPMessage msg = soapMessageBuilder.buildRequestMessage();
				ByteArrayOutputStream str = new ByteArrayOutputStream();
				msg.writeTo(str);
				logger.info("converted messages : "
						+ new String(str.toByteArray()));
				logger.debug("finished conversion");
				return msg;
			} else {

				SoapResponseMessageBuilder soapMessageBuilder = new SoapResponseMessageBuilder(
						getCtx());
				String str = soapMessageBuilder.buildResponseMessage();
				logger.info("converted messages : " + str);
				logger.debug("finished conversion");
				return str;
			}		
		}

		catch (MessageException e) {
			logger.error(e.getMessage(), e);
			ResourceBundle bundle = ResourceBundle
					.getBundle("com.jawise.serviceadapter.core.messages");
			throw new MessageException("1013", e.getMessage(bundle));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		targetMap.put(targetName, targetValue);
		targetReferences.put(targetValue, targetValue);
		trargetParameters.add(targetName);		
	}

}
