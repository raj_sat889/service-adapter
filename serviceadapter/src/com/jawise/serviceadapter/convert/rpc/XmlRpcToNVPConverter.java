package com.jawise.serviceadapter.convert.rpc;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.NVPHelper;
import com.jawise.serviceadapter.convert.NvpTargetValueAccepter;
import com.jawise.serviceadapter.convert.TargetValue;
import com.jawise.serviceadapter.core.MessageException;

public class XmlRpcToNVPConverter extends AbstractXmlRpcConverter {

	private static Logger logger = Logger.getLogger(XmlRpcToNVPConverter.class);

	
	public XmlRpcToNVPConverter(MessageContext ctx)
			throws MessageException {
		super(ctx);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object doConvertion() throws MessageException {
		try {
			logger.debug("started conversion");
			
			if (fault) {
				return getFaultString();
			}	
			
			populateSourceMap();
			convertParts();		
			String convertedMsg = buildNvpMessage(getTargetReferances());
			logger.debug("finished conversion");
			return convertedMsg;
		} catch (MessageException e) {
			logger.error(e.getMessage(), e);
			ResourceBundle bundle = ResourceBundle
					.getBundle("com.jawise.serviceadapter.core.messages");
			XmlRpcSerialiser xmlrpc = new XmlRpcSerialiser();
			String fault = xmlrpc.serialise(2000, e.getMessage(bundle));
			throw new XmlRpcMessageFault(fault);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			XmlRpcSerialiser xmlrpc = new XmlRpcSerialiser();
			String fault = xmlrpc.serialise(2000, e.getMessage());
			throw new XmlRpcMessageFault(fault);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		NvpTargetValueAccepter accepter = new NvpTargetValueAccepter(this,targetReferences,targetMap,null);
		accepter.acceptTargetValue(new TargetValue(targetName,targetValue,recursive));	
	}
	
	@Override
	protected Object getSourceValue(String sourceName) throws MessageException {
		Object val = sourceMap.get(sourceName);
		if (val == null) {
			return null;
		} else if (val instanceof String) {
			return val;
		} else {
			return val.toString();
		}
	}	

}
