package com.jawise.serviceadapter.convert.rpc;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.common.XmlRpcHttpRequestConfigImpl;
import org.apache.xmlrpc.common.XmlRpcStreamConfig;
import org.apache.xmlrpc.common.XmlRpcStreamRequestConfig;
import org.apache.xmlrpc.parser.XmlRpcRequestParser;
import org.apache.xmlrpc.parser.XmlRpcResponseParser;
import org.apache.xmlrpc.util.SAXParsers;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.jawise.serviceadapter.convert.AbstractXmlConverter;
import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;
import com.jawise.serviceadapter.core.Operation;
import com.jawise.serviceadapter.core.Part;
import com.jawise.serviceadapter.core.PartConversion;
import com.jawise.serviceadapter.core.PartConversions;
import com.jawise.serviceadapter.core.Protocol;
import com.jawise.serviceadapter.core.Type;

public abstract class AbstractXmlRpcConverter extends AbstractXmlConverter {

	private static Logger logger = Logger
			.getLogger(AbstractXmlRpcConverter.class);

	protected XmlRpcRequestParser requestParser;
	protected XmlRpcResponseParser responseParser;
	protected boolean fault;
	protected String faultmessage;
	protected List<String> trargetParameters;

	public AbstractXmlRpcConverter(MessageContext ctx)
			throws MessageException {
		super(ctx);

		try {
			Object messagse = ctx.get("messagse");
			if (messagse instanceof String) {
				String msg = (String) messagse;
				if (msg.indexOf("<methodCall>") >= 0
						|| msg.indexOf("<fault>") >= 0
						|| msg.indexOf("<methodResponse>") >= 0) {
					XmlRpcClient client = new XmlRpcClient();
					if (isInput()) {
						XmlRpcStreamConfig config = new XmlRpcHttpRequestConfigImpl();
						requestParser = new XmlRpcRequestParser(config, client
								.getTypeFactory());
						XMLReader xr = SAXParsers.newXMLReader();
						xr.setContentHandler(requestParser);
						xr.parse(new InputSource(new StringReader(msg)));
					} else if (msg.indexOf("<fault>") >= 0) {
						fault = true;
						faultmessage = msg;
					} else {
						XmlRpcStreamRequestConfig config = new XmlRpcClientConfigImpl();
						responseParser = new XmlRpcResponseParser(config,
								client.getTypeFactory());
						XMLReader xr = SAXParsers.newXMLReader();
						xr.setContentHandler(responseParser);
						xr.parse(new InputSource(new StringReader(msg)));
					}
				}
			}

			trargetParameters = new ArrayList<String>();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	protected void populateSourceMap() throws Exception {
		// there no names for parmeters the order of the prameter have
		// to be used

		String fromProtocol = getAdapterService().getPort().getBinding()
				.getProtocol();
		String toProtocol = getAdapterService().getAdapter().getService().getPort()
				.getBinding().getProtocol();

		if (isInput() && Protocol.XML_HTTP.toString().equals(fromProtocol)
				&& Protocol.XML_RPC.toString().equals(toProtocol)) {
			super.populateSourceMap();
		} 
		else if (!isInput() && Protocol.XML_RPC.toString().equals(fromProtocol)
				&& Protocol.XML_HTTP.toString().equals(toProtocol)) {
			super.populateSourceMap();
		}				
		else {
			// for xml rpc
			if (isInput()) {
				String opname = getMsgConversion().getAdapteroperation();
				Operation op = getAdapterService().getPort().getBinding()
						.getPortyype().findOperation(opname);
				if (op == null) {
					throw new MessageException("1019");
				}
				List<Part> parts = op.getInput().getParts();
				List params = requestParser.getParams();
				int expected = getActualPramsetersSize(op.getInput().getParts());
				if (params.size() != expected) {
					throw new MessageException("1020", "" + expected);
				}
				populateSourceMap(params, parts, 0, params.size(), null, null);
			} else {
				String opname = getMsgConversion().getAdapteeoperation();
				Operation op = getAdapterService().getAdapter().getService()
						.getPort().getBinding().getPortyype().findOperation(
								opname);
				List<Part> parts = op.getOutput().getParts();
				List params = new ArrayList();
				params.add(responseParser.getResult());
				populateSourceMap(params, parts, 0, params.size(), null, null);
			}
		}

	}

	private int getActualPramsetersSize(List<Part> parts) {
		int parmSize = 0;
		boolean inGroup = false;

		for (Part p : parts) {
			if (p.getName().indexOf("/") >= 0) {
				if (!inGroup) {
					parmSize++;
				}
				inGroup = true;
			} else {
				parmSize++;
				inGroup = false;
			}
		}
		return parmSize;
	}

	@SuppressWarnings("unchecked")
	private void populateSourceMap(List params, List<Part> parts,
			final int spos, final int epos, final String pn, final Object val)
			throws MessageException {

		for (int i = spos; i < epos; i++) {
			Object o = val == null ? params.get(i) : val;
			String partname = pn == null ? parts.get(i).getName() : pn;

			Part part = findPart(parts, partname);
			if (o instanceof Object[]) {
				// array
				addSourceEntry(part, partname, o);
			} else if (o instanceof Number) {
				addSourceEntry(part, partname, o);
			} else if (o instanceof Map) {
				// struct
				Map m = (Map) o;
				Iterator iter = m.keySet().iterator();
				int j = i;
				while (iter.hasNext()) {
					String key = (String) iter.next();
					String partnamePrefix = partname.substring(0, partname
							.lastIndexOf("/"));
					String thispartname = partnamePrefix + "/" + key;
					populateSourceMap(params, parts, j, j + 1, thispartname, m
							.get(key));
					j++;
				}

			} else {
				addSourceEntry(part, partname, o);
			}
		}
	}

	private Part findPart(List<Part> parts, String name) {
		for (Part p : parts) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private void addSourceEntry(Part part, String partname, Object o)
			throws MessageException {
		String referanceName = new XmlPath(partname).getName();
		String sourceName = partname;
		validateSourceEntry(part, partname, o);
		if (o instanceof Object[]) {
			referanceName = referanceName.replaceAll("\\x5b\\x5d", "");
			sourceReferences.put(referanceName, o);
			sourceMap.put(sourceName, o);
		} else if (o instanceof Number) {
			sourceReferences.put(referanceName, o);
			sourceMap.put(sourceName, o);
		} else {
			String value = o == null ? null : o.toString();
			sourceReferences.put(referanceName, value);
			sourceMap.put(sourceName, value);
		}

		logger.debug(sourceName + " = " + o.toString());
	}

	private void validateSourceEntry(Part part, String partname, Object o)
			throws MessageException {
		if (partname.indexOf("[]") > 0 && !(o instanceof Object[])) {
			throw new MessageException("1022", partname);
		}

		if (part != null) {
			if (part.getType().isArray() && !(o instanceof Object[])) {
				throw new MessageException("1022", partname);
			} else if (part.getType().isNumeric() && !(o instanceof Number)) {
				throw new MessageException("1023", partname);
			} else if (part.getType().isBoolean() && !(o instanceof Boolean)) {
				throw new MessageException("1023", partname);
			} else if (part.getType().isString() && !(o instanceof String)) {
				throw new MessageException("1023", partname);
			} else if (part.getType().isStruct() && !(o instanceof Map)) {
				throw new MessageException("1023", partname);
			} else if (part.getType().isDate() && !(o instanceof Date)) {
				throw new MessageException("1023", partname);
			}
		}
	}
/*
	@SuppressWarnings("unchecked")
	protected List convertTargetToXmlRpcResult() throws MessageException {
		List result = new ArrayList();
		Iterator iter = trargetParameters.iterator();
		int index = 0;
		while (iter.hasNext()) {
			String key = (String) iter.next();
			Object value = targetMap.get(key);
			addParameter(result, key, value, index);
			index++;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	protected List convertTargetToXmlRpcParmeters() throws MessageException {

		List parmeters = new ArrayList();
		Iterator iter = trargetParameters.iterator();
		int index = 0;
		while (iter.hasNext()) {
			String key = (String) iter.next();
			Object value = targetMap.get(key);
			addParameter(parmeters, key, value, index);
			index++;
		}
		return parmeters;
	}

	@SuppressWarnings("unchecked")
	private void addParameter(List parmeters, final String key,
			final Object val, final int index) throws MessageException {

		Part part = findPart(key, index);
		Object value = val;
		if (value == null)
			value = getNullReplacement(part);

		if (part.getName().indexOf("/") >= 0) {
			XmlPath p = new XmlPath(key);
			int lastparam = parmeters.size() == 0 ? 0 : parmeters.size() - 1;
			Object param = parmeters.get(lastparam);
			if (param instanceof Map) {
				((Map) param).put(p.getName(), value);
			} else {
				Map sturct = new HashMap();
				sturct.put(p.getName(), value);
				parmeters.add(sturct);
			}
		} else if (value instanceof String) {
			parmeters.add(value);
		} else if (value instanceof Byte) {
			parmeters.add(value);
		} else if (value instanceof Short) {
			parmeters.add(value);
		} else if (value instanceof Integer) {
			parmeters.add(value);
		} else if (value instanceof Long) {
			parmeters.add(value);
		} else if (value instanceof Double) {
			parmeters.add(value);
		} else if (value instanceof Boolean) {
			parmeters.add(value);
		} else if (value instanceof Date) {
			parmeters.add(value);
		} else if (value instanceof Calendar) {
			parmeters.add(value);
		} else if (value instanceof Byte[]) {
			parmeters.add(value);
		} else if (value instanceof Object[]) {
			parmeters.add(value);
		} else if (value instanceof Map) {
			parmeters.add(value);
		} else if (value instanceof List) {
			parmeters.add(value);
		} else if (value == null) {
			parmeters.add(value);
		} else {
			throw new MessageException("1020", value != null ? value.getClass()
					.toString() : "Null");
		}
	}

	@SuppressWarnings("unchecked")
	private Object getNullReplacement(Part part) {
		Type type = part.getType();
		if (type.isNumeric()) {
			return 0;
		} else if (type.isString()) {
			return "";
		} else if (type.isArray()) {
			return new Object[0];
		} else if (type.isBoolean()) {
			return false;
		} else if (type.isStruct()) {
			return new HashMap();
		} else if (type.isDate()) {
			return new Date();
		} else
			return new Object();
	}

	@SuppressWarnings("unused")
	private Part findPart(String key, int index) {
		logger.debug("finding part for :" + key);

		PartConversions partConversions = isInput() ? getMsgConversion()
				.getInputPartConversions() : getMsgConversion()
				.getOutputPartConversions();

		PartConversion pc = partConversions.get(index);
		Part adapteePart = isInput() ? pc.getAdapteePart() : pc.getAdapterPart();

		String referanceName = new XmlPath(adapteePart.getName()).getName();
		if (!key.equals(referanceName) && key.indexOf(referanceName) == -1) {
			logger.debug("part key miss match for " + key + " - "
					+ referanceName + " - using index " + index);
		}
		return adapteePart;
	}
*/
	@SuppressWarnings("unchecked")
	@Override
	public Map getSourceReferences() throws MessageException {
		return sourceReferences;
	}

	@Override
	protected Object getSourceValue(String sourceName) throws MessageException {
		Object val = sourceMap.get(sourceName);
		if (val == null) {
			return null;
		} else if (val instanceof String) {
			return val;
		} else if (val instanceof Number) {
			return val;
		} else {
			return val.toString();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Map getTargetReferances() throws MessageException {
		return targetReferences;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		XmlPath p = new XmlPath(targetName);
		targetMap.put(targetName, targetValue);
		targetReferences.put(p.getName(), targetValue);
		trargetParameters.add(targetName);
	}

	@Override
	protected String getScriptTargetName(String targetName) {
		return new XmlPath(targetName).getName();
	}


	protected String getFaultString() {

		try {
			DocumentBuilderFactory i = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = i.newDocumentBuilder();
			Document faultDoc = db.parse(new ByteArrayInputStream(faultmessage
					.getBytes()));
			faultDoc.getElementsByTagName("value");

			NodeList memberNodeList = XPathAPI.selectNodeList(faultDoc,
					"methodResponse/fault/value/struct/member/value");
			String value = memberNodeList.item(1).getFirstChild()
					.getNodeValue();
			return value;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			int spos = faultmessage.indexOf("<name>faultString</name><value>") + 31;
			int epos = faultmessage
					.indexOf("</value></member></struct></value></fault>");
			String msg = faultmessage.substring(spos, epos);
			if (msg.isEmpty()) {
				return faultmessage;
			} else {
				msg = "the following error occurred inprocessing the request : "
						+ msg;
				return msg;
			}
		}

	}
}
