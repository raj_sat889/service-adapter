package com.jawise.serviceadapter.convert.rpc;

import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.ws.commons.serialize.XMLWriter;
import org.apache.ws.commons.serialize.XMLWriterImpl;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfig;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcSunHttpTransportFactory;
import org.apache.xmlrpc.common.XmlRpcStreamConfig;
import org.apache.xmlrpc.serializer.XmlRpcWriter;
import org.xml.sax.SAXException;

public class XmlRpcSerialiser {

	private static Logger logger = Logger.getLogger(XmlRpcSerialiser.class);

	public String serialise(int errocode, String error) {
		try {
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			XmlRpcClient client = new XmlRpcClient();
			client
					.setTransportFactory(new XmlRpcSunHttpTransportFactory(
							client));
			client.setConfig((XmlRpcClientConfig) config);

			StringWriter sw = new StringWriter();
			XMLWriter xw = new XMLWriterImpl();
			xw.setEncoding("US-ASCII");
			xw.setDeclarating(true);
			xw.setIndenting(false);
			xw.setWriter(sw);
			XmlRpcWriter xrw = new XmlRpcWriter((XmlRpcStreamConfig) client
					.getConfig(), xw, client.getTypeFactory());

			xrw.write(config, errocode, error);
			return sw.toString();
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
			return e.getMessage();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String serialise(XmlRpcClient pClient,
			XmlRpcClientConfigImpl config, Object result) throws SAXException {

		StringWriter sw = new StringWriter();
		XMLWriter xw = new XMLWriterImpl();
		xw.setEncoding("US-ASCII");
		xw.setDeclarating(true);
		xw.setIndenting(false);
		xw.setWriter(sw);
		XmlRpcWriter xrw = new XmlRpcWriter((XmlRpcStreamConfig) pClient
				.getConfig(), xw, pClient.getTypeFactory());
		xrw.write(config, result);
		return sw.toString();

	}

	public String serialise(XmlRpcClient pClient, XmlRpcRequest pRequest)
			throws SAXException {
		StringWriter sw = new StringWriter();
		XMLWriter xw = new XMLWriterImpl();
		xw.setEncoding("US-ASCII");
		xw.setDeclarating(true);
		xw.setIndenting(false);
		xw.setWriter(sw);
		XmlRpcWriter xrw = new XmlRpcWriter((XmlRpcStreamConfig) pClient
				.getConfig(), xw, pClient.getTypeFactory());
		xrw.write(pRequest);
		return sw.toString();
	}	
}
