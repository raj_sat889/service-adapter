package com.jawise.serviceadapter.convert;

import com.jawise.serviceadapter.convert.xml.XmlPath;

public final class TargetValue {
	final String targetName;
	final Object targetValue;
	final boolean recursive;
	

	public TargetValue(String targetName, Object targetValue, boolean recursive) {
		super();
		this.targetName = targetName;
		this.targetValue = targetValue;
		this.recursive = recursive;
	}


	public String getTargetName() {
		return targetName;
	}

	public Object getTargetValue() {
		return targetValue;
	}

	public boolean getRecursive() {
		return recursive;
	}

}
