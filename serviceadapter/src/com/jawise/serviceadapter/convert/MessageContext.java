package com.jawise.serviceadapter.convert;

import java.util.HashMap;

import com.jawise.serviceadapter.GUID;

public class MessageContext extends HashMap<String, Object> {
	
	private static final long serialVersionUID = 3013500662179388732L;

	public static final String REQUEST = "request";
	
	public static final String RESPONSE = "response";
	
	public static final String SOAP_VERSION = "SoapVersion";

	public static final String BODY = "body";

	public static final String MESSAGE_CONVERSION = "messageConversion";

	public static final String INPUT = "input";

	public static final String ADAPTER_SERVICE = "adapterService";

	public static final String XMLSTREAMREADER = "xmlstreamreader";

	public static final String PARAMTERSMAP = "paramtersmap";
	
	public static final String PARAMTERSLIST = "paramterslist";

	public static final String CONVERTEDMESSAGE = "convertedmessage";

	public static final String MSG_DEF = "msgdef";

	public static final String SERVICE_ADAPTER_REQUEST = "serviceadapterrequest";
	
	private Exception exception;
	private String id = new GUID().toString();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isError() {
		return (exception == null ? false : true);
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}
}
