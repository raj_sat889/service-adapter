package com.jawise.serviceadapter.convert;

import java.util.List;
import java.util.Map;

import com.jawise.serviceadapter.core.MessageException;

public abstract class TargetValueAccepter {

	@SuppressWarnings( { "unused", "unchecked" })
	protected Map targetReferences;
	@SuppressWarnings("unchecked")
	protected Map targetMap;
	@SuppressWarnings( { "unchecked", "unused" })
	protected List trargetParameters;
	protected MessageConverter mc;


	@SuppressWarnings("unchecked")
	public TargetValueAccepter(MessageConverter mc, Map targetReferences,
			Map targetMap, List trargetParameters) {
		super();
		this.targetReferences = targetReferences;
		this.targetMap = targetMap;
		this.trargetParameters = trargetParameters;
		this.mc = mc;
	}


	@SuppressWarnings("unchecked")
	abstract public void acceptTargetValue(TargetValue value) throws MessageException;
}
