package com.jawise.serviceadapter.convert.xml;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.AbstractXmlConverter;
import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.soap.binding.SoapResponseMessageBuilder;
import com.jawise.serviceadapter.convert.soap.binding.SoapRequestMessageBuilder;
import com.jawise.serviceadapter.core.MessageException;

public class XmlToSoapConverter extends AbstractXmlConverter {

	private static Logger logger = Logger.getLogger(XmlToSoapConverter.class);

	@SuppressWarnings("unchecked")
	protected List trargetParameters;

	public XmlToSoapConverter(MessageContext ctx) throws MessageException {
		super(ctx);
		trargetParameters = new ArrayList<String>();
		try {
			createSourceDoc();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}

	}

	@Override
	public Object doConvertion() throws MessageException {

		try {
			logger.debug("started conversion");
			populateSourceMap();
			convertParts();
			getCtx().put("paramterslist", trargetParameters);
			getCtx().put("paramtersmap", targetMap);

			if (isInput()) {
				SoapRequestMessageBuilder soapMessageBuilder = new SoapRequestMessageBuilder(
						getCtx());
				SOAPMessage msg = soapMessageBuilder.buildRequestMessage();
				ByteArrayOutputStream str = new ByteArrayOutputStream();
				msg.writeTo(str);
				logger.info("converted messages : "
						+ new String(str.toByteArray()));
				logger.debug("finished conversion");
				return msg;
			} else {

				SoapResponseMessageBuilder soapMessageBuilder = new SoapResponseMessageBuilder(
						getCtx());
				String str = soapMessageBuilder.buildResponseMessage();
				logger.info("converted messages : " + str);
				logger.debug("finished conversion");
				return str;
			}
		} catch (MessageException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException(e.getMessage(), e);
		}

	}

	@Override
	protected Map getSourceReferences() throws MessageException {
		return sourceReferences;
	}

	@Override
	protected Object getSourceValue(String sourceName) throws MessageException {
		try {
			String sourceValue = (String) sourceMap.get(sourceName);
			return applyEncoding(sourceValue);
		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}
	}

	@Override
	protected Map getTargetReferances() throws MessageException {
		return targetMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		targetMap.put(targetName, targetValue);
		targetReferences.put(targetValue, targetValue);
		trargetParameters.add(targetName);

	}

	@Override
	protected String getScriptTargetName(String targetName) {
		return new XmlPath(targetName).getName();
	}
}
