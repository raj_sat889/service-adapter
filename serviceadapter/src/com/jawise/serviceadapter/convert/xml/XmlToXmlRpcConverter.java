package com.jawise.serviceadapter.convert.xml;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfig;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcClientRequestImpl;
import org.apache.xmlrpc.client.XmlRpcSunHttpTransportFactory;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.rpc.AbstractXmlRpcConverter;
import com.jawise.serviceadapter.convert.rpc.XmlRpcConversionHelper;
import com.jawise.serviceadapter.convert.rpc.XmlRpcSerialiser;
import com.jawise.serviceadapter.core.MessageException;

public class XmlToXmlRpcConverter extends AbstractXmlRpcConverter {
	private static Logger logger = Logger.getLogger(XmlToXmlRpcConverter.class);

	public XmlToXmlRpcConverter(MessageContext ctx) throws MessageException {
		super(ctx);
		try {
			createSourceDoc();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object doConvertion() throws MessageException {
		try {
			logger.debug("started conversion");
			if (fault) {
				return getFaultString();
			}

			populateSourceMap();
			convertParts();
			String convertedMsg = "";
			XmlRpcConversionHelper xmlrpcHelper = new XmlRpcConversionHelper();
			if (isInput()) {
				List parmeters = xmlrpcHelper.convertTargetToXmlRpcParmeters(
						this, targetMap, trargetParameters);
				Object[] parmeterArray = new Object[parmeters.size()];
				parmeterArray = parmeters.toArray(parmeterArray);
				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

				String method = xmlrpcHelper.getAdapteeMethod(this);
				XmlRpcRequest request = new XmlRpcClientRequestImpl(config,
						method, parmeterArray);
				XmlRpcClient client = new XmlRpcClient();
				client.setTransportFactory(new XmlRpcSunHttpTransportFactory(
						client));
				client.setConfig((XmlRpcClientConfig) config);
				convertedMsg = new XmlRpcSerialiser()
						.serialise(client, request);
			} else {
				List result = xmlrpcHelper.convertTargetToXmlRpcResult(this,
						targetMap, trargetParameters);
				Object[] resultArray = new Object[result.size()];
				resultArray = result.toArray(resultArray);

				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
				XmlRpcClient client = new XmlRpcClient();
				client.setTransportFactory(new XmlRpcSunHttpTransportFactory(
						client));
				client.setConfig((XmlRpcClientConfig) config);
				convertedMsg = new XmlRpcSerialiser().serialise(client, config,
						result.get(0));
			}

			return convertedMsg;

		} catch (MessageException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException(e.getMessage());
		}
	}

	@Override
	protected Object getSourceValue(String sourceName) throws MessageException {
		try {
			String sourceValue = (String) sourceMap.get(sourceName);
			return applyEncoding(sourceValue);

		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}
	}

	@Override
	protected Map getTargetReferances() throws MessageException {
		return targetMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		XmlPath p = new XmlPath(targetName);
		targetMap.put(targetName, targetValue);
		targetReferences.put(p.getName(), targetValue);
		trargetParameters.add(targetName);

	}

}
