package com.jawise.serviceadapter.convert.xml;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.AbstractXmlConverter;
import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.NvpTargetValueAccepter;
import com.jawise.serviceadapter.convert.TargetValue;
import com.jawise.serviceadapter.core.MessageException;

public class XmlToNvpConverter extends AbstractXmlConverter {
	private static Logger logger = Logger.getLogger(XmlToNvpConverter.class);

	@SuppressWarnings("unchecked")
	public XmlToNvpConverter(MessageContext ctx) throws MessageException {
		super(ctx);
		try {
			createSourceDoc();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object doConvertion() throws MessageException {
		try {
			logger.debug("started conversion");
			populateSourceMap();
			convertParts();
			String m = buildNvpMessage(targetMap);
			logger.debug("finished conversion");
			return m;
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1008");
		} catch (MessageException e) {
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	protected Map getTargetReferances() {
		return targetMap;
	}

	@SuppressWarnings("unchecked")
	protected Map getSourceReferences() {
		return sourceReferences;
	}

	@SuppressWarnings("unchecked")
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {

		NvpTargetValueAccepter accepter = new NvpTargetValueAccepter(this,
				targetReferences, targetMap, null);
		accepter.acceptTargetValue(new TargetValue(targetName, targetValue,
				recursive));
	}

	protected Object getSourceValue(String sourceName) throws MessageException{
		try {
			String sourceValue = (String) sourceMap.get(sourceName);
			return applyEncoding(sourceValue);
		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}
	}

}
