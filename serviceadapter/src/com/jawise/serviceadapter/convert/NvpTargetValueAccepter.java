package com.jawise.serviceadapter.convert;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jawise.serviceadapter.core.MessageException;

public class NvpTargetValueAccepter extends TargetValueAccepter {

	public NvpTargetValueAccepter(MessageConverter mc, Map targetReferences,
			Map targetMap, List trargetParameters) {
		super(mc, targetReferences, targetMap, trargetParameters);
	}

	@SuppressWarnings("unchecked")
	public void acceptTargetValue(TargetValue value) throws MessageException {
		try {
			if (value.getRecursive()) {
				// TODO NVP=NVP recursive testing
				Map nvpParams = NVPHelper.parseNvpMessage((String) value
						.getTargetValue(), mc.getUrlEncodeParameter(), mc
						.getTargetSeperator());
				Set keySet = nvpParams.keySet();
				Iterator iterator = keySet.iterator();
				while (iterator.hasNext()) {
					String key = (String) iterator.next();
					String[] val = (String[]) nvpParams.get(key);
					String encodedval = mc.applyEncoding(val[0]);
					targetMap.put(key, encodedval);
					if (targetReferences != null) {
						targetReferences.put(key, encodedval);
					}
					if (trargetParameters != null) {
						trargetParameters.add(key);
					}

				}
			} else {
				targetMap.put(value.getTargetName(), mc
						.applyEncoding((String) value.getTargetValue()));
				
				if (targetReferences != null) {
					targetReferences.put(value.getTargetName(), value
							.getTargetValue());
				}
				if (trargetParameters != null) {
					trargetParameters.add(value.getTargetName());
				}
			}
		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}

	}
}
