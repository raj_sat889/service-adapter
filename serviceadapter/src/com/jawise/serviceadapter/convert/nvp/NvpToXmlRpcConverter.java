package com.jawise.serviceadapter.convert.nvp;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfig;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcClientRequestImpl;
import org.apache.xmlrpc.client.XmlRpcSunHttpTransportFactory;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.rpc.AbstractXmlRpcConverter;
import com.jawise.serviceadapter.convert.rpc.XmlRpcConversionHelper;
import com.jawise.serviceadapter.convert.rpc.XmlRpcSerialiser;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;

public class NvpToXmlRpcConverter extends AbstractXmlRpcConverter {

	private static Logger logger = Logger.getLogger(NvpToXmlRpcConverter.class);

	@SuppressWarnings("unchecked")
	public NvpToXmlRpcConverter(MessageContext ctx) throws MessageException {
		super(ctx);
		sourceMap = (Map) ctx.get("messagse");
		sourceReferences = (Map) ctx.get("messagse");
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object doConvertion() throws MessageException {
		try {
			logger.debug("started conversion");
			if (fault) {
				return getFaultString();
			}
			convertParts();
			String convertedMsg = "";
			XmlRpcConversionHelper xmlrpcHelper = new XmlRpcConversionHelper();

			if (isInput()) {

				List parmeters = xmlrpcHelper.convertTargetToXmlRpcParmeters(
						this, targetMap, trargetParameters);
				Object[] parmeterArray = new Object[parmeters.size()];
				parmeterArray = parmeters.toArray(parmeterArray);
				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
				String method = xmlrpcHelper.getAdapteeMethod(this);
				XmlRpcRequest request = new XmlRpcClientRequestImpl(config,
						method, parmeterArray);
				XmlRpcClient client = new XmlRpcClient();
				client.setTransportFactory(new XmlRpcSunHttpTransportFactory(
						client));
				client.setConfig((XmlRpcClientConfig) config);
				convertedMsg = new XmlRpcSerialiser()
						.serialise(client, request);
			} else {
				List result = xmlrpcHelper.convertTargetToXmlRpcResult(this,
						targetMap, trargetParameters);
				Object[] resultArray = new Object[result.size()];
				resultArray = result.toArray(resultArray);

				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
				XmlRpcClient client = new XmlRpcClient();
				client.setTransportFactory(new XmlRpcSunHttpTransportFactory(
						client));
				client.setConfig((XmlRpcClientConfig) config);
				convertedMsg = new XmlRpcSerialiser().serialise(client, config,
						result.get(0));
			}
			logger.debug("finished conversion");
			return convertedMsg;
		} catch (MessageException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		XmlPath p = new XmlPath(targetName);
		targetMap.put(targetName, targetValue);
		targetReferences.put(p.getName(), targetValue);
		trargetParameters.add(targetName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map getSourceReferences() throws MessageException {
		return sourceMap;
	}

	@Override
	protected String getSourceValue(String sourceName) throws MessageException {
		try {
			String sourceValue = "";
			String[] sourceValues = (String[]) sourceMap.get(sourceName);
			if (sourceValues != null) {
				sourceValue = sourceValues[0];
			}
			return applyEncoding(sourceValue);
		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}
	}
}
