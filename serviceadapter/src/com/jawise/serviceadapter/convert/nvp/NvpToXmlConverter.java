package com.jawise.serviceadapter.convert.nvp;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.MessageConverter;
import com.jawise.serviceadapter.convert.TargetValue;
import com.jawise.serviceadapter.convert.XmlTargetValueAccepter;
import com.jawise.serviceadapter.convert.xml.XmlComposer;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;

public class NvpToXmlConverter extends MessageConverter {
	private static Logger logger = Logger.getLogger(NvpToXmlConverter.class);

	@SuppressWarnings("unchecked")
	private Map targetMap;
	@SuppressWarnings("unchecked")
	private Map targetReferences;
	@SuppressWarnings("unchecked")
	private Map sourceMap;
	private XmlPath lastAddedTargetPath;
	@SuppressWarnings("unchecked")
	private Map recursiveNodesMap;

	@SuppressWarnings("unchecked")
	public NvpToXmlConverter(MessageContext ctx)
			throws MessageException {
		super(ctx);
		sourceMap = (Map) ctx.get("messagse");
		targetMap = new HashMap<XmlPath, String>();
		targetReferences = new HashMap<String, String>();
		recursiveNodesMap = new HashMap<XmlPath, String>();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object doConvertion() throws MessageException {
		XmlComposer composer = new XmlComposer();
		try {
			convertParts();
			String xml = composer.compose((HashMap) targetMap,
					(HashMap) recursiveNodesMap);
			return xml;
		}
		catch (MessageException e) {
			logger.error(e.getMessage(), e);
			ResourceBundle bundle = ResourceBundle
					.getBundle("com.jawise.serviceadapter.core.messages");
			throw new MessageException("1013", e.getMessage(bundle));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MessageException("1013", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Map getSourceReferences() {
		return sourceMap;
	}

	@Override
	protected String getSourceValue(String sourceName) throws MessageException {
		try {
			String sourceValue = "";
			String[] sourceValues = (String[]) getSourceReferences().get(
					sourceName);
			if (sourceValues != null) {
				sourceValue = sourceValues[0];
			}
			return applyEncoding(sourceValue);
		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Map getTargetReferances() {
		return targetReferences;
	}

	@SuppressWarnings( { "unchecked", "unchecked" })
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		XmlTargetValueAccepter accepter = new XmlTargetValueAccepter(this,
				targetReferences, targetMap, null, recursiveNodesMap,
				lastAddedTargetPath);
		accepter.acceptTargetValue(new TargetValue(targetName, targetValue,
				recursive));
		lastAddedTargetPath = accepter.getLastAddedTargetPath();		

	}

	@Override
	protected String getScriptTargetName(String targetName) {
		return new XmlPath(targetName).getName();
	}

}
