package com.jawise.serviceadapter.convert.nvp;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.MessageConverter;
import com.jawise.serviceadapter.convert.soap.binding.SoapResponseMessageBuilder;
import com.jawise.serviceadapter.convert.soap.binding.SoapRequestMessageBuilder;
import com.jawise.serviceadapter.convert.xml.XmlPath;
import com.jawise.serviceadapter.core.MessageException;

public class NvpToSoapConverter extends MessageConverter {


	private static Logger logger = Logger.getLogger(NvpToNvpConverter.class);
	@SuppressWarnings("unchecked")
	private Map sourceMap;
	@SuppressWarnings("unchecked")
	private Map targetMap;	
	@SuppressWarnings("unchecked")
	protected Map targetReferences;
	@SuppressWarnings("unchecked")
	protected List trargetParameters;
	
	public NvpToSoapConverter(MessageContext ctx) throws MessageException {
		super(ctx);
		sourceMap = (Map) ctx.get("messagse");
		targetMap = new HashMap<String, String>();
		targetReferences = new HashMap<String, String>();
		trargetParameters = new ArrayList<String>();		
	}
	
	@Override
	public Object doConvertion() throws MessageException {
		try {
			logger.debug("started conversion");
			convertParts();
			getCtx().put("paramterslist", trargetParameters);
			getCtx().put("paramtersmap", targetMap);			
			
			if (isInput()) {
				SoapRequestMessageBuilder soapMessageBuilder = new SoapRequestMessageBuilder(getCtx());
				SOAPMessage msg = soapMessageBuilder.buildRequestMessage();
				ByteArrayOutputStream str = new ByteArrayOutputStream();
				msg.writeTo(str);
				logger.info("converted messages : "
						+ new String(str.toByteArray()));
				logger.debug("finished conversion");
				return msg;
			} else {

				SoapResponseMessageBuilder soapMessageBuilder = new SoapResponseMessageBuilder(
						getCtx());
				String str = soapMessageBuilder.buildResponseMessage();
				logger.info("converted messages : " + str);
				logger.debug("finished conversion");
				return str;
			}			
			
		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}
		catch(MessageException e) {
			throw e;
		}
		catch (Exception e) {
			throw new MessageException(e.getMessage(), e);
		}		
	}


	@Override
	@SuppressWarnings("unchecked")
	protected Map getTargetReferances() {
		return targetMap;
	}
	@Override
	@SuppressWarnings("unchecked")
	protected Map getSourceReferences() {
		return sourceMap;
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void setTargetValue(String targetName, Object targetValue,
			boolean recursive) throws MessageException {
		targetMap.put(targetName, targetValue);
		targetReferences.put(targetValue, targetValue);
		trargetParameters.add(targetName);		
	}

	@Override
	protected Object getSourceValue(String sourceName) throws MessageException {
		try {
			String sourceValue = "";
			String[] sourceValues = (String[]) getSourceReferences().get(
					sourceName);
			if (sourceValues != null) {
				sourceValue = sourceValues[0];
			}

			return applyEncoding(sourceValue);
		} catch (UnsupportedEncodingException e) {
			throw new MessageException("1008");
		}
	}
	
	@Override
	protected String getScriptTargetName(String targetName) {
		return new XmlPath(targetName).getName();
	}		
}
