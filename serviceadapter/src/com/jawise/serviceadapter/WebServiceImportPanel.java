package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.wsdl.Binding;
import javax.wsdl.BindingOperation;
import javax.wsdl.Input;
import javax.wsdl.Operation;
import javax.wsdl.Output;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.wsdl.WSDLException;

import org.apache.log4j.Logger;
import org.exolab.castor.xml.schema.ComplexType;
import org.exolab.castor.xml.schema.ElementDecl;
import org.exolab.castor.xml.schema.Group;
import org.exolab.castor.xml.schema.Particle;
import org.exolab.castor.xml.schema.Structure;
import org.exolab.castor.xml.schema.XMLType;

import com.jawise.serviceadapter.core.Message;
import com.jawise.serviceadapter.core.PortType;
import com.jawise.serviceadapter.core.Protocol;
import com.jawise.serviceadapter.imp.WsdlImporter;

public class WebServiceImportPanel extends Panel {
	private static Logger logger = Logger
			.getLogger(WebServiceImportPanel.class);

	private String wsdllocation;
	private int screenstate = 1;
	private String servicename;
	private String servicelocation;
	private String binding;
	private String operation;
	private String port;
	private SelectItem[] ports;
	private SelectItem[] operations;
	private SelectItem[] services;
	private WsdlImporter importer;

	private String servicenametouse;

	public void loadWSDL(ActionEvent event) {
		ValidationErrors errors = new ValidationErrors();
		if (wsdllocation == null || wsdllocation.isEmpty()) {
			errors.add("wsdl location parameter is required");
			error("webserviceImportForm", errors);
			return;
		}

		try {
			importer = new WsdlImporter();
			importer.importWsdl(wsdllocation);

		} catch (WSDLException e) {
			errors.add(e.getMessage());
			error("webserviceImportForm", errors);
			return;
		}

		loadServices();

		screenstate = 2;

	}

	public void loadServiceDetails(ActionEvent event) {

		if (importer == null) {
			screenstate = 2;
			return;
		}

		ValidationErrors errors = new ValidationErrors();
		if (servicename == null || servicename.isEmpty()) {
			errors.add("service must be specified");
			error("webserviceImportForm", errors);
			return;
		}

		importer.extractService(servicename);

		// now load ports and select the 1st

		loadPorts();

		Port serviceport = importer.getPort();

		// location info

		servicelocation = importer.getServiceLocation();
		// load binding info
		binding = serviceport.getBinding().getQName().getLocalPart();

		// load operation
		loadOperations();

		screenstate = 3;
		return;
	}

	public void cancel(ActionEvent event) {

		Map<String, Object> sessionMap = getSessionMap();
		Map<String, Object> requestMap = getRequestMap();

		requestMap.remove("newservice");
		requestMap.remove("service");
		requestMap.remove("webServiceImportPanel");

		serviceRegistryBrowser.setSelectedPanel("emptyPanel");
		if (serviceRegistryBrowser.getSelectednode() != null) {
			serviceRegistryBrowser.getSelectednode().removeFromParent();
		}
	}

	public void portChange(ValueChangeEvent e) {

		Object newValue = e.getNewValue();
		importer.extractPort((String) newValue);
		Port serviceport = importer.getPort();

		// location info

		servicelocation = importer.getServiceLocation();
		// load binding info
		binding = serviceport.getBinding().getQName().getLocalPart();

		// load operation
		loadOperations();

	}

	public void serviceChange(ValueChangeEvent e) {
		Object newValue = e.getNewValue();
		servicenametouse = (String) newValue;
	}

	public void back(ActionEvent event) {
		if (screenstate > 1) {
			screenstate--;
		}
	}

	private void loadServices() {
		Collection<SelectItem> serviceitems = new ArrayList<SelectItem>();
		Map improtservices = importer.getServices();
		Iterator iter = improtservices.keySet().iterator();
		while (iter.hasNext()) {
			Service s = (Service) improtservices.get(iter.next());

			serviceitems.add(new SelectItem(s.getQName().getLocalPart(), s
					.getQName().getLocalPart()));

		}

		services = new SelectItem[serviceitems.size()];
		services = serviceitems.toArray(services);
		if (services.length > 0) {
			servicenametouse = services[0].getLabel();
		}
	}

	private void loadPorts() {
		Collection<SelectItem> portitems = new ArrayList<SelectItem>();

		Service service = importer.getService();
		Map serviceports = service.getPorts();
		Iterator iter = serviceports.keySet().iterator();
		while (iter.hasNext()) {
			Port p = (Port) serviceports.get(iter.next());
			portitems.add(new SelectItem(p.getName(), p.getName()));
		}

		ports = new SelectItem[portitems.size()];
		ports = portitems.toArray(ports);
		importer.extractPort(ports[0].getLabel());
	}

	private void loadOperations() {
		Collection<SelectItem> operationitems = new ArrayList<SelectItem>();

		List ops = importer.getPort().getBinding().getBindingOperations();

		for (int i = 0; i < ops.size(); i++) {
			BindingOperation bop = (BindingOperation) ops.get(i);

			System.out.println("binding : " + bop.getName());
			Operation op = bop.getOperation();
			operationitems.add(new SelectItem(op.getName(), op.getName()));

		}

		operations = new SelectItem[operationitems.size()];
		operations = operationitems.toArray(operations);
	}

	public void importService(ActionEvent event) {

		ValidationErrors errors = new ValidationErrors();

		try {

			ServiceRegistry registry = ServiceRegistry.getInstance();

			// create service

			com.jawise.serviceadapter.core.Service newservice = new com.jawise.serviceadapter.core.Service();

			if (servicenametouse != null && !servicenametouse.isEmpty()) {
				newservice.setName(servicenametouse);
			} else {
				newservice.setName(servicename);
			}

			if (registry.findService(newservice.getName()) != null) {
				errors
						.add("A service with same name already exist in the registry");
				error("webserviceImportForm", errors);
				return;
			}

			Service service = importer.getService();
			newservice.setNamespace(service.getQName().getNamespaceURI());

			com.jawise.serviceadapter.core.Port port = newservice.getPort();
			port.setName(importer.getPort().getName());
			port.setLocation(servicelocation);

			com.jawise.serviceadapter.core.Binding binding = port.getBinding();
			binding.setName(importer.getPort().getBinding().getQName()
					.getLocalPart());
			binding.setProtocol(Protocol.SOAP11.toString());
			binding.setEncodingStyle("");
			binding.setTranport("HTTP");
			addOperations(newservice, importer.getPort().getBinding());
			registry.register(newservice);

			wsdllocation = "";
			screenstate = 1;
			serviceRegistryBrowser.refresh();
			serviceRegistryBrowser.setSelectedPanel("emptyPanel");
			return;
		} catch (IOException e) {
			errors.add(e.getMessage());
			error("webserviceImportForm", errors);
		}

	}

	private void addOperations(
			com.jawise.serviceadapter.core.Service newservice, Binding binding) {
		List operations = binding.getBindingOperations();

		PortType portyype = newservice.getPort().getBinding().getPortyype();
		Collection<com.jawise.serviceadapter.core.Operation> newops = portyype
				.getOperations();
		for (int i = 0; i < operations.size(); i++) {
			BindingOperation bop = (BindingOperation) operations.get(i);

			Operation operation = bop.getOperation();
			com.jawise.serviceadapter.core.Operation newop = new com.jawise.serviceadapter.core.Operation();

			newop.setName(operation.getName());
			newop.setOperationtype(operation.getStyle().toString());

			Input input = operation.getInput();
			com.jawise.serviceadapter.core.Message inmsg = new Message();
			String msgname = input.getMessage().getQName().getLocalPart();
			inmsg.setName(msgname);
			Map inputparts = input.getMessage().getParts();

			inmsg.setParts(getParts(operation, false, inputparts));

			Output output = operation.getOutput();
			com.jawise.serviceadapter.core.Message outmsg = new Message();
			msgname = output.getMessage().getQName().getLocalPart();
			outmsg.setName(msgname);
			Map outputparts = output.getMessage().getParts();

			outmsg.setParts(getParts(operation, true, outputparts));

			if ("REQUEST_RESPONSE".equals(newop.getOperationtype())) {
				newop.setInput(inmsg);
				newop.setOutput(outmsg);
			} else if ("NOTIFICATION".equals(newop.getOperationtype())) {
				newop.setInput(outmsg);
			} else {
				newop.setOutput(outmsg);
				newop.setInput(inmsg);
			}

			// 5th identify the Part, if there is only 1 part then the parts are
			// defined types sections
			newops.add(newop);
		}
	}

	private List<com.jawise.serviceadapter.core.Part> getParts(
			Operation operation, boolean output, Map parts) {

		ArrayList<com.jawise.serviceadapter.core.Part> msgparts = new ArrayList<com.jawise.serviceadapter.core.Part>();
		Iterator iter = parts.keySet().iterator();
		while (iter.hasNext()) {
			// Get each part
			Part part = (Part) parts.get(iter.next());
			// Add content for each message part
			String partName = part.getName();

			if (partName != null) {
				// Determine if this message part's type is complex
				XMLType xmlType = importer.getXMLType(part);

				if (xmlType != null && xmlType.isComplexType()) {
					// Build the message structure
					String path = ""; // part.getElementName().getLocalPart();
					buildComplexPart((ComplexType) xmlType, path, msgparts);
				} else {
					// Build the element that will be added to the message
					com.jawise.serviceadapter.core.Part p = new com.jawise.serviceadapter.core.Part();
					p.setName(partName);
					p.setOptional(false);
					if (operation.getStyle().toString().equalsIgnoreCase("rpc")) {
						p.setType(getType(part));
					} else {
						p.setType(getType(part));
					}
					handleArrayTypes(p);
					msgparts.add(p);
				}
			}
		}

		if (output && msgparts.size() > 0) {
			// only one message part is allowed in the response
			com.jawise.serviceadapter.core.Part part = msgparts.get(0);
			msgparts.clear();
			String name = part.getName();
			if (name.indexOf("/") >= 0) {
				name = name.substring(0, name.indexOf("/"));
				part.setName(name);
				part.getType().setName("struct");
			}
			msgparts.add(part);

		}
		return msgparts;
	}

	private com.jawise.serviceadapter.core.Type getType(Part part) {
		com.jawise.serviceadapter.core.Type type = new com.jawise.serviceadapter.core.Type();
		if (part.getTypeName() != null) {
			type.setName(getTypeName(part.getTypeName().getLocalPart()));
		} else {
			type.setName(getTypeName("string"));
		}
		return type;
	}

	private void handleArrayTypes(com.jawise.serviceadapter.core.Part p) {
		int i = p.getName().indexOf("ArrayOf");
		if (i >= 0) {
			String newname = p.getName().substring(i + 7);
			p.setName(newname + "[]");
		}

	}

	private void buildComplexPart(ComplexType complexType, final String path,
			ArrayList<com.jawise.serviceadapter.core.Part> msgparts) {

		Enumeration particleEnum = complexType.enumerate();
		Group group = null;

		while (particleEnum.hasMoreElements()) {
			Particle particle = (Particle) particleEnum.nextElement();

			if (particle instanceof Group) {
				group = (Group) particle;
				break;
			}
		}

		if (group != null) {
			Enumeration groupEnum = group.enumerate();

			while (groupEnum.hasMoreElements()) {
				Structure item = (Structure) groupEnum.nextElement();

				if (item.getStructureType() == Structure.ELEMENT) {
					ElementDecl elementDecl = (ElementDecl) item;
					XMLType xmlType = elementDecl.getType();

					if (xmlType != null && xmlType.isComplexType()
							&& !isArray(xmlType.getName())
							&& !isSoapType(xmlType.getName())) {
						// complex type so build path
						String thispath = xmlType.getName();

						if (thispath == null || thispath.isEmpty()) {
							thispath = elementDecl.getName();
						}

						if (path != null && !path.isEmpty()) {
							thispath = path + "/" + thispath;
						}
						// for complex array break out
						if (isArray(thispath)) {
							String partname = getTypeNameFromArrayName(thispath);
							if (path != null && !path.isEmpty()) {
								thispath = path + "/" + partname + "[]";
							}
							addPart(msgparts, thispath, xmlType);
							continue;
						}

						buildComplexPart((ComplexType) xmlType, thispath,
								msgparts);
					} else {

						String thispath = "";
						if (path == null || path.isEmpty())
							thispath = elementDecl.getName();
						else
							thispath = path + "/" + elementDecl.getName();
						if(isArray(xmlType.getName())) {
							thispath += "[]";
						}
						addPart(msgparts, thispath, xmlType);
					}

				} else if (item.getStructureType() == Structure.WILDCARD) {
					String thispath = "ANY";
					if (path != null && path.isEmpty())
						thispath = path;
					addPart(msgparts, thispath, "XML");
				}
			}
		}
	}

	public String getTypeNameFromArrayName(String type) {
		int pos = type.lastIndexOf("/");
		String latpart = type.substring(pos + 1);
		if (latpart.indexOf("ArrayOf") == 0) {
			String partname = latpart.substring(7);
			return partname;
		} else {
			return type;
		}
	}

	public boolean isArray(String type) {
		if(type == null)
			return false;
		
		int pos = type.lastIndexOf("/");
		String latpart = type.substring(pos + 1);
		if (latpart.indexOf("ArrayOf") == 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isSoapType(String name) {
		
		if(name == null)
			return false;
		
		int pos = name.lastIndexOf("/");
		String latpart = name.substring(pos + 1);
		if (latpart.indexOf("ArrayOf") == 0) {
			String partname = latpart.substring(7);
			WsdlTypes types = new WsdlTypes();
			if (types.getType(partname) != null) {
				;
				return true;
			}
		}
		return false;
	}

	private void addPart(
			ArrayList<com.jawise.serviceadapter.core.Part> msgparts,
			String partname, String typename) {
		com.jawise.serviceadapter.core.Part p = new com.jawise.serviceadapter.core.Part();
		p.setName(partname);
		p.setOptional(false);
		com.jawise.serviceadapter.core.Type type = new com.jawise.serviceadapter.core.Type();
		
		type.setName(getTypeName(typename));
		p.setType(type);
		handleArrayTypes(p);
		msgparts.add(p);

	}

	private void addPart(
			ArrayList<com.jawise.serviceadapter.core.Part> msgparts,
			String partname, XMLType xmlType) {
		addPart(msgparts, partname, xmlType.getName());
	}

	private String getTypeName(String soaptype) {

		String type = soaptype;
		if (soaptype.indexOf("xsd:") >= 0) {
			type = soaptype.substring(soaptype.indexOf("xsd:"));
		}

		String value = "";

		type = type == null ? "" : type;
		value = value == null ? "" : value;

		if(isArray(type)) {
			type =getTypeNameFromArrayName(type);
		}
		
		WsdlTypes types = new WsdlTypes();
		String thistype = types.getType(type);
		if (thistype == null || thistype.isEmpty()) {
			thistype = "struct";
		}
		logger.info(soaptype + " is mapped to " + thistype);
		return thistype;
	}

	public String getWsdllocation() {
		return wsdllocation;
	}

	public void setWsdllocation(String wsdllocation) {
		this.wsdllocation = wsdllocation;
	}

	public int getScreenstate() {
		return screenstate;
	}

	public void setScreenstate(int screenstate) {
		this.screenstate = screenstate;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public String getServicelocation() {
		return servicelocation;
	}

	public void setServicelocation(String servicelocation) {
		this.servicelocation = servicelocation;
	}

	public String getBinding() {
		return binding;
	}

	public void setBinding(String binding) {
		this.binding = binding;
	}

	public SelectItem[] getPorts() {
		return ports;
	}

	public void setPorts(SelectItem[] ports) {
		this.ports = ports;
	}

	public SelectItem[] getOperations() {
		return operations;
	}

	public void setOperations(SelectItem[] operations) {
		this.operations = operations;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public SelectItem[] getServices() {
		return services;
	}

	public void setServices(SelectItem[] services) {
		this.services = services;
	}

	public String getServicenametouse() {
		return servicenametouse;
	}

	public void setServicenametouse(String servicenametouse) {
		this.servicenametouse = servicenametouse;
	}

}
