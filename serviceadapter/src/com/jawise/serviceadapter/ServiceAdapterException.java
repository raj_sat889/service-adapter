package com.jawise.serviceadapter;

public class ServiceAdapterException  extends Exception {

	private static final long serialVersionUID = 1L;

	public ServiceAdapterException() {
		super();
	}

	public ServiceAdapterException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ServiceAdapterException(String arg0) {
		super(arg0);
	}

	public ServiceAdapterException(Throwable arg0) {
		super(arg0);
	}

}
