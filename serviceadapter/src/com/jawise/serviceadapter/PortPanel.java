package com.jawise.serviceadapter;

import javax.faces.event.ActionEvent;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Port;

public class PortPanel extends Panel {
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(MessagePartsPanel.class);

	@Override
	public void startEdit(ActionEvent event) {

		backup(service.getPort());
		super.startEdit(event);
	}

	@Override
	public void save(ActionEvent event) throws Exception {
		Port p = service.getPort();
		ValidationErrors errors = p.validate();
		if (errors.isEmpty()) {
			ServiceRegistry.getInstance().save();
			super.save(event);

			DefaultMutableTreeNode selectednode = serviceRegistryBrowser
					.getSelectednode();
			BrowserUserObject userObject = (BrowserUserObject) selectednode
					.getUserObject();
			if(!userObject.getText().equals(p.getName())) {
				userObject.setText(p.getName());
				userObject.setServicename(service.getName());
			}			

		} else {
			error("portForm", errors);
		}
	}

	@Override
	public void cancelEdit(ActionEvent event) {
		restore(service.getPort());
		super.cancelEdit(event);
	}

}
