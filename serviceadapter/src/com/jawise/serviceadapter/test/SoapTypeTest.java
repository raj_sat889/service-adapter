package com.jawise.serviceadapter.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Map;

import javax.xml.stream.XMLStreamWriter;

import junit.framework.TestCase;

import com.jawise.serviceadapter.convert.MessageContext;
import com.jawise.serviceadapter.convert.soap.binding.MessageElementReader;
import com.jawise.serviceadapter.convert.soap.binding.MessageElementWriter;
import com.jawise.serviceadapter.convert.soap.binding.SoapException;
import com.jawise.serviceadapter.convert.soap.binding.SoapXmlUtil;
import com.jawise.serviceadapter.convert.soap.encoding.SoapTypeMapping;
import com.jawise.serviceadapter.convert.soap.encoding.SoapTypeMappingRegistry;
import com.jawise.serviceadapter.convert.soap.type.SoapType;


public class SoapTypeTest extends TestCase {

	public void testTypeRegistryWithArray() throws SoapException {
		SoapTypeMappingRegistry reg = new SoapTypeMappingRegistry();
	}

	public void testStringArrayType() throws Exception {
		SoapTypeMappingRegistry reg = new SoapTypeMappingRegistry();
		SoapTypeMapping stm = reg.getMappings().get("default");
		SoapType soapType = stm.getSoapType(String[].class);
		String xml = new String("<return xmlns:xsi='http://www.w3.org/1999/XMLSchema-instance' xmlns:ns2='http://schemas.xmlsoap.org/soap/encoding/' xsi:type='ns2:Array' ns2:arrayType='xsd:string[2]'><item xsi:type='xsd:string'>Bill Posters</item><item xsi:type='xsd:string'>+1-212-7370194</item></return>");
		
		
		
        // Test reading
		MessageElementReader reader = new MessageElementReader(new ByteArrayInputStream(xml.getBytes()));
        
        String[] strings = (String[]) soapType.read(reader);
        assertEquals(2, strings.length);
        assertEquals("Bill Posters", strings[0]);
        assertEquals("+1-212-7370194", strings[1]);
        reader.getXMLStreamReader().close();
        
        // Test writing
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XMLStreamWriter xswr = SoapXmlUtil.createXMLStreamWriter(bos, "utf-8", new MessageContext());
        MessageElementWriter w = new MessageElementWriter(xswr);
        MessageElementWriter r = w.getElementWriter("return");
        soapType.write(strings, r);
        xswr.close();
        System.out.println(bos.toString());
        assertTrue(bos.toString().indexOf("Bill Posters") != -1);
        assertTrue(bos.toString().indexOf("+1-212-7370194") != -1);                		
	}
	
	
	public void testStructArray() throws Exception {
		SoapTypeMappingRegistry reg = new SoapTypeMappingRegistry();
		SoapTypeMapping stm = reg.getMappings().get("default");
		SoapType soapType = stm.getSoapType(Map[].class);
		
		String bookxml =  "<Books><Book><author>M K Low</author><preface>War and Peace</preface><intro>This is a book.</intro><stock>19</stock></Book><Book><author>Henry Ford</author><preface>Prefatory text</preface><intro>This is a book.</intro><stock>79</stock></Book></Books>";		
		MessageElementReader reader = new MessageElementReader(new ByteArrayInputStream(bookxml.getBytes()));
		Map[] books = (Map[]) soapType.read(reader);
		
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XMLStreamWriter xswr = SoapXmlUtil.createXMLStreamWriter(bos, "utf-8", new MessageContext());
        MessageElementWriter w = new MessageElementWriter(xswr);
        MessageElementWriter r = w.getElementWriter("return");
        soapType.write(books, r);
        xswr.close();
        System.out.println(bos.toString());		
	}
	
	public void testStructType() throws Exception {
		SoapTypeMappingRegistry reg = new SoapTypeMappingRegistry();
		SoapTypeMapping stm = reg.getMappings().get("default");
		SoapType soapType = stm.getSoapType(Map.class);
		//String xml = new String("<return xmlns:xsi='http://www.w3.org/1999/XMLSchema-instance' xmlns:ns2='http://schemas.xmlsoap.org/soap/encoding/' xsi:type='ns2:Array' ns2:arrayType='xsd:string[2]'><item xsi:type='xsd:string'>Bill Posters</item><item xsi:type='xsd:string'>+1-212-7370194</item></return>");
		String bookxml =  "<e:Book xmlns:e='http://wwww.books.com' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:ns2='http://schemas.xmlsoap.org/soap/encoding/'><author xsi:type='xsd:string'>Henry Ford</author><preface xsi:type='xsd:string'>Prefatory text</preface><intro xsi:type='xsd:string'>This is a book.</intro><stock xsi:type='xsd:int'>79</stock></e:Book>";		
		MessageElementReader reader = new MessageElementReader(new ByteArrayInputStream(bookxml.getBytes()));
		Map book = (Map) soapType.read(reader);
		
		assertEquals("Henry Ford", book.get("author"));
		assertEquals("Prefatory text", book.get("preface"));
		assertEquals("This is a book.", book.get("intro"));
		assertEquals(79, book.get("stock"));
		
				
		bookxml =  "<Book ><author>Henry Ford</author><preface>Prefatory text</preface><intro>This is a book.</intro><stock>79</stock></Book>";		
		reader = new MessageElementReader(new ByteArrayInputStream(bookxml.getBytes()));
		book = (Map) soapType.read(reader);		
		
		assertEquals("Henry Ford", book.get("author"));
		assertEquals("Prefatory text", book.get("preface"));
		assertEquals("This is a book.", book.get("intro"));
		assertEquals("79", book.get("stock"));		
		
		bookxml = "<e:Book xmlns:e='http://wwww.books.com' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:ns2='http://schemas.xmlsoap.org/soap/encoding/'><title>My Life and Work</title><author xsi:type='Aurther'><name>Henry Ford</name><address xsi:type='Address'><email>mailto:henryford@hotmail.com</email><web>www.henryford.com</web></address></author></e:Book>";
		
		reader = new MessageElementReader(new ByteArrayInputStream(bookxml.getBytes()));
		book = (Map) soapType.read(reader);			
		assertTrue(book.get("author") != null);
		
		
		String xml = "<return xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:ns1='http://schemas.xmlsoap.org/soap/encoding/' xsi:type='ns1:EmployeeContactDetail'><employeeName xsi:type='xsd:string'>Bill Posters</employeeName><phoneNumber xsi:type='xsd:string'>+1-212-7370194</phoneNumber><tempPhoneNumber xmlns:ns2='http://schemas.xmlsoap.org/soap/encoding/' xsi:type='ns2:Array' ns2:arrayType='ns1:TemporaryPhoneNumber[3]'><item xsi:type='ns1:TemporaryPhoneNumber'><startDate xsi:type='xsd:int'>37060</startDate><endDate xsi:type='xsd:int'>37064</endDate><phoneNumber xsi:type='xsd:string'>+1-515-2887505</phoneNumber></item><item xsi:type='ns1:TemporaryPhoneNumber'><startDate xsi:type='xsd:int'>37074</startDate><endDate xsi:type='xsd:int'>37078</endDate><phoneNumber xsi:type='xsd:string'>+1-516-2890033</phoneNumber></item><item xsi:type='ns1:TemporaryPhoneNumber'><startDate xsi:type='xsd:int'>37088</startDate><endDate xsi:type='xsd:int'>37092</endDate><phoneNumber xsi:type='xsd:string'>+1-212-7376609</phoneNumber></item></tempPhoneNumber></return>";
		reader = new MessageElementReader(new ByteArrayInputStream(xml.getBytes()));
		Map ec = (Map) soapType.read(reader);			
		assertTrue(ec.get("tempPhoneNumber") != null);
		assertTrue(ec.get("tempPhoneNumber") instanceof Map[]);
		
        // Test writing
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XMLStreamWriter xswr = SoapXmlUtil.createXMLStreamWriter(bos, "utf-8", new MessageContext());
        MessageElementWriter w = new MessageElementWriter(xswr);
        MessageElementWriter r = w.getElementWriter("return");
        soapType.write(ec, r);
        xswr.close();
        System.out.println(bos.toString());
        
	}
}
