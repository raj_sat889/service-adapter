package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.Map;

import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.MessageConversion;

public class ConversionPanel extends Panel {
	private static Logger logger = Logger.getLogger(ConversionPanel.class);
	
	private MessageConversion conversion;

	public MessageConversion getConversion() {
		return conversion;
	}

	public void setConversion(MessageConversion conversion) {
		this.conversion = conversion;
	}

	public void remove(ActionEvent event) {

		popup.setTitle("Remove Message Conversion - " + conversion.getName());
		popup.setMessage("Do you want to remove the messages conversion ?");
		popup.setRendered(true);

	}

	@Override
	public String yes() throws IOException {
		removeMessageConversion();
		super.yes();
		return null;
	}

	private void removeMessageConversion() {
		try {
			Map<String, MessageConversion> messageConversions = service
					.getAdapter().getMessageConversions();
			messageConversions.remove(conversion.getName());
			ServiceRegistry.getInstance().save();
			conversion = null;
			Map<String, Object> sessionMap = getSessionMap();
			Map<String, Object> requestMap = getRequestMap();
			requestMap.remove("conversion");
			sessionMap.remove("conversion");
			serviceRegistryBrowser.setSelectedPanel("emptyPanel");
			serviceRegistryBrowser.getSelectednode().removeFromParent();

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
