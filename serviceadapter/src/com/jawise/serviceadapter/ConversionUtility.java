package com.jawise.serviceadapter;

import java.util.HashMap;

public class ConversionUtility {

	HashMap<String, String> lookuptable = new HashMap<String, String>();
	
	public void addLookupEntry(String key, String value) {
		lookuptable.put(key, value);
	}
	
	public String lookup(String key) {
		return lookuptable.get(key);
	}

	public String getFirstName(String fullname) {
		return fullname;
	}

	public String getLastName(String fullname) {
		return fullname;
	}
	
	public String getRandomAlpha(int size) {
		return "sdfasasdsx";
	}
	
	public String getStreet(String address) {
		return address;
	}
	
	public String getCity(String address) {
		return "Brisbane";
	}	
	
	public String getState(String address) {
		return "QLD";
	}	
}
