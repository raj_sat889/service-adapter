package com.jawise.serviceadapter;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jawise.serviceadapter.core.Operation;

public class OperationsPanel extends Panel {
	private static Logger logger = Logger.getLogger(OperationsPanel.class);
	private Operation operation;

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public String saveOperation() throws IOException {
		logger.debug("saving operation");

		ValidationErrors errors = operation.validate();
		if (errors.size() != 0) {
			error("apForm", errors);
			return "";
		}
		operation.initMessages();
		service.getPort().getBinding().getPortyype().getOperations().add(
				operation);
		ServiceRegistry.getInstance().save();
		serviceRegistryBrowser.addOperation(service, serviceRegistryBrowser
				.getSelectednode(), operation);

		serviceRegistryBrowser.setSelectedPanel("emptyPanel");
		Map<String, Object> requestMap = getRequestMap();
		requestMap.remove("newoperation");
		operation = new Operation();		
		return "";
	}
}
