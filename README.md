### What is this repository for? ###

![sa_screen2.jpg](https://bitbucket.org/repo/benEbe/images/3199976936-sa_screen2.jpg)

Service adapter is an integration platform for simplifying the integration of web services between service providers and consumers.
The use of web services has gathered greater momentum over the years, with this the complexities of integration of different technologies have also increased. Web services integration with third party service providers are achieved by developing the client software to understand the service business protocols (i.e. interfaces ) and wire protocols (i.e. SOAP, XML RPC). These service providers expose different types of business protocols (i.e. interfaces ) and wire protocols (i.e. SOAP, XML RPC). So when it comes to changing service providers, clients have to go through same development process again to achieve the integration with new service providers. 

The service adapter is an integration software for adapting web services from one provider to web services from another provider. With the Service Adapter it become much easier to adapt both business protocol and wire protocol from one service to another.

There are two reasons for adaptation of a service -:

-replacement of service provider or service 
-provision a compatible service
 
In the first case, a service consumer wants to switch to different service provider. Lets say the retail company which is using a fraud screening services from company XYZ want's to switch to a new service provider ABC. The ABC and XYZ used different business protocols and wire protocols.

**Replacement Of Service**   

In the second case, major low cost air line wants to integrate with new credit card payment service provider (PSP), they want to do it fast and want the PSP  to provide a compatible business and wire protocol to the one they are currently using. 

**Provision Of Compatible Service** 

In both of these cases, in normal circumstances the software owners have to modify their software to achieve the required integration. But by using the service adapter software they will be able to create an adapter to do the job.

The adaptation process involves the following steps -: 
 
-Defining the services using Service Adapter browser
-Defining adapter and the mappings between the services
-Using the service adapter URL with the service consumer

The service adapter comes with the service adapter browser which is a web application used in managing the web services and adapter definitions 
 
The features and benefits of using service adapter are :

-Less time to develop and deploy the solution
-Less code to maintain and trouble shoot
-Multiple adapters can be created and managed in on place
-Export and import adapters, so a service provider could provide an adapter for their service
-Supports web service protocol XML-RPC, XML/HTTP , NVP/ HTTP Post and SOAP1.1
-Build on Java ,Platform independent and scalable
-Support Java scripting and BeanShell scripting for the fine grained service mapping
 

The typical applications for service adapters - :

Provide backup service provider to fail over
Integrate clients quickly to their service's by service provider
 
See the service adapter user guide for more information.


### How do I get set up? ###

Download / check out the source code and look at the user guide in doc / serviceadapter_userguide.html

### Who do I talk to? ###

* Repo owner or admin