<%@ include file="includes.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.jawise.serviceadapter.test.car.*"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Car Rental Service Example</title>
</head>
<body>
<%
	String err = "";
	String action = request.getParameter("action");
	String location = request.getParameter("location");
	if (location == null || location.isEmpty()) {
		location = "London";
	}
	
	String collectiondate = request.getParameter("collectiondate");
	
	if(collectiondate ==null)
		collectiondate = "01/01/09";
		
		
	String returndate = request.getParameter("returndate");
	
	if(returndate ==null)
		returndate = "02/01/09";
		
	String service = "";
	
	if ("find".equals(action)) {
		service = request.getParameter("service");
		String url = "";

		/* the url of the service is set her */
		if ("CarRentalServiceA".equals(service)) {
			url = "http://" + server + "/serviceadapter-testservices/soa/CarRentalServiceA";
		} else if ("CarRentalServiceB".equals(service)) {
			url = "http://" + server + "/serviceadapter/process/adapter?userid=raj&service=CarRentalServiceA&messageconversion=listTosearchConverter";
		}
		else if ("CarRentalServiceC".equals(service)) {
			url = "http://" + server + "/serviceadapter/process/adapter?userid=sathyan&service=CarRentalServiceA2&messageconversion=carrentallist";
		}		
		 else {
			throw new IllegalArgumentException(
					"service requested not supported :" + service);
		}
		try {

			CarRentalServiceALocator loc = new CarRentalServiceALocator();
			loc.setCarRentalServiceAHttpPortEndpointAddress(url);
			CarRentalServiceAPortType port = loc.getCarRentalServiceAHttpPort();
			Car[] listRentalCars = port.listRentalCars(location,collectiondate,returndate);
			request.setAttribute("cars",listRentalCars);
		} catch (Exception f) {
			err = f.getMessage();
			f.printStackTrace();
		}	

	}
%>

<center>Car Rental Service Example</center>

<br>
<br>
<br>
<center>
<table border=0 " style="width: 692px" align="center">
	<tr>
		<td>This example shows a client using the CarRentalServiceA web
		service ( <a
			href="http://<%=server%>/serviceadapter-testservices/soa/CarRentalServiceA?wsdl">wsdl</a>
		) being adapted for use with the the CarRentalServiceB which is a
		based NVP-HTTP(HTTP post) and CarRentalServiceC which is based on
		XML-HTTP via the service adapter. Respective service and their method
		signatures are show in the tables here. <br>
		<br>
		<table border="1" style="width: 692px">
			<tr>
				<td style="width: 200px">CarRentalServiceA</td>
				<td>CarRentalServiceB</td>
			</tr>
			<tr>
				<td><i>	public Car[] listRentalCars(String location, String collectiondate, String returndate)</i></td>
				<td><i> Sample HTTP POST <br>
				action=searchrentalcars&area=london&from=010910&to=020910</i></td>
			</tr>
		</table>
		<br>
		<table border="1" style="width: 692px">
			<tr>
				<td style="width: 200px">CarRentalServiceA</td>
				<td>CarRentalServiceC</td>
			</tr>
			<tr>
				<td><i>	public Car[] listRentalCars(String location, String collectiondate, String returndate)</i></td>
				<td><i> Sample XML HTTP request
				<br>
&lt;CarRentalService&gt;
    &lt;GetCars&gt;
        &lt;Area&gt;
            &lt;Country&gt;Lodnon&lt;/Country&gt;
            &lt;PostCode&gt;Lodnon&lt;/PostCode&gt;
        &lt;/Area&gt;
        &lt;Model/&gt;
        &lt;TransmisionType&gt;ALL&lt;/TransmisionType&gt;
    &lt;/GetCars&gt;
&lt;/CarRentalService&gt;							
				</i></td>
			</tr>
		</table>		
		</td>
	</tr>
</table>
</center>

<center><br>
<br>
<table border="1" bordercolor="gray" style="width: 692px" align="center">
	<tr>
		<td align="center">

		<form action="carrentalsservice.jsp"><font color="red"
			face="Vardana"> <%=err%> </font> <br>

		<%
			boolean carrentalservicea = "CarRentalServiceA".equals(service);
			boolean carrentalserviceb = "CarRentalServiceB".equals(service);
			boolean carrentalservicec = "CarRentalServiceC".equals(service);
		%>
		<table>
			<tr>
				<td>Service To Use:</td>
				<td><select name="service">
					<option id="CarRentalServiceA" value="CarRentalServiceA"<%=carrentalservicea ? "selected" : ""%>">CarRentalServiceA</option>
					<option id="CarRentalServiceB" value="CarRentalServiceB"<%=carrentalserviceb ? "selected" : ""%>">CarRentalServiceB (Service Adapter)</option>
					<option id="CarRentalServiceC" value="CarRentalServiceC"<%=carrentalservicec ? "selected" : ""%>">CarRentalServiceC (Service Adapter)</option>
				</select></td>
			</tr>
			<tr>
				<td>Location:</td>
				<td><input name='location' value=<%=location%> style="width: 169px" /></td>
			</tr>
			
			<tr>
				<td>CollectionDate:</td>
				<td><input name='collectiondate' value=<%=collectiondate%> style="width: 169px" /></td>
			</tr>
						
			<tr>
				<td>ReturnDate:</td>
				<td><input name='returndate' value=<%=returndate%> style="width: 169px" /></td>
			</tr>						
			<tr>
				<td><input type="hidden" name="action" value="find" /></td>
				<td><input type="submit" value="List Cars"></td>
			</tr>
		</table>
		</form>
		</td>
	</tr>
</table>
</center>

<br>
<br>
<br>
<%
	Car[] listRentalCars = (Car[]) request.getAttribute("cars");
	if (listRentalCars != null) {
%>
<center>
<%
	for(int i = 0;i <listRentalCars.length; i++) {
%>

<table border="1" bordercolor="gray" align="center" style="width: 692px">
	<tr>
		<td style="width: 142px">Make</td>
		<td><%=listRentalCars[i].getMake()%></td>
	</tr>
	<tr>
		<td>Model</td>
		<td><%=listRentalCars[i].getModel()%></td>
	</tr>
	<tr>
		<td>Color</td>
		<td><%=listRentalCars[i].getColor()%></td>
	</tr>
	<tr>
		<td>Year</td>
		<td><%=listRentalCars[i].getYear()%></td>
	</tr>	
	<tr>
		<td>Rate</td>
		<td><%=listRentalCars[i].getRate()%></td>
	</tr>	
	</td>
	</tr>
</table>
<br>
</center>
<%
		}
	}
%>
</body>
</html>