<%@ include file="includes.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.jawise.serviceadapter.test.svc.soap.bookshop.*"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BookShopServiceExample</title>
</head>
<body>
<%
	String err = "";
	String action = request.getParameter("action");
	String isbn = request.getParameter("isbn");
	if(isbn ==  null || isbn.isEmpty()) {
		isbn = "1234-5678-6645-1";
	}
	String service = "";
	if ("find".equals(action)) {
		service = request.getParameter("service");
		String url = "";
		
		/* the url of the service is set her */
		if ("BookShopService".equals(service)) {
			url = "http://" + server + "/serviceadapter-testservices/soa/BookShopService";
		} else if ("LibraryService".equals(service)) {
			url = "http://" + server + "/serviceadapter/process/adapter?userid=raj&service=BookShopService&messageconversion=bookfinderConversion";
		} else {
			throw new IllegalArgumentException(
					"service requested not supported :" + service);
		}
		try {
			BookShopServiceClient c = new BookShopServiceClient();
			BookShopServicePortType bookShopServicePortTypeLocalEndpoint = c
					.getBookShopServiceHttpPort(url);			
			Book book = bookShopServicePortTypeLocalEndpoint
					.findBook(isbn);
			request.setAttribute("book", book);
		} catch (BookFault f) {
			err = f.getMessage();
		} catch (Exception f) {
			err = f.getMessage();
		}


	}
%>

<center>Book Shop Service Example</center>

<br>
<br>
<br>
<center>
<table border=0" style="width: 692px" align="center">
<tr>
<td>
This example shows a client using the Book Shop web service (
<a href="http://<%=server%>/serviceadapter-testservices/soa">wsdl</a>
) being adapted for use with the the LibraryService (
<a
	href="http://<%=server%>f/serviceadapter-testservices/soa/LibraryService?wsdl">wsdl</a>
) via the service adapter. Respective service and their method
signatures are show in the table here.
<br><br>
<table border="1" style="width: 692px">
	<tr>
		<td>BookShopService</td>
		<td>LibraryService</td>
	</tr>
	<tr>
		<td><i>public Book findBook(String isbn)</i></td>
		<td><i>public String getBookInfo(String isbn, String tittle,
		String author)</i></td>
	</tr>
</table>
</td>
</tr>
</table>
</center>

<center><br>
<br>
<table border="1" bordercolor="gray"  style="width: 692px" align="center">
	<tr><td align="center">
			
		<form action="bookshopservice.jsp"><font color="red"
			face="Vardana"> <%=err%> </font> <br>
		
		<%
		 boolean booksvcSelected = "BookShopService".equals(service);
		 boolean libsvcSelected = "LibraryService".equals(service);
		
		%>
		<table>
			<tr>
				<td>Service To Use:</td>
				<td><select name="service">
					<option id="BookShopService" value="BookShopService" <%=booksvcSelected ? "selected" : ""%>">BookShopService</option>
					<option id="LibraryService" value="LibraryService" <%=libsvcSelected ? "selected" : ""%>">LibraryService Using Adapter</option>
				</select></td>
			</tr>
			<tr>
				<td>ISBN No:</td>
				<td><input name='isbn' value=<%=isbn%>
					style="width: 169px" /></td>
			</tr>
			<tr>
				<td><input type="hidden" name="action" value="find" /></td>
				<td><input type="submit" value="Find Book"></td>
			</tr>
		</table>
		</form>
</td>
</tr>
</table>		
</center>

<br>
<br>
<br>
<%
	Book book = (Book) request.getAttribute("book");
	if (book != null) {
%>
<center>
<table border="1" bordercolor="gray"  style="width: 692px" align="center">
	<tr>
		<td>Isbn</td>
		<td><%=book.getIsbn().getValue()%></td>
	</tr>
	<tr>
		<td>Author</td>
		<td><%=book.getAuthor().getValue()%></td>
	</tr>
	<tr>
		<td>Title</td>
		<td><%=book.getTitle().getValue()%></td>
	</tr>
	</td>
	</tr>
</table>

</center>
<%
	}
%>
</body>
</html>