package com.jawise.serviceadapter.test.car;

public class CarRentalServiceAPortTypeProxy implements com.jawise.serviceadapter.test.car.CarRentalServiceAPortType {
  private String _endpoint = null;
  private com.jawise.serviceadapter.test.car.CarRentalServiceAPortType carRentalServiceAPortType = null;
  
  public CarRentalServiceAPortTypeProxy() {
    _initCarRentalServiceAPortTypeProxy();
  }
  
  public CarRentalServiceAPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initCarRentalServiceAPortTypeProxy();
  }
  
  private void _initCarRentalServiceAPortTypeProxy() {
    try {
      carRentalServiceAPortType = (new com.jawise.serviceadapter.test.car.CarRentalServiceALocator()).getCarRentalServiceAHttpPort();
      if (carRentalServiceAPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)carRentalServiceAPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)carRentalServiceAPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (carRentalServiceAPortType != null)
      ((javax.xml.rpc.Stub)carRentalServiceAPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.jawise.serviceadapter.test.car.CarRentalServiceAPortType getCarRentalServiceAPortType() {
    if (carRentalServiceAPortType == null)
      _initCarRentalServiceAPortTypeProxy();
    return carRentalServiceAPortType;
  }
  
  public com.jawise.serviceadapter.test.car.Car[] listRentalCars(java.lang.String location, java.lang.String collectiondate, java.lang.String returndate) throws java.rmi.RemoteException{
    if (carRentalServiceAPortType == null)
      _initCarRentalServiceAPortTypeProxy();
    return carRentalServiceAPortType.listRentalCars(location, collectiondate, returndate);
  }
  
  
}