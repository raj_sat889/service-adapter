/**
 * CarRentalServiceAPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jawise.serviceadapter.test.car;

public interface CarRentalServiceAPortType extends java.rmi.Remote {
    public com.jawise.serviceadapter.test.car.Car[] listRentalCars(java.lang.String location, java.lang.String collectiondate, java.lang.String returndate) throws java.rmi.RemoteException;
}
