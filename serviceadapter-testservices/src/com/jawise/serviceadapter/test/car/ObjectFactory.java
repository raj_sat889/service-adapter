
package com.jawise.serviceadapter.test.car;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jawise.serviceadapter.test.svc.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CarYear_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "year");
    private final static QName _CarMake_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "make");
    private final static QName _CarModel_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "model");
    private final static QName _CarRate_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "rate");
    private final static QName _CarColor_QNAME = new QName("http://soap.svc.test.serviceadapter.jawise.com", "color");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jawise.serviceadapter.test.svc.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Car }
     * 
     */
    public Car createCar() {
        return new Car();
    }

    /**
     * Create an instance of {@link ListRentalCarsResponse }
     * 
     */
    public ListRentalCarsResponse createListRentalCarsResponse() {
        return new ListRentalCarsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCar }
     * 
     */
    public ArrayOfCar createArrayOfCar() {
        return new ArrayOfCar();
    }

    /**
     * Create an instance of {@link ListRentalCars }
     * 
     */
    public ListRentalCars createListRentalCars() {
        return new ListRentalCars();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "year", scope = Car.class)
    public JAXBElement<String> createCarYear(String value) {
        return new JAXBElement<String>(_CarYear_QNAME, String.class, Car.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "make", scope = Car.class)
    public JAXBElement<String> createCarMake(String value) {
        return new JAXBElement<String>(_CarMake_QNAME, String.class, Car.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "model", scope = Car.class)
    public JAXBElement<String> createCarModel(String value) {
        return new JAXBElement<String>(_CarModel_QNAME, String.class, Car.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "rate", scope = Car.class)
    public JAXBElement<String> createCarRate(String value) {
        return new JAXBElement<String>(_CarRate_QNAME, String.class, Car.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.svc.test.serviceadapter.jawise.com", name = "color", scope = Car.class)
    public JAXBElement<String> createCarColor(String value) {
        return new JAXBElement<String>(_CarColor_QNAME, String.class, Car.class, value);
    }

}
