/**
 * CarRentalServiceA.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jawise.serviceadapter.test.car;

public interface CarRentalServiceA extends javax.xml.rpc.Service {
    public java.lang.String getCarRentalServiceAHttpPortAddress();

    public com.jawise.serviceadapter.test.car.CarRentalServiceAPortType getCarRentalServiceAHttpPort() throws javax.xml.rpc.ServiceException;

    public com.jawise.serviceadapter.test.car.CarRentalServiceAPortType getCarRentalServiceAHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
