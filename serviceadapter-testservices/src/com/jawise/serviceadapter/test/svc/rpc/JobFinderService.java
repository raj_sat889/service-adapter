package com.jawise.serviceadapter.test.svc.rpc;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class JobFinderService {

	private static final Logger logger = Logger
			.getLogger(JobFinderService.class);

	public String checkHeartBeat(String req) {
		logger.debug("procesing request");
		return "call success";
	}

	@SuppressWarnings("unchecked")
	public Map[] findJobs(String id, String password, String roles,
			String skills, Object[] locations) throws Exception {

		if ("raj".equals(id)) {
			return findJobs(roles, skills, locations);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private Map[] findJobs(String roles, String skills, Object[] locations)  throws Exception {

		try {			
			Map[] jobs = new HashMap[2];
			logger.info("searching for roles : " + roles);
			logger.info("searching for skills : " + roles);
			for (int i = 0; i < locations.length; i++) {
				logger.info("searching for locations : " + locations[i]);
				if(locations[i].equals("China"))
					throw new IllegalArgumentException("Jobs in China not avilable");
			}

			jobs[0] = new HashMap();
			jobs[1] = new HashMap();
			jobs[0].put("tittle", "Java Software Developer");
			jobs[0]
					.put(
							"jobdescription",
							"Java/J2EE Enterprise Applications. Strong Business facing role with vendor and third party co-ordination. Career prospects unlimited. Attractive $80k-90k Neg base Salary + Super + Benefits. Sydney CBD location. This dynamic and well known Australian company has a reputation for demanding IT roles and providing their staff with great career progression. Applications development is very much a decentralised team activity and as such, you get to work closely with the business units to really make");
			jobs[0].put("locale", "Brisbane");

			jobs[1].put("tittle", "Java/J2EE Senior Developer");
			jobs[1]
					.put(
							"jobdescription",
							"Java/J2EE Enterprise Applications. Strong Business facing role with vendor and third party co-ordination. Career prospects unlimited. Attractive $80k-90k Neg base Salary + Super + Benefits. Sydney CBD location. This dynamic and well known Australian company has a reputation for demanding IT roles and providing their staff with great career progression. Applications development is very much a decentralised team activity and as such, you get to work closely with the business units to really make");
			jobs[1].put("locale", "Sydney");
			
			return jobs;
		} catch (RuntimeException e) {
			logger.error(e.getMessage(),e);
			throw e;
		}
	}
}
