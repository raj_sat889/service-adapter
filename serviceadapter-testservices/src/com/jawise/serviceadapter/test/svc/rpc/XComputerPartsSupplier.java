package com.jawise.serviceadapter.test.svc.rpc;

import java.util.Map;

import org.apache.log4j.Logger;

public class XComputerPartsSupplier {
	private static final Logger logger = Logger
			.getLogger(XComputerPartsSupplier.class);

	public String checkHeartBeat(String req) {
		logger.debug("procesing request");
		return "call success";
	}
	
	@SuppressWarnings("unchecked")
	
	public boolean submitPurchaseOrder(String id , String password, Map po){
		
		if("xcomputerparts".equals(id)) {
			return processPurchaceOrder(po);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private boolean processPurchaceOrder(Map po) {
	    String poid = (String)po.get("poid");	    	    
	    String email = (String)po.get("email");
	    String orderdate = (String)po.get("orderdate");
	    String orderplacer = (String)po.get("orderplacer");
	    String totalprice = (String)po.get("totalprice");	    
	    
	    logger.debug("poid - "+ poid);
	    logger.debug("email - "+ email);
	    logger.debug("orderdate - "+ orderdate);
	    logger.debug("orderplacer - "+ orderplacer);
	    logger.debug("totalprice - "+ totalprice);	    
	    
	    Object[] oitems = (Object[])po.get("items");	    
	    logger.debug("item size - " +  oitems.length);
	    for(int i = 0; i< oitems.length;i++) {
	    	Map item = (Map)oitems[i];
	    	String id = (String)item.get("id");
	    	String details = (String)item.get("details");
	    	String quantity = (String)item.get("quantity");
	    	String amout = (String)item.get("amout");
	    	logger.debug("item id - "+ id);
	    	logger.debug("item details - "+ details);
	    	logger.debug("item quantity - "+ quantity);
	    	logger.debug("item amout - "+ amout);
	    }
		return true;
	}
	
	public double getValueAddedTax(double total, String country) {
		if(total <= 0) {
			throw new IllegalArgumentException("total value must be > 0");
		}
		if(country != null && country.length() > 2) {
			throw new IllegalArgumentException("country format invalid");
		}
		double tax = total * 0.10;
		
		if("TI".equals(country)) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
			}
		}
				
		return tax;
	}
}
