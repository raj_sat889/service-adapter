package com.jawise.serviceadapter.test.svc;


import javax.servlet.ServletException;

import org.apache.xmlrpc.webserver.ServletWebServer;
import org.apache.xmlrpc.webserver.XmlRpcServlet;


public class XmlRpcServer {
    private static final int port = 8090;
	private ServletWebServer webServer;

    public XmlRpcServer() throws ServletException {
        XmlRpcServlet servlet = new CustomXmlRpcServlet();
        webServer = new ServletWebServer(servlet, port);    	
    }
    
    public void start() throws Exception {
        webServer.start();
    }

}
