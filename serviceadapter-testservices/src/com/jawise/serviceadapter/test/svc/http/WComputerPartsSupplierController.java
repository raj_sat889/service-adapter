package com.jawise.serviceadapter.test.svc.http;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.jawise.serviceadapter.test.svc.GUID;


public class WComputerPartsSupplierController extends ServiceController {
	private static final Logger logger = Logger
			.getLogger(WComputerPartsSupplierController.class);

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		
		//logger.debug("request  " + readContent(req));
		String op = req.getParameter("operation");
		if("PurchaseOrder".equals(op)) {
			handlePurchaseOrder(req,res);
		}
		else if("vat".equals(op)) {
			getValueAddedTax(req,res);
		}		
		else {
			PrintWriter wr = res.getWriter();
			wr.write("Error=Invalid Request or Operation\r\n");					
		}
		return null;
		
	}

	public void getValueAddedTax(HttpServletRequest req, HttpServletResponse res) throws IOException {
		double total = Double.valueOf(req.getParameter("totalvalue"));
		@SuppressWarnings("unused")
		String country = (req.getParameter("country"));
		double tax = total * 0.10;
		
		PrintWriter wr = res.getWriter();
	    wr.write("taxvalue=" + Double.valueOf(tax).toString());
		wr.write("\r\n");
	}
	
	void handlePurchaseOrder(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter wr = res.getWriter();
	    String poid = (String)req.getParameter("orderid");
	    if(poid.length() > 50) {
	    	wr.write("Error=orderid length to big\r\n");
	    	return;
	    }
	    	    
	    
	    if(poid.length() == 0) {
	    	wr.write("Error=orderid is required\r\n");
	    	return;
	    }

	    
	    if("timeout".equals(poid)) {
	    	try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
	    }
	    String email = req.getParameter("emailaddress");
	    
	    if(email.length() > 200) {
	    	wr.write("Error=emailaddress length to big\r\n");
	    	return;
	    }	    
	    String orderdate = req.getParameter("dateoforder");
	    String contactname = req.getParameter("contactname");
	    
	    if(contactname.length() > 50) {
	    	wr.write("Error=contactname length to big\r\n");
	    	return;
	    }	    

    	logger.debug("orderid - "+ poid);
	    logger.debug("email address - "+ email);
	    logger.debug("dateoforder - "+ orderdate);
	    logger.debug("contactname - "+ contactname);
	    
	    
	    @SuppressWarnings("unused")
		String totalprice = req.getParameter("totalvalue");
	    String totalitems = req.getParameter("totalitems");
	    int itemsCount = 0;
	    if(totalitems != null && !totalitems.isEmpty()) {
	    	itemsCount = Integer.valueOf(totalitems);
	    }
	    for(int i = 0; i < itemsCount; i++){
	    	String desc = req.getParameter("itemdescription" + (i+1));
	    	String quantity = req.getParameter("itemquantity" + (i+1));
	    	String amount = req.getParameter("amount" + (i+1));	    	
	    	logger.debug("desc - "+ desc);
		    logger.debug("quantity - "+ quantity);
		    logger.debug("amount - "+ amount);
	    }	   
	    wr.write("OrderSubmissionID=" + new GUID().toString());
		wr.write("\r\n");		
		return;
	}
}