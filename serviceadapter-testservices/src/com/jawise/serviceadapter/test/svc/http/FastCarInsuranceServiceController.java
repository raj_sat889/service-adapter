package com.jawise.serviceadapter.test.svc.http;

import java.io.ByteArrayInputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


public class FastCarInsuranceServiceController extends ServiceController {
	private static final Logger logger = Logger
			.getLogger(FastCarInsuranceServiceController.class);

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		String msg = readContent(req);
		logger.debug("request  " + msg);
		
		PrintWriter out = res.getWriter();
		
		if(msg.trim().isEmpty()) {			
			out.write("<InsuranceTypes><Type><name>Home Insurance</name><description>standard home inusrnace</description></Type><Type><name>Motor Insurance</name><description>standard motor inusrnace</description></Type></InsuranceTypes>");
			return null;
		}
		DocumentBuilderFactory i = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = i.newDocumentBuilder();
		Document quoteReq = null;
		
		try {
			quoteReq = db.parse(new ByteArrayInputStream(msg
					.getBytes()));
		} catch (RuntimeException e1) {
			e1.printStackTrace();
			return null;
		}
		
		Node e = quoteReq.getElementsByTagName("Year").item(0);
		String syear = e.getFirstChild().getTextContent();		
		int year = Integer.valueOf(syear);
		
		
		e = quoteReq.getElementsByTagName("Make").item(0);
		String model = e.getFirstChild().getTextContent();
		if("TI".equals(model)) {
			Thread.sleep(20000);
		}
		
	
		
		if( year < 1970) {
			out.write("<QuoteResponse><QuoteAmount>0</QuoteAmount><Error>Car is far to old</Error></QuoteResponse>");
		}
		else {
			out.write("<QuoteResponse><QuoteAmount>344.34</QuoteAmount></QuoteResponse>");
		}
		
		

		
		return null;
	}

}

/*
<QuoteRequest>
<Make>BMW</Make>
<Year>1992</year>
<Value>23829</Value>
<Owner>Raj<Owner>
<Address></Address>
<Phone></Phone>
<Claims>
	  <Claim>
	  	<Date>02082003</Date>
	  	<Description>Card Right Off</Description>
	  	<Value>29000</Value>
	  </Claim>
	  
	  <Claim>
	  	<Date>01011993</Date>
	  	<Description>Window Broken by Storm</Description>
	  	<Value>3000</Value>
	  </Claim>		  
</Claims>
</QuoteRequest>
*/