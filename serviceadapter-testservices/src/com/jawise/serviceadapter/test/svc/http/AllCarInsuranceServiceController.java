package com.jawise.serviceadapter.test.svc.http;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;


public class AllCarInsuranceServiceController extends ServiceController {
	private static final Logger logger = Logger
			.getLogger(AllCarInsuranceServiceController.class);

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		String op = req.getParameter("operation");
		if("quote".equals(op)) {
			quoteCarInsurance(req,res);
			return null;
		}
		
		PrintWriter wr = res.getWriter();
	    wr.write("Error=operation not supported");
		wr.write("\r\n");
		
		return null;
	}

	private void quoteCarInsurance(HttpServletRequest req,
			HttpServletResponse res) throws IOException {
		
		String carmodel = req.getParameter("carmodel");
		String yearofmanufacuring = req.getParameter("yearofmanufacuring");
		String carvalue = req.getParameter("carvalue");
		String registerdOwner =  req.getParameter("registerdowner");
		String registerdAddress = req.getParameter("registerdaddress");
		String phone = req.getParameter("phone");
		String noofclaims = req.getParameter("noofclaims");
		String[] claimdetails = null;
		
		
		logger.debug("car :" + carmodel);
		logger.debug("yearofmanufacuring :" + yearofmanufacuring);
		logger.debug("carvalue :" + carvalue);
		logger.debug("registerdOwner :" + registerdOwner);
		logger.debug("registerdAddress :" + registerdAddress);
		
		if(noofclaims != null && !noofclaims.isEmpty()) {
			int c = Integer.valueOf(noofclaims);
			claimdetails = new String[c];
			for(int i=0; i < c; i++) {
				claimdetails[i] = req.getParameter("claimdetails"+c);
				logger.debug("claimdetails :" + claimdetails[i]);
			}
		}

		PrintWriter wr = res.getWriter();
	    wr.write("quote=456.34");
		wr.write("\r\n");
	}
}
