package com.jawise.serviceadapter.test.svc.http;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

public class CarRentalServiceBController extends ServiceController {

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		PrintWriter wr = res.getWriter();
		String op = req.getParameter("action");
		if("searchrentalcars".equals(op)) {
		
			wr.write("CarCount=2");
			wr.write("\r\n");
			wr.write("CarMake1=Mazda");
			wr.write("\r\n");
			wr.write("CarModel1=Mazda 3 Hatchback");
			wr.write("\r\n");						
			wr.write("CarRate1=99");
			wr.write("\r\n");			
			wr.write("CarMake2=Nissan");
			wr.write("\r\n");
			wr.write("CarModel2=Pulser Hatchback");
			wr.write("\r\n");						
			wr.write("CarRate2=79");
			wr.write("\r\n");			
			return null;
		}
		
		
	    wr.write("Error=operation not supported");
		wr.write("\r\n");
		
		return null;
	}

}
