package com.jawise.serviceadapter.test.svc.http;

import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;



public class CheapFraudCheckServiceController extends ServiceController {
	private static final Logger logger = Logger
			.getLogger(CheapFraudCheckServiceController.class);

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		String request = readContent(req);
		logger.debug("request  " + request); 			
		PrintWriter out = res.getWriter();		
		
		if(request.indexOf("492901") >= 0) {
			throw new NullPointerException("internal error, configuration missing");
		}
		
		if(request.indexOf("492988") >= 0) {
			out.write("request invalid, check the cardnumber");
			return null;			
		}
		
		if(request.indexOf("492902") >= 0) {
			Thread.sleep(10000);
		}
		if(request.indexOf("492903") >= 0) {
			out.write(URLEncoder.encode("countryMatch=Yes&countryCode=us&highRiskCountry=no&score=1&riskScore=69","UTF-8"));
			return null;
		}
		
		out.write("countryMatch=Yes&countryCode=us&highRiskCountry=no&score=1&riskScore=69");
		return null;
	}

}
