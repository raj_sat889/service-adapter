package com.jawise.serviceadapter.test.svc.http;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;



public class XmlFraudCheckServiceController extends ServiceController {
	private static final Logger logger = Logger
			.getLogger(CheapFraudCheckServiceController.class);

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		logger.debug("request  " + readContent(req));
		PrintWriter out = res.getWriter();
		out
				.write("<FraudCheckRes><countryMatch>Yes</countryMatch><countryCode>us</countryCode><highRiskCountry>no</highRiskCountry><score>1</score><riskScore>69</riskScore></FraudCheckRes>");
		
		
		
		return null;
	}

}
