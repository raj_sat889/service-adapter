package com.jawise.serviceadapter.test.svc.http;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

public class CarRentalServiceCContoller extends ServiceController{

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		
		StringBuffer buf = new StringBuffer();
		String reqmsg = readContent(req);
		logger.debug("request  " + reqmsg);

		
		if (reqmsg.indexOf("China") >= 0) {
			return null;
		} else if (reqmsg.indexOf("Africa") >= 0) {
			buf.append("<Cars></Cars>");
		} 
		else if (reqmsg.indexOf("UK") >= 0) {
			buf.append("<Cars>");
			
			buf.append("<Car>");
			buf.append("<Model>Mazda 3</Model>");		
			buf.append("<Location>UK</Location>");
			buf.append("<TransmisionType>AUTO</TransmisionType>");
			buf.append("<Color>RED</Color>");
			buf.append("<Rate>55 per day</Rate>");
			buf.append("</Car>");

			buf.append("<Car>");
			buf.append("<Model>Mazda 6</Model>");		
			buf.append("<Location>UK</Location>");
			buf.append("<TransmisionType>AUTO</TransmisionType>");
			buf.append("<Color>BLACK</Color>");
			buf.append("<Rate>55 per day</Rate>");
			buf.append("</Car>");

			buf.append("<Car>");
			buf.append("<Model>Nissan</Model>");		
			buf.append("<Location>UK</Location>");
			buf.append("<TransmisionType>AUTO</TransmisionType>");
			buf.append("<Color>BLUE</Color>");
			buf.append("<Rate>55 per day</Rate>");
			buf.append("</Car>");
			
			buf.append("</Cars>");			
		}
		else {
			buf.append("<Cars>");
			
			buf.append("<Car>");
			buf.append("<Model>Mazda 3</Model>");		
			buf.append("<Location>Sydney, NSW</Location>");
			buf.append("<TransmisionType>AUTO</TransmisionType>");
			buf.append("<Color>RED</Color>");
			buf.append("<Rate>55 per day</Rate>");
			buf.append("</Car>");

			buf.append("<Car>");
			buf.append("<Model>Mazda 6</Model>");		
			buf.append("<Location>Sydney, NSW</Location>");
			buf.append("<TransmisionType>AUTO</TransmisionType>");
			buf.append("<Color>BLACK</Color>");
			buf.append("<Rate>55 per day</Rate>");
			buf.append("</Car>");

			buf.append("<Car>");
			buf.append("<Model>Nissan</Model>");		
			buf.append("<Location>Sydney, NSW</Location>");
			buf.append("<TransmisionType>AUTO</TransmisionType>");
			buf.append("<Color>BLUE</Color>");
			buf.append("<Rate>55 per day</Rate>");
			buf.append("</Car>");
			
			buf.append("</Cars>");
		}

		PrintWriter out = res.getWriter();
		out.write(buf.toString());
		out.flush();
		return null;		
	}

}
