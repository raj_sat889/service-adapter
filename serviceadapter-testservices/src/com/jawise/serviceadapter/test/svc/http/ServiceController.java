package com.jawise.serviceadapter.test.svc.http;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.mvc.AbstractController;

public abstract class ServiceController extends AbstractController {

	protected String readContent(HttpServletRequest request) throws IOException {
		StringBuffer buf = new StringBuffer();
		BufferedReader reader = request.getReader();
		int contentLength = request.getContentLength();
		char data[] = new char[contentLength];
		while (reader.read(data, 0, contentLength) != -1) {
			buf.append(data);
		}
		reader.read(data);
		return buf.toString();

	}
}
