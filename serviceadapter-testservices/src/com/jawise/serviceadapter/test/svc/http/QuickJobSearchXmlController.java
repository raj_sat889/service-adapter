package com.jawise.serviceadapter.test.svc.http;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;


public class QuickJobSearchXmlController extends ServiceController {

	private static final Logger logger = Logger
			.getLogger(QuickJobSearchXmlController.class);

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		StringBuffer buf = new StringBuffer();

		String reqmsg = readContent(req);

		logger.debug("request  " + reqmsg);

		if (reqmsg.indexOf("China") >= 0) {
			return null;
		} else if (reqmsg.indexOf("Africa") >= 0) {
			buf.append("<Jobs></Jobs>");
		} else {
			buf.append("<Jobs>");
			buf.append("<Job>");
			buf.append("<Tittle>Java Software Developer</Tittle>");
			buf
					.append("<Description>Java/J2EE Enterprise Applications. Strong Business facing role with vendor and third party co-ordination. Career prospects unlimited. Attractive $80k-90k Neg base Salary + Super + Benefits. Sydney CBD location. This dynamic and well known Australian company has a reputation for demanding IT roles and providing their staff with great career progression. Applications development is very much a decentralised team activity and as such, you get to work closely with the business units to really make</Description>");
			buf.append("<Location>Sydney, NSW</Location>");
			buf.append("<Type>Permanent</Type>");
			buf.append("<Salary>100K-120K</Salary>");
			buf.append("</Job>");
			buf.append("<Job>");
			buf.append("<Tittle>Java And C++ Software Developer </Tittle>");
			buf
					.append("<Description>Java and C++ Software Developer (Initially 3 months) An exceptional opportunity has arisen within a well established global organisation leading the way in the financial and technology investment space for an experienced Java/C++ Software Developer. This opportunity will initially be offered as a three months engagement but will most likely extend well beyond depending on your enthusiasm and contribution. You will be a part of a growing team of developers working on the development of trading  </Description>");
			buf.append("<Location>Melbourne, Victoria </Location>");
			buf.append("<Type>Contract</Type>");
			buf.append("<Salary>100K-120K</Salary>");
			buf.append("</Job>");
			buf.append("<Job>");
			buf.append("<Tittle>Java/J2EE Senior Developer</Tittle>");
			buf
					.append("<Description>Cutting edge Java/J2EE projects Financial markets applications Modern CBD location Successful Australian banking organisation seeks Senior Java/J2EE Developer. Join this established Java team, producing banking applications with a financial markets flavour. We are looking for Senior Java Developers with 5+ years experience, preferably within the financial services industry. Candidates should have developed complex Java applications with exposure to Struts, Hibernate and Spring frameworks</Description>");
			buf.append("<Location>Brisbane, QLD </Location>");
			buf.append("<Type>Contract/Permanent</Type>");
			buf.append("<Salary>100K-120K</Salary>");
			buf.append("</Job>");
			buf.append("</Jobs>");
		}

		PrintWriter out = res.getWriter();
		out.write(buf.toString());
		out.flush();
		return null;
	}
}
