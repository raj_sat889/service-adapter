package com.jawise.serviceadapter.test.svc.http;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;



public class FraudScreeningServiceController extends ServiceController {
	private static final Logger logger = Logger
			.getLogger(FraudScreeningServiceController.class);

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest req,
			HttpServletResponse res) throws Exception {

		String xml = req.getParameter("xml");
		logger.debug("request  " + xml);
		String action = req.getParameter("action");
		PrintWriter out = res.getWriter();
		if ("post".equals(action)) {
			out.write("<RealTimeResponse><Score>69</Score><recommendation>0</recommendation><merchant_attributes></merchant_attributes></RealTimeResponse>");
		} else {
			out.write("score=68&recommendation=0&merchant_attributes=");		
		}

		return null;
	}

}
