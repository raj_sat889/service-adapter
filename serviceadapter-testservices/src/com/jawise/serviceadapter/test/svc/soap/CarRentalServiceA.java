package com.jawise.serviceadapter.test.svc.soap;

public interface CarRentalServiceA {
	public Car[] listRentalCars(String location, String collectiondate,
			String returndate);
}
