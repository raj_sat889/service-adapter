package com.jawise.serviceadapter.test.svc.soap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.namespace.QName;

public class ComplexRequest {
	String stringvalue;
	Boolean booleanobject;
	boolean booleanvalue;
	Double doubleobject;
	double doublevalue;
	Long longobject;
	long longvalue;
	Float floatobject;
	float floatvalue;
	Integer intobject;
	int intvalue;
	Short shortobject;
	short shortvalue;
	Byte byteobject;
	byte bytevalue;
	BigDecimal bigdecobjectl;
	GregorianCalendar gcobjectl;
	Date dateobjectl;
	QName qnameobject;

	@SuppressWarnings("unchecked")
	// collections
	java.util.Vector vectorobject;
	@SuppressWarnings("unchecked")
	java.util.Enumeration enumobject;
	@SuppressWarnings("unchecked")
	java.util.Hashtable hashtable;
	@SuppressWarnings("unchecked")
	java.util.Map mapobject;
	byte[] base64;

	public String getStringvalue() {
		return stringvalue;
	}

	public void setStringvalue(String stringvalue) {
		this.stringvalue = stringvalue;
	}

	public Boolean getBooleanobject() {
		return booleanobject;
	}

	public void setBooleanobject(Boolean booleanobject) {
		this.booleanobject = booleanobject;
	}

	public boolean isBooleanvalue() {
		return booleanvalue;
	}

	public void setBooleanvalue(boolean booleanvalue) {
		this.booleanvalue = booleanvalue;
	}

	public Double getDoubleobject() {
		return doubleobject;
	}

	public void setDoubleobject(Double doubleobject) {
		this.doubleobject = doubleobject;
	}

	public double getDoublevalue() {
		return doublevalue;
	}

	public void setDoublevalue(double doublevalue) {
		this.doublevalue = doublevalue;
	}

	public Long getLongobject() {
		return longobject;
	}

	public void setLongobject(Long longobject) {
		this.longobject = longobject;
	}

	public long getLongvalue() {
		return longvalue;
	}

	public void setLongvalue(long longvalue) {
		this.longvalue = longvalue;
	}

	public Float getFloatobject() {
		return floatobject;
	}

	public void setFloatobject(Float floatobject) {
		this.floatobject = floatobject;
	}

	public float getFloatvalue() {
		return floatvalue;
	}

	public void setFloatvalue(float floatvalue) {
		this.floatvalue = floatvalue;
	}

	public Integer getIntobject() {
		return intobject;
	}

	public void setIntobject(Integer intobject) {
		this.intobject = intobject;
	}

	public int getIntvalue() {
		return intvalue;
	}

	public void setIntvalue(int intvalue) {
		this.intvalue = intvalue;
	}

	public Short getShortobject() {
		return shortobject;
	}

	public void setShortobject(Short shortobject) {
		this.shortobject = shortobject;
	}

	public short getShortvalue() {
		return shortvalue;
	}

	public void setShortvalue(short shortvalue) {
		this.shortvalue = shortvalue;
	}

	public Byte getByteobject() {
		return byteobject;
	}

	public void setByteobject(Byte byteobject) {
		this.byteobject = byteobject;
	}

	public byte getBytevalue() {
		return bytevalue;
	}

	public void setBytevalue(byte bytevalue) {
		this.bytevalue = bytevalue;
	}

	public BigDecimal getBigdecobjectl() {
		return bigdecobjectl;
	}

	public void setBigdecobjectl(BigDecimal bigdecobjectl) {
		this.bigdecobjectl = bigdecobjectl;
	}

	public GregorianCalendar getGcobjectl() {
		return gcobjectl;
	}

	public void setGcobjectl(GregorianCalendar gcobjectl) {
		this.gcobjectl = gcobjectl;
	}

	public Date getDateobjectl() {
		return dateobjectl;
	}

	public void setDateobjectl(Date dateobjectl) {
		this.dateobjectl = dateobjectl;
	}

	public QName getQnameobject() {
		return qnameobject;
	}

	public void setQnameobject(QName qnameobject) {
		this.qnameobject = qnameobject;
	}

	public java.util.Vector getVectorobject() {
		return vectorobject;
	}

	public void setVectorobject(java.util.Vector vectorobject) {
		this.vectorobject = vectorobject;
	}

	public java.util.Enumeration getEnumobject() {
		return enumobject;
	}

	public void setEnumobject(java.util.Enumeration enumobject) {
		this.enumobject = enumobject;
	}

	public java.util.Hashtable getHashtable() {
		return hashtable;
	}

	public void setHashtable(java.util.Hashtable hashtable) {
		this.hashtable = hashtable;
	}

	public java.util.Map getMapobject() {
		return mapobject;
	}

	public void setMapobject(java.util.Map mapobject) {
		this.mapobject = mapobject;
	}

	public byte[] getBase64() {
		return base64;
	}

	public void setBase64(byte[] base64) {
		this.base64 = base64;
	}
}
