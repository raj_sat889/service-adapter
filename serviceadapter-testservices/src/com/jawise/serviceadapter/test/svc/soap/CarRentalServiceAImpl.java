package com.jawise.serviceadapter.test.svc.soap;

public class CarRentalServiceAImpl implements CarRentalServiceA {

	@Override
	public Car[] listRentalCars(String location, String collectiondate,
			String returndate) {
		Car[] cars = new Car[2];
		cars[0] = new Car();
		cars[0].color = "balck";
		cars[0].make = "Mazda";
		cars[0].model = "3 hatchback";
		cars[0].rate = "30.00";
		cars[0].year = "2009";
		
		cars[1] = new Car();
		cars[1].color = "red";
		cars[1].make = "BMW";
		cars[1].model = "320i hatchback";
		cars[1].rate = "90.00";
		cars[1].year = "2009";
		
		return cars;
	}

}
