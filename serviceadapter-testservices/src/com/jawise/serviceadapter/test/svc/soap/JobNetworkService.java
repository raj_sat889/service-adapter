package com.jawise.serviceadapter.test.svc.soap;

public interface JobNetworkService {
	Job[] searchJobs(JobQuery query);

	ComplexRequest testComplexRequest(ComplexRequest complexrequest);
}
