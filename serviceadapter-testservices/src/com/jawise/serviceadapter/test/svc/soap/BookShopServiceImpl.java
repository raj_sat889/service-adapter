package com.jawise.serviceadapter.test.svc.soap;

import java.util.HashMap;
import java.util.Map;

public class BookShopServiceImpl implements BookShopService {
		
	private Map<String,Book> books = new HashMap<String,Book>();

	public BookShopServiceImpl() {
		Book book = new Book();
		book.setAuthor("Dan More");
		book.setTitle("Using XFire");
		book.setIsbn("1234-5678-6645-1");
		books.put(book.getIsbn(), book);
		
		book = new Book();
		book.setAuthor("David Hamingway");
		book.setTitle("C++ in 7 days");
		book.setIsbn("1234-5678-6645-2");
		books.put(book.getIsbn(), book);
		
		book = new Book();
		book.setAuthor("John Builder");
		book.setTitle("How to fix you house");
		book.setIsbn("1234-5678-6645-3");
		books.put(book.getIsbn(), book);		
		
	}

	public Book[] getBooks() {
		Book[] thebooks  = new Book[books.size()];
		thebooks = books.entrySet().toArray(thebooks);
		return thebooks;
	}

	public Book findBook(String isbn) throws BookException {
		Book book = books.get(isbn);
		if (book != null)
			return book;

		throw new BookException("Can't find the book with requested isbn " + isbn, new BookExceptionDetail(
				"NOT_EXIST", "Can't find the book with requested isbn " + isbn));
	}

	public Map getBooksMap() {
		return books;
	}

	@Override
	public String getBookPrice(String isbn) {
		// TODO Auto-generated method stub
		
		if("1234-5678-6645-1".equals(isbn)) {
			return "89.00";
		}
		else {
			return "";
		}
	}
}