package com.jawise.serviceadapter.test.svc.soap;

import java.util.Date;

public class LibraryService {
	public String listBooksInfo() {
		return "<books><book><title>C Prgramming in 30 Days</title><isbn>1234-5678-6645-1</isbn><author>Collin Powl</author></book></books>";
	}

	public String getBookInfo(String isbn, String tittle, String author) {
		if("FAULT1".equals(isbn)) {
			throw new IllegalArgumentException("invalid isbn vlaue");
		}
		if("1234-5678-6645-1".equals(isbn)) {
			return "<book><title>Using XFire</title><isbn>1234-5678-6645-1</isbn><author>Dan F More</author></book>";	
		}
		throw new IllegalArgumentException("book with requested information not found");
	}

	public int getStockLevl(String isbn) {
		return 10;
	}

	public boolean getBookAvilability(String isbn) {
		return true;
	}

	public boolean[] getBookssAvilability(String[] isbn) {
		boolean[] avs = { true, false };
		return avs;
	}

	public java.util.Date getBookReturnDate(String isbn) {
		return new java.util.Date(System.currentTimeMillis());
	}

	public long getBookReturnDateAsLong(String isbn) {
		return System.currentTimeMillis();
	}

	public double getBookReplacementCost(String isbn) {
		return 99.99;
	}

	public void updateBookReturnDate(String isbn, Date duedate) {
		return;
	}

	public void updateBookAvilability(String[] isbn, boolean[] avilability) {
		return;
	}
}
