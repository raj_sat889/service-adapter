package com.jawise.serviceadapter.test.svc.soap;

public class Order {
	String id;
	String requestdate;
	String total;
	String email;
	String telephone;	
	Part[] parts;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRequestdate() {
		return requestdate;
	}
	public void setRequestdate(String requestdate) {
		this.requestdate = requestdate;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Part[] getParts() {
		return parts;
	}
	public void setParts(Part[] parts) {
		this.parts = parts;
	}
	
}
