package com.jawise.serviceadapter.test.svc.soap;

import java.util.Map;

public interface BookShopService {
    /**
     * returns array of the books
     * @return all books
     */
    public Book[] getBooks();
    
    /**
     * Find book by id
     * @param isbn isbn number
     * @return found book
     * @throws BookException cos tam
     */
    public Book findBook(String isbn) throws BookException;
    
    
    /**
     * return book map
     * @return all book as map
     */
    public Map getBooksMap();
    
    
    public String getBookPrice(String isbn);
}
